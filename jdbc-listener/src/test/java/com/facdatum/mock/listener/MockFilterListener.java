/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.listener;

import com.facdatum.jdbc.listener.JDBCListener;
import com.facdatum.jdbc.listener.ListenerCallEvent;
import com.facdatum.jdbc.listener.ListenerExceptionEvent;
import com.facdatum.jdbc.listener.ListenerReturnEvent;

/**
 * A mock JDBCListener that that filters calls to subsequent listeners.
 */
public class MockFilterListener implements JDBCListener {

    /**
     * Index for clearing filters.
     */
    public static final int FILTER_CLEAR = 0;

    /**
     * Index for filtering begin step.
     */
    public static final int FILTER_BEGIN = 1;

    /**
     * Index for filtering end step.
     */
    public static final int FILTER_END = 2;

    /**
     * Index for filtering fail step.
     */
    public static final int FILTER_FAIL = 3;

    /**
     * Holds the setting for the step to filter.
     */
    public static int filterStep = 0; //NOCS

    /**
     * {@inheritDoc}
     * @see com.facdatum.jdbc.listener.driver.JDBCListener#init()
     */
    public final void init() {
        //do nothing
    }

    /**
     * {@inheritDoc}
     * @see com.facdatum.jdbc.listener.driver.JDBCListener#
     * acceptBegin(com.facdatum.jdbc.listener.driver.ListenerCallEvent)
     */
    public final void acceptBegin(final ListenerCallEvent event) {
        if (filterStep == FILTER_BEGIN) {
            event.setFilterListeners(true);
        }
    }

    /**
     * {@inheritDoc}
     * @see com.facdatum.jdbc.listener.driver.JDBCListener#
     * acceptEnd(com.facdatum.jdbc.listener.driver.ListenerReturnEvent)
     */
    public final void acceptEnd(final ListenerReturnEvent event) {
        if (filterStep == FILTER_END) {
            event.setFilterListeners(true);
        }
    }

    /**
     * {@inheritDoc}
     * @see com.facdatum.jdbc.listener.driver.JDBCListener#
     * acceptException(com.facdatum.jdbc.listener.driver.ListenerExceptionEvent)
     */
    public final void acceptException(final ListenerExceptionEvent event) {
        if (filterStep == FILTER_FAIL) {
            event.setFilterListeners(true);
        }
    }

    /**
     * Sets the step to filter.
     * @param step the step to filter
     */
    public static final void setFilterStep(final int step) {
        filterStep = step;
    }
}
