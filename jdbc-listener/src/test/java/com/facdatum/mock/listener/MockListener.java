/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.listener;

import com.facdatum.jdbc.listener.JDBCListener;
import com.facdatum.jdbc.listener.ListenerCallEvent;
import com.facdatum.jdbc.listener.ListenerExceptionEvent;
import com.facdatum.jdbc.listener.ListenerReturnEvent;

/**
 * A mock JDBCListener that counts each call so the tests can check that
 * all calls have been made.
 */
public class MockListener implements JDBCListener {

    /**
     * Holds count of calls made.
     */
    private int counter; //NOPMD

    /**
     * {@inheritDoc}
     * @see com.facdatum.jdbc.listener.driver.JDBCListener#init()
     */
    public final void init() {
        counter = 1;
    }

    /**
     * {@inheritDoc}
     * @see com.facdatum.jdbc.listener.driver.JDBCListener#
     * acceptBegin(com.facdatum.jdbc.listener.driver.ListenerCallEvent)
     */
    public final void acceptBegin(final ListenerCallEvent event) {
        counter++;
    }

    /**
     * {@inheritDoc}
     * @see com.facdatum.jdbc.listener.driver.JDBCListener#
     * acceptEnd(com.facdatum.jdbc.listener.driver.ListenerReturnEvent)
     */
    public final void acceptEnd(final ListenerReturnEvent event) {
        counter++;
    }

    /**
     * {@inheritDoc}
     * @see com.facdatum.jdbc.listener.driver.JDBCListener#
     * acceptException(com.facdatum.jdbc.listener.driver.ListenerExceptionEvent)
     */
    public final void acceptException(final ListenerExceptionEvent event) {
        counter++;
    }

    /**
     * Returns the call count.
     * @return the call count
     */
    public final int getCallCount() {
        return counter;
    }
}
