/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.naming;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;

/**
 * ContextFactory to return simple MockContext objects. Need to set property
 * java.naming.factory.inital=com.facdatum.mock.naming.MockInitialContextFactory
 * to enable InitialContext() to pick this up.
 */
public class MockInitialContextFactory implements InitialContextFactory {

    /**
     * Returns a new MockObject object.
     * @param env unused factory parameters
     * @return MockContext
     * @throws NamingException never thrown in this case
     * @see javax.naming.spi.InitialContextFactory#getInitialContext(
     * java.util.Hashtable)
     */
    public final Context getInitialContext(final Hashtable < ? , ? > env)
            throws NamingException {
        return new MockContext();
    }

}
