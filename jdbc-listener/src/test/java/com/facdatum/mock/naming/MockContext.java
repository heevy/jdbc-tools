/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.naming;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.naming.Binding;
import javax.naming.Context;
import javax.naming.Name;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NameClassPair;
import javax.naming.NameParser;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

/**
 * This implementation of Context uses a simple HashMap to use as a simple
 * naming service. Only a couple of methods are implemented to bind object
 * into mock context and to perform a lookup.
 */
public class MockContext implements Context {

    /**
     * Stores the objects bound within the naming service.
     */
    private static Map < String, Object > map =
            new HashMap < String, Object > (); //NOPMD NOCS

    /**
     * Creates a mock context by creating the map to store bound objects in.
     */
    public MockContext() {
        //do nothing
    }

    /**
     * Adds the named object to the map.
     * {@inheritDoc}
     * @see javax.naming.Context#bind(java.lang.String, java.lang.Object)
     */
    public void bind(final String name, final Object obj) //NOCS
            throws NamingException {
        if (map.containsKey(name)) {
            throw new NameAlreadyBoundException("'" + name
                    + "' already bound in context.");
        }
        map.put(name, obj);
    }

    /**
     * Returns the object stored in map under name.
     * @see javax.naming.Context#lookup(java.lang.String)
     */
    public Object lookup(final String name) throws NamingException { //NOCS
        final Object result = map.get(name);
        if (result == null) {
            throw new NamingException("'" + name + "' not bound in context.");
        }
        return result;
    }

    /**
     * Destroys the map.
     * {@inheritDoc}
     * @see javax.naming.Context#close()
     */
    public void close() throws NamingException { //NOCS
        map = null;
    }


    /**
     * Removes object from map.
     * {@inheritDoc}
     * @see javax.naming.Context#unbind(java.lang.String)
     */
    public void unbind(final String name) throws NamingException { //NOCS
        map.remove(name);
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#addToEnvironment(java.lang.String,
     *      java.lang.Object)
     */
    public Object addToEnvironment(final String propName, //NOCS
            final Object propVal) throws NamingException {
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#bind(javax.naming.Name, java.lang.Object)
     */
    public void bind(final Name name, final Object obj) //NOCS
            throws NamingException {
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#composeName(javax.naming.Name,
     *      javax.naming.Name)
     */
    public Name composeName(final Name name, final Name prefix) //NOCS
            throws NamingException {
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#composeName(java.lang.String, java.lang.String)
     */
    public String composeName(final String name, final String prefix) //NOCS
            throws NamingException {
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#createSubcontext(javax.naming.Name)
     */
    public Context createSubcontext(final Name name) //NOCS
            throws NamingException {
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#createSubcontext(java.lang.String)
     */
    public Context createSubcontext(final String name) //NOCS
            throws NamingException {
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#destroySubcontext(javax.naming.Name)
     */
    public void destroySubcontext(final Name name) //NOCS
            throws NamingException {
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#destroySubcontext(java.lang.String)
     */
    public void destroySubcontext(final String name) //NOCS
            throws NamingException {
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#getEnvironment()
     */
    public Hashtable < ? , ? > getEnvironment() throws NamingException { //NOCS
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#getNameInNamespace()
     */
    public String getNameInNamespace() throws NamingException { //NOCS
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#getNameParser(javax.naming.Name)
     */
    public NameParser getNameParser(final Name name) //NOCS
            throws NamingException {
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#getNameParser(java.lang.String)
     */
    public NameParser getNameParser(final String name) //NOCS
            throws NamingException {
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#list(javax.naming.Name)
     */
    public NamingEnumeration < NameClassPair > list(final Name name) //NOCS
            throws NamingException {
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#list(java.lang.String)
     */
    public NamingEnumeration < NameClassPair > list(final String name) //NOCS
            throws NamingException {
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#listBindings(javax.naming.Name)
     */
    public NamingEnumeration < Binding > listBindings(final Name name) //NOCS
            throws NamingException {
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#listBindings(java.lang.String)
     */
    public NamingEnumeration < Binding > listBindings(final String name) //NOCS
            throws NamingException {
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#lookup(javax.naming.Name)
     */
    public Object lookup(final Name name) throws NamingException { //NOCS
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#lookupLink(javax.naming.Name)
     */
    public Object lookupLink(final Name name) throws NamingException { //NOCS
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#lookupLink(java.lang.String)
     */
    public Object lookupLink(final String name) throws NamingException { //NOCS
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#rebind(javax.naming.Name, java.lang.Object)
     */
    public void rebind(final Name name, final Object obj) //NOCS
            throws NamingException {
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#rebind(java.lang.String, java.lang.Object)
     */
    public void rebind(final String name, final Object obj) //NOCS
            throws NamingException {
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#removeFromEnvironment(java.lang.String)
     */
    public Object removeFromEnvironment(final String propName) //NOCS
            throws NamingException {
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#rename(javax.naming.Name, javax.naming.Name)
     */
    public void rename(final Name oldName, final Name newName) //NOCS
            throws NamingException {
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#rename(java.lang.String, java.lang.String)
     */
    public void rename(final String oldName, final String newName) //NOCS
            throws NamingException {
        throw new java.lang.UnsupportedOperationException();
    }

    /**
     * Unsupported in mock context.
     * {@inheritDoc}
     * @see javax.naming.Context#unbind(javax.naming.Name)
     */
    public void unbind(final Name name) throws NamingException { //NOCS
        throw new java.lang.UnsupportedOperationException();
    }
}
