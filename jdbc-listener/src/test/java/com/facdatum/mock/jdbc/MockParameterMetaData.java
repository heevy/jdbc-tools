/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.jdbc;

import java.sql.ParameterMetaData;
import java.sql.SQLException;

/**
 * Mock of the ParameterMetaData class that checks each call to ensure that it
 * has been called from delegating listener class.
 */
public class MockParameterMetaData extends MockJDBCBase implements
        ParameterMetaData {

    /**
     * {@inheritDoc}
     * @see java.sql.ParameterMetaData#getParameterClassName(int)
     */
    public final String getParameterClassName(final int arg0)
            throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ParameterMetaData#getParameterCount()
     */
    public final int getParameterCount() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ParameterMetaData#getParameterMode(int)
     */
    public final int getParameterMode(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ParameterMetaData#getParameterType(int)
     */
    public final int getParameterType(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ParameterMetaData#getParameterTypeName(int)
     */
    public final String getParameterTypeName(final int arg0)
            throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ParameterMetaData#getPrecision(int)
     */
    public final int getPrecision(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ParameterMetaData#getScale(int)
     */
    public final int getScale(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ParameterMetaData#isNullable(int)
     */
    public final int isNullable(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ParameterMetaData#isSigned(int)
     */
    public final boolean isSigned(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return false;
    }

    //TODO joudek implement correctly

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }
}
