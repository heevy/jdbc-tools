/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.jdbc;

import javax.sql.PooledConnection;
import java.sql.SQLException;

import javax.sql.ConnectionPoolDataSource;

/**
 * Mock of the ConnectionPoolDataSource class that checks each call to ensure
 * that it has been called from delegating listener class.
 */
public class MockConnectionPoolDataSource extends MockDataSource
        implements ConnectionPoolDataSource {

    /**
     * {@inheritDoc}
     * @see javax.sql.ConnectionPoolDataSource#getPooledConnection()
     */
    public final PooledConnection getPooledConnection() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see javax.sql.ConnectionPoolDataSource#getPooledConnection(
     *      java.lang.String, java.lang.String)
     */
    public final PooledConnection getPooledConnection(final String arg0,
            final String arg1) throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return null;
    }
}
