/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.jdbc;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 * Mock of the ResultSetMetaData class that checks each call to ensure that it
 * has been called from delegating listener class.
 */
public class MockResultSetMetaData extends MockJDBCBase implements
        ResultSetMetaData {

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getCatalogName(int)
     */
    public final String getCatalogName(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getColumnClassName(int)
     */
    public final String getColumnClassName(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getColumnCount()
     */
    public final int getColumnCount() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getColumnDisplaySize(int)
     */
    public final int getColumnDisplaySize(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getColumnLabel(int)
     */
    public final String getColumnLabel(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getColumnName(int)
     */
    public final String getColumnName(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getColumnType(int)
     */
    public final int getColumnType(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getColumnTypeName(int)
     */
    public final String getColumnTypeName(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getPrecision(int)
     */
    public final int getPrecision(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getScale(int)
     */
    public final int getScale(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getSchemaName(int)
     */
    public final String getSchemaName(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getTableName(int)
     */
    public final String getTableName(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#isAutoIncrement(int)
     */
    public final boolean isAutoIncrement(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#isCaseSensitive(int)
     */
    public final boolean isCaseSensitive(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#isCurrency(int)
     */
    public final boolean isCurrency(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#isDefinitelyWritable(int)
     */
    public final boolean isDefinitelyWritable(final int arg0)
            throws SQLException {
        checkCall(new Object[] {arg0});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#isNullable(int)
     */
    public final int isNullable(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#isReadOnly(int)
     */
    public final boolean isReadOnly(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#isSearchable(int)
     */
    public final boolean isSearchable(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#isSigned(int)
     */
    public final boolean isSigned(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#isWritable(int)
     */
    public final boolean isWritable(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return false;
    }

    //TODO Joudek Implement correctly

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }
}
