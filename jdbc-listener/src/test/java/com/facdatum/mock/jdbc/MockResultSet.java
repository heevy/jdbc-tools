/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.jdbc; //NOPMD

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.NClob;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Map;

/**
 * Mock of the ResultSet class that checks each call to ensure that it
 * has been called from delegating listener class.
 */
public class MockResultSet extends MockJDBCBase implements ResultSet { //NOPMD

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#absolute(int)
     */
    public final boolean absolute(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#afterLast()
     */
    public final void afterLast() throws SQLException {
        checkCall(new Object[] {});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#beforeFirst()
     */
    public final void beforeFirst() throws SQLException {
        checkCall(new Object[] {});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#cancelRowUpdates()
     */
    public final void cancelRowUpdates() throws SQLException {
        checkCall(new Object[] {});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#clearWarnings()
     */
    public final void clearWarnings() throws SQLException {
        checkCall(new Object[] {});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#close()
     */
    public final void close() throws SQLException {
        checkCall(new Object[] {});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#deleteRow()
     */
    public final void deleteRow() throws SQLException {
        checkCall(new Object[] {});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#findColumn(java.lang.String)
     */
    public final int findColumn(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#first()
     */
    public final boolean first() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getArray(int)
     */
    public final Array getArray(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getArray(java.lang.String)
     */
    public final Array getArray(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getAsciiStream(int)
     */
    public final InputStream getAsciiStream(final int arg0)
            throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getAsciiStream(java.lang.String)
     */
    public final InputStream getAsciiStream(final String arg0)
            throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getBigDecimal(int)
     */
    public final BigDecimal getBigDecimal(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getBigDecimal(java.lang.String)
     */
    public final BigDecimal getBigDecimal(final String arg0)
            throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getBigDecimal(int, int)
     */
    public final BigDecimal getBigDecimal(final int arg0, final int arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getBigDecimal(java.lang.String, int)
     */
    public final BigDecimal getBigDecimal(final String arg0, final int arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getBinaryStream(int)
     */
    public final InputStream getBinaryStream(final int arg0)
            throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getBinaryStream(java.lang.String)
     */
    public final InputStream getBinaryStream(final String arg0)
            throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getBlob(int)
     */
    public final Blob getBlob(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getBlob(java.lang.String)
     */
    public final Blob getBlob(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getBoolean(int)
     */
    public final boolean getBoolean(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getBoolean(java.lang.String)
     */
    public final boolean getBoolean(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getByte(int)
     */
    public final byte getByte(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getByte(java.lang.String)
     */
    public final byte getByte(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getBytes(int)
     */
    public final byte[] getBytes(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getBytes(java.lang.String)
     */
    public final byte[] getBytes(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getCharacterStream(int)
     */
    public final Reader getCharacterStream(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getCharacterStream(java.lang.String)
     */
    public final Reader getCharacterStream(final String arg0)
            throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getClob(int)
     */
    public final Clob getClob(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getClob(java.lang.String)
     */
    public final Clob getClob(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getConcurrency()
     */
    public final int getConcurrency() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getCursorName()
     */
    public final String getCursorName() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getDate(int)
     */
    public final Date getDate(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getDate(java.lang.String)
     */
    public final Date getDate(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getDate(int, java.util.Calendar)
     */
    public final Date getDate(final int arg0, final Calendar arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getDate(java.lang.String, java.util.Calendar)
     */
    public final Date getDate(final String arg0, final Calendar arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getDouble(int)
     */
    public final double getDouble(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getDouble(java.lang.String)
     */
    public final double getDouble(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getFetchDirection()
     */
    public final int getFetchDirection() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getFetchSize()
     */
    public final int getFetchSize() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getFloat(int)
     */
    public final float getFloat(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getFloat(java.lang.String)
     */
    public final float getFloat(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getInt(int)
     */
    public final int getInt(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getInt(java.lang.String)
     */
    public final int getInt(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getLong(int)
     */
    public final long getLong(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getLong(java.lang.String)
     */
    public final long getLong(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getMetaData()
     */
    public final ResultSetMetaData getMetaData() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getObject(int)
     */
    public final Object getObject(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getObject(java.lang.String)
     */
    public final Object getObject(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getObject(int, java.util.Map)
     */
    public final Object getObject(final int arg0,
            final Map < String, Class < ? > > arg1) throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getObject(java.lang.String, java.util.Map)
     */
    public final Object getObject(final String arg0,
            final Map < String, Class < ? > > arg1) throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getRef(int)
     */
    public final Ref getRef(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getRef(java.lang.String)
     */
    public final Ref getRef(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getRow()
     */
    public final int getRow() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getShort(int)
     */
    public final short getShort(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getShort(java.lang.String)
     */
    public final short getShort(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getStatement()
     */
    public final Statement getStatement() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getString(int)
     */
    public final String getString(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getString(java.lang.String)
     */
    public final String getString(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getTime(int)
     */
    public final Time getTime(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getTime(java.lang.String)
     */
    public final Time getTime(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getTime(int, java.util.Calendar)
     */
    public final Time getTime(final int arg0, final Calendar arg1)
                throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getTime(java.lang.String, java.util.Calendar)
     */
    public final Time getTime(final String arg0, final Calendar arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getTimestamp(int)
     */
    public final Timestamp getTimestamp(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getTimestamp(java.lang.String)
     */
    public final Timestamp getTimestamp(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getTimestamp(int, java.util.Calendar)
     */
    public final Timestamp getTimestamp(final int arg0, final Calendar arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getTimestamp(java.lang.String,
     *      java.util.Calendar)
     */
    public final Timestamp getTimestamp(final String arg0, final Calendar arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getType()
     */
    public final int getType() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getURL(int)
     */
    public final URL getURL(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getURL(java.lang.String)
     */
    public final URL getURL(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getUnicodeStream(int)
     */
    public final InputStream getUnicodeStream(final int arg0)
            throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getUnicodeStream(java.lang.String)
     */
    public final InputStream getUnicodeStream(final String arg0)
            throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#getWarnings()
     */
    public final SQLWarning getWarnings() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#insertRow()
     */
    public final void insertRow() throws SQLException {
        checkCall(new Object[] {});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#isAfterLast()
     */
    public final boolean isAfterLast() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#isBeforeFirst()
     */
    public final boolean isBeforeFirst() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#isFirst()
     */
    public final boolean isFirst() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#isLast()
     */
    public final boolean isLast() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#last()
     */
    public final boolean last() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#moveToCurrentRow()
     */
    public final void moveToCurrentRow() throws SQLException {
        checkCall(new Object[] {});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#moveToInsertRow()
     */
    public final void moveToInsertRow() throws SQLException {
        checkCall(new Object[] {});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#next()
     */
    public final boolean next() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#previous()
     */
    public final boolean previous() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#refreshRow()
     */
    public final void refreshRow() throws SQLException {
        checkCall(new Object[] {});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#relative(int)
     */
    public final boolean relative(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#rowDeleted()
     */
    public final boolean rowDeleted() throws SQLException {
        checkCall(new Object[] {});
       return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#rowInserted()
     */
    public final boolean rowInserted() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#rowUpdated()
     */
    public final boolean rowUpdated() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#setFetchDirection(int)
     */
    public final void setFetchDirection(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#setFetchSize(int)
     */
    public final void setFetchSize(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateArray(int, java.sql.Array)
     */
    public final void updateArray(final int arg0, final Array arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateArray(java.lang.String, java.sql.Array)
     */
    public final void updateArray(final String arg0, final Array arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }


    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateAsciiStream(int, java.io.InputStream, int)
     */
    public final void updateAsciiStream(final int arg0,
            final InputStream arg1, final int arg2) throws SQLException {
        checkCall(new Object[] {arg0, arg1, arg2});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateAsciiStream(java.lang.String,
     *      java.io.InputStream, int)
     */
    public final void updateAsciiStream(final String arg0,
            final InputStream arg1, final int arg2) throws SQLException {
        checkCall(new Object[] {arg0, arg1, arg2});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateBigDecimal(int, java.math.BigDecimal)
     */
    public final void updateBigDecimal(final int arg0, final BigDecimal arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateBigDecimal(java.lang.String,
     *      java.math.BigDecimal)
     */
    public final void updateBigDecimal(final String arg0, final BigDecimal arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateBinaryStream(int, java.io.InputStream, int)
     */
    public final void updateBinaryStream(final int arg0, final InputStream arg1,
            final int arg2) throws SQLException {
        checkCall(new Object[] {arg0, arg1, arg2});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateBinaryStream(java.lang.String,
     *      java.io.InputStream, int)
     */
    public final void updateBinaryStream(final String arg0,
            final InputStream arg1, final int arg2) throws SQLException {
        checkCall(new Object[] {arg0, arg1, arg2});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateBlob(int, java.sql.Blob)
     */
    public final void updateBlob(final int arg0, final Blob arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateBlob(java.lang.String, java.sql.Blob)
     */
    public final void updateBlob(final String arg0, final Blob arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateBoolean(int, boolean)
     */
    public final void updateBoolean(final int arg0, final boolean arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateBoolean(java.lang.String, boolean)
     */
    public final void updateBoolean(final String arg0, final boolean arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateByte(int, byte)
     */
    public final void updateByte(final int arg0, final byte arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateByte(java.lang.String, byte)
     */
    public final void updateByte(final String arg0, final byte arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateBytes(int, byte[])
     */
    public final void updateBytes(final int arg0, final byte[] arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateBytes(java.lang.String, byte[])
     */
    public final void updateBytes(final String arg0, final byte[] arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateCharacterStream(int, java.io.Reader, int)
     */
    public final void updateCharacterStream(final int arg0, final Reader arg1,
            final int arg2) throws SQLException {
        checkCall(new Object[] {arg0, arg1, arg2});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateCharacterStream(java.lang.String,
     *      java.io.Reader, int)
     */
    public final void updateCharacterStream(final String arg0,
            final Reader arg1, final int arg2) throws SQLException {
        checkCall(new Object[] {arg0, arg1, arg2});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateClob(int, java.sql.Clob)
     */
    public final void updateClob(final int arg0, final Clob arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateClob(java.lang.String, java.sql.Clob)
     */
    public final void updateClob(final String arg0, final Clob arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateDate(int, java.sql.Date)
     */
    public final void updateDate(final int arg0, final Date arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateDate(java.lang.String, java.sql.Date)
     */
    public final void updateDate(final String arg0, final Date arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateDouble(int, double)
     */
    public final void updateDouble(final int arg0, final double arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateDouble(java.lang.String, double)
     */
    public final void updateDouble(final String arg0, final double arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateFloat(int, float)
     */
    public final void updateFloat(final int arg0, final float arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateFloat(java.lang.String, float)
     */
    public final void updateFloat(final String arg0, final float arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateInt(int, int)
     */
    public final void updateInt(final int arg0, final int arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateInt(java.lang.String, int)
     */
    public final void updateInt(final String arg0, final int arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateLong(int, long)
     */
    public final void updateLong(final int arg0, final long arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateLong(java.lang.String, long)
     */
    public final void updateLong(final String arg0, final long arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateNull(int)
     */
    public final void updateNull(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateNull(java.lang.String)
     */
    public final void updateNull(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateObject(int, java.lang.Object)
     */
    public final void updateObject(final int arg0, final Object arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateObject(java.lang.String, java.lang.Object)
     */
    public final void updateObject(final String arg0, final Object arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateObject(int, java.lang.Object, int)
     */
    public final void updateObject(final int arg0, final Object arg1,
            final int arg2) throws SQLException {
        checkCall(new Object[] {arg0, arg1, arg2});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateObject(java.lang.String,
     *      java.lang.Object, int)
     */
    public final void updateObject(final String arg0, final Object arg1,
            final int arg2) throws SQLException {
        checkCall(new Object[] {arg0, arg1, arg2});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateRef(int, java.sql.Ref)
     */
    public final void updateRef(final int arg0, final Ref arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateRef(java.lang.String, java.sql.Ref)
     */
    public final void updateRef(final String arg0, final Ref arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateRow()
     */
    public final void updateRow() throws SQLException {
        checkCall(new Object[] {});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateShort(int, short)
     */
    public final void updateShort(final int arg0, final short arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateShort(java.lang.String, short)
     */
    public final void updateShort(final String arg0, final short arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateString(int, java.lang.String)
     */
    public final void updateString(final int arg0, final String arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateString(java.lang.String, java.lang.String)
     */
    public final void updateString(final String arg0, final String arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateTime(int, java.sql.Time)
     */
    public final void updateTime(final int arg0, final Time arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateTime(java.lang.String, java.sql.Time)
     */
    public final void updateTime(final String arg0, final Time arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateTimestamp(int, java.sql.Timestamp)
     */
    public final void updateTimestamp(final int arg0, final Timestamp arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#updateTimestamp(java.lang.String,
     *      java.sql.Timestamp)
     */
    public final void updateTimestamp(final String arg0, final Timestamp arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSet#wasNull()
     */
    public final boolean wasNull() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    //TODO joudek implement correctly

    @Override
    public RowId getRowId(int columnIndex) throws SQLException {
        return null;
    }

    @Override
    public RowId getRowId(String columnLabel) throws SQLException {
        return null;
    }

    @Override
    public void updateRowId(int columnIndex, RowId x) throws SQLException {

    }

    @Override
    public void updateRowId(String columnLabel, RowId x) throws SQLException {

    }

    @Override
    public int getHoldability() throws SQLException {
        return 0;
    }

    @Override
    public boolean isClosed() throws SQLException {
        return false;
    }

    @Override
    public void updateNString(int columnIndex, String nString) throws SQLException {

    }

    @Override
    public void updateNString(String columnLabel, String nString) throws SQLException {

    }

    @Override
    public void updateNClob(int columnIndex, NClob nClob) throws SQLException {

    }

    @Override
    public void updateNClob(String columnLabel, NClob nClob) throws SQLException {

    }

    @Override
    public NClob getNClob(int columnIndex) throws SQLException {
        return null;
    }

    @Override
    public NClob getNClob(String columnLabel) throws SQLException {
        return null;
    }

    @Override
    public SQLXML getSQLXML(int columnIndex) throws SQLException {
        return null;
    }

    @Override
    public SQLXML getSQLXML(String columnLabel) throws SQLException {
        return null;
    }

    @Override
    public void updateSQLXML(int columnIndex, SQLXML xmlObject) throws SQLException {

    }

    @Override
    public void updateSQLXML(String columnLabel, SQLXML xmlObject) throws SQLException {

    }

    @Override
    public String getNString(int columnIndex) throws SQLException {
        return null;
    }

    @Override
    public String getNString(String columnLabel) throws SQLException {
        return null;
    }

    @Override
    public Reader getNCharacterStream(int columnIndex) throws SQLException {
        return null;
    }

    @Override
    public Reader getNCharacterStream(String columnLabel) throws SQLException {
        return null;
    }

    @Override
    public void updateNCharacterStream(int columnIndex, Reader x, long length) throws SQLException {

    }

    @Override
    public void updateNCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {

    }

    @Override
    public void updateAsciiStream(int columnIndex, InputStream x, long length) throws SQLException {

    }

    @Override
    public void updateBinaryStream(int columnIndex, InputStream x, long length) throws SQLException {

    }

    @Override
    public void updateCharacterStream(int columnIndex, Reader x, long length) throws SQLException {

    }

    @Override
    public void updateAsciiStream(String columnLabel, InputStream x, long length) throws SQLException {

    }

    @Override
    public void updateBinaryStream(String columnLabel, InputStream x, long length) throws SQLException {

    }

    @Override
    public void updateCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {

    }

    @Override
    public void updateBlob(int columnIndex, InputStream inputStream, long length) throws SQLException {

    }

    @Override
    public void updateBlob(String columnLabel, InputStream inputStream, long length) throws SQLException {

    }

    @Override
    public void updateClob(int columnIndex, Reader reader, long length) throws SQLException {

    }

    @Override
    public void updateClob(String columnLabel, Reader reader, long length) throws SQLException {

    }

    @Override
    public void updateNClob(int columnIndex, Reader reader, long length) throws SQLException {

    }

    @Override
    public void updateNClob(String columnLabel, Reader reader, long length) throws SQLException {

    }

    @Override
    public void updateNCharacterStream(int columnIndex, Reader x) throws SQLException {

    }

    @Override
    public void updateNCharacterStream(String columnLabel, Reader reader) throws SQLException {

    }

    @Override
    public void updateAsciiStream(int columnIndex, InputStream x) throws SQLException {

    }

    @Override
    public void updateBinaryStream(int columnIndex, InputStream x) throws SQLException {

    }

    @Override
    public void updateCharacterStream(int columnIndex, Reader x) throws SQLException {

    }

    @Override
    public void updateAsciiStream(String columnLabel, InputStream x) throws SQLException {

    }

    @Override
    public void updateBinaryStream(String columnLabel, InputStream x) throws SQLException {

    }

    @Override
    public void updateCharacterStream(String columnLabel, Reader reader) throws SQLException {

    }

    @Override
    public void updateBlob(int columnIndex, InputStream inputStream) throws SQLException {

    }

    @Override
    public void updateBlob(String columnLabel, InputStream inputStream) throws SQLException {

    }

    @Override
    public void updateClob(int columnIndex, Reader reader) throws SQLException {

    }

    @Override
    public void updateClob(String columnLabel, Reader reader) throws SQLException {

    }

    @Override
    public void updateNClob(int columnIndex, Reader reader) throws SQLException {

    }

    @Override
    public void updateNClob(String columnLabel, Reader reader) throws SQLException {

    }

    @Override
    public <T> T getObject(int columnIndex, Class<T> type) throws SQLException {
        return null;
    }

    @Override
    public <T> T getObject(String columnLabel, Class<T> type) throws SQLException {
        return null;
    }


    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }
}
