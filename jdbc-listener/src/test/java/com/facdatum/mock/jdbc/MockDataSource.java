/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.jdbc;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

import javax.sql.DataSource;

/**
 * Mock of the DataSource class that checks each call to ensure that it
 * has been called from delegating listener class.
 */
public class MockDataSource extends MockJDBCBase implements DataSource {

    /**
     * {@inheritDoc}
     * @see javax.sql.DataSource#getConnection()
     */
    public final Connection getConnection() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see javax.sql.DataSource#getConnection(java.lang.String,
     *      java.lang.String)
     */
    public final Connection getConnection(final String arg0, final String arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see javax.sql.DataSource#getLogWriter()
     */
    public final PrintWriter getLogWriter() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see javax.sql.DataSource#getLoginTimeout()
     */
    public final int getLoginTimeout() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }


    /**
     * {@inheritDoc}
     * @see javax.sql.DataSource#setLogWriter(java.io.PrintWriter)
     */
    public final void setLogWriter(final PrintWriter arg0) throws SQLException {
        checkCall(new Object[] {arg0});
    }

    /**
     * {@inheritDoc}
     * @see javax.sql.DataSource#setLoginTimeout(int)
     */
    public final void setLoginTimeout(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
    }

    //TODO joudek implement corrctly

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return null;
    }


    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }
}
