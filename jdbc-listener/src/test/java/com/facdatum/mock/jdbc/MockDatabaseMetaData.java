/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.jdbc; //NOPMD

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.RowIdLifetime;
import java.sql.SQLException;

/**
 * Mock of the DatabaseMetaData class that checks each call to ensure that it
 * has been called from delegating listener class.
 */
public class MockDatabaseMetaData extends MockJDBCBase implements //NOPMD
        DatabaseMetaData {

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#allProceduresAreCallable()
     */
    public final boolean allProceduresAreCallable() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#allTablesAreSelectable()
     */
    public final boolean allTablesAreSelectable() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#dataDefinitionCausesTransactionCommit()
     */
    public final boolean dataDefinitionCausesTransactionCommit()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#dataDefinitionIgnoredInTransactions()
     */
    public final boolean dataDefinitionIgnoredInTransactions()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#deletesAreDetected(int)
     */
    public final boolean deletesAreDetected(final int type)
            throws SQLException {
        checkCall(new Object[] {type});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#doesMaxRowSizeIncludeBlobs()
     */
    public final boolean doesMaxRowSizeIncludeBlobs() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getAttributes(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    public final ResultSet getAttributes(final String catalog,
            final String schemaPattern, final String typeNamePattern,
            final String attrNamePattern) throws SQLException {
        checkCall(new Object[] {catalog, schemaPattern, typeNamePattern,
                attrNamePattern});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getBestRowIdentifier(java.lang.String,
     *      java.lang.String, java.lang.String, int, boolean)
     */
    public final ResultSet getBestRowIdentifier(final String catalog,
            final String schema, final String table, final int scope,
            final boolean nullable) throws SQLException {
        checkCall(new Object[] {catalog, schema, table, scope, nullable});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getCatalogSeparator()
     */
    public final String getCatalogSeparator() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getCatalogTerm()
     */
    public final String getCatalogTerm() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getCatalogs()
     */
    public final ResultSet getCatalogs() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getColumnPrivileges(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    public final ResultSet getColumnPrivileges(final String catalog,
            final String schema, final String table,
            final String colNamePattern) throws SQLException {
        checkCall(new Object[] {catalog, schema, table, colNamePattern});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getColumns(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    public final ResultSet getColumns(final String catalog,
            final String schemaPattern, final String tableNamePattern,
            final String colNamePattern)
            throws SQLException {
        checkCall(new Object[] {catalog, schemaPattern, tableNamePattern,
                colNamePattern});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getConnection()
     */
    public final Connection getConnection() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getCrossReference(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public final ResultSet getCrossReference(final String primaryCatalog,
            final String primarySchema, final String primaryTable,
            final String foreignCatalog, final String foreignSchema,
            final String foreignTable) throws SQLException {
        checkCall(new Object[] {primaryCatalog, primarySchema, primaryTable,
                foreignCatalog, foreignSchema, foreignTable});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getDatabaseMajorVersion()
     */
    public final int getDatabaseMajorVersion() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getDatabaseMinorVersion()
     */
    public final int getDatabaseMinorVersion() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getDatabaseProductName()
     */
    public final String getDatabaseProductName() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getDatabaseProductVersion()
     */
    public final String getDatabaseProductVersion() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getDefaultTransactionIsolation()
     */
    public final int getDefaultTransactionIsolation() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getDriverMajorVersion()
     */
    public final int getDriverMajorVersion() {
        cleanCheckCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getDriverMinorVersion()
     */
    public final int getDriverMinorVersion() {
        cleanCheckCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getDriverName()
     */
    public final String getDriverName() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getDriverVersion()
     */
    public final String getDriverVersion() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getExportedKeys(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public final ResultSet getExportedKeys(final String catalog,
            final String schema, final String table) throws SQLException {
        checkCall(new Object[] {catalog, schema, table});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getExtraNameCharacters()
     */
    public final String getExtraNameCharacters() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getIdentifierQuoteString()
     */
    public final String getIdentifierQuoteString() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getImportedKeys(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public final ResultSet getImportedKeys(final String catalog,
            final String schema, final String table) throws SQLException {
        checkCall(new Object[] {catalog, schema, table});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getIndexInfo(java.lang.String,
     *      java.lang.String, java.lang.String, boolean, boolean)
     */
    public final ResultSet getIndexInfo(final String catalog,
            final String schema, final String table, final boolean unique,
            final boolean approx) throws SQLException {
        checkCall(new Object[] {catalog, schema, table, unique, approx});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getJDBCMajorVersion()
     */
    public final int getJDBCMajorVersion() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getJDBCMinorVersion()
     */
    public final int getJDBCMinorVersion() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxBinaryLiteralLength()
     */
    public final int getMaxBinaryLiteralLength() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxCatalogNameLength()
     */
    public final int getMaxCatalogNameLength() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxCharLiteralLength()
     */
    public final int getMaxCharLiteralLength() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxColumnNameLength()
     */
    public final int getMaxColumnNameLength() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxColumnsInGroupBy()
     */
    public final int getMaxColumnsInGroupBy() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxColumnsInIndex()
     */
    public final int getMaxColumnsInIndex() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxColumnsInOrderBy()
     */
    public final int getMaxColumnsInOrderBy() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxColumnsInSelect()
     */
    public final int getMaxColumnsInSelect() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxColumnsInTable()
     */
    public final int getMaxColumnsInTable() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxConnections()
     */
    public final int getMaxConnections() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxCursorNameLength()
     */
    public final int getMaxCursorNameLength() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxIndexLength()
     */
    public final int getMaxIndexLength() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxProcedureNameLength()
     */
    public final int getMaxProcedureNameLength() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxRowSize()
     */
    public final int getMaxRowSize() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxSchemaNameLength()
     */
    public final int getMaxSchemaNameLength() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxStatementLength()
     */
    public final int getMaxStatementLength() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxStatements()
     */
    public final int getMaxStatements() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxTableNameLength()
     */
    public final int getMaxTableNameLength() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxTablesInSelect()
     */
    public final int getMaxTablesInSelect() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxUserNameLength()
     */
    public final int getMaxUserNameLength() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getNumericFunctions()
     */
    public final String getNumericFunctions() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getPrimaryKeys(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public final ResultSet getPrimaryKeys(final String catalog,
            final String schema, final String table) throws SQLException {
        checkCall(new Object[] {catalog, schema, table});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getProcedureColumns(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    public final ResultSet getProcedureColumns(final String catalog,
            final String schemaPattern, final String procNamePattern,
            final String colNamePattern) throws SQLException {
        checkCall(new Object[] {catalog, schemaPattern, procNamePattern,
                colNamePattern});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getProcedureTerm()
     */
    public final String getProcedureTerm() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getProcedures(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public final ResultSet getProcedures(final String catalog,
            final String schemaPattern, final String procNamePattern)
            throws SQLException {
        checkCall(new Object[] {catalog, schemaPattern, procNamePattern});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getResultSetHoldability()
     */
    public final int getResultSetHoldability() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getSQLKeywords()
     */
    public final String getSQLKeywords() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getSQLStateType()
     */
    public final int getSQLStateType() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getSchemaTerm()
     */
    public final String getSchemaTerm() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**{@inheritDoc}
     * @see java.sql.DatabaseMetaData#getSchemas()
     */
    public final ResultSet getSchemas() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getSearchStringEscape()
     */
    public final String getSearchStringEscape() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getStringFunctions()
     */
    public final String getStringFunctions() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getSuperTables(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public final ResultSet getSuperTables(final String catalog,
            final String schemaPattern, final String tableNamePattern)
            throws SQLException {
        checkCall(new Object[] {catalog, schemaPattern, tableNamePattern});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getSuperTypes(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public final ResultSet getSuperTypes(final String catalog,
            final String schemaPattern, final String typeNamePattern)
            throws SQLException {
        checkCall(new Object[] {catalog, schemaPattern, typeNamePattern});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getSystemFunctions()
     */
    public final String getSystemFunctions() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getTablePrivileges(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public final ResultSet getTablePrivileges(final String catalog,
            final String schemaPattern, final String tableNamePattern)
            throws SQLException {
        checkCall(new Object[] {catalog, schemaPattern, tableNamePattern});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getTableTypes()
     */
    public final ResultSet getTableTypes() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getTables(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String[])
     */
    public final ResultSet getTables(final String catalog,
            final String schemaPattern, final String tableNamePattern,
            final String[] types) throws SQLException {
        checkCall(new Object[] {catalog, schemaPattern, tableNamePattern,
                types});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getTimeDateFunctions()
     */
    public final String getTimeDateFunctions() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getTypeInfo()
     */
    public final ResultSet getTypeInfo() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getUDTs(java.lang.String,
     *      java.lang.String, java.lang.String, int[])
     */
    public final ResultSet getUDTs(final String catalog,
            final String schemaPattern, final String typeNamePattern,
            final int[] types) throws SQLException {
        checkCall(new Object[] {catalog, schemaPattern, typeNamePattern,
                types});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getURL()
     */
    public final String getURL() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getUserName()
     */
    public final String getUserName() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getVersionColumns(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public final ResultSet getVersionColumns(final String catalog,
            final String schema, final String table) throws SQLException {
        checkCall(new Object[] {catalog, schema, table});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#insertsAreDetected(int)
     */
    public final boolean insertsAreDetected(final int type)
            throws SQLException {
        checkCall(new Object[] {type});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#isCatalogAtStart()
     */
    public final boolean isCatalogAtStart() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#isReadOnly()
     */
    public final boolean isReadOnly() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#locatorsUpdateCopy()
     */
    public final boolean locatorsUpdateCopy() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#nullPlusNonNullIsNull()
     */
    public final boolean nullPlusNonNullIsNull() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#nullsAreSortedAtEnd()
     */
    public final boolean nullsAreSortedAtEnd() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#nullsAreSortedAtStart()
     */
    public final boolean nullsAreSortedAtStart() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#nullsAreSortedHigh()
     */
    public final boolean nullsAreSortedHigh() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#nullsAreSortedLow()
     */
    public final boolean nullsAreSortedLow() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#othersDeletesAreVisible(int)
     */
    public final boolean othersDeletesAreVisible(final int type)
            throws SQLException {
        checkCall(new Object[] {type});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#othersInsertsAreVisible(int)
     */
    public final boolean othersInsertsAreVisible(final int type)
            throws SQLException {
        checkCall(new Object[] {type});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#othersUpdatesAreVisible(int)
     */
    public final boolean othersUpdatesAreVisible(final int type)
            throws SQLException {
        checkCall(new Object[] {type});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#ownDeletesAreVisible(int)
     */
    public final boolean ownDeletesAreVisible(final int type)
            throws SQLException {
        checkCall(new Object[] {type});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#ownInsertsAreVisible(int)
     */
    public final boolean ownInsertsAreVisible(final int type)
            throws SQLException {
        checkCall(new Object[] {type});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#ownUpdatesAreVisible(int)
     */
    public final boolean ownUpdatesAreVisible(final int type)
            throws SQLException {
        checkCall(new Object[] {type});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#storesLowerCaseIdentifiers()
     */
    public final boolean storesLowerCaseIdentifiers() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#storesLowerCaseQuotedIdentifiers()
     */
    public final boolean storesLowerCaseQuotedIdentifiers()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#storesMixedCaseIdentifiers()
     */
    public final boolean storesMixedCaseIdentifiers() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#storesMixedCaseQuotedIdentifiers()
     */
    public final boolean storesMixedCaseQuotedIdentifiers()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#storesUpperCaseIdentifiers()
     */
    public final boolean storesUpperCaseIdentifiers() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#storesUpperCaseQuotedIdentifiers()
     */
    public final boolean storesUpperCaseQuotedIdentifiers()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsANSI92EntryLevelSQL()
     */
    public final boolean supportsANSI92EntryLevelSQL() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsANSI92FullSQL()
     */
    public final boolean supportsANSI92FullSQL() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsANSI92IntermediateSQL()
     */
    public final boolean supportsANSI92IntermediateSQL() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsAlterTableWithAddColumn()
     */
    public final boolean supportsAlterTableWithAddColumn() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsAlterTableWithDropColumn()
     */
    public final boolean supportsAlterTableWithDropColumn()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsBatchUpdates()
     */
    public final boolean supportsBatchUpdates() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsCatalogsInDataManipulation()
     */
    public final boolean supportsCatalogsInDataManipulation()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsCatalogsInIndexDefinitions()
     */
    public final boolean supportsCatalogsInIndexDefinitions()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsCatalogsInPrivilegeDefinitions()
     */
    public final boolean supportsCatalogsInPrivilegeDefinitions()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsCatalogsInProcedureCalls()
     */
    public final boolean supportsCatalogsInProcedureCalls()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsCatalogsInTableDefinitions()
     */
    public final boolean supportsCatalogsInTableDefinitions()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsColumnAliasing()
     */
    public final boolean supportsColumnAliasing() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsConvert()
     */
    public final boolean supportsConvert() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsConvert(int, int)
     */
    public final boolean supportsConvert(final int fromType, final int toType)
            throws SQLException {
        checkCall(new Object[] {fromType, toType});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsCoreSQLGrammar()
     */
    public final boolean supportsCoreSQLGrammar() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsCorrelatedSubqueries()
     */
    public final boolean supportsCorrelatedSubqueries() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#
     *      supportsDataDefinitionAndDataManipulationTransactions()
     */
    public final boolean supportsDataDefinitionAndDataManipulationTransactions()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsDataManipulationTransactionsOnly()
     */
    public final boolean supportsDataManipulationTransactionsOnly()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsDifferentTableCorrelationNames()
     */
    public final boolean supportsDifferentTableCorrelationNames()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsExpressionsInOrderBy()
     */
    public final boolean supportsExpressionsInOrderBy() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsExtendedSQLGrammar()
     */
    public final boolean supportsExtendedSQLGrammar() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsFullOuterJoins()
     */
    public final boolean supportsFullOuterJoins() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsGetGeneratedKeys()
     */
    public final boolean supportsGetGeneratedKeys() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsGroupBy()
     */
    public final boolean supportsGroupBy() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsGroupByBeyondSelect()
     */
    public final boolean supportsGroupByBeyondSelect() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsGroupByUnrelated()
     */
    public final boolean supportsGroupByUnrelated() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsIntegrityEnhancementFacility()
     */
    public final boolean supportsIntegrityEnhancementFacility()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsLikeEscapeClause()
     */
    public final boolean supportsLikeEscapeClause() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsLimitedOuterJoins()
     */
    public final boolean supportsLimitedOuterJoins() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsMinimumSQLGrammar()
     */
    public final boolean supportsMinimumSQLGrammar() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsMixedCaseIdentifiers()
     */
    public final boolean supportsMixedCaseIdentifiers() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsMixedCaseQuotedIdentifiers()
     */
    public final boolean supportsMixedCaseQuotedIdentifiers()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsMultipleOpenResults()
     */
    public final boolean supportsMultipleOpenResults() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsMultipleResultSets()
     */
    public final boolean supportsMultipleResultSets() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsMultipleTransactions()
     */
    public final boolean supportsMultipleTransactions() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsNamedParameters()
     */
    public final boolean supportsNamedParameters() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsNonNullableColumns()
     */
    public final boolean supportsNonNullableColumns() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsOpenCursorsAcrossCommit()
     */
    public final boolean supportsOpenCursorsAcrossCommit() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsOpenCursorsAcrossRollback()
     */
    public final boolean supportsOpenCursorsAcrossRollback()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsOpenStatementsAcrossCommit()
     */
    public final boolean supportsOpenStatementsAcrossCommit()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsOpenStatementsAcrossRollback()
     */
    public final boolean supportsOpenStatementsAcrossRollback()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsOrderByUnrelated()
     */
    public final boolean supportsOrderByUnrelated() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsOuterJoins()
     */
    public final boolean supportsOuterJoins() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsPositionedDelete()
     */
    public final boolean supportsPositionedDelete() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsPositionedUpdate()
     */
    public final boolean supportsPositionedUpdate() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsResultSetConcurrency(int, int)
     */
    public final boolean supportsResultSetConcurrency(final int type,
            final int concurrency) throws SQLException {
        checkCall(new Object[] {type, concurrency});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsResultSetHoldability(int)
     */
    public final boolean supportsResultSetHoldability(final int holdability)
            throws SQLException {
        checkCall(new Object[] {holdability});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsResultSetType(int)
     */
    public final boolean supportsResultSetType(final int type)
            throws SQLException {
        checkCall(new Object[] {type});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSavepoints()
     */
    public final boolean supportsSavepoints() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSchemasInDataManipulation()
     */
    public final boolean supportsSchemasInDataManipulation()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSchemasInIndexDefinitions()
     */
    public final boolean supportsSchemasInIndexDefinitions()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSchemasInPrivilegeDefinitions()
     */
    public final boolean supportsSchemasInPrivilegeDefinitions()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSchemasInProcedureCalls()
     */
    public final boolean supportsSchemasInProcedureCalls() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSchemasInTableDefinitions()
     */
    public final boolean supportsSchemasInTableDefinitions()
            throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSelectForUpdate()
     */
    public final boolean supportsSelectForUpdate() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsStatementPooling()
     */
    public final boolean supportsStatementPooling() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }


    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsStoredProcedures()
     */
    public final boolean supportsStoredProcedures() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSubqueriesInComparisons()
     */
    public final boolean supportsSubqueriesInComparisons() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSubqueriesInExists()
     */
    public final boolean supportsSubqueriesInExists() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSubqueriesInIns()
     */
    public final boolean supportsSubqueriesInIns() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSubqueriesInQuantifieds()
     */
    public final boolean supportsSubqueriesInQuantifieds() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsTableCorrelationNames()
     */
    public final boolean supportsTableCorrelationNames() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsTransactionIsolationLevel(int)
     */
    public final boolean supportsTransactionIsolationLevel(final int level)
            throws SQLException {
        checkCall(new Object[] {level});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsTransactions()
     */
    public final boolean supportsTransactions() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsUnion()
     */
    public final boolean supportsUnion() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsUnionAll()
     */
    public final boolean supportsUnionAll() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#updatesAreDetected(int)
     */
    public final boolean updatesAreDetected(final int type)
            throws SQLException {
        checkCall(new Object[] {type});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#usesLocalFilePerTable()
     */
    public final boolean usesLocalFilePerTable() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#usesLocalFiles()
     */
    public final boolean usesLocalFiles() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }


    //TODO joudek implement correctly

    @Override
    public RowIdLifetime getRowIdLifetime() throws SQLException {
        return null;
    }

    @Override
    public ResultSet getSchemas(String catalog, String schemaPattern) throws SQLException {
        return null;
    }

    @Override
    public boolean supportsStoredFunctionsUsingCallSyntax() throws SQLException {
        return false;
    }

    @Override
    public boolean autoCommitFailureClosesAllResultSets() throws SQLException {
        return false;
    }

    @Override
    public ResultSet getClientInfoProperties() throws SQLException {
        return null;
    }

    @Override
    public ResultSet getFunctions(String catalog, String schemaPattern, String functionNamePattern) throws SQLException {
        return null;
    }

    @Override
    public ResultSet getFunctionColumns(String catalog, String schemaPattern, String functionNamePattern, String columnNamePattern) throws SQLException {
        return null;
    }

    @Override
    public ResultSet getPseudoColumns(String catalog, String schemaPattern, String tableNamePattern, String columnNamePattern) throws SQLException {
        return null;
    }

    @Override
    public boolean generatedKeyAlwaysReturned() throws SQLException {
        return false;
    }


    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }
}
