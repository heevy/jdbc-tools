/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.jdbc;

import java.sql.SQLException;

import com.facdatum.jdbc.listener.driver.AbstractJDBCTestCase;

import junit.framework.Assert;

/**
 * Mock of base class that provides a helper method for checking the call is
 * correct for calling a delegate jdbc class method.
 */
public class  MockJDBCBase extends Assert {

    /**
     * The pre-string before the proper class name appears for mock classes.
     */
    protected static final String MOCK_PATH = "com.facdatum.mock.jdbc.Mock";

    /**
     * The depth in the call stack that mock calls should appear.
     */
    protected static final int MOCK_DEPTH = 1;

    /**
     * The pre-string before the proper class name appears for listener classes.
     */
    protected static final String LISTENER_PATH =
            "com.facdatum.jdbc.listener.driver.";


    /**
     * The depth in the call stack that listener calls should appear.
     */
    protected static final int LISTENER_DEPTH = 2;

    /**
     * Determines if an exception should be generated after checking call.
     */
    protected boolean failCall = false; //NOPMD NOCS

    /**
     * Determines whether to check the call stack or not.
     */
    protected boolean skipCheck = false; //NOPMD NOCS

    /**
     * Sets the failCall variable that if set will cause the check call
     * to raise a SQLException after completing checks.
     * @param fail specifies whether calls should throw exception or not
     */
    public final void setToFail(final boolean fail) {
        failCall = fail;
    }

    /**
     * Sets the status of the method to skip the checkCall.
     * @param skip true if the checkCall method is to be skipped; false
     * otherwise
     */
    public final void setSkipCheckCall(final boolean skip) {
        skipCheck = skip;
    }

    /**
     * Checks the call stack to within the mock JDBC layer to ensure that
     * the listener is called correctly.
     * @param args arguments to the call
     * @throws SQLException if test is configured to check the exception
     * handling of listener.
     */
    protected final void checkCall(final Object[] args) throws SQLException {
        if (!skipCheck) {
            final Exception e = new Exception(); //NOPMD
            final StackTraceElement[] stackTrace = e.getStackTrace();
            assertEquals("testing class",
                    stackTrace[MOCK_DEPTH].getClassName().substring(
                            MOCK_PATH.length()),
                    stackTrace[LISTENER_DEPTH].getClassName().substring(
                            LISTENER_PATH.length(),
                            stackTrace[LISTENER_DEPTH].getClassName().indexOf(
                                    "Listener")));
            assertEquals("testing method name",
                    stackTrace[MOCK_DEPTH].getMethodName(),
                    stackTrace[LISTENER_DEPTH].getMethodName());
            final Object[] callArgs = AbstractJDBCTestCase.getArgs();
            assertEquals("testing number of args", args.length,
                    callArgs.length);
            for (int i = 0; i < args.length; i++) {
                assertEquals("testing arg " + i, args[i], callArgs[i]);
            }
        }
        if (failCall) {
            throw new SQLException();
        }
    }

    /**
     * Copy of checkCall method without the exception check. This
     * is called by test methods that do not need to handle exceptions.
     * The checkCall code is copied rather than called so stack trace
     * depths remain the same for testing call stack.
     * @param args arguments to the call
     */
    protected final void cleanCheckCall(final Object[] args) {
        if (!skipCheck) {
            final Exception e = new Exception(); //NOPMD
            final StackTraceElement[] stackTrace = e.getStackTrace();
            assertEquals("testing class",
                    stackTrace[MOCK_DEPTH].getClassName().substring(
                            MOCK_PATH.length()),
                    stackTrace[LISTENER_DEPTH].getClassName().substring(
                            LISTENER_PATH.length(),
                            stackTrace[LISTENER_DEPTH].getClassName().indexOf(
                                    "Listener")));
            assertEquals("testing method name",
                    stackTrace[MOCK_DEPTH].getMethodName(),
                    stackTrace[LISTENER_DEPTH].getMethodName());
            final Object[] callArgs = AbstractJDBCTestCase.getArgs();
            assertEquals("testing number of args", args.length,
                    callArgs.length);
            for (int i = 0; i < args.length; i++) {
                assertEquals("testing arg " + i, args[i], callArgs[i]);
            }
        }
    }
}
