/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.jdbc;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

/**
 * Mock of the Array class that checks each call to ensure that it has been
 * called from delegating listener class.
 */
public class MockArray extends MockJDBCBase implements Array {

    /**
     * {@inheritDoc}
     * @see java.sql.Array#getArray()
     */
    public final Object getArray() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Array#getArray(java.util.Map)
     */
    public final Object getArray(final Map < String, Class < ? > > map)
            throws SQLException {
        checkCall(new Object[] {map});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Array#getArray(long, int)
     */
    public final Object getArray(final long index, final int count)
            throws SQLException {
        checkCall(new Object[] {index, count});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Array#getArray(long, int, java.util.Map)
     */
    public final Object getArray(final long index, final int count,
            final Map < String, Class < ? > > map) throws SQLException {
        checkCall(new Object[] {index, count, map});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Array#getBaseType()
     */
    public final int getBaseType() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Array#getBaseTypeName()
     */
    public final String getBaseTypeName() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Array#getResultSet()
     */
    public final ResultSet getResultSet() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Array#getResultSet(java.util.Map)
     */
    public final ResultSet getResultSet(final Map < String, Class < ? > > map)
            throws SQLException {
        checkCall(new Object[] {map});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Array#getResultSet(long, int)
     */
    public final ResultSet getResultSet(final long index, final int count)
            throws SQLException {
        checkCall(new Object[] {index, count});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Array#getResultSet(long, int, java.util.Map)
     */
    public final ResultSet getResultSet(final long index, final int count,
            final Map < String, Class < ? > > map) throws SQLException {
        checkCall(new Object[] {index, count, map});
        return null;
    }

    //TODO joudek implement correctly

    @Override
    public void free() throws SQLException {

    }
}
