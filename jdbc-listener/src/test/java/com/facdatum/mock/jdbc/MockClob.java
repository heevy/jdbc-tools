/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.jdbc;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.sql.Clob;
import java.sql.SQLException;

/**
 * Mock of the Clob class that checks each call to ensure that it has been
 * called from delegating listener class.
 */
public class MockClob extends MockJDBCBase implements Clob {

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#getAsciiStream()
     */
    public final InputStream getAsciiStream() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#getCharacterStream()
     */
    public final Reader getCharacterStream() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#getSubString(long, int)
     */
    public final String getSubString(final long arg0, final int arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#length()
     */
    public final long length() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#position(java.lang.String, long)
     */
    public final long position(final String arg0, final long arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#position(java.sql.Clob, long)
     */
    public final long position(final Clob arg0, final long arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#setAsciiStream(long)
     */
    public final OutputStream setAsciiStream(final long arg0)
            throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#setCharacterStream(long)
     */
    public final Writer setCharacterStream(final long arg0)
            throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#setString(long, java.lang.String)
     */
    public final int setString(final long arg0, final String arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#setString(long, java.lang.String, int, int)
     */
    public final int setString(final long arg0, final String arg1,
            final int arg2, final int arg3) throws SQLException {
        checkCall(new Object[] {arg0, arg1, arg2, arg3});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#truncate(long)
     */
    public final void truncate(final long arg0) throws SQLException {
        checkCall(new Object[] {arg0});
    }

    //TODO Joudek implement correctly

    @Override
    public void free() throws SQLException {

    }

    @Override
    public Reader getCharacterStream(long pos, long length) throws SQLException {
        return null;
    }
}
