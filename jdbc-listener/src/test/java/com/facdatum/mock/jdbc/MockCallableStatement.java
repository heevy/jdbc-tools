/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.jdbc; //NOPMD

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Date;
import java.sql.NClob;
import java.sql.Ref;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Map;

/**
 * Mock of the CallableStatement class that checks each call to ensure that it
 * has been called from delegating listener class.
 */
public class MockCallableStatement extends MockPreparedStatement implements
        CallableStatement {

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getArray(int)
     */
    public final Array getArray(final int index) throws SQLException {
        checkCall(new Object[] {index});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getArray(java.lang.String)
     */
    public final Array getArray(final String parameterName)
            throws SQLException {
        checkCall(new Object[] {parameterName});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getBigDecimal(int)
     */
    public final BigDecimal getBigDecimal(final int parameterIndex)
            throws SQLException {
        checkCall(new Object[] {parameterIndex});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getBigDecimal(java.lang.String)
     */
    public final BigDecimal getBigDecimal(final String parameterName)
            throws SQLException {
        checkCall(new Object[] {parameterName});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getBigDecimal(int, int)
     */
    public final BigDecimal getBigDecimal(final int parameterIndex,
            final int scale) throws SQLException {
        checkCall(new Object[] {parameterIndex, scale});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getBlob(int)
     */
    public final Blob getBlob(final int index) throws SQLException {
        checkCall(new Object[] {index});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getBlob(java.lang.String)
     */
    public final Blob getBlob(final String parameterName) throws SQLException {
        checkCall(new Object[] {parameterName});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getBoolean(int)
     */
    public final boolean getBoolean(final int parameterIndex)
            throws SQLException {
        checkCall(new Object[] {parameterIndex});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getBoolean(java.lang.String)
     */
    public final boolean getBoolean(final String parameterName)
            throws SQLException {
        checkCall(new Object[] {parameterName});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getByte(int)
     */
    public final byte getByte(final int parameterIndex) throws SQLException {
        checkCall(new Object[] {parameterIndex});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getByte(java.lang.String)
     */
    public final byte getByte(final String parameterIndex) throws SQLException {
        checkCall(new Object[] {parameterIndex});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getBytes(int)
     */
    public final byte[] getBytes(final int parameterIndex) throws SQLException {
        checkCall(new Object[] {parameterIndex});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getBytes(java.lang.String)
     */
    public final byte[] getBytes(final String parameterName)
            throws SQLException {
        checkCall(new Object[] {parameterName});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getClob(int)
     */
    public final Clob getClob(final int index) throws SQLException {
        checkCall(new Object[] {index});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getClob(java.lang.String)
     */
    public final Clob getClob(final String parameterName) throws SQLException {
        checkCall(new Object[] {parameterName});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getDate(int)
     */
    public final Date getDate(final int parameterIndex) throws SQLException {
        checkCall(new Object[] {parameterIndex});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getDate(java.lang.String)
     */
    public final Date getDate(final String parameterName) throws SQLException {
        checkCall(new Object[] {parameterName});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getDate(int, java.util.Calendar)
     */
    public final Date getDate(final int parameterIndex, final Calendar cal)
            throws SQLException {
        checkCall(new Object[] {parameterIndex, cal});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getDate(java.lang.String,
     *      java.util.Calendar)
     */
    public final Date getDate(final String parameterName, final Calendar cal)
            throws SQLException {
        checkCall(new Object[] {parameterName, cal});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getDouble(int)
     */
    public final double getDouble(final int parameterIndex)
            throws SQLException {
        checkCall(new Object[] {parameterIndex});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getDouble(java.lang.String)
     */
    public final double getDouble(final String parameterName)
            throws SQLException {
        checkCall(new Object[] {parameterName});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getFloat(int)
     */
    public final float getFloat(final int parameterIndex) throws SQLException {
        checkCall(new Object[] {parameterIndex});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getFloat(java.lang.String)
     */
    public final float getFloat(final String parameterName)
            throws SQLException {
        checkCall(new Object[] {parameterName});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getInt(int)
     */
    public final int getInt(final int parameterIndex) throws SQLException {
        checkCall(new Object[] {parameterIndex});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getInt(java.lang.String)
     */
    public final int getInt(final String parameterName) throws SQLException {
        checkCall(new Object[] {parameterName});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getLong(int)
     */
    public final long getLong(final int parameterIndex) throws SQLException {
        checkCall(new Object[] {parameterIndex});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getLong(java.lang.String)
     */
    public final long getLong(final String parameterName) throws SQLException {
        checkCall(new Object[] {parameterName});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getObject(int)
     */
    public final Object getObject(final int parameterIndex)
            throws SQLException {
        checkCall(new Object[] {parameterIndex});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getObject(java.lang.String)
     */
    public final Object getObject(final String parameterName)
            throws SQLException {
        checkCall(new Object[] {parameterName});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getObject(int, java.util.Map)
     */
    public final Object getObject(final int index,
            final Map < String, Class < ? > > map) throws SQLException {
        checkCall(new Object[] {index, map});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getObject(java.lang.String,
     *      java.util.Map)
     */
    public final Object getObject(final String parameterName,
            final Map < String, Class < ? > > map) throws SQLException {
        checkCall(new Object[] {parameterName, map});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getRef(int)
     */
    public final Ref getRef(final int index) throws SQLException {
        checkCall(new Object[] {index});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getRef(java.lang.String)
     */
    public final Ref getRef(final String parameterName)
            throws SQLException {
        checkCall(new Object[] {parameterName});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getShort(int)
     */
    public final short getShort(final int parameterIndex)
            throws SQLException {
        checkCall(new Object[] {parameterIndex});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getShort(java.lang.String)
     */
    public final short getShort(final String parameterName)
            throws SQLException {
        checkCall(new Object[] {parameterName});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getString(int)
     */
    public final String getString(final int parameterIndex)
            throws SQLException {
        checkCall(new Object[] {parameterIndex});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getString(java.lang.String)
     */
    public final String getString(final String parameterName)
            throws SQLException {
        checkCall(new Object[] {parameterName});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getTime(int)
     */
    public final Time getTime(final int parameterIndex) throws SQLException {
        checkCall(new Object[] {parameterIndex});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getTime(java.lang.String)
     */
    public final Time getTime(final String parameterName) throws SQLException {
        checkCall(new Object[] {parameterName});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getTime(int, java.util.Calendar)
     */
    public final Time getTime(final int parameterIndex, final Calendar cal)
            throws SQLException {
        checkCall(new Object[] {parameterIndex, cal});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getTime(java.lang.String,
     *      java.util.Calendar)
     */
    public final Time getTime(final String parameterName, final Calendar cal)
            throws SQLException {
        checkCall(new Object[] {parameterName, cal});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getTimestamp(int)
     */
    public final Timestamp getTimestamp(final int parameterIndex)
            throws SQLException {
        checkCall(new Object[] {parameterIndex});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getTimestamp(java.lang.String)
     */
    public final Timestamp getTimestamp(final String parameterName)
            throws SQLException {
        checkCall(new Object[] {parameterName});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getTimestamp(int, java.util.Calendar)
     */
    public final Timestamp getTimestamp(final int parameterIndex,
            final Calendar cal) throws SQLException {
        checkCall(new Object[] {parameterIndex, cal});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getTimestamp(java.lang.String,
     *      java.util.Calendar)
     */
    public final Timestamp getTimestamp(final String parameterName,
            final Calendar cal) throws SQLException {
        checkCall(new Object[] {parameterName, cal});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getURL(int)
     */
    public final URL getURL(final int parameterIndex) throws SQLException {
        checkCall(new Object[] {parameterIndex});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#getURL(java.lang.String)
     */
    public final URL getURL(final String parameterName) throws SQLException {
        checkCall(new Object[] {parameterName});
        return null;
    }


    //TODO Joudek implement correctly

    @Override
    public RowId getRowId(int parameterIndex) throws SQLException {
        return null;
    }

    @Override
    public RowId getRowId(String parameterName) throws SQLException {
        return null;
    }

    @Override
    public void setRowId(String parameterName, RowId x) throws SQLException {

    }

    @Override
    public void setNString(String parameterName, String value) throws SQLException {

    }

    @Override
    public void setNCharacterStream(String parameterName, Reader value, long length) throws SQLException {

    }

    @Override
    public void setNClob(String parameterName, NClob value) throws SQLException {

    }

    @Override
    public void setClob(String parameterName, Reader reader, long length) throws SQLException {

    }

    @Override
    public void setBlob(String parameterName, InputStream inputStream, long length) throws SQLException {

    }

    @Override
    public void setNClob(String parameterName, Reader reader, long length) throws SQLException {

    }

    @Override
    public NClob getNClob(int parameterIndex) throws SQLException {
        return null;
    }

    @Override
    public NClob getNClob(String parameterName) throws SQLException {
        return null;
    }

    @Override
    public void setSQLXML(String parameterName, SQLXML xmlObject) throws SQLException {

    }

    @Override
    public SQLXML getSQLXML(int parameterIndex) throws SQLException {
        return null;
    }

    @Override
    public SQLXML getSQLXML(String parameterName) throws SQLException {
        return null;
    }

    @Override
    public String getNString(int parameterIndex) throws SQLException {
        return null;
    }

    @Override
    public String getNString(String parameterName) throws SQLException {
        return null;
    }

    @Override
    public Reader getNCharacterStream(int parameterIndex) throws SQLException {
        return null;
    }

    @Override
    public Reader getNCharacterStream(String parameterName) throws SQLException {
        return null;
    }

    @Override
    public Reader getCharacterStream(int parameterIndex) throws SQLException {
        return null;
    }

    @Override
    public Reader getCharacterStream(String parameterName) throws SQLException {
        return null;
    }

    @Override
    public void setBlob(String parameterName, Blob x) throws SQLException {

    }

    @Override
    public void setClob(String parameterName, Clob x) throws SQLException {

    }

    @Override
    public void setAsciiStream(String parameterName, InputStream x, long length) throws SQLException {

    }

    @Override
    public void setBinaryStream(String parameterName, InputStream x, long length) throws SQLException {

    }

    @Override
    public void setCharacterStream(String parameterName, Reader reader, long length) throws SQLException {

    }

    @Override
    public void setAsciiStream(String parameterName, InputStream x) throws SQLException {

    }

    @Override
    public void setBinaryStream(String parameterName, InputStream x) throws SQLException {

    }

    @Override
    public void setCharacterStream(String parameterName, Reader reader) throws SQLException {

    }

    @Override
    public void setNCharacterStream(String parameterName, Reader value) throws SQLException {

    }

    @Override
    public void setClob(String parameterName, Reader reader) throws SQLException {

    }

    @Override
    public void setBlob(String parameterName, InputStream inputStream) throws SQLException {

    }

    @Override
    public void setNClob(String parameterName, Reader reader) throws SQLException {

    }

    @Override
    public <T> T getObject(int parameterIndex, Class<T> type) throws SQLException {
        return null;
    }

    @Override
    public <T> T getObject(String parameterName, Class<T> type) throws SQLException {
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#registerOutParameter(int, int)
     */
    public final void registerOutParameter(final int parameterIndex,
            final int sqlType) throws SQLException {
        checkCall(new Object[] {parameterIndex, sqlType});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#registerOutParameter(java.lang.String,
     *      int)
     */
    public final void registerOutParameter(final String parameterName,
            final int sqlType) throws SQLException {
        checkCall(new Object[] {parameterName, sqlType});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#registerOutParameter(int, int, int)
     */
    public final void registerOutParameter(final int parameterIndex,
            final int sqlType, final int scale) throws SQLException {
        checkCall(new Object[] {parameterIndex, sqlType, scale});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#registerOutParameter(int, int,
     *      java.lang.String)
     */
    public final void registerOutParameter(final int paramIndex,
            final int sqlType, final String typeName) throws SQLException {
        checkCall(new Object[] {paramIndex, sqlType, typeName});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#registerOutParameter(java.lang.String,
     *      int, int)
     */
    public final void registerOutParameter(final String parameterName,
            final int sqlType, final int scale) throws SQLException {
        checkCall(new Object[] {parameterName, sqlType, scale});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#registerOutParameter(java.lang.String,
     *      int, java.lang.String)
     */
    public final void registerOutParameter(final String parameterName,
            final int sqlType, final String typeName) throws SQLException {
        checkCall(new Object[] {parameterName, sqlType, typeName});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setAsciiStream(java.lang.String,
     *      java.io.InputStream, int)
     */
    public final void setAsciiStream(final String parameterName,
            final InputStream val, final int length) throws SQLException {
        checkCall(new Object[] {parameterName, val, length});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setBigDecimal(java.lang.String,
     *      java.math.BigDecimal)
     */
    public final void setBigDecimal(final String parameterName,
            final BigDecimal val) throws SQLException {
        checkCall(new Object[] {parameterName, val});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setBinaryStream(java.lang.String,
     *      java.io.InputStream, int)
     */
    public final void setBinaryStream(final String parameterName,
            final InputStream val, final int length) throws SQLException {
        checkCall(new Object[] {parameterName, val, length});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setBoolean(java.lang.String, boolean)
     */
    public final void setBoolean(final String parameterName, final boolean val)
            throws SQLException {
        checkCall(new Object[] {parameterName, val});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setByte(java.lang.String, byte)
     */
    public final void setByte(final String parameterName, final byte val)
            throws SQLException {
        checkCall(new Object[] {parameterName, val});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setBytes(java.lang.String, byte[])
     */
    public final void setBytes(final String parameterName, final byte[] val)
            throws SQLException {
        checkCall(new Object[] {parameterName, val});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setCharacterStream(java.lang.String,
     *      java.io.Reader, int)
     */
    public final void setCharacterStream(final String parameterName,
            final Reader reader, final int length) throws SQLException {
        checkCall(new Object[] {parameterName, reader, length});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setDate(java.lang.String, java.sql.Date)
     */
    public final void setDate(final String parameterName, final Date val)
            throws SQLException {
        checkCall(new Object[] {parameterName, val});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setDate(java.lang.String, java.sql.Date,
     *      java.util.Calendar)
     */
    public final void setDate(final String parameterName, final Date val,
            final Calendar cal) throws SQLException {
        checkCall(new Object[] {parameterName, val, cal});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setDouble(java.lang.String, double)
     */
    public final void setDouble(final String parameterName, final double val)
            throws SQLException {
        checkCall(new Object[] {parameterName, val});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setFloat(java.lang.String, float)
     */
    public final void setFloat(final String parameterName, final float val)
            throws SQLException {
        checkCall(new Object[] {parameterName, val});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setInt(java.lang.String, int)
     */
    public final void setInt(final String parameterName, final int val)
            throws SQLException {
        checkCall(new Object[] {parameterName, val});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setLong(java.lang.String, long)
     */
    public final void setLong(final String parameterName, final long val)
            throws SQLException {
        checkCall(new Object[] {parameterName, val});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setNull(java.lang.String, int)
     */
    public final void setNull(final String parameterName, final int sqlType)
            throws SQLException {
        checkCall(new Object[] {parameterName, sqlType});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setNull(java.lang.String, int,
     *      java.lang.String)
     */
    public final void setNull(final String parameterName, final int sqlType,
            final String typeName) throws SQLException {
        checkCall(new Object[] {parameterName, sqlType, typeName});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setObject(java.lang.String,
     *      java.lang.Object)
     */
    public final void setObject(final String parameterName, final Object val)
            throws SQLException {
        checkCall(new Object[] {parameterName, val});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setObject(java.lang.String,
     *      java.lang.Object, int)
     */
    public final void setObject(final String parameterName, final Object val,
            final int targetSqlType) throws SQLException {
        checkCall(new Object[] {parameterName, val, targetSqlType});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setObject(java.lang.String,
     *      java.lang.Object, int, int)
     */
    public final void setObject(final String parameterName, final Object val,
            final int targetSqlType, final int scale) throws SQLException {
        checkCall(new Object[] {parameterName, val, targetSqlType, scale});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setShort(java.lang.String, short)
     */
    public final void setShort(final String parameterName, final short val)
            throws SQLException {
        checkCall(new Object[] {parameterName, val});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setString(java.lang.String,
     *      java.lang.String)
     */
    public final void setString(final String parameterName, final String val)
            throws SQLException {
        checkCall(new Object[] {parameterName, val});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setTime(java.lang.String, java.sql.Time)
     */
    public final void setTime(final String parameterName, final Time val)
            throws SQLException {
        checkCall(new Object[] {parameterName, val});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setTime(java.lang.String, java.sql.Time,
     *      java.util.Calendar)
     */
    public final void setTime(final String parameterName, final Time val,
            final Calendar cal) throws SQLException {
        checkCall(new Object[] {parameterName, val, cal});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setTimestamp(java.lang.String,
     *      java.sql.Timestamp)
     */
    public final void setTimestamp(final String parameterName,
            final Timestamp val) throws SQLException {
        checkCall(new Object[] {parameterName, val});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setTimestamp(java.lang.String,
     *      java.sql.Timestamp, java.util.Calendar)
     */
    public final void setTimestamp(final String parameterName,
            final Timestamp val, final Calendar cal) throws SQLException {
        checkCall(new Object[] {parameterName, val, cal});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#setURL(java.lang.String, java.net.URL)
     */
    public final void setURL(final String parameterName, final URL val)
            throws SQLException {
        checkCall(new Object[] {parameterName, val});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.CallableStatement#wasNull()
     */
    public final boolean wasNull() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }
}
