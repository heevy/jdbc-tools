/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.jdbc;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.ConnectionEventListener;
import javax.sql.PooledConnection;
import javax.sql.StatementEventListener;

/**
 * Mock of the PooledConnection class that checks each call to ensure that it
 * has been called from delegating listener class.
 */
public class MockPooledConnection extends MockJDBCBase implements
        PooledConnection {

    /**
     * {@inheritDoc}
     * @see javax.sql.PooledConnection#addConnectionEventListener(
     *      javax.sql.ConnectionEventListener)
     */
    public final void addConnectionEventListener(
            final ConnectionEventListener arg0) {
        cleanCheckCall(new Object[] {arg0});
    }

    /**
     * {@inheritDoc}
     * @see javax.sql.PooledConnection#close()
     */
    public final void close() throws SQLException {
        checkCall(new Object[] {});
    }

    /**
     * {@inheritDoc}
     * @see javax.sql.PooledConnection#getConnection()
     */
    public final Connection getConnection() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see javax.sql.PooledConnection#removeConnectionEventListener(
     *      javax.sql.ConnectionEventListener)
     */
    public final void removeConnectionEventListener(
            final ConnectionEventListener arg0) {
        cleanCheckCall(new Object[] {arg0});
    }

    //TODO joudek implement correctly

    @Override
    public void addStatementEventListener(StatementEventListener listener) {

    }

    @Override
    public void removeStatementEventListener(StatementEventListener listener) {

    }
}
