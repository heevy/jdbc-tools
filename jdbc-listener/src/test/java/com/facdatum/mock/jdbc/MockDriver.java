/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.jdbc;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Mock of the Driver class that checks each call to ensure that it
 * has been called from delegating listener class.
 */
public class MockDriver extends MockJDBCBase implements Driver {

    /**
     * {@inheritDoc}
     * @see java.sql.Driver#acceptsURL(java.lang.String)
     */
    public final boolean acceptsURL(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Driver#connect(java.lang.String, java.util.Properties)
     */
    public final Connection connect(final String arg0, final Properties arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return new MockConnection(); //need to return real value to test return
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Driver#getMajorVersion()
     */
    public final int getMajorVersion() {
        cleanCheckCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Driver#getMinorVersion()
     */
    public final int getMinorVersion() {
        cleanCheckCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Driver#getPropertyInfo(java.lang.String,
     *      java.util.Properties)
     */
    public final DriverPropertyInfo[] getPropertyInfo(final String arg0,
            final Properties arg1) throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Driver#jdbcCompliant()
     */
    public final boolean jdbcCompliant() {
        cleanCheckCall(new Object[] {});
        return false;
    }

    //TODO joudek implement correctly

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return null;
    }
}
