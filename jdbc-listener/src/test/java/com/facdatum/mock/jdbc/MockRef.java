/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.jdbc;

import java.sql.Ref;
import java.sql.SQLException;
import java.util.Map;

/**
 * Mock of the Ref class that checks each call to ensure that it
 * has been called from delegating listener class.
 */
public class MockRef extends MockJDBCBase implements Ref {

    /**
     * {@inheritDoc}
     * @see java.sql.Ref#getBaseTypeName()
     */
    public final String getBaseTypeName() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Ref#getObject()
     */
    public final Object getObject() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Ref#getObject(java.util.Map)
     */
    public final Object getObject(final Map < String, Class < ? > > arg0)
            throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Ref#setObject(java.lang.Object)
     */
    public final void setObject(final Object arg0) throws SQLException {
        checkCall(new Object[] {arg0});
    }
}
