/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.jdbc;

import java.sql.SQLException;
import java.sql.Savepoint;

/**
 * Mock of the Savepoint class that checks each call to ensure that it
 * has been called from delegating listener class.
 */
public class MockSavepoint extends MockJDBCBase implements Savepoint {

    /**
     * {@inheritDoc}
     * @see java.sql.Savepoint#getSavepointId()
     */
    public final int getSavepointId() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Savepoint#getSavepointName()
     */
    public final String getSavepointName() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }
}
