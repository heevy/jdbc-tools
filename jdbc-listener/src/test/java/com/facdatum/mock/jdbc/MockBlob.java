/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.jdbc;

import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.SQLException;

/**
 * Mock of the Blob class that checks each call to ensure that it has been
 * called from delegating listener class.
 */
public class MockBlob extends MockJDBCBase implements Blob {

    /**
     * {@inheritDoc}
     * @see java.sql.Blob#getBinaryStream()
     */
    public final InputStream getBinaryStream() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Blob#getBytes(long, int)
     */
    public final byte[] getBytes(final long pos, final int length)
            throws SQLException {
        checkCall(new Object[] {pos, length});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Blob#length()
     */
    public final long length() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Blob#position(byte[], long)
     */
    public final long position(final byte[] pattern, final long start)
            throws SQLException {
        checkCall(new Object[] {pattern, start});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Blob#position(java.sql.Blob, long)
     */
    public final long position(final Blob pattern, final long start)
            throws SQLException {
        checkCall(new Object[] {pattern, start});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Blob#setBinaryStream(long)
     */
    public final OutputStream setBinaryStream(final long pos)
            throws SQLException {
        checkCall(new Object[] {pos});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Blob#setBytes(long, byte[])
     */
    public final int setBytes(final long pos, final byte[] bytes)
            throws SQLException {
        checkCall(new Object[] {pos, bytes});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Blob#setBytes(long, byte[], int, int)
     */
    public final int setBytes(final long pos, final byte[] bytes,
            final int offset, final int len) throws SQLException {
        checkCall(new Object[] {pos, bytes, offset, len});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Blob#truncate(long)
     */
    public final void truncate(final long len) throws SQLException {
        checkCall(new Object[] {len});
    }

    //TODO joudek implement correctly

    @Override
    public void free() throws SQLException {

    }

    @Override
    public InputStream getBinaryStream(long pos, long length) throws SQLException {
        return null;
    }
}
