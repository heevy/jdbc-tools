/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.jdbc;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

/**
 * Mock of the Connection class that checks each call to ensure that it has been
 * called from delegating listener class.
 */
public class MockConnection extends MockJDBCBase implements Connection {

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#clearWarnings()
     */
    public final void clearWarnings() throws SQLException {
        checkCall(new Object[] {});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#close()
     */
    public final void close() throws SQLException {
        checkCall(new Object[] {});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#commit()
     */
    public final void commit() throws SQLException {
        checkCall(new Object[] {});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#createStatement()
     */
    public final Statement createStatement() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#createStatement(int, int)
     */
    public final Statement createStatement(final int arg0, final int arg1)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#createStatement(int, int, int)
     */
    public final Statement createStatement(final int arg0, final int arg1,
            final int arg2) throws SQLException {
        checkCall(new Object[] {arg0, arg1, arg2});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#getAutoCommit()
     */
    public final boolean getAutoCommit() throws SQLException { //NOPMD
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#getCatalog()
     */
    public final String getCatalog() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#getHoldability()
     */
    public final int getHoldability() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#getMetaData()
     */
    public final DatabaseMetaData getMetaData() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#getTransactionIsolation()
     */
    public final int getTransactionIsolation() throws SQLException {
        checkCall(new Object[] {});
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#getTypeMap()
     */
    public final Map < String, Class < ? > > getTypeMap() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#getWarnings()
     */
    public final SQLWarning getWarnings() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#isClosed()
     */
    public final boolean isClosed() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#isReadOnly()
     */
    public final boolean isReadOnly() throws SQLException {
        checkCall(new Object[] {});
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#nativeSQL(java.lang.String)
     */
    public final String nativeSQL(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#prepareCall(java.lang.String)
     */
    public final CallableStatement prepareCall(final String arg0)
            throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#prepareCall(java.lang.String, int, int)
     */
    public final CallableStatement prepareCall(final String arg0,
            final int arg1, final int arg2) throws SQLException {
        checkCall(new Object[] {arg0, arg1, arg2});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#prepareCall(java.lang.String, int, int, int)
     */
    public final CallableStatement prepareCall(final String arg0,
            final int arg1, final int arg2, final int arg3)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1, arg2, arg3});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#prepareStatement(java.lang.String)
     */
    public final PreparedStatement prepareStatement(final String arg0)
            throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#prepareStatement(java.lang.String, int)
     */
    public final PreparedStatement prepareStatement(final String arg0,
            final int arg1) throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#prepareStatement(java.lang.String, int[])
     */
    public final PreparedStatement prepareStatement(final String arg0,
            final int[] arg1) throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#prepareStatement(java.lang.String,
     *      java.lang.String[])
     */
    public final PreparedStatement prepareStatement(final String arg0,
            final String[] arg1) throws SQLException {
        checkCall(new Object[] {arg0, arg1});
        return null;
    }



    /**
     * {@inheritDoc}
     * @see java.sql.Connection#prepareStatement(java.lang.String, int, int)
     */
    public final PreparedStatement prepareStatement(final String arg0,
            final int arg1, final int arg2) throws SQLException {
        checkCall(new Object[] {arg0, arg1, arg2});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#prepareStatement(java.lang.String,
     *      int, int, int)
     */
    public final PreparedStatement prepareStatement(final String arg0,
            final int arg1, final int arg2, final int arg3)
            throws SQLException {
        checkCall(new Object[] {arg0, arg1, arg2, arg3});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#releaseSavepoint(java.sql.Savepoint)
     */
    public final void releaseSavepoint(final Savepoint arg0)
            throws SQLException {
        checkCall(new Object[] {arg0});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#rollback()
     */
    public final void rollback() throws SQLException {
        checkCall(new Object[] {});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#rollback(java.sql.Savepoint)
     */
    public final void rollback(final Savepoint arg0) throws SQLException {
        checkCall(new Object[] {arg0});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#setAutoCommit(boolean)
     */
    public final void setAutoCommit(final boolean arg0) throws SQLException {
        checkCall(new Object[] {arg0});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#setCatalog(java.lang.String)
     */
    public final void setCatalog(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#setHoldability(int)
     */
    public final void setHoldability(final int arg0) throws SQLException {
        checkCall(new Object[] {arg0});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#setReadOnly(boolean)
     */
    public final void setReadOnly(final boolean arg0) throws SQLException {
        checkCall(new Object[] {arg0});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#setSavepoint()
     */
    public final Savepoint setSavepoint() throws SQLException {
        checkCall(new Object[] {});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#setSavepoint(java.lang.String)
     */
    public final Savepoint setSavepoint(final String arg0) throws SQLException {
        checkCall(new Object[] {arg0});
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#setTransactionIsolation(int)
     */
    public final void setTransactionIsolation(final int arg0)
            throws SQLException {
        checkCall(new Object[] {arg0});
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Connection#setTypeMap(java.util.Map)
     */
    public final void setTypeMap(final Map < String, Class < ? > > arg0)
            throws SQLException {
        checkCall(new Object[] {arg0});
    }

    //TODO Joudek implement correctly

    @Override
    public Clob createClob() throws SQLException {
        return null;
    }

    @Override
    public Blob createBlob() throws SQLException {
        return null;
    }

    @Override
    public NClob createNClob() throws SQLException {
        return null;
    }

    @Override
    public SQLXML createSQLXML() throws SQLException {
        return null;
    }

    @Override
    public boolean isValid(int timeout) throws SQLException {
        return false;
    }

    @Override
    public void setClientInfo(String name, String value) throws SQLClientInfoException {

    }

    @Override
    public void setClientInfo(Properties properties) throws SQLClientInfoException {

    }

    @Override
    public String getClientInfo(String name) throws SQLException {
        return null;
    }

    @Override
    public Properties getClientInfo() throws SQLException {
        return null;
    }

    @Override
    public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
        return null;
    }

    @Override
    public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
        return null;
    }

    @Override
    public void setSchema(String schema) throws SQLException {

    }

    @Override
    public String getSchema() throws SQLException {
        return null;
    }

    @Override
    public void abort(Executor executor) throws SQLException {

    }

    @Override
    public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {

    }

    @Override
    public int getNetworkTimeout() throws SQLException {
        return 0;
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }
}
