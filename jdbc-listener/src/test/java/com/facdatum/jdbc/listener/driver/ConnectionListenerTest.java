/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.SQLException;

import com.facdatum.mock.jdbc.MockConnection;

/**
 * Test of ConnectionListener.
 */
public class ConnectionListenerTest extends AbstractJDBCTestCase {

    /**
     * ConnectionListener under test.
     */
    protected ConnectionListener testConn = null; //NOPMD NOCS

    /**
     * Sets up the test.
     * @throws Exception if problem setting up test
     */
    protected void setUp() throws Exception { //NOPMD NOCS
        super.setUp();
        testConn = new ConnectionListener(new MockConnection());
    }

    /**
     * Cleans up the test.
     * @throws Exception if problem cleaning up test
     */
    protected final void tearDown() throws Exception { //NOPMD
        super.tearDown();
        testConn = null;
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      clearWarnings()}.
     */
    public final void testClearWarnings() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testConn.clearWarnings();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      close()}.
     */
    public final void testClose() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testConn.close();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      commit()}.
     */
    public final void testCommit() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testConn.commit();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      createStatement()}.
     */
    public final void testCreateStatement() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            checkListenerReturn(testConn.createStatement());
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     * createStatement(int, int)}.
     */
    public final void testCreateStatementIntInt() { //NOPMD
        try {
            final Object[] args = {0, 0};
            setArgs(args);
            checkListenerReturn(testConn.createStatement(0, 0));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      createStatement(int, int, int)}.
     */
    public final void testCreateStatementIntIntInt() { //NOPMD
        try {
            final Object[] args = {0, 0, 0};
            setArgs(args);
            checkListenerReturn(testConn.createStatement(0, 0, 0));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      getAutoCommit()}.
     */
    public final void testGetAutoCommit() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testConn.getAutoCommit();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      getCatalog()}.
     */
    public final void testGetCatalog() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testConn.getCatalog();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      getHoldability()}.
     */
    public final void testGetHoldability() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testConn.getHoldability();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      getMetaData()}.
     */
    public final void testGetMetaData() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            checkListenerReturn(testConn.getMetaData());
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      getTransactionIsolation()}.
     */
    public final void testGetTransactionIsolation() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testConn.getTransactionIsolation();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      getTypeMap()}.
     */
    public final void testGetTypeMap() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testConn.getTypeMap();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      getWarnings()}.
     */
    public final void testGetWarnings() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testConn.getWarnings();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      isClosed()}.
     */
    public final void testIsClosed() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testConn.isClosed();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      isReadOnly()}.
     */
    public final void testIsReadOnly() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testConn.isReadOnly();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      nativeSQL(java.lang.String)}.
     */
    public final void testNativeSQL() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testConn.nativeSQL(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      prepareCall(java.lang.String)}.
     */
    public final void testPrepareCallString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            checkListenerReturn(testConn.prepareCall(null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      prepareCall(java.lang.String, int, int)}.
     */
    public final void testPrepareCallStringIntInt() { //NOPMD
        try {
            final Object[] args = {null, 0, 0};
            setArgs(args);
            checkListenerReturn(testConn.prepareCall(null, 0, 0));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      prepareCall(java.lang.String, int, int, int)}.
     */
    public final void testPrepareCallStringIntIntInt() { //NOPMD
        try {
            final Object[] args = {null, 0, 0, 0};
            setArgs(args);
            checkListenerReturn(testConn.prepareCall(null, 0, 0, 0));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      prepareStatement(java.lang.String)}.
     */
    public final void testPrepareStatementString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            checkListenerReturn(testConn.prepareStatement(null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      prepareStatement(java.lang.String, int)}.
     */
    public final void testPrepareStatementStringInt() { //NOPMD
        try {
            final Object[] args = {null, 0};
            setArgs(args);
            checkListenerReturn(testConn.prepareStatement(null, 0));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      prepareStatement(java.lang.String, int[])}.
     */
    public final void testPrepareStatementStringIntArray() { //NOPMD
        try {
            final Object[] args = {null, (int[]) null};
            setArgs(args);
            checkListenerReturn(
                    testConn.prepareStatement(null, (int[]) null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      prepareStatement(java.lang.String, java.lang.String[])}.
     */
    public final void testPrepareStatementStringStringArray() { //NOPMD
        try {
            final Object[] args = {null, (String[]) null};
            setArgs(args);
            checkListenerReturn(
                    testConn.prepareStatement(null, (String[]) null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      prepareStatement(java.lang.String, int, int)}.
     */
    public final void testPrepareStatementStringIntInt() { //NOPMD
        try {
            final Object[] args = {null, 0, 0};
            setArgs(args);
            checkListenerReturn(testConn.prepareStatement(null, 0, 0));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      prepareStatement(java.lang.String, int, int, int)}.
     */
    public final void testPrepareStatementStringIntIntInt() { //NOPMD
        try {
            final Object[] args = {null, 0, 0, 0};
            setArgs(args);
            checkListenerReturn(testConn.prepareStatement(null, 0, 0, 0));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      releaseSavepoint(java.sql.Savepoint)}.
     */
    public final void testReleaseSavepoint() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testConn.releaseSavepoint(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      rollback()}.
     */
    public final void testRollback() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testConn.rollback();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      rollback(java.sql.Savepoint)}.
     */
    public final void testRollbackSavepoint() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testConn.rollback(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      setAutoCommit(boolean)}.
     */
    public final void testSetAutoCommit() { //NOPMD
        try {
            final Object[] args = {false};
            setArgs(args);
            testConn.setAutoCommit(false);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      setCatalog(java.lang.String)}.
     */
    public final void testSetCatalog() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testConn.setCatalog(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      setHoldability(int)}.
     */
    public final void testSetHoldability() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testConn.setHoldability(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      setReadOnly(boolean)}.
     */
    public final void testSetReadOnly() { //NOPMD
        try {
            final Object[] args = {false};
            setArgs(args);
            testConn.setReadOnly(false);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      setSavepoint()}.
     */
    public final void testSetSavepoint() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            checkListenerReturn(testConn.setSavepoint());
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      setSavepoint(java.lang.String)}.
     */
    public final void testSetSavepointString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            checkListenerReturn(testConn.setSavepoint(null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      setTransactionIsolation(int)}.
     */
    public final void testSetTransactionIsolation() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testConn.setTransactionIsolation(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ConnectionListener#
     *      setTypeMap(java.util.Map)}.
     */
    public final void testSetTypeMap() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testConn.setTypeMap(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }
}
