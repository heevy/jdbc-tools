/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.SQLException;

import com.facdatum.mock.jdbc.MockRef;

/**
 * Test of RefListener.
 */
public class RefListenerTest extends AbstractJDBCTestCase {

    /**
     * RefListener under test.
     */
    protected RefListener testRef = null; //NOPMD NOCS

    /**
     * Sets up the test.
     * @throws Exception if problem setting up test
     */
    protected void setUp() throws Exception { //NOPMD NOCS
        testRef = new RefListener(
                new MockRef());
        super.setUp();
    }

    /**
     * Cleans up the test.
     * @throws Exception if problem cleaning up test
     */
    protected final void tearDown() throws Exception { //NOPMD
        super.tearDown();
        testRef = null;
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.RefListener#
     *      getBaseTypeName()}.
     */
    public final void testGetBaseTypeName() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testRef.getBaseTypeName();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.RefListener#
     *      getObject()}.
     */
    public final void testGetObject() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testRef.getObject();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.RefListener#
     *      getObject(java.util.Map)}.
     */
    public final void testGetObjectMapOfStringClassOfQ() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testRef.getObject(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.RefListener#
     *      setObject(java.lang.Object)}.
     */
    public final void testSetObject() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testRef.setObject(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }
}
