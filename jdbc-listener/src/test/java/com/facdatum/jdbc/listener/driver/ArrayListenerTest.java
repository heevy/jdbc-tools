/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.SQLException;

import com.facdatum.mock.jdbc.MockArray;

/**
 * Test of the ArrayListener.
 */
public class ArrayListenerTest extends AbstractJDBCTestCase {

    /**
     * ArrayListener under test.
     */
    protected ArrayListener testArray = null; //NOPMD NOCS

    /**
     * Sets up the test.
     * @throws Exception if problem setting up test
     */
    protected void setUp() throws Exception { //NOPMD NOCS
        super.setUp();
        testArray = new ArrayListener(new MockArray());
    }

    /**
     * Cleans up the test.
     * @throws Exception if problem cleaning up test
     */
    protected final void tearDown() throws Exception { //NOPMD
        super.tearDown();
        testArray = null;
    }

    /**
     * Test method for {@link
     *      com.facdatum.jdbc.listener.driver.ArrayListener#getArray()}.
     */
    public final void testGetArray() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testArray.getArray();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ArrayListener#getArray(
     *      java.util.Map)}.
     */
    public final void testGetArrayMapOfStringClassOfQ() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testArray.getArray(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ArrayListener#getArray(
     *      long, int)}.
     */
    public final void testGetArrayLongInt() { //NOPMD
        try {
            final Object[] args = {Long.valueOf(0), 0};
            setArgs(args);
            testArray.getArray(0, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ArrayListener#getArray(
     *      long, int, java.util.Map)}.
     */
    public final void testGetArrayLongIntMapOfStringClassOfQ() { //NOPMD
        try {
            final Object[] args = {Long.valueOf(0), 0, null};
            setArgs(args);
            testArray.getArray(0, 0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ArrayListener#
     *      getBaseType()}.
     */
    public final void testGetBaseType() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testArray.getBaseType();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ArrayListener#
     *      getBaseTypeName()}.
     */
    public final void testGetBaseTypeName() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testArray.getBaseTypeName();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ArrayListener#
     *      getResultSet()}.
     */
    public final void testGetResultSet() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            checkListenerReturn(testArray.getResultSet());
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ArrayListener#
     *      getResultSet(java.util.Map)}.
     */
    public final void testGetResultSetMapOfStringClassOfQ() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            checkListenerReturn(testArray.getResultSet(null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ArrayListener#
     *      getResultSet(long, int)}.
     */
    public final void testGetResultSetLongInt() { //NOPMD
        try {
            final Object[] args = {Long.valueOf(0), 0};
            setArgs(args);
            checkListenerReturn(testArray.getResultSet(Long.valueOf(0), 0));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ArrayListener#
     *      getResultSet(long, int, java.util.Map)}.
     */
    public final void testGetResultSetLongIntMapOfStringClassOfQ() { //NOPMD
        try {
            final Object[] args = {Long.valueOf(0), 0, null};
            setArgs(args);
            checkListenerReturn(testArray.getResultSet(Long.valueOf(0), 0,
                    null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }
}
