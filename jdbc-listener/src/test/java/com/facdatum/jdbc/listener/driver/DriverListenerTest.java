/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.SQLException;

import com.facdatum.mock.jdbc.MockDriver;

/**
 * Test of DriverListener.
 */
public class DriverListenerTest extends AbstractJDBCTestCase {

    /**
     * DriverListener under test.
     */
    protected DriverListener testDriver = null; //NOPMD NOCS

    /**
     * Sets up the test.
     * @throws Exception if problem setting up test
     */
    protected void setUp() throws Exception { //NOPMD NOCS
        testDriver = new DriverListener(
                new MockDriver());
        super.setUp();
    }

    /**
     * Cleans up the test.
     * @throws Exception if problem cleaning up test
     */
    protected final void tearDown() throws Exception { //NOPMD
        super.tearDown();
        testDriver = null;
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.DriverListener#
     *      acceptsURL(java.lang.String)}.
     */
    public final void testAcceptsURL() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testDriver.acceptsURL(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.DriverListener#
     *      connect(java.lang.String, java.util.Properties)}.
     */
    public final void testConnect() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            checkListenerReturn(testDriver.connect(null, null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.DriverListener#
     *      getMajorVersion()}.
     */
    public final void testGetMajorVersion() { //NOPMD
        final Object[] args = {};
        setArgs(args);
        testDriver.getMajorVersion();
        checkLog();
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.DriverListener#
     *      getMinorVersion()}.
     */
    public final void testGetMinorVersion() { //NOPMD
        final Object[] args = {};
        setArgs(args);
        testDriver.getMinorVersion();
        checkLog();
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.DriverListener#
     *      getPropertyInfo(java.lang.String, java.util.Properties)}.
     */
    public final void testGetPropertyInfo() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            testDriver.getPropertyInfo(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.DriverListener#
     *      jdbcCompliant()}.
     */
    public final void testJdbcCompliant() { //NOPMD
        final Object[] args = {};
        setArgs(args);
        testDriver.jdbcCompliant();
        checkLog();
    }
}
