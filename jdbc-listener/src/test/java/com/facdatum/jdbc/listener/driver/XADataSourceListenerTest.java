/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.SQLException;

import com.facdatum.mock.jdbc.MockXADataSource;

/**
 * Test of XADataSourceListener.
 */
public class XADataSourceListenerTest extends AbstractJDBCTestCase {

    /**
     * DataSourceListener under test.
     */
    protected XADataSourceListener testDataSource = null; //NOPMD NOCS

    /**
     * Sets up the test.
     * @throws Exception if problem setting up test
     */
    protected void setUp() throws Exception { //NOPMD NOCS
        testDataSource = new XADataSourceListener(
                new MockXADataSource());
        super.setUp();
    }

    /**
     * Cleans up the test.
     * @throws Exception if problem cleaning up test
     */
    protected final void tearDown() throws Exception { //NOPMD
        super.tearDown();
        testDataSource = null;
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.XADataSourceListener#
     *      getXAConnection()}.
     */
    public final void testGetXAConnection() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            checkListenerReturn(testDataSource.getXAConnection());
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.XADataSourceListener#
     *      getXAConnection(java.lang.String, java.lang.String)}.
     */
    public final void testGetXAConnectionStringString() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            checkListenerReturn(testDataSource.getXAConnection(null, null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.DataSourceListener#
     *      getLogWriter()}.
     */
    public final void testGetLogWriter() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testDataSource.getLogWriter();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.DataSourceListener#
     *      getLoginTimeout()}.
     */
    public final void testGetLoginTimeout() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testDataSource.getLoginTimeout();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.DataSourceListener#
     *      setLogWriter(java.io.PrintWriter)}.
     */
    public final void testSetLogWriter() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testDataSource.setLogWriter(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.DataSourceListener#
     *      setLoginTimeout(int)}.
     */
    public final void testSetLoginTimeout() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testDataSource.setLoginTimeout(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }
}
