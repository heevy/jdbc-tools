/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.SQLException;
import java.sql.Statement;

import com.facdatum.mock.jdbc.MockStatement;

/**
 * Test of StatementListener.
 */
public class StatementListenerTest extends AbstractJDBCTestCase {

    /**
     * StatementListener under test.
     */
    protected Statement testStatement = null; //NOPMD NOCS

    /**
     * Sets up the test.
     * @throws Exception if problem setting up test
     */
    protected void setUp() throws Exception { //NOPMD NOCS
        testStatement = new StatementListener(
                new MockStatement());
        super.setUp();
    }

    /**
     * Cleans up the test.
     * @throws Exception if problem cleaning up test
     */
    protected void tearDown() throws Exception { //NOPMD NOCS
        super.tearDown();
        testStatement = null;
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      addBatch(java.lang.String)}.
     */
    public final void testAddBatchString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testStatement.addBatch(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      cancel()}.
     */
    public final void testCancel() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testStatement.cancel();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      clearBatch()}.
     */
    public final void testClearBatch() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testStatement.clearBatch();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      clearWarnings()}.
     */
    public final void testClearWarnings() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testStatement.clearWarnings();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      close()}.
     */
    public final void testClose() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testStatement.close();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      execute(java.lang.String)}.
     */
    public final void testExecuteString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testStatement.execute(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      execute(java.lang.String, int)}.
     */
    public final void testExecuteStringInt() { //NOPMD
        try {
            final Object[] args = {null, 0};
            setArgs(args);
            testStatement.execute(null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      execute(java.lang.String, int[])}.
     */
    public final void testExecuteStringIntArray() { //NOPMD
        try {
            final Object[] args = {null, (int[]) null};
            setArgs(args);
            testStatement.execute(null, (int[]) null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      execute(java.lang.String, java.lang.String[])}.
     */
    public final void testExecuteStringStringArray() { //NOPMD
        try {
            final Object[] args = {null, (String[]) null};
            setArgs(args);
            testStatement.execute(null, (String[]) null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      executeBatch()}.
     */
    public final void testExecuteBatch() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testStatement.executeBatch();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      executeQuery(java.lang.String)}.
     */
    public final void testExecuteQueryString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            checkListenerReturn(testStatement.executeQuery(null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      executeUpdate(java.lang.String)}.
     */
    public final void testExecuteUpdateString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testStatement.executeUpdate(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      executeUpdate(java.lang.String, int)}.
     */
    public final void testExecuteUpdateStringInt() { //NOPMD
        try {
            final Object[] args = {null, 0};
            setArgs(args);
            testStatement.executeUpdate(null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      executeUpdate(java.lang.String, int[])}.
     */
    public final void testExecuteUpdateStringIntArray() { //NOPMD
        try {
            final Object[] args = {null, (int[]) null};
            setArgs(args);
            testStatement.executeUpdate(null, (int[]) null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      executeUpdate(java.lang.String, java.lang.String[])}.
     */
    public final void testExecuteUpdateStringStringArray() { //NOPMD
        try {
            final Object[] args = {null, (String[]) null};
            setArgs(args);
            testStatement.executeUpdate(null, (String[]) null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      getConnection()}.
     */
    public final void testGetConnection() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            checkListenerReturn(testStatement.getConnection());
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      getFetchDirection()}.
     */
    public final void testGetFetchDirection() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testStatement.getFetchDirection();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      getFetchSize()}.
     */
    public final void testGetFetchSize() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testStatement.getFetchSize();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      getGeneratedKeys()}.
     */
    public final void testGetGeneratedKeys() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            checkListenerReturn(testStatement.getGeneratedKeys());
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      getMaxFieldSize()}.
     */
    public final void testGetMaxFieldSize() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testStatement.getMaxFieldSize();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      getMaxRows()}.
     */
    public final void testGetMaxRows() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testStatement.getMaxRows();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      getMoreResults()}.
     */
    public final void testGetMoreResults() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testStatement.getMoreResults();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      getMoreResults(int)}.
     */
    public final void testGetMoreResultsInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testStatement.getMoreResults(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      getQueryTimeout()}.
     */
    public final void testGetQueryTimeout() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testStatement.getQueryTimeout();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      getResultSet()}.
     */
    public final void testGetResultSet() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            checkListenerReturn(testStatement.getResultSet());
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      getResultSetConcurrency()}.
     */
    public final void testGetResultSetConcurrency() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testStatement.getResultSetConcurrency();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      getResultSetHoldability()}.
     */
    public final void testGetResultSetHoldability() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testStatement.getResultSetHoldability();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      getResultSetType()}.
     */
    public final void testGetResultSetType() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testStatement.getResultSetType();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      getUpdateCount()}.
     */
    public final void testGetUpdateCount() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testStatement.getUpdateCount();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      getWarnings()}.
     */
    public final void testGetWarnings() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testStatement.getWarnings();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      setCursorName(java.lang.String)}.
     */
    public final void testSetCursorName() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testStatement.setCursorName(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      setEscapeProcessing(boolean)}.
     */
    public final void testSetEscapeProcessing() { //NOPMD
        try {
            final Object[] args = {false};
            setArgs(args);
            testStatement.setEscapeProcessing(false);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      setFetchDirection(int)}.
     */
    public final void testSetFetchDirection() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testStatement.setFetchDirection(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      setFetchSize(int)}.
     */
    public final void testSetFetchSize() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testStatement.setFetchSize(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      setMaxFieldSize(int)}.
     */
    public final void testSetMaxFieldSize() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testStatement.setMaxFieldSize(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      setMaxRows(int)}.
     */
    public final void testSetMaxRows() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testStatement.setMaxRows(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.StatementListener#
     *      setQueryTimeout(int)}.
     */
    public final void testSetQueryTimeout() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testStatement.setQueryTimeout(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }
}
