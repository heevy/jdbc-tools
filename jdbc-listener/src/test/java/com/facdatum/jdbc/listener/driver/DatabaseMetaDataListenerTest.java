//NOCS
/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver; //NOPMD

import java.sql.SQLException;

import com.facdatum.mock.jdbc.MockDatabaseMetaData;

/**
 * Test of DatabaseMetaDataListener.
 */
public class DatabaseMetaDataListenerTest extends AbstractJDBCTestCase { //NOPMD

    /**
     * DatabaseMetaDataListener under test.
     */
    protected DatabaseMetaDataListener testMetaData = null; //NOPMD NOCS

    /**
     * Sets up the test.
     * @throws Exception if problem setting up test
     */
    protected void setUp() throws Exception { //NOPMD NOCS
        super.setUp();
        testMetaData = new DatabaseMetaDataListener(
                new MockDatabaseMetaData());
    }

    /**
     * Cleans up the test.
     * @throws Exception if problem cleaning up test
     */
    protected final void tearDown() throws Exception { //NOPMD
        super.tearDown();
        testMetaData = null;
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * allProceduresAreCallable()}.
     */
    public final void testAllProceduresAreCallable() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.allProceduresAreCallable();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * allTablesAreSelectable()}.
     */
    public final void testAllTablesAreSelectable() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.allTablesAreSelectable();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * dataDefinitionCausesTransactionCommit()}.
     */
    public final void testDataDefinitionCausesTransactionCommit() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.dataDefinitionCausesTransactionCommit();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * dataDefinitionIgnoredInTransactions()}.
     */
    public final void testDataDefinitionIgnoredInTransactions() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.dataDefinitionIgnoredInTransactions();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * deletesAreDetected(int)}.
     */
    public final void testDeletesAreDetected() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.deletesAreDetected(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * doesMaxRowSizeIncludeBlobs()}.
     */
    public final void testDoesMaxRowSizeIncludeBlobs() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.doesMaxRowSizeIncludeBlobs();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getAttributes(java.lang.String, java.lang.String, java.lang.String,
     * java.lang.String)}.
     */
    public final void testGetAttributes() { //NOPMD
        try {
            final Object[] args = {null, null, null, null};
            setArgs(args);
            checkListenerReturn(
                    testMetaData.getAttributes(null, null, null, null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getBestRowIdentifier(java.lang.String, java.lang.String,
     * java.lang.String, int, boolean)}.
     */
    public final void testGetBestRowIdentifier() { //NOPMD
        try {
            final Object[] args = {null, null, null, 0, false};
            setArgs(args);
            checkListenerReturn(
                    testMetaData.getBestRowIdentifier(null, null, null, 0,
                            false));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getCatalogSeparator()}.
     */
    public final void testGetCatalogSeparator() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getCatalogSeparator();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getCatalogTerm()}.
     */
    public final void testGetCatalogTerm() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getCatalogTerm();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getCatalogs()}.
     */
    public final void testGetCatalogs() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            checkListenerReturn(testMetaData.getCatalogs());
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getColumnPrivileges(java.lang.String, java.lang.String, java.lang.String,
     * java.lang.String)}.
     */
    public final void testGetColumnPrivileges() { //NOPMD
        try {
            final Object[] args = {null, null, null, null};
            setArgs(args);
            checkListenerReturn(
                    testMetaData.getColumnPrivileges(null, null, null,
                            null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getColumns(java.lang.String, java.lang.String, java.lang.String,
     * java.lang.String)}.
     */
    public final void testGetColumns() { //NOPMD
        try {
            final Object[] args = {null, null, null, null};
            setArgs(args);
            checkListenerReturn(
                    testMetaData.getColumns(null, null, null, null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getConnection()}.
     */
    public final void testGetConnection() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            checkListenerReturn(testMetaData.getConnection());
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getCrossReference(java.lang.String, java.lang.String, java.lang.String,
     * java.lang.String, java.lang.String, java.lang.String)}.
     */
    public final void testGetCrossReference() { //NOPMD
        try {
            final Object[] args = {null, null, null, null, null, null};
            setArgs(args);
            checkListenerReturn(
                    testMetaData.getCrossReference(null, null, null, null,
                            null, null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getDatabaseMajorVersion()}.
     */
    public final void testGetDatabaseMajorVersion() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getDatabaseMajorVersion();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getDatabaseMinorVersion()}.
     */
    public final void testGetDatabaseMinorVersion() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getDatabaseMinorVersion();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getDatabaseProductName()}.
     */
    public final void testGetDatabaseProductName() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getDatabaseProductName();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getDatabaseProductVersion()}.
     */
    public final void testGetDatabaseProductVersion() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getDatabaseProductVersion();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getDefaultTransactionIsolation()}.
     */
    public final void testGetDefaultTransactionIsolation() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getDefaultTransactionIsolation();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getDriverMajorVersion()}.
     */
    public final void testGetDriverMajorVersion() { //NOPMD
        final Object[] args = {};
        setArgs(args);
        testMetaData.getDriverMajorVersion();
        checkLog();
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getDriverMinorVersion()}.
     */
    public final void testGetDriverMinorVersion() { //NOPMD
        final Object[] args = {};
        setArgs(args);
        testMetaData.getDriverMinorVersion();
        checkLog();
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getDriverName()}.
     */
    public final void testGetDriverName() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getDriverName();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getDriverVersion()}.
     */
    public final void testGetDriverVersion() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getDriverVersion();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getExportedKeys(java.lang.String, java.lang.String, java.lang.String)}.
     */
    public final void testGetExportedKeys() { //NOPMD
        try {
            final Object[] args = {null, null, null};
            setArgs(args);
            checkListenerReturn(
                    testMetaData.getExportedKeys(null, null, null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getExtraNameCharacters()}.
     */
    public final void testGetExtraNameCharacters() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getExtraNameCharacters();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getIdentifierQuoteString()}.
     */
    public final void testGetIdentifierQuoteString() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getIdentifierQuoteString();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getImportedKeys(java.lang.String, java.lang.String, java.lang.String)}.
     */
    public final void testGetImportedKeys() { //NOPMD
        try {
            final Object[] args = {null, null, null};
            setArgs(args);
            checkListenerReturn(
                    testMetaData.getImportedKeys(null, null, null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getIndexInfo(java.lang.String, java.lang.String, java.lang.String,
     * boolean, boolean)}.
     */
    public final void testGetIndexInfo() { //NOPMD
        try {
            final Object[] args = {null, null, null, false, false};
            setArgs(args);
            checkListenerReturn(
                    testMetaData.getIndexInfo(null, null, null, false,
                            false));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getJDBCMajorVersion()}.
     */
    public final void testGetJDBCMajorVersion() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getJDBCMajorVersion();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getJDBCMinorVersion()}.
     */
    public final void testGetJDBCMinorVersion() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getJDBCMinorVersion();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getMaxBinaryLiteralLength()}.
     */
    public final void testGetMaxBinaryLiteralLength() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getMaxBinaryLiteralLength();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getMaxCatalogNameLength()}.
     */
    public final void testGetMaxCatalogNameLength() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getMaxCatalogNameLength();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getMaxCharLiteralLength()}.
     */
    public final void testGetMaxCharLiteralLength() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getMaxCharLiteralLength();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getMaxColumnNameLength()}.
     */
    public final void testGetMaxColumnNameLength() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getMaxColumnNameLength();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getMaxColumnsInGroupBy()}.
     */
    public final void testGetMaxColumnsInGroupBy() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getMaxColumnsInGroupBy();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getMaxColumnsInIndex()}.
     */
    public final void testGetMaxColumnsInIndex() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getMaxColumnsInIndex();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getMaxColumnsInOrderBy()}.
     */
    public final void testGetMaxColumnsInOrderBy() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getMaxColumnsInOrderBy();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getMaxColumnsInSelect()}.
     */
    public final void testGetMaxColumnsInSelect() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getMaxColumnsInSelect();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getMaxColumnsInTable()}.
     */
    public final void testGetMaxColumnsInTable() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getMaxColumnsInTable();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getMaxConnections()}.
     */
    public final void testGetMaxConnections() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getMaxConnections();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getMaxCursorNameLength()}.
     */
    public final void testGetMaxCursorNameLength() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getMaxCursorNameLength();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getMaxIndexLength()}.
     */
    public final void testGetMaxIndexLength() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getMaxIndexLength();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getMaxProcedureNameLength()}.
     */
    public final void testGetMaxProcedureNameLength() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getMaxProcedureNameLength();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getMaxRowSize()}.
     */
    public final void testGetMaxRowSize() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getMaxRowSize();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getMaxSchemaNameLength()}.
     */
    public final void testGetMaxSchemaNameLength() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getMaxSchemaNameLength();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getMaxStatementLength()}.
     */
    public final void testGetMaxStatementLength() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getMaxStatementLength();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getMaxStatements()}.
     */
    public final void testGetMaxStatements() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getMaxStatements();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getMaxTableNameLength()}.
     */
    public final void testGetMaxTableNameLength() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getMaxTableNameLength();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getMaxTablesInSelect()}.
     */
    public final void testGetMaxTablesInSelect() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getMaxTablesInSelect();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getMaxUserNameLength()}.
     */
    public final void testGetMaxUserNameLength() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getMaxUserNameLength();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getNumericFunctions()}.
     */
    public final void testGetNumericFunctions() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getNumericFunctions();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getPrimaryKeys(java.lang.String, java.lang.String, java.lang.String)}.
     */
    public final void testGetPrimaryKeys() { //NOPMD
        try {
            final Object[] args = {null, null, null};
            setArgs(args);
            checkListenerReturn(
                    testMetaData.getPrimaryKeys(null, null, null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getProcedureColumns(java.lang.String, java.lang.String, java.lang.String,
     * java.lang.String)}.
     */
    public final void testGetProcedureColumns() { //NOPMD
        try {
            final Object[] args = {null, null, null, null};
            setArgs(args);
            checkListenerReturn(
                    testMetaData.getProcedureColumns(null, null, null,
                            null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getProcedureTerm()}.
     */
    public final void testGetProcedureTerm() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getProcedureTerm();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getProcedures(java.lang.String, java.lang.String, java.lang.String)}.
     */
    public final void testGetProcedures() { //NOPMD
        try {
            final Object[] args = {null, null, null};
            setArgs(args);
            checkListenerReturn(testMetaData.getProcedures(
                    null, null, null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getResultSetHoldability()}.
     */
    public final void testGetResultSetHoldability() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getResultSetHoldability();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getSQLKeywords()}.
     */
    public final void testGetSQLKeywords() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getSQLKeywords();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getSQLStateType()}.
     */
    public final void testGetSQLStateType() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getSQLStateType();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getSchemaTerm()}.
     */
    public final void testGetSchemaTerm() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getSchemaTerm();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getSchemas()}.
     */
    public final void testGetSchemas() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            checkListenerReturn(testMetaData.getSchemas());
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getSearchStringEscape()}.
     */
    public final void testGetSearchStringEscape() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getSearchStringEscape();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getStringFunctions()}.
     */
    public final void testGetStringFunctions() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getStringFunctions();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getSuperTables(java.lang.String, java.lang.String, java.lang.String)}.
     */
    public final void testGetSuperTables() { //NOPMD
        try {
            final Object[] args = {null, null, null};
            setArgs(args);
            checkListenerReturn(
                    testMetaData.getSuperTables(null, null, null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getSuperTypes(java.lang.String, java.lang.String, java.lang.String)}.
     */
    public final void testGetSuperTypes() { //NOPMD
        try {
            final Object[] args = {null, null, null};
            setArgs(args);
            checkListenerReturn(testMetaData.getSuperTypes(
                    null, null, null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getSystemFunctions()}.
     */
    public final void testGetSystemFunctions() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getSystemFunctions();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getTablePrivileges(java.lang.String, java.lang.String,
     * java.lang.String)}.
     */
    public final void testGetTablePrivileges() { //NOPMD
        try {
            final Object[] args = {null, null, null};
            setArgs(args);
            checkListenerReturn(
                    testMetaData.getTablePrivileges(null, null, null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getTableTypes()}.
     */
    public final void testGetTableTypes() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            checkListenerReturn(testMetaData.getTableTypes());
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getTables(java.lang.String, java.lang.String, java.lang.String,
     * java.lang.String[])}.
     */
    public final void testGetTables() { //NOPMD
        try {
            final Object[] args = {null, null, null, null};
            setArgs(args);
            checkListenerReturn(
                    testMetaData.getTables(null, null, null, null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getTimeDateFunctions()}.
     */
    public final void testGetTimeDateFunctions() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getTimeDateFunctions();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getTypeInfo()}.
     */
    public final void testGetTypeInfo() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            checkListenerReturn(testMetaData.getTypeInfo());
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getUDTs(java.lang.String, java.lang.String, java.lang.String, int[])}.
     */
    public final void testGetUDTs() { //NOPMD
        try {
            final Object[] args = {null, null, null, null};
            setArgs(args);
            checkListenerReturn(testMetaData.getUDTs(
                    null, null, null, null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     *  getURL()}.
     */
    public final void testGetURL() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getURL();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getUserName()}.
     */
    public final void testGetUserName() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getUserName();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * getVersionColumns(java.lang.String, java.lang.String, java.lang.String)}.
     */
    public final void testGetVersionColumns() { //NOPMD
        try {
            final Object[] args = {null, null, null};
            setArgs(args);
            checkListenerReturn(
                    testMetaData.getVersionColumns(null, null, null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * insertsAreDetected(int)}.
     */
    public final void testInsertsAreDetected() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.insertsAreDetected(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * isCatalogAtStart()}.
     */
    public final void testIsCatalogAtStart() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.isCatalogAtStart();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * isReadOnly()}.
     */
    public final void testIsReadOnly() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.isReadOnly();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * locatorsUpdateCopy()}.
     */
    public final void testLocatorsUpdateCopy() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.locatorsUpdateCopy();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * nullPlusNonNullIsNull()}.
     */
    public final void testNullPlusNonNullIsNull() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.nullPlusNonNullIsNull();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * nullsAreSortedAtEnd()}.
     */
    public final void testNullsAreSortedAtEnd() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.nullsAreSortedAtEnd();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * nullsAreSortedAtStart()}.
     */
    public final void testNullsAreSortedAtStart() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.nullsAreSortedAtStart();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * nullsAreSortedHigh()}.
     */
    public final void testNullsAreSortedHigh() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.nullsAreSortedHigh();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * nullsAreSortedLow()}.
     */
    public final void testNullsAreSortedLow() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.nullsAreSortedLow();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * othersDeletesAreVisible(int)}.
     */
    public final void testOthersDeletesAreVisible() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.othersDeletesAreVisible(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * othersInsertsAreVisible(int)}.
     */
    public final void testOthersInsertsAreVisible() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.othersInsertsAreVisible(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * othersUpdatesAreVisible(int)}.
     */
    public final void testOthersUpdatesAreVisible() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.othersUpdatesAreVisible(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * ownDeletesAreVisible(int)}.
     */
    public final void testOwnDeletesAreVisible() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.ownDeletesAreVisible(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * ownInsertsAreVisible(int)}.
     */
    public final void testOwnInsertsAreVisible() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.ownInsertsAreVisible(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * ownUpdatesAreVisible(int)}.
     */
    public final void testOwnUpdatesAreVisible() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.ownUpdatesAreVisible(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * storesLowerCaseIdentifiers()}.
     */
    public final void testStoresLowerCaseIdentifiers() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.storesLowerCaseIdentifiers();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * storesLowerCaseQuotedIdentifiers()}.
     */
    public final void testStoresLowerCaseQuotedIdentifiers() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.storesLowerCaseQuotedIdentifiers();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * storesMixedCaseIdentifiers()}.
     */
    public final void testStoresMixedCaseIdentifiers() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.storesMixedCaseIdentifiers();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * storesMixedCaseQuotedIdentifiers()}.
     */
    public final void testStoresMixedCaseQuotedIdentifiers() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.storesMixedCaseQuotedIdentifiers();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * storesUpperCaseIdentifiers()}.
     */
    public final void testStoresUpperCaseIdentifiers() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.storesUpperCaseIdentifiers();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * storesUpperCaseQuotedIdentifiers()}.
     */
    public final void testStoresUpperCaseQuotedIdentifiers() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.storesUpperCaseQuotedIdentifiers();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsANSI92EntryLevelSQL()}.
     */
    public final void testSupportsANSI92EntryLevelSQL() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsANSI92EntryLevelSQL();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsANSI92FullSQL()}.
     */
    public final void testSupportsANSI92FullSQL() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsANSI92FullSQL();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsANSI92IntermediateSQL()}.
     */
    public final void testSupportsANSI92IntermediateSQL() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsANSI92IntermediateSQL();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsAlterTableWithAddColumn()}.
     */
    public final void testSupportsAlterTableWithAddColumn() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsAlterTableWithAddColumn();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsAlterTableWithDropColumn()}.
     */
    public final void testSupportsAlterTableWithDropColumn() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsAlterTableWithDropColumn();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsBatchUpdates()}.
     */
    public final void testSupportsBatchUpdates() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsBatchUpdates();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsCatalogsInDataManipulation()}.
     */
    public final void testSupportsCatalogsInDataManipulation() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsCatalogsInDataManipulation();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsCatalogsInIndexDefinitions()}.
     */
    public final void testSupportsCatalogsInIndexDefinitions() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsCatalogsInIndexDefinitions();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsCatalogsInPrivilegeDefinitions()}.
     */
    public final void testSupportsCatalogsInPrivilegeDefinitions() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsCatalogsInPrivilegeDefinitions();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsCatalogsInProcedureCalls()}.
     */
    public final void testSupportsCatalogsInProcedureCalls() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsCatalogsInProcedureCalls();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsCatalogsInTableDefinitions()}.
     */
    public final void testSupportsCatalogsInTableDefinitions() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsCatalogsInTableDefinitions();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsColumnAliasing()}.
     */
    public final void testSupportsColumnAliasing() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsColumnAliasing();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsConvert()}.
     */
    public final void testSupportsConvert() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsConvert();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsConvert(int, int)}.
     */
    public final void testSupportsConvertIntInt() { //NOPMD
        try {
            final Object[] args = {0, 0};
            setArgs(args);
            testMetaData.supportsConvert(0, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsCoreSQLGrammar()}.
     */
    public final void testSupportsCoreSQLGrammar() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsCoreSQLGrammar();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsCorrelatedSubqueries()}.
     */
    public final void testSupportsCorrelatedSubqueries() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsCorrelatedSubqueries();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsDataDefinitionAndDataManipulationTransactions()}.
     */
    public final void //NOPMD
            testSupportsDataDefinitionAndDataManipulationTransactions() {
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.
                    supportsDataDefinitionAndDataManipulationTransactions();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsDataManipulationTransactionsOnly()}.
     */
    public final void testSupportsDataManipulationTransactionsOnly() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsDataManipulationTransactionsOnly();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsDifferentTableCorrelationNames()}.
     */
    public final void testSupportsDifferentTableCorrelationNames() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsDifferentTableCorrelationNames();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsExpressionsInOrderBy()}.
     */
    public final void testSupportsExpressionsInOrderBy() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsExpressionsInOrderBy();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsExtendedSQLGrammar()}.
     */
    public final void testSupportsExtendedSQLGrammar() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsExtendedSQLGrammar();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsFullOuterJoins()}.
     */
    public final void testSupportsFullOuterJoins() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsFullOuterJoins();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsGetGeneratedKeys()}.
     */
    public final void testSupportsGetGeneratedKeys() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsGetGeneratedKeys();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsGroupBy()}.
     */
    public final void testSupportsGroupBy() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsGroupBy();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsGroupByBeyondSelect()}.
     */
    public final void testSupportsGroupByBeyondSelect() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsGroupByBeyondSelect();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsGroupByUnrelated()}.
     */
    public final void testSupportsGroupByUnrelated() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsGroupByUnrelated();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsIntegrityEnhancementFacility()}.
     */
    public final void testSupportsIntegrityEnhancementFacility() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsIntegrityEnhancementFacility();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsLikeEscapeClause()}.
     */
    public final void testSupportsLikeEscapeClause() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsLikeEscapeClause();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsLimitedOuterJoins()}.
     */
    public final void testSupportsLimitedOuterJoins() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsLimitedOuterJoins();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsMinimumSQLGrammar()}.
     */
    public final void testSupportsMinimumSQLGrammar() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsMinimumSQLGrammar();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsMixedCaseIdentifiers()}.
     */
    public final void testSupportsMixedCaseIdentifiers() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsMixedCaseIdentifiers();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsMixedCaseQuotedIdentifiers()}.
     */
    public final void testSupportsMixedCaseQuotedIdentifiers() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsMixedCaseQuotedIdentifiers();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsMultipleOpenResults()}.
     */
    public final void testSupportsMultipleOpenResults() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsMultipleOpenResults();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsMultipleResultSets()}.
     */
    public final void testSupportsMultipleResultSets() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsMultipleResultSets();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsMultipleTransactions()}.
     */
    public final void testSupportsMultipleTransactions() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsMultipleTransactions();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsNamedParameters()}.
     */
    public final void testSupportsNamedParameters() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsNamedParameters();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsNonNullableColumns()}.
     */
    public final void testSupportsNonNullableColumns() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsNonNullableColumns();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsOpenCursorsAcrossCommit()}.
     */
    public final void testSupportsOpenCursorsAcrossCommit() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsOpenCursorsAcrossCommit();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsOpenCursorsAcrossRollback()}.
     */
    public final void testSupportsOpenCursorsAcrossRollback() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsOpenCursorsAcrossRollback();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsOpenStatementsAcrossCommit()}.
     */
    public final void testSupportsOpenStatementsAcrossCommit() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsOpenStatementsAcrossCommit();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsOpenStatementsAcrossRollback()}.
     */
    public final void testSupportsOpenStatementsAcrossRollback() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsOpenStatementsAcrossRollback();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsOrderByUnrelated()}.
     */
    public final void testSupportsOrderByUnrelated() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsOrderByUnrelated();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsOuterJoins()}.
     */
    public final void testSupportsOuterJoins() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsOuterJoins();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsPositionedDelete()}.
     */
    public final void testSupportsPositionedDelete() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsPositionedDelete();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsPositionedUpdate()}.
     */
    public final void testSupportsPositionedUpdate() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsPositionedUpdate();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsResultSetConcurrency(int, int)}.
     */
    public final void testSupportsResultSetConcurrency() { //NOPMD
        try {
            final Object[] args = {0, 0};
            setArgs(args);
            testMetaData.supportsResultSetConcurrency(0, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsResultSetHoldability(int)}.
     */
    public final void testSupportsResultSetHoldability() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.supportsResultSetHoldability(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsResultSetType(int)}.
     */
    public final void testSupportsResultSetType() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.supportsResultSetType(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsSavepoints()}.
     */
    public final void testSupportsSavepoints() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsSavepoints();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsSchemasInDataManipulation()}.
     */
    public final void testSupportsSchemasInDataManipulation() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsSchemasInDataManipulation();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsSchemasInIndexDefinitions()}.
     */
    public final void testSupportsSchemasInIndexDefinitions() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsSchemasInIndexDefinitions();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsSchemasInPrivilegeDefinitions()}.
     */
    public final void testSupportsSchemasInPrivilegeDefinitions() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsSchemasInPrivilegeDefinitions();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsSchemasInProcedureCalls()}.
     */
    public final void testSupportsSchemasInProcedureCalls() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsSchemasInProcedureCalls();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsSchemasInTableDefinitions()}.
     */
    public final void testSupportsSchemasInTableDefinitions() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsSchemasInTableDefinitions();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsSelectForUpdate()}.
     */
    public final void testSupportsSelectForUpdate() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsSelectForUpdate();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsStatementPooling()}.
     */
    public final void testSupportsStatementPooling() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsStatementPooling();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsStoredProcedures()}.
     */
    public final void testSupportsStoredProcedures() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsStoredProcedures();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsSubqueriesInComparisons()}.
     */
    public final void testSupportsSubqueriesInComparisons() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsSubqueriesInComparisons();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsSubqueriesInExists()}.
     */
    public final void testSupportsSubqueriesInExists() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsSubqueriesInExists();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsSubqueriesInIns()}.
     */
    public final void testSupportsSubqueriesInIns() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsSubqueriesInIns();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsSubqueriesInQuantifieds()}.
     */
    public final void testSupportsSubqueriesInQuantifieds() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsSubqueriesInQuantifieds();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsTableCorrelationNames()}.
     */
    public final void testSupportsTableCorrelationNames() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsTableCorrelationNames();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsTransactionIsolationLevel(int)}.
     */
    public final void testSupportsTransactionIsolationLevel() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.supportsTransactionIsolationLevel(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsTransactions()}.
     */
    public final void testSupportsTransactions() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsTransactions();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsUnion()}.
     */
    public final void testSupportsUnion() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsUnion();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * supportsUnionAll()}.
     */
    public final void testSupportsUnionAll() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.supportsUnionAll();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * updatesAreDetected(int)}.
     */
    public final void testUpdatesAreDetected() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.updatesAreDetected(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * usesLocalFilePerTable()}.
     */
    public final void testUsesLocalFilePerTable() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.usesLocalFilePerTable();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.DatabaseMetaDataListener#
     * usesLocalFiles()}.
     */
    public final void testUsesLocalFiles() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.usesLocalFiles();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }
}
