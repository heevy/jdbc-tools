/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;

import junit.framework.TestCase;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCListener;
import com.facdatum.jdbc.listener.JDBCListenerProperties;
import com.facdatum.mock.listener.MockListener;

/**
 * Runs some simple steps against JDBC listener driver to generate some
 * log output. Assumes running against the default Oracle test HR database.
 */
public class SimpleJDBCRunTest extends TestCase {

    /**
     * Holds oracle connection url under test.
     */
    protected static final String ORA_CONNECT_URL =
        "jdbc:oracle:thin:@localhost:1521:xe";

    /**
     * Holds the connection used by the test.
     */
    protected Connection conn = null; //NOPMD NOCS

    /**
     * Holds the number of jdbc calls that should have been logged.
     */
    protected int steps; //NOPMD NOCS

    /**
     * Prepares the test by setting up the logger properties and creating
     * the connection.
     * @throws Exception if there is a problem creating the connection
     */
    protected void setUp() throws Exception { //NOCS
        System.setProperty(JDBCListenerProperties.LISTENERS_KEY,
                MockListener.class.getName());
        System.setProperty(JDBCListenerProperties.DRIVER_CLASS_KEY,
                "oracle.jdbc.OracleDriver");
        JDBCCallListener.init();
        Class.forName(DriverListener.class.getName());
        DriverListener.registerDriver();
        final Properties props = new Properties();
        props.setProperty("user", "hr");
        props.setProperty("password", "hr");
        conn = DriverManager.getConnection(ORA_CONNECT_URL, props);
        steps = 1;
    }


    /**
     * Cleans up after test by removing generated log file and closing
     * connection.
     * @throws Exception if there is a problem closing connection
     */
    protected void tearDown() throws Exception { //NOCS
        conn.close();
        JDBCListenerProperties.clearProperties();
        JDBCCallListener.resetInit();
        DriverListener.clearDriver();
        DataSourceListener.clearDataSource();
        final Enumeration < Driver > drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            DriverManager.deregisterDriver(drivers.nextElement());
        }
    }

    /**
     * Executes the test steps.
     */
    public final void testRun() {
        executeSteps();
        final int lines = readLog();
        assertEquals("Checking steps executed were logged", (steps * 2) + 1,
                lines); // *2 for begin/end +1 for init
    }

    /**
     * Executes the jdbc steps that are to be performed.
     */
    private void executeSteps() {
        PreparedStatement sql = null;
        ResultSet results = null;
        try {
            sql = conn.prepareStatement("select * from hr.employees");
            steps++;
            if (sql.execute()) {
                steps++;
                results = sql.getResultSet();
                steps++;
                while (results.next()) {
                    // just skipping through results
                    steps++;
                }
                steps++;
            }
        } catch (SQLException e) {
            fail("problem running test: " + e);
        } finally {
            if (results != null) {
                try {
                    results.close();
                    steps++;
                } catch (SQLException e2) { //NOPMD NOCS
                    //do nothing
                }
            }
            if (sql != null) {
                try {
                    sql.close();
                    steps++;
                } catch (SQLException e2) { //NOPMD NOCS
                    // do nothing
                }
            }
        }
    }

    /**
     * Reads the number of calls made to the MockListener.
     * @return the number of calls
     */
    private int readLog() {
        final JDBCListener[] listeners = JDBCCallListener.getListeners();
        final MockListener mockListener = (MockListener) listeners[0];
        return mockListener.getCallCount();
    }
}
