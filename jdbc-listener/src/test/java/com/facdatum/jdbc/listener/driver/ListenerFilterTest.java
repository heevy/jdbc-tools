/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.SQLException;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCListener;
import com.facdatum.jdbc.listener.JDBCListenerProperties;
import com.facdatum.mock.jdbc.MockArray;
import com.facdatum.mock.listener.MockFilterListener;
import com.facdatum.mock.listener.MockListener;

import junit.framework.TestCase;

/**
 * Test of listener acting as a filter.
 */
public class ListenerFilterTest extends TestCase {

    /**
     * Test driver that will get registered (although is not used).
     */
    private static final String DRIVER_CLASS = "oracle.jdbc.OracleDriver";

    /**
     * Number of begin calls expected when filtered.
     */
    private static final int BEGIN_LOG_CALLS = 5;

    /**
     * Number of exit calls expected when filtered.
     */
    private static final int END_LOG_CALLS = 6;

    /**
     * Number of fail calls expected when filtered.
     */
    private static final int FAIL_LOG_CALLS = 6;

    /**
     * ArrayListener under test.
     */
    protected ArrayListener testArray = null; //NOPMD NOCS

    /**
     * Sets up the test.
     * @throws Exception if problem setting up test
     */
    protected final void setUp() throws Exception {
        super.setUp();
        System.setProperty(JDBCListenerProperties.LISTENERS_KEY,
                MockFilterListener.class.getName() + ","
                + MockListener.class.getName());
        System.setProperty(JDBCListenerProperties.DRIVER_CLASS_KEY,
                DRIVER_CLASS);
        JDBCCallListener.init();
        testArray = new ArrayListener(new MockArray());
    }

    /**
     * Cleans up the test.
     * @throws Exception if problem cleaning up test
     */
    protected final void tearDown() throws Exception {
        super.tearDown();
        JDBCListenerProperties.clearProperties();
        JDBCCallListener.resetInit();
        testArray = null;
    }

    /**
     * Test filtering of begin calls.
     */
    public final void testFilterBegin() {
        try {
            ((MockArray) testArray.getDelegateArray()).setSkipCheckCall(true);
            testArray.getArray();
            MockFilterListener.setFilterStep(MockFilterListener.FILTER_BEGIN);
            testArray.getArray();
            MockFilterListener.setFilterStep(MockFilterListener.FILTER_CLEAR);
            testArray.getArray();
            assertEquals("checking calls logged/filtered", BEGIN_LOG_CALLS,
                    countCalls());
        } catch (Exception e) {
            fail("Error running testFilterBegin: " + e);
        }
    }

    /**
     * Test filtering of end calls.
     */
    public final void testFilterEnd() {
        try {
            ((MockArray) testArray.getDelegateArray()).setSkipCheckCall(true);
            testArray.getArray();
            MockFilterListener.setFilterStep(MockFilterListener.FILTER_END);
            testArray.getArray();
            MockFilterListener.setFilterStep(MockFilterListener.FILTER_CLEAR);
            testArray.getArray();
            assertEquals("checking calls logged/filtered", END_LOG_CALLS,
                    countCalls());
        } catch (Exception e) {
            fail("Error running testFilterBegin: " + e);
        }
    }

    /**
     * Test filtering of fail calls.
     */
    public final void testFilterFail() {
        try {
            ((MockArray) testArray.getDelegateArray()).setSkipCheckCall(true);
            ((MockArray) testArray.getDelegateArray()).setToFail(true);
            try {
                testArray.getArray();
            } catch (SQLException e) { //NOPMD NOCS
                //ignore
            }
            MockFilterListener.setFilterStep(MockFilterListener.FILTER_FAIL);
            try {
                testArray.getArray();
            } catch (SQLException e) { //NOPMD NOCS
                //ignore
            }
            MockFilterListener.setFilterStep(MockFilterListener.FILTER_CLEAR);
            try {
                testArray.getArray();
            } catch (SQLException e) { //NOPMD NOCS
                //ignore
            }
            assertEquals("checking calls logged/filtered", FAIL_LOG_CALLS,
                    countCalls());
        } catch (Exception e) {
            fail("Error running testFilterBegin: " + e);
        }
    }

    /**
     * Gets the number of times the listener has been called.
     * @return the number of listener calls
     */
    private int countCalls() {
        final JDBCListener[] listeners = JDBCCallListener.getListeners();
        final MockListener mockListener = (MockListener) listeners[1];
        return mockListener.getCallCount();
    }

}
