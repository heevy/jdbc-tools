/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.io.File;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCListenerProperties;
import com.facdatum.mock.listener.MockListener;

import junit.framework.TestCase;

/**
 * Test of DriverListener when using the listener connection URL string.
 */
public class DriverListenerURLTest extends TestCase {

    /**
     * Holds oracle connection url under test.
     */
    static final String ORA_CONNECT_URL =
        "jdbc:listener:oracle:thin:@localhost:1521:xe";

    /**
     * Test log file path.
     */
    private static final String TEST_LOG_PATH = "/tmp";

    /**
     * Test log file name.
     */
    private static final String TEST_LOG_FILE = "test.log";

    /**
     * Setup creates instance of driver under test.
     * @throws Exception when driver setup problem
     */
    protected final void setUp() throws Exception {
        super.setUp();
        System.setProperty(JDBCListenerProperties.LISTENERS_KEY,
                MockListener.class.getName());
        System.setProperty(JDBCListenerProperties.DRIVER_CLASS_KEY,
                "oracle.jdbc.OracleDriver");
        JDBCCallListener.init();
    }

    /**
     * Tidy up after test by deleted generated log file.
     * @throws Exception if there is a problem tidying up test
     */
    protected final void tearDown() throws Exception {
        super.tearDown();
        (new File(TEST_LOG_PATH, TEST_LOG_FILE)).delete();
        JDBCListenerProperties.clearProperties();
        JDBCCallListener.resetInit();
        DriverListener.clearDriver();
        DataSourceListener.clearDataSource();
        final Enumeration < Driver > drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            DriverManager.deregisterDriver(drivers.nextElement());
        }
    }

    /**
     * Test of using normal forName, DriverManager.getConnection steps to
     * get connection via Driver.connect.
     */
    public final void testConnect() {
        Connection conn = null;
        try {
            Class.forName(DriverListener.class.getName());
            DriverListener.registerDriver();
            conn = DriverManager.getConnection(ORA_CONNECT_URL, "hr",
                    "hr");
            assertNotNull("Testing connection", conn);
        } catch (Exception e) {
            fail("Problem getting connection (" + e + ")");
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    fail("Problem closing connection");
                }
            }
        }
    }

    /**
     * Test of using normal forName, DriverManager.getDriver, which in turn
     * will be testing Driver.accept.
     */
    public final void testAccept() {
        Driver driver = null;
        try {
            Class.forName(DriverListener.class.getName());
            DriverListener.registerDriver();
            driver = DriverManager.getDriver(ORA_CONNECT_URL);
            assertNotNull("Testing driver", driver);
        } catch (Exception e) {
            fail("Problem getting driver (" + e + ")");
        }
    }
}
