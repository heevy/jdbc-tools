/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCListenerProperties;
import com.facdatum.mock.listener.MockListener;
import com.facdatum.mock.naming.MockInitialContextFactory;

import junit.framework.TestCase;

import oracle.jdbc.pool.OracleDataSource;

/**
 * Test class for testing the creation of data sources.
 */
public class DataSourceListenerFullTest extends TestCase {

    /**
     * The delgate datasource name bound to naming service.
     */
    public static final String TEST_DATASOURCE = "jdbc/logging/testDB";

    /**
     * The logging datasource name bound to naming service.
     */
    public static final String DEL_DATASOURCE = "jdbc/testDB";

    /**
     * Holds oracle driver type under test.
     */
    public static final String DRIVER_TYPE = "thin";

    /**
     * Holds database server under test.
     */
    public static final String DB_SERVER = "localhost";

    /**
     * Holds database port under test.
     */
    public static final int DB_PORT = 1521;

    /**
     * Holds database name under test.
     */
    public static final String DB_NAME = "XE";

    /**
     * The jdbc connection user id under test.
     */
    public static final String DRIVER_USER = "hr";

    /**
     * The jdbc connection password under test.
     */
    public static final String DRIVER_PASSWORD = "hr";

    /**
     * Shared context for setup.
     */
    private Context context = null; //NOPMD

    /**
     * Setup test by creating delegate and listener data sources.
     * @throws Exception if problem setting up test
     */
    protected final void setUp() throws Exception {
        super.setUp();
        final OracleDataSource datasource = new OracleDataSource();
        datasource.setDriverType(DRIVER_TYPE);
        datasource.setServerName(DB_SERVER);
        datasource.setPortNumber(DB_PORT);
        datasource.setDatabaseName(DB_NAME);
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                MockInitialContextFactory.class.getName());
        context = new InitialContext();
        context.bind(DEL_DATASOURCE, datasource);
        System.setProperty(JDBCListenerProperties.DS_NAME_KEY,
                DEL_DATASOURCE);
        System.setProperty(JDBCListenerProperties.LISTENERS_KEY,
                MockListener.class.getName());
        JDBCCallListener.init();
        final DataSource logDatasource = new DataSourceListener();
        context.bind(TEST_DATASOURCE, logDatasource);
    }

    /**
     * Clean up test.
     * @throws Exception if there is a problem cleaning up after test
     */
    protected final void tearDown() throws Exception {
        super.tearDown();
        context.unbind(TEST_DATASOURCE);
        context.unbind(DEL_DATASOURCE);
        JDBCListenerProperties.clearProperties();
        JDBCCallListener.resetInit();
        DriverListener.clearDriver();
        DataSourceListener.clearDataSource();
        final Enumeration < Driver > drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            DriverManager.deregisterDriver(drivers.nextElement());
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.DataSourceListener#
     *      getConnection()}.
     */
    public final void testGetConnection() {
        Connection conn = null;
        try {
            final Context testContext = new InitialContext();
            final DataSource datasource = (DataSource) testContext.lookup(
                    TEST_DATASOURCE);
            conn = datasource.getConnection(DRIVER_USER, DRIVER_PASSWORD);
            assertNotNull("test connection created", conn);
        } catch (NamingException e) {
            fail("Problem performing jndi lookup (" + e + ")");
        } catch (SQLException e) {
            fail("Problem creating connection (" + e + ")");
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e2) {
                    fail("Problem closing connection.");
                }
            }
        }
    }
}
