/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCListenerProperties;
import com.facdatum.mock.listener.MockListener;

import junit.framework.TestCase;

/**
 * Test class for DriverListener.
 */
public class DriverListenerFullTest extends TestCase {

    /**
     * Holds oracle major version under test.
     */
    static final int ORA_MAJOR_VERSION = 10;

   /**
    * Holds oracle minor version under test.
    */
    static final int ORA_MINOR_VERSION = 2;

    /**
     * Holds oracle connection url under test.
     */
    static final String ORA_CONNECT_URL =
        "jdbc:oracle:thin:@localhost:1521:xe";

    /**
     * Holds driver instances used for tests.
     */
    private Driver driver = null; //NOPMD

    /**
     * Setup creates instance of driver under test.
     * @throws Exception when driver setup problem
     */
    protected final void setUp() throws Exception {
        super.setUp();
        System.setProperty(JDBCListenerProperties.LISTENERS_KEY,
                MockListener.class.getName());
        System.setProperty(JDBCListenerProperties.DRIVER_CLASS_KEY,
                "oracle.jdbc.OracleDriver");
        JDBCCallListener.init();
        Class.forName("com.facdatum.jdbc.listener.driver.DriverListener");
        DriverListener.registerDriver();
        driver = DriverManager.getDriver(ORA_CONNECT_URL);
    }

    /**
     * Tidy up after test by deleted generated log file.
     * @throws Exception if there is a problem tidying up test
     */
    protected final void tearDown() throws Exception {
        super.tearDown();
        JDBCListenerProperties.clearProperties();
        JDBCCallListener.resetInit();
        DriverListener.clearDriver();
        DataSourceListener.clearDataSource();
        final Enumeration < Driver > drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            DriverManager.deregisterDriver(drivers.nextElement());
        }
    }

    /**
     * Test for accepts url.
     */
    public final void testAcceptsURL() {
        try {
            assertTrue("Testing successful acceptsURL", driver.
                    acceptsURL(ORA_CONNECT_URL));
            assertFalse("Testing failed acceptsURL", driver.
                    acceptsURL("xxxx"));
        } catch (SQLException e) {
            fail("Call to acceptsURL failed.");
        }

    }

    /**
     * Test creating connection directly.
     */
    public final void testDirectConnect() {
        Connection conn = null;
        final Properties props = new Properties();
        props.setProperty("user", "hr");
        props.setProperty("password", "hr");
        try {
            conn = driver.connect(ORA_CONNECT_URL, props);
            assertNotNull("Testing direct call to connect", conn);
        } catch (SQLException e) {
            fail("Call to connect failed.");
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    fail("failed to close direct test connection");
                }
            }
        }
    }

    /**
     * Test creating a connection via driver manager.
     */
    public final void testDriverManagerConnect() {
        Connection conn = null;
        final Properties props = new Properties();
        props.setProperty("user", "hr");
        props.setProperty("password", "hr");
        try {
            conn = DriverManager.getConnection(ORA_CONNECT_URL, props);
            assertNotNull("Testing connection via DriverManager", conn);
        } catch (SQLException e) {
            fail("Call to DriverManager.getConnection failed");
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    fail("failed to close driver manager test connection");
                }
            }
        }
    }

    /**
     * Test connection fails when it should.
     */
    public final void testConnectFails() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(ORA_CONNECT_URL,
                    new Properties());
            fail("Call to get connection should not have succeeded" + conn);
        } catch (SQLException e) {
            assertTrue("Get connection should have failed", true); //NOPMD
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    fail("failed to close failed test connection");
                }
            }
        }
    }

    /**
     * Test for getMajorVersion.
     */
    public final void testGetMajorVersion() {
        assertEquals("Testing major version number",
                driver.getMajorVersion(), ORA_MAJOR_VERSION);
    }

    /**
     * Test for getMinorVersion.
     */
    public final void testGetMinorVersion() {
        assertEquals("Testing minor version number",
                driver.getMinorVersion(), ORA_MINOR_VERSION);
    }

    /**
     * Test for getPropertyInfo.
     */
    public final void testGetPropertyInfo() {
        try {
            final DriverPropertyInfo[] props = driver.getPropertyInfo(
                    ORA_CONNECT_URL, null);
            assertEquals("Testing getting username property", props.length, 0);
        } catch (SQLException e) {
            fail("Call to getPropertyInfo failed.");
        }
    }

    /**
     * Test for testJdbcCompliant.
     */
    public final void testJdbcCompliant() {
        assertTrue("Testing jdbc compliant", driver.jdbcCompliant());
    }

}
