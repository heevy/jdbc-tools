/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.SQLException;

import com.facdatum.mock.jdbc.MockPooledConnection;

/**
 * Test of PooledConnectionListener.
 */
public class PooledConnectionListenerTest extends AbstractJDBCTestCase {

    /**
     * PooledConnectionListener under test.
     */
    protected PooledConnectionListener testConn = null; //NOPMD NOCS

    /**
     * Sets up the test.
     * @throws Exception if problem setting up test
     */
    protected void setUp() throws Exception { //NOPMD NOCS
        testConn = new PooledConnectionListener(
                new MockPooledConnection());
        super.setUp();
    }

    /**
     * Cleans up the test.
     * @throws Exception if problem cleaning up test
     */
    protected void tearDown() throws Exception { //NOPMD NOCS
        super.tearDown();
        testConn = null;
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PooledConnectionListener#
     * addConnectionEventListener(javax.sql.ConnectionEventListener)}.
     */
    public final void testAddConnectionEventListener() { //NOPMD
        final Object[] args = {null};
        setArgs(args);
        testConn.addConnectionEventListener(null);
        checkLog();
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PooledConnectionListener#
     *  close()}.
     */
    public final void testClose() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testConn.close();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PooledConnectionListener#
     * getConnection()}.
     */
    public final void testGetConnection() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            checkListenerReturn(testConn.getConnection());
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PooledConnectionListener#
     * removeConnectionEventListener(javax.sql.ConnectionEventListener)}.
     */
    public final void testRemoveConnectionEventListener() { //NOPMD
        final Object[] args = {null};
        setArgs(args);
        testConn.removeConnectionEventListener(null);
        checkLog();
    }
}
