/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.Blob;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import com.facdatum.mock.jdbc.MockPreparedStatement;

/**
 * Test of PreparedStatementListener.
 */
public class PreparedStatementListenerTest extends StatementListenerTest {

    /**
     * Sets up the test.
     * @throws Exception if problem setting up test
     */
    protected void setUp() throws Exception { //NOPMD NOCS
        super.setUp();
        testStatement = (Statement) new PreparedStatementListener(
                new MockPreparedStatement());
    }

    /**
     * Cleans up the test.
     * @throws Exception if problem cleaning up test
     */
    protected void tearDown() throws Exception { //NOPMD NOCS
        super.tearDown();
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #clearParameters()}.
     */
    public final void testClearParameters() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            ((PreparedStatement) testStatement).clearParameters();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     *  #execute()}.
     */
    public final void testExecute() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            ((PreparedStatement) testStatement).execute();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #executeUpdate()}.
     */
    public final void testExecuteUpdate() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            ((PreparedStatement) testStatement).executeUpdate();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #getMetaData()}.
     */
    public final void testGetMetaData() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            checkListenerReturn(
                    ((PreparedStatement) testStatement).getMetaData());
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #getParameterMetaData()}.
     */
    public final void testGetParameterMetaData() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            checkListenerReturn(
                    ((PreparedStatement) testStatement).
                    getParameterMetaData());
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #setArray(int, java.sql.Array)}.
     */
    public final void testSetArray() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            ((PreparedStatement) testStatement).setArray(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #setAsciiStream(int, java.io.InputStream, int)}.
     */
    public final void testSetAsciiStream() { //NOPMD
        try {
            final Object[] args = {0, null, 0};
            setArgs(args);
            ((PreparedStatement) testStatement).setAsciiStream(0, null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #setBigDecimal(int, java.math.BigDecimal)}.
     */
    public final void testSetBigDecimal() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            ((PreparedStatement) testStatement).setBigDecimal(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #setBinaryStream(int, java.io.InputStream, int)}.
     */
    public final void testSetBinaryStream() { //NOPMD
        try {
            final Object[] args = {0, null, 0};
            setArgs(args);
            ((PreparedStatement) testStatement).setBinaryStream(0, null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     *  #setBlob(int,
     * java.sql.Blob)}.
     */
    public final void testSetBlob() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            ((PreparedStatement) testStatement).setBlob(0, (Blob)null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #setBoolean(int, boolean)}.
     */
    public final void testSetBoolean() { //NOPMD
        try {
            final Object[] args = {0, false};
            setArgs(args);
            ((PreparedStatement) testStatement).setBoolean(0, false);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     *  #setByte(int,
     * byte)}.
     */
    public final void testSetByte() { //NOPMD
        try {
            final Object[] args = {0, (byte) 0};
            setArgs(args);
            ((PreparedStatement) testStatement).setByte(0, (byte) 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #setBytes(int, byte[])}.
     */
    public final void testSetBytes() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            ((PreparedStatement) testStatement).setBytes(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #setCharacterStream(int, java.io.Reader, int)}.
     */
    public final void testSetCharacterStream() { //NOPMD
        try {
            final Object[] args = {0, null, 0};
            setArgs(args);
            ((PreparedStatement) testStatement).setCharacterStream(0, null,
                    0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     *  #setClob(int,
     * java.sql.Clob)}.
     */
    public final void testSetClob() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            ((PreparedStatement) testStatement).setClob(0, (Clob)null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     *  #setDate(int,
     * java.sql.Date)}.
     */
    public final void testSetDateIntDate() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            ((PreparedStatement) testStatement).setDate(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     *  #setDate(int,
     * java.sql.Date, java.util.Calendar)}.
     */
    public final void testSetDateIntDateCalendar() { //NOPMD
        try {
            final Object[] args = {0, null, null};
            setArgs(args);
            ((PreparedStatement) testStatement).setDate(0, null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #setDouble(int, double)}.
     */
    public final void testSetDouble() { //NOPMD
        try {
            final Object[] args = {0, (double) 0.0};
            setArgs(args);
            ((PreparedStatement) testStatement).setDouble(0, (double) 0.0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #setFloat(int, float)}.
     */
    public final void testSetFloat() { //NOPMD
        try {
            final Object[] args = {0, (float) 0.0};
            setArgs(args);
            ((PreparedStatement) testStatement).setFloat(0, (float) 0.0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     *  #setInt(int,
     * int)}.
     */
    public final void testSetInt() { //NOPMD
        try {
            final Object[] args = {0, 0};
            setArgs(args);
            ((PreparedStatement) testStatement).setInt(0, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     *  #setLong(int,
     * long)}.
     */
    public final void testSetLong() { //NOPMD
        try {
            final Object[] args = {0, (long) 0};
            setArgs(args);
            ((PreparedStatement) testStatement).setLong(0, (long) 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     *  #setNull(int,
     * int)}.
     */
    public final void testSetNullIntInt() { //NOPMD
        try {
            final Object[] args = {0, 0};
            setArgs(args);
            ((PreparedStatement) testStatement).setNull(0, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     *  #setNull(int,
     * int, java.lang.String)}.
     */
    public final void testSetNullIntIntString() { //NOPMD
        try {
            final Object[] args = {0, 0, null};
            setArgs(args);
            ((PreparedStatement) testStatement).setNull(0, 0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #setObject(int, java.lang.Object)}.
     */
    public final void testSetObjectIntObject() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            ((PreparedStatement) testStatement).setObject(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #setObject(int, java.lang.Object, int)}.
     */
    public final void testSetObjectIntObjectInt() { //NOPMD
        try {
            final Object[] args = {0, null, 0};
            setArgs(args);
            ((PreparedStatement) testStatement).setObject(0, null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #setObject(int, java.lang.Object, int, int)}.
     */
    public final void testSetObjectIntObjectIntInt() { //NOPMD
        try {
            final Object[] args = {0, null, 0, 0};
            setArgs(args);
            ((PreparedStatement) testStatement).setObject(0, null, 0, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     *  #setRef(int,
     * java.sql.Ref)}.
     */
    public final void testSetRef() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            ((PreparedStatement) testStatement).setRef(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #setShort(int, short)}.
     */
    public final void testSetShort() { //NOPMD
        try {
            final Object[] args = {0, (short) 0};
            setArgs(args);
            ((PreparedStatement) testStatement).setShort(0, (short) 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #setString(int, java.lang.String)}.
     */
    public final void testSetString() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            ((PreparedStatement) testStatement).setString(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     *  #setTime(int,
     * java.sql.Time)}.
     */
    public final void testSetTimeIntTime() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            ((PreparedStatement) testStatement).setTime(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     *  #setTime(int,
     * java.sql.Time, java.util.Calendar)}.
     */
    public final void testSetTimeIntTimeCalendar() { //NOPMD
        try {
            final Object[] args = {0, null, null};
            setArgs(args);
            ((PreparedStatement) testStatement).setTime(0, null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #setTimestamp(int, java.sql.Timestamp)}.
     */
    public final void testSetTimestampIntTimestamp() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            ((PreparedStatement) testStatement).setTimestamp(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #setTimestamp(int, java.sql.Timestamp, java.util.Calendar)}.
     */
    public final void testSetTimestampIntTimestampCalendar() { //NOPMD
        try {
            final Object[] args = {0, null, null};
            setArgs(args);
            ((PreparedStatement) testStatement).setTimestamp(0, null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     *  #setURL(int,
     * java.net.URL)}.
     */
    public final void testSetURL() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            ((PreparedStatement) testStatement).setURL(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #setUnicodeStream(int, java.io.InputStream, int)}.
     */
    @SuppressWarnings("deprecation")
    public final void testSetUnicodeStream() { //NOPMD
        try {
            final Object[] args = {0, null, 0};
            setArgs(args);
            ((PreparedStatement) testStatement).setUnicodeStream(0, null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #addBatch(java.lang.String)}.
     */
    public final void testAddBatch() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            ((PreparedStatement) testStatement).addBatch();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.PreparedStatementListener
     * #executeQuery(java.lang.String)}.
     */
    public final void testExecuteQuery() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            checkListenerReturn(
                    ((PreparedStatement) testStatement).executeQuery());
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }
}
