/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.Driver;
import java.sql.DriverManager;
import java.util.Enumeration;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCDelegator;
import com.facdatum.jdbc.listener.JDBCListener;
import com.facdatum.jdbc.listener.JDBCListenerProperties;
import com.facdatum.mock.listener.MockListener;

import junit.framework.TestCase;

/**
 * Provides base case for testing the call stack for all listener methods.
 */
public abstract class AbstractJDBCTestCase extends TestCase {

    /**
     * Holds the arguments used to call last listener method.
     */
    protected static Object[] savedArgs = null; //NOCS

    /**
     * Test driver that will get registered (although is not used).
     */
    private static final String DRIVER_CLASS = "oracle.jdbc.OracleDriver";

    /**
     * The number of calls that should have been made to the listener.
     */
    private static final int EXPECTED_CALLS = 3;

    /**
     * Sets up test by setting up log file.
     * @throws Exception if problem setting up test
     */
    protected void setUp() throws Exception { //NOCS
        super.setUp();
        System.setProperty(JDBCListenerProperties.LISTENERS_KEY,
                MockListener.class.getName());
        System.setProperty(JDBCListenerProperties.DRIVER_CLASS_KEY,
                DRIVER_CLASS);
        JDBCCallListener.init();
    }

    /**
     * Cleans up after test by removing log file.
     * @throws Exception if problem cleaning up test
     */
    protected void tearDown() throws Exception { //NOCS
        super.tearDown();
        JDBCListenerProperties.clearProperties();
        JDBCCallListener.resetInit();
        DriverListener.clearDriver();
        DataSourceListener.clearDataSource();
        final Enumeration < Driver > drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            DriverManager.deregisterDriver(drivers.nextElement());
        }
    }

    /**
     * Sets the saved arguments from last method call.
     * @param args the method call arguments
     */
    protected final void setArgs(final Object[] args) { //NOPMD
        savedArgs = args;
    }

    /**
     * Returns the saved arguments from last method call.
     * @return the saved arguments
     */
    public static final Object[] getArgs() { //NOPMD
        return savedArgs; //NOPMD
    }

    /**
     * Tests that the object returned from a method is an instance of
     * JDBCDelegator. Used on methods that return wrapped values.
     * @param obj return value from method to be tested
     */
    public final void checkListenerReturn(final Object obj) {
        assertTrue("Testing return", obj instanceof JDBCDelegator);
    }

    /**
     * Tests the length of the log file to ensure both call and exit have been
     * logged.
     */
    public final void checkLog() {
        final JDBCListener[] listeners = JDBCCallListener.getListeners();
        final MockListener mockListener = (MockListener) listeners[0];
        assertEquals("testing calls", EXPECTED_CALLS,
                mockListener.getCallCount());
    }
}
