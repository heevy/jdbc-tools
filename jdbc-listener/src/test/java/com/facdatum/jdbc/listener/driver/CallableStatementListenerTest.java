/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver; // NOPMD

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import com.facdatum.mock.jdbc.MockCallableStatement;

/**
 * Test of CallableStatementListener.
 */
public class CallableStatementListenerTest extends //NOPMD
        PreparedStatementListenerTest {

    /**
     * Sets up the test.
     * @throws Exception
     *             if problem setting up test
     */
    protected void setUp() throws Exception { // NOPMD NOCS
        super.setUp();
        testStatement = (Statement) new CallableStatementListener(
                new MockCallableStatement());
    }

    /**
     * Cleans up the test.
     * @throws Exception
     *             if problem cleaning up test
     */
    protected final void tearDown() throws Exception { // NOPMD
        super.tearDown();
    }

    /**
     * Test method for {@link
     * com.facdatum.jdbc.listener.driver.CallableStatementListener
     *  #getArray(int)}.
     */
    public final void testGetArrayInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            checkListenerReturn(((CallableStatement) testStatement)
                    .getArray(0));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     * com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getArray(java.lang.String)}.
     */
    public final void testGetArrayString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            checkListenerReturn(((CallableStatement) testStatement)
                    .getArray(null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     * com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getBigDecimal(int)}.
     */
    public final void testGetBigDecimalInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            ((CallableStatement) testStatement).getBigDecimal(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     * com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getBigDecimal(java.lang.String)}.
     */
    public final void testGetBigDecimalString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            ((CallableStatement) testStatement).getBigDecimal(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     * com.facdatum.jdbc.listener.driver.CallableStatementListener
     *  #getBigDecimal(int, int)}.
     */
    @SuppressWarnings("deprecation")
    public final void testGetBigDecimalIntInt() { //NOPMD
        try {
            final Object[] args = {0, 0};
            setArgs(args);
            ((CallableStatement) testStatement).getBigDecimal(0, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     * com.facdatum.jdbc.listener.driver.CallableStatementListener
     *  #getBlob(int)}.
     */
    public final void testGetBlobInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            checkListenerReturn(((CallableStatement) testStatement)
                    .getBlob(0));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     * com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getBlob(java.lang.String)}.
     */
    public final void testGetBlobString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            checkListenerReturn(((CallableStatement) testStatement)
                    .getBlob(null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     * com.facdatum.jdbc.listener.driver.CallableStatementListener
     *  #getBoolean(int)}.
     */
    public final void testGetBooleanInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            ((CallableStatement) testStatement).getBoolean(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getBoolean(java.lang.String)}.
     */
    public final void testGetBooleanString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            ((CallableStatement) testStatement).getBoolean(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getByte(int)}.
     */
    public final void testGetByteInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            ((CallableStatement) testStatement).getByte(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getByte(java.lang.String)}.
     */
    public final void testGetByteString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            ((CallableStatement) testStatement).getByte(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getBytes(int)}.
     */
    public final void testGetBytesInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            ((CallableStatement) testStatement).getBytes(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getBytes(java.lang.String)}.
     */
    public final void testGetBytesString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            ((CallableStatement) testStatement).getBytes(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getClob(int)}.
     */
    public final void testGetClobInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            checkListenerReturn(((CallableStatement) testStatement)
                    .getClob(0));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getClob(java.lang.String)}.
     */
    public final void testGetClobString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            checkListenerReturn(((CallableStatement) testStatement)
                    .getClob(null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getDate(int)}.
     */
    public final void testGetDateInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            ((CallableStatement) testStatement).getDate(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getDate(java.lang.String)}.
     */
    public final void testGetDateString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            ((CallableStatement) testStatement).getDate(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     *  #getDate(int, java.util.Calendar)}.
     */
    public final void testGetDateIntCalendar() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            ((CallableStatement) testStatement).getDate(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getDate(java.lang.String, java.util.Calendar)}.
     */
    public final void testGetDateStringCalendar() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            ((CallableStatement) testStatement).getDate(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getDouble(int)}.
     */
    public final void testGetDoubleInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            ((CallableStatement) testStatement).getDouble(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getDouble(java.lang.String)}.
     */
    public final void testGetDoubleString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            ((CallableStatement) testStatement).getDouble(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getFloat(int)}.
     */
    public final void testGetFloatInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            ((CallableStatement) testStatement).getFloat(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getFloat(java.lang.String)}.
     */
    public final void testGetFloatString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            ((CallableStatement) testStatement).getFloat(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getInt(int)}.
     */
    public final void testGetIntInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            ((CallableStatement) testStatement).getInt(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getInt(java.lang.String)}.
     */
    public final void testGetIntString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            ((CallableStatement) testStatement).getInt(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getLong(int)}.
     */
    public final void testGetLongInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            ((CallableStatement) testStatement).getLong(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getLong(java.lang.String)}.
     */
    public final void testGetLongString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            ((CallableStatement) testStatement).getLong(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getObject(int)}.
     */
    public final void testGetObjectInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            ((CallableStatement) testStatement).getObject(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getObject(java.lang.String)}.
     */
    public final void testGetObjectString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            ((CallableStatement) testStatement).getObject(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getObject(int, java.util.Map)}.
     */
    public final void testGetObjectIntMapOfStringClassOfQ() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            ((CallableStatement) testStatement).getObject(0, (Map)null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getObject(java.lang.String, java.util.Map)}.
     */
    public final void testGetObjectStringMapOfStringClassOfQ() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            ((CallableStatement) testStatement).getObject(null, (Map)null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getRef(int)}.
     */
    public final void testGetRefInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            checkListenerReturn(((CallableStatement) testStatement).
                    getRef(0));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getRef(java.lang.String)}.
     */
    public final void testGetRefString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            checkListenerReturn(((CallableStatement) testStatement)
                    .getRef(null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getShort(int)}.
     */
    public final void testGetShortInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            ((CallableStatement) testStatement).getShort(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getShort(java.lang.String)}.
     */
    public final void testGetShortString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            ((CallableStatement) testStatement).getShort(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getString(int)}.
     */
    public final void testGetStringInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            ((CallableStatement) testStatement).getString(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getString(java.lang.String)}.
     */
    public final void testGetStringString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            ((CallableStatement) testStatement).getString(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getTime(int)}.
     */
    public final void testGetTimeInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            ((CallableStatement) testStatement).getTime(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getTime(java.lang.String)}.
     */
    public final void testGetTimeString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            ((CallableStatement) testStatement).getTime(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     *  #getTime(int, java.util.Calendar)}.
     */
    public final void testGetTimeIntCalendar() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            ((CallableStatement) testStatement).getTime(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getTime(java.lang.String, java.util.Calendar)}.
     */
    public final void testGetTimeStringCalendar() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            ((CallableStatement) testStatement).getTime(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getTimestamp(int)}.
     */
    public final void testGetTimestampInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            ((CallableStatement) testStatement).getTimestamp(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getTimestamp(java.lang.String)}.
     */
    public final void testGetTimestampString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            ((CallableStatement) testStatement).getTimestamp(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getTimestamp(int, java.util.Calendar)}.
     */
    public final void testGetTimestampIntCalendar() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            ((CallableStatement) testStatement).getTimestamp(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getTimestamp(java.lang.String, java.util.Calendar)}.
     */
    public final void testGetTimestampStringCalendar() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            ((CallableStatement) testStatement).getTimestamp(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getURL(int)}.
     */
    public final void testGetURLInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            ((CallableStatement) testStatement).getURL(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #getURL(java.lang.String)}.
     */
    public final void testGetURLString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            ((CallableStatement) testStatement).getURL(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #registerOutParameter(int, int)}.
     */
    public final void testRegisterOutParameterIntInt() { //NOPMD
        try {
            final Object[] args = {0, 0};
            setArgs(args);
            ((CallableStatement) testStatement).registerOutParameter(0, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #registerOutParameter(java.lang.String, int)}.
     */
    public final void testRegisterOutParameterStringInt() { //NOPMD
        try {
            final Object[] args = {null, 0};
            setArgs(args);
            ((CallableStatement) testStatement)
                    .registerOutParameter(null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #registerOutParameter(int, int, int)}.
     */
    public final void testRegisterOutParameterIntIntInt() { //NOPMD
        try {
            final Object[] args = {0, 0, 0};
            setArgs(args);
            ((CallableStatement) testStatement)
                    .registerOutParameter(0, 0, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #registerOutParameter(int, int, java.lang.String)}.
     */
    public final void testRegisterOutParameterIntIntString() { //NOPMD
        try {
            final Object[] args = {0, 0, null};
            setArgs(args);
            ((CallableStatement) testStatement).registerOutParameter(0, 0,
                    null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #registerOutParameter(java.lang.String, int, int)}.
     */
    public final void testRegisterOutParameterStringIntInt() { //NOPMD
        try {
            final Object[] args = {null, 0, 0};
            setArgs(args);
            ((CallableStatement) testStatement).registerOutParameter(null,
                    0, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #registerOutParameter(java.lang.String, int, java.lang.String)}.
     */
    public final void testRegisterOutParameterStringIntString() { //NOPMD
        try {
            final Object[] args = {null, 0, null};
            setArgs(args);
            ((CallableStatement) testStatement).registerOutParameter(null,
                    0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setAsciiStream(java.lang.String, java.io.InputStream, int)}.
     */
    public final void testSetAsciiStreamStringInputStreamInt() { //NOPMD
        try {
            final Object[] args = {null, null, 0};
            setArgs(args);
            ((CallableStatement) testStatement)
                    .setAsciiStream(null, null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setBigDecimal(java.lang.String, java.math.BigDecimal)}.
     */
    public final void testSetBigDecimalStringBigDecimal() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            ((CallableStatement) testStatement).setBigDecimal(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setBinaryStream(java.lang.String, java.io.InputStream, int)}.
     */
    public final void testSetBinaryStreamStringInputStreamInt() { //NOPMD
        try {
            final Object[] args = {null, null, 0};
            setArgs(args);
            ((CallableStatement) testStatement).setBinaryStream(null, null,
                    0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setBoolean(java.lang.String, boolean)}.
     */
    public final void testSetBooleanStringBoolean() { //NOPMD
        try {
            final Object[] args = {null, false};
            setArgs(args);
            ((CallableStatement) testStatement).setBoolean(null, false);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setByte(java.lang.String, byte)}.
     */
    public final void testSetByteStringByte() { //NOPMD
        try {
            final Object[] args = {null, (byte) 0};
            setArgs(args);
            ((CallableStatement) testStatement).setByte(null, (byte) 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setBytes(java.lang.String, byte[])}.
     */
    public final void testSetBytesStringByteArray() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            ((CallableStatement) testStatement).setBytes(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setCharacterStream(java.lang.String, java.io.Reader, int)}.
     */
    public final void testSetCharacterStreamStringReaderInt() { //NOPMD
        try {
            final Object[] args = {null, null, 0};
            setArgs(args);
            ((CallableStatement) testStatement).setCharacterStream(null,
                    null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setDate(java.lang.String, java.sql.Date)}.
     */
    public final void testSetDateStringDate() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            ((CallableStatement) testStatement).setDate(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setDate(java.lang.String, java.sql.Date, java.util.Calendar)}.
     */
    public final void testSetDateStringDateCalendar() { //NOPMD
        try {
            final Object[] args = {null, null, null};
            setArgs(args);
            ((CallableStatement) testStatement).setDate(null, null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setDouble(java.lang.String, double)}.
     */
    public final void testSetDoubleStringDouble() { //NOPMD
        try {
            final Object[] args = {null, (double) 0.0};
            setArgs(args);
            ((CallableStatement) testStatement)
                    .setDouble(null, (double) 0.0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setFloat(java.lang.String, float)}.
     */
    public final void testSetFloatStringFloat() { //NOPMD
        try {
            final Object[] args = {null, (float) 0.0};
            setArgs(args);
            ((CallableStatement) testStatement).setFloat(null, (float) 0.0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setInt(java.lang.String, int)}.
     */
    public final void testSetIntStringInt() { //NOPMD
        try {
            final Object[] args = {null, 0};
            setArgs(args);
            ((CallableStatement) testStatement).setInt(null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setLong(java.lang.String, long)}.
     */
    public final void testSetLongStringLong() { //NOPMD
        try {
            final Object[] args = {null, (long) 0};
            setArgs(args);
            ((CallableStatement) testStatement).setLong(null, (long) 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setNull(java.lang.String, int)}.
     */
    public final void testSetNullStringInt() { //NOPMD
        try {
            final Object[] args = {null, 0};
            setArgs(args);
            ((CallableStatement) testStatement).setNull(null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setNull(java.lang.String, int, java.lang.String)}.
     */
    public final void testSetNullStringIntString() { //NOPMD
        try {
            final Object[] args = {null, 0, null};
            setArgs(args);
            ((CallableStatement) testStatement).setNull(null, 0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setObject(java.lang.String, java.lang.Object)}.
     */
    public final void testSetObjectStringObject() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            ((CallableStatement) testStatement).setObject(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setObject(java.lang.String, java.lang.Object, int)}.
     */
    public final void testSetObjectStringObjectInt() { //NOPMD
        try {
            final Object[] args = {null, null, 0};
            setArgs(args);
            ((CallableStatement) testStatement).setObject(null, null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setObject(java.lang.String, java.lang.Object, int, int)}.
     */
    public final void testSetObjectStringObjectIntInt() { //NOPMD
        try {
            final Object[] args = {null, null, 0, 0};
            setArgs(args);
            ((CallableStatement) testStatement).setObject(null, null, 0, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setShort(java.lang.String, short)}.
     */
    public final void testSetShortStringShort() { //NOPMD
        try {
            final Object[] args = {null, (short) 0};
            setArgs(args);
            ((CallableStatement) testStatement).setShort(null, (short) 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setString(java.lang.String, java.lang.String)}.
     */
    public final void testSetStringStringString() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            ((CallableStatement) testStatement).setString(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setTime(java.lang.String, java.sql.Time)}.
     */
    public final void testSetTimeStringTime() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            ((CallableStatement) testStatement).setTime(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setTime(java.lang.String, java.sql.Time, java.util.Calendar)}.
     */
    public final void testSetTimeStringTimeCalendar() { //NOPMD
        try {
            final Object[] args = {null, null, null};
            setArgs(args);
            ((CallableStatement) testStatement).setTime(null, null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setTimestamp(java.lang.String, java.sql.Timestamp)}.
     */
    public final void testSetTimestampStringTimestamp() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            ((CallableStatement) testStatement).setTimestamp(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setTimestamp(java.lang.String, java.sql.Timestamp, java.util.Calendar)}.
     */
    public final void testSetTimestampStringTimestampCalendar() { //NOPMD
        try {
            final Object[] args = {null, null, null};
            setArgs(args);
            ((CallableStatement) testStatement).setTimestamp(null, null,
                    null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     * #setURL(java.lang.String, java.net.URL)}.
     */
    public final void testSetURLStringURL() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            ((CallableStatement) testStatement).setURL(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.CallableStatementListener
     *  #wasNull()}.
     */
    public final void testWasNull() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            ((CallableStatement) testStatement).wasNull();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }
}
