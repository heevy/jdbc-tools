/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.Driver;
import java.sql.DriverManager;
import java.util.Enumeration;
import java.util.Properties;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCListenerProperties;
import com.facdatum.mock.listener.MockListener;

import junit.framework.TestCase;

/**
 * Test class for DriverListener.
 */
public class DriverListenerClassLoadedTest extends TestCase {

    /**
     * Setup creates instance of driver under test.
     * @throws Exception when driver setup problem
     */
    protected final void setUp() throws Exception {
        super.setUp();
        System.setProperty(JDBCListenerProperties.LISTENERS_KEY,
                MockListener.class.getName());
        System.setProperty(JDBCListenerProperties.DRIVER_CLASS_KEY,
                "oracle.jdbc.OracleDriver");
        JDBCCallListener.init();
    }

    /**
     * Cleans up after test by removing log file.
     * @throws Exception if problem cleaning up test
     */
    protected void tearDown() throws Exception { //NOCS
        super.tearDown();
        JDBCListenerProperties.clearProperties();
        JDBCCallListener.resetInit();
        DriverListener.clearDriver();
        DataSourceListener.clearDataSource();
        final Enumeration < Driver > drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            DriverManager.deregisterDriver(drivers.nextElement());
        }
    }

    /**
     * Test for when loaded through ClassLoader.
     */
    public final void testConnect() {
        try {
            final Properties props = new Properties();
            props.setProperty("user", "hr");
            props.setProperty("password", "hr");
            final Thread currentThread = Thread.currentThread(); //NOPMD
            final ClassLoader classLoader = currentThread.
                    getContextClassLoader();
            final Driver driver = (Driver) classLoader.loadClass(
                    "com.facdatum.jdbc.listener.driver.DriverListener").
                    newInstance();
            assertNotNull("testing loading driver via classloader",
                    driver.connect(DriverListenerFullTest.ORA_CONNECT_URL,
                            props));
        } catch (Exception e) {
            fail("failed to load driver via classloader" + e);
        }
    }

}
