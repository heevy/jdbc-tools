/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import com.facdatum.mock.jdbc.MockCallableStatement;

/**
 * Test of CallableStatementListener when exceptions are thrown.
 */
public class CallableStatementListenerFailTest //NOPMD
        extends CallableStatementListenerTest {

    /**
     * Sets up the test.
     * @throws Exception if problem setting up test
     */
    protected final void setUp() throws Exception { //NOPMD
        super.setUp();
        ((MockCallableStatement) ((CallableStatementListener) testStatement).
                getDelegateStatement()).setToFail(true);
    }
}
