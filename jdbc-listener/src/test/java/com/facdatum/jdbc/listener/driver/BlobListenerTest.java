/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.Blob;
import java.sql.SQLException;

import com.facdatum.mock.jdbc.MockBlob;

/**
 * Test of BlobListener.
 */
public class BlobListenerTest extends AbstractJDBCTestCase {

    /**
     * BlobListener under test.
     */
    protected BlobListener testBlob = null; //NOPMD NOCS

    /**
     * Sets up the test.
     * @throws Exception if problem setting up test
     */
    protected void setUp() throws Exception { //NOPMD NOCS
        super.setUp();
        testBlob = new BlobListener(new MockBlob());
    }

    /**
     * Cleans up the test.
     * @throws Exception if problem cleaning up test
     */
    protected final void tearDown() throws Exception { //NOPMD
        super.tearDown();
        testBlob = null;
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.BlobListener#
     *      getBinaryStream()}.
     */
    public final void testGetBinaryStream() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testBlob.getBinaryStream();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.BlobListener#getBytes(
     *      long, int)}.
     */
    public final void testGetBytes() { //NOPMD
        try {
            final Object[] args = {Long.valueOf(0), 0};
            setArgs(args);
            testBlob.getBytes(0, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.BlobListener#length()}.
     */
    public final void testLength() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testBlob.length();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.BlobListener#position(
     *      byte[], long)}.
     */
    public final void testPositionByteArrayLong() { //NOPMD
        try {
            final Object[] args = {(byte[]) null, Long.valueOf(0)};
            setArgs(args);
            testBlob.position((byte[]) null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.BlobListener#position(
     *      java.sql.Blob, long)}.
     */
    public final void testPositionBlobLong() { //NOPMD
        try {
            final Object[] args = {(Blob) null, Long.valueOf(0)};
            setArgs(args);
            testBlob.position((Blob) null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.BlobListener#
     *      setBinaryStream(long)}.
     */
    public final void testSetBinaryStream() { //NOPMD
        try {
            final Object[] args = {Long.valueOf(0)};
            setArgs(args);
            testBlob.setBinaryStream(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.BlobListener#setBytes(
     *      long, byte[])}.
     */
    public final void testSetBytesLongByteArray() { //NOPMD
        try {
            final Object[] args = {Long.valueOf(0), (byte[]) null};
            setArgs(args);
            testBlob.setBytes(0, (byte[]) null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.BlobListener#setBytes(
     *      long, byte[], int, int)}.
     */
    public final void testSetBytesLongByteArrayIntInt() { //NOPMD
        try {
            final Object[] args = {Long.valueOf(0), (byte[]) null, 0, 0};
            setArgs(args);
            testBlob.setBytes(0, (byte[]) null, 0, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.BlobListener#truncate(
     *      long)}.
     */
    public final void testTruncate() { //NOPMD
        try {
            final Object[] args = {Long.valueOf(0)};
            setArgs(args);
            testBlob.truncate(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }
}
