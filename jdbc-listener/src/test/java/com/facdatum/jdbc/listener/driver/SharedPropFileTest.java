/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import com.facdatum.jdbc.listener.JDBCListenerProperties;

import junit.framework.TestCase;

/**
 * Test of overriding the properties file name with overall config value.
 */
public class SharedPropFileTest extends TestCase {

    /**
     * Properties file name that is set in properties.
     */
    private static final String PROP_FILE = "test-properties";

    /**
     * Holds the properties file reference.
     */
    private File testFile; //NOPMD

    /**
     * Properties that are tested.
     */
    private Properties testProps; //NOPMD

    /**
     * Set up test by creating the overriding properties file.
     * @throws Exception if there is a problem setting up test
     */
    protected final void setUp() throws Exception {
        super.setUp();

        new File("config/com/facdatum/jdbc/config/").mkdirs();
        testFile = new File("config/com/facdatum/jdbc/config/"
                + "JDBCPropertiesFileName.properties");
        testProps = new Properties();
        testProps.setProperty(JDBCListenerProperties.SHARED_FILE_KEY,
                PROP_FILE);
        clearResourceBundleCache();
        testProps.store(new FileOutputStream(testFile), "Test properties");
        JDBCListenerProperties.init();
    }

    /**
     * Clean up after test.
     * @throws Exception if there is a problem cleaning up test
     */
    protected final void tearDown() throws Exception {
        super.tearDown();
        testFile.delete();
        new File("config/com/facdatum/jdbc/config").delete();
        new File("config/com/facdatum/jdbc").delete();
        new File("config/com/facdatum").delete();
        new File("config/com").delete();
        JDBCListenerProperties.clearProperties();
        clearResourceBundleCache();
    }

    /**
     * This will clear the ResourceBundle cache to allow new version to
     * be loaded.
     */
    @SuppressWarnings("unchecked")
    private void clearResourceBundleCache() {
        try {
            final Class < ResourceBundle > type = ResourceBundle.class;
            final Field cacheList = type.getDeclaredField("cacheList");
            cacheList.setAccessible(true);
            ((Map) cacheList.get(ResourceBundle.class)).clear();
        } catch (Exception e) {
            fail("Problem cleaning ResourceBundle");
        }
    }

    /**
     * Test the properties filename set in properties file.
     */
    public final void testPropFile() {
        assertEquals("Testing loaded log path",
                PROP_FILE, JDBCListenerProperties.getPropFile());
    }

}
