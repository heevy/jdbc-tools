//NOCS
/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver; //NOPMD

import java.sql.Blob;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.Map;

import com.facdatum.mock.jdbc.MockResultSet;

/**
 * Test of ResultSetListener.
 */
public class ResultSetListenerTest extends AbstractJDBCTestCase { //NOPMD

    /**
     * ResultSetListener under test.
     */
    protected ResultSetListener testResultSet = null; //NOPMD NOCS

    /**
     * Sets up the test.
     * @throws Exception if problem setting up test
     */
    protected void setUp() throws Exception { //NOPMD NOCS
        testResultSet = new ResultSetListener(
                new MockResultSet());
        super.setUp();
    }

    /**
     * Cleans up the test.
     * @throws Exception if problem cleaning up test
     */
    protected final void tearDown() throws Exception { //NOPMD
        super.tearDown();
        testResultSet = null;
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      absolute(int)}.
     */
    public final void testAbsolute() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.absolute(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      afterLast()}.
     */
    public final void testAfterLast() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.afterLast();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      beforeFirst()}.
     */
    public final void testBeforeFirst() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.beforeFirst();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      cancelRowUpdates()}.
     */
    public final void testCancelRowUpdates() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.cancelRowUpdates();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      clearWarnings()}.
     */
    public final void testClearWarnings() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.clearWarnings();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      close()}.
     */
    public final void testClose() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.close();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      deleteRow()}.
     */
    public final void testDeleteRow() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.deleteRow();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      findColumn(java.lang.String)}.
     */
    public final void testFindColumn() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testResultSet.findColumn(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      first()}.
     */
    public final void testFirst() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.first();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getArray(int)}.
     */
    public final void testGetArrayInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            checkListenerReturn(testResultSet.getArray(0));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getArray(java.lang.String)}.
     */
    public final void testGetArrayString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            checkListenerReturn(testResultSet.getArray(null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getAsciiStream(int)}.
     */
    public final void testGetAsciiStreamInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.getAsciiStream(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getAsciiStream(java.lang.String)}.
     */
    public final void testGetAsciiStreamString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testResultSet.getAsciiStream(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getBigDecimal(int)}.
     */
    public final void testGetBigDecimalInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.getBigDecimal(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getBigDecimal(java.lang.String)}.
     */
    public final void testGetBigDecimalString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testResultSet.getBigDecimal(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getBigDecimal(int, int)}.
     */
    public final void testGetBigDecimalIntInt() { //NOPMD
        try {
            final Object[] args = {0, 0};
            setArgs(args);
            testResultSet.getBigDecimal(0, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getBigDecimal(java.lang.String, int)}.
     */
    public final void testGetBigDecimalStringInt() { //NOPMD
        try {
            final Object[] args = {null, 0};
            setArgs(args);
            testResultSet.getBigDecimal(null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getBinaryStream(int)}.
     */
    public final void testGetBinaryStreamInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.getBinaryStream(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getBinaryStream(java.lang.String)}.
     */
    public final void testGetBinaryStreamString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testResultSet.getBinaryStream(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getBlob(int)}.
     */
    public final void testGetBlobInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            checkListenerReturn(testResultSet.getBlob(0));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getBlob(java.lang.String)}.
     */
    public final void testGetBlobString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            checkListenerReturn(testResultSet.getBlob(null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getBoolean(int)}.
     */
    public final void testGetBooleanInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.getBoolean(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getBoolean(java.lang.String)}.
     */
    public final void testGetBooleanString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testResultSet.getBoolean(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getByte(int)}.
     */
    public final void testGetByteInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.getByte(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getByte(java.lang.String)}.
     */
    public final void testGetByteString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testResultSet.getByte(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getBytes(int)}.
     */
    public final void testGetBytesInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.getBytes(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getBytes(java.lang.String)}.
     */
    public final void testGetBytesString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testResultSet.getBytes(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getCharacterStream(int)}.
     */
    public final void testGetCharacterStreamInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.getCharacterStream(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getCharacterStream(java.lang.String)}.
     */
    public final void testGetCharacterStreamString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testResultSet.getCharacterStream(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getClob(int)}.
     */
    public final void testGetClobInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            checkListenerReturn(testResultSet.getClob(0));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getClob(java.lang.String)}.
     */
    public final void testGetClobString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            checkListenerReturn(testResultSet.getClob(null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getConcurrency()}.
     */
    public final void testGetConcurrency() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.getConcurrency();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getCursorName()}.
     */
    public final void testGetCursorName() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.getCursorName();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getDate(int)}.
     */
    public final void testGetDateInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.getDate(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getDate(java.lang.String)}.
     */
    public final void testGetDateString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testResultSet.getDate(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getDate(int, java.util.Calendar)}.
     */
    public final void testGetDateIntCalendar() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            testResultSet.getDate(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getDate(java.lang.String, java.util.Calendar)}.
     */
    public final void testGetDateStringCalendar() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            testResultSet.getDate(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getDouble(int)}.
     */
    public final void testGetDoubleInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.getDouble(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getDouble(java.lang.String)}.
     */
    public final void testGetDoubleString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testResultSet.getDouble(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getFetchDirection()}.
     */
    public final void testGetFetchDirection() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.getFetchDirection();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getFetchSize()}.
     */
    public final void testGetFetchSize() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.getFetchSize();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getFloat(int)}.
     */
    public final void testGetFloatInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.getFloat(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getFloat(java.lang.String)}.
     */
    public final void testGetFloatString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testResultSet.getFloat(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getInt(int)}.
     */
    public final void testGetIntInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.getInt(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getInt(java.lang.String)}.
     */
    public final void testGetIntString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testResultSet.getInt(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getLong(int)}.
     */
    public final void testGetLongInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.getLong(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getLong(java.lang.String)}.
     */
    public final void testGetLongString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testResultSet.getLong(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getMetaData()}.
     */
    public final void testGetMetaData() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            checkListenerReturn(testResultSet.getMetaData());
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getObject(int)}.
     */
    public final void testGetObjectInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.getObject(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getObject(java.lang.String)}.
     */
    public final void testGetObjectString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testResultSet.getObject(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#g
     *      etObject(int, java.util.Map)}.
     */
    public final void testGetObjectIntMapOfStringClassOfQ() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            testResultSet.getObject(0, (Map)null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getObject(java.lang.String, java.util.Map)}.
     */
    public final void testGetObjectStringMapOfStringClassOfQ() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            testResultSet.getObject(null, (Map)null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getRef(int)}.
     */
    public final void testGetRefInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            checkListenerReturn(testResultSet.getRef(0));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getRef(java.lang.String)}.
     */
    public final void testGetRefString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            checkListenerReturn(testResultSet.getRef(null));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getRow()}.
     */
    public final void testGetRow() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.getRow();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getShort(int)}.
     */
    public final void testGetShortInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.getShort(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getShort(java.lang.String)}.
     */
    public final void testGetShortString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testResultSet.getShort(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getStatement()}.
     */
    public final void testGetStatement() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            checkListenerReturn(testResultSet.getStatement());
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getString(int)}.
     */
    public final void testGetStringInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.getString(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getString(java.lang.String)}.
     */
    public final void testGetStringString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testResultSet.getString(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getTime(int)}.
     */
    public final void testGetTimeInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.getTime(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getTime(java.lang.String)}.
     */
    public final void testGetTimeString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testResultSet.getTime(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getTime(int, java.util.Calendar)}.
     */
    public final void testGetTimeIntCalendar() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            testResultSet.getTime(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getTime(java.lang.String, java.util.Calendar)}.
     */
    public final void testGetTimeStringCalendar() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            testResultSet.getTime(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getTimestamp(int)}.
     */
    public final void testGetTimestampInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.getTimestamp(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getTimestamp(java.lang.String)}.
     */
    public final void testGetTimestampString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testResultSet.getTimestamp(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getTimestamp(int, java.util.Calendar)}.
     */
    public final void testGetTimestampIntCalendar() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            testResultSet.getTimestamp(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getTimestamp(java.lang.String, java.util.Calendar)}.
     */
    public final void testGetTimestampStringCalendar() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            testResultSet.getTimestamp(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getType()}.
     */
    public final void testGetType() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.getType();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getURL(int)}.
     */
    public final void testGetURLInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.getURL(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getURL(java.lang.String)}.
     */
    public final void testGetURLString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testResultSet.getURL(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getUnicodeStream(int)}.
     */
    public final void testGetUnicodeStreamInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.getUnicodeStream(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getUnicodeStream(java.lang.String)}.
     */
    public final void testGetUnicodeStreamString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testResultSet.getUnicodeStream(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      getWarnings()}.
     */
    public final void testGetWarnings() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.getWarnings();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      insertRow()}.
     */
    public final void testInsertRow() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.insertRow();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      isAfterLast()}.
     */
    public final void testIsAfterLast() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.isAfterLast();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      isBeforeFirst()}.
     */
    public final void testIsBeforeFirst() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.isBeforeFirst();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      isFirst()}.
     */
    public final void testIsFirst() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.isFirst();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      isLast()}.
     */
    public final void testIsLast() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.isLast();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      last()}.
     */
    public final void testLast() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.last();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      moveToCurrentRow()}.
     */
    public final void testMoveToCurrentRow() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.moveToCurrentRow();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      moveToInsertRow()}.
     */
    public final void testMoveToInsertRow() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.moveToInsertRow();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      next()}.
     */
    public final void testNext() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.next();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      previous()}.
     */
    public final void testPrevious() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.previous();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      refreshRow()}.
     */
    public final void testRefreshRow() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.refreshRow();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      relative(int)}.
     */
    public final void testRelative() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.relative(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      rowDeleted()}.
     */
    public final void testRowDeleted() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.rowDeleted();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      rowInserted()}.
     */
    public final void testRowInserted() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.rowInserted();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      rowUpdated()}.
     */
    public final void testRowUpdated() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.rowUpdated();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      setFetchDirection(int)}.
     */
    public final void testSetFetchDirection() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.setFetchDirection(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      setFetchSize(int)}.
     */
    public final void testSetFetchSize() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.setFetchSize(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateArray(int, java.sql.Array)}.
     */
    public final void testUpdateArrayIntArray() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            testResultSet.updateArray(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateArray(java.lang.String, java.sql.Array)}.
     */
    public final void testUpdateArrayStringArray() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            testResultSet.updateArray(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateAsciiStream(int, java.io.InputStream, int)}.
     */
    public final void testUpdateAsciiStreamIntInputStreamInt() { //NOPMD
        try {
            final Object[] args = {0, null, 0};
            setArgs(args);
            testResultSet.updateAsciiStream(0, null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateAsciiStream(java.lang.String, java.io.InputStream, int)}.
     */
    public final void testUpdateAsciiStreamStringInputStreamInt() { //NOPMD
        try {
            final Object[] args = {null, null, 0};
            setArgs(args);
            testResultSet.updateAsciiStream(null, null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateBigDecimal(int, java.math.BigDecimal)}.
     */
    public final void testUpdateBigDecimalIntBigDecimal() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            testResultSet.updateBigDecimal(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateBigDecimal(java.lang.String, java.math.BigDecimal)}.
     */
    public final void testUpdateBigDecimalStringBigDecimal() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            testResultSet.updateBigDecimal(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     *  {@link com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateBinaryStream(int, java.io.InputStream, int)}.
     */
    public final void testUpdateBinaryStreamIntInputStreamInt() { //NOPMD
        try {
            final Object[] args = {0, null, 0};
            setArgs(args);
            testResultSet.updateBinaryStream(0, null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateBinaryStream(java.lang.String, java.io.InputStream, int)}.
     */
    public final void testUpdateBinaryStreamStringInputStreamInt() { //NOPMD
        try {
            final Object[] args = {null, null, 0};
            setArgs(args);
            testResultSet.updateBinaryStream(null, null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateBlob(int, java.sql.Blob)}.
     */
    public final void testUpdateBlobIntBlob() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            testResultSet.updateBlob(0, (Blob)null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateBlob(java.lang.String, java.sql.Blob)}.
     */
    public final void testUpdateBlobStringBlob() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            testResultSet.updateBlob(null, (Blob)null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateBoolean(int, boolean)}.
     */
    public final void testUpdateBooleanIntBoolean() { //NOPMD
        try {
            final Object[] args = {0, false};
            setArgs(args);
            testResultSet.updateBoolean(0, false);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateBoolean(java.lang.String, boolean)}.
     */
    public final void testUpdateBooleanStringBoolean() { //NOPMD
        try {
            final Object[] args = {null, false};
            setArgs(args);
            testResultSet.updateBoolean(null, false);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateByte(int, byte)}.
     */
    public final void testUpdateByteIntByte() { //NOPMD
        try {
            final Object[] args = {0, (byte) 0};
            setArgs(args);
            testResultSet.updateByte(0, (byte) 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateByte(java.lang.String, byte)}.
     */
    public final void testUpdateByteStringByte() { //NOPMD
        try {
            final Object[] args = {null, (byte) 0};
            setArgs(args);
            testResultSet.updateByte(null, (byte) 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateBytes(int, byte[])}.
     */
    public final void testUpdateBytesIntByteArray() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            testResultSet.updateBytes(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateBytes(java.lang.String, byte[])}.
     */
    public final void testUpdateBytesStringByteArray() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            testResultSet.updateBytes(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateCharacterStream(int, java.io.Reader, int)}.
     */
    public final void testUpdateCharacterStreamIntReaderInt() { //NOPMD
        try {
            final Object[] args = {0, null, 0};
            setArgs(args);
            testResultSet.updateCharacterStream(0, null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateCharacterStream(java.lang.String, java.io.Reader, int)}.
     */
    public final void testUpdateCharacterStreamStringReaderInt() { //NOPMD
        try {
            final Object[] args = {null, null, 0};
            setArgs(args);
            testResultSet.updateCharacterStream(null, null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateClob(int, java.sql.Clob)}.
     */
    public final void testUpdateClobIntClob() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            testResultSet.updateClob(0, (Clob)null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateClob(java.lang.String, java.sql.Clob)}.
     */
    public final void testUpdateClobStringClob() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            testResultSet.updateClob(null, (Clob)null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateDate(int, java.sql.Date)}.
     */
    public final void testUpdateDateIntDate() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            testResultSet.updateDate(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
      *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateDate(java.lang.String, java.sql.Date)}.
     */
    public final void testUpdateDateStringDate() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            testResultSet.updateDate(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateDouble(int, double)}.
     */
    public final void testUpdateDoubleIntDouble() { //NOPMD
        try {
            final Object[] args = {0, (double) 0.0};
            setArgs(args);
            testResultSet.updateDouble(0, (double) 0.0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateDouble(java.lang.String, double)}.
     */
    public final void testUpdateDoubleStringDouble() { //NOPMD
        try {
            final Object[] args = {null, (double) 0.0};
            setArgs(args);
            testResultSet.updateDouble(null, (double) 0.0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateFloat(int, float)}.
     */
    public final void testUpdateFloatIntFloat() { //NOPMD
        try {
            final Object[] args = {0, (float) 0.0};
            setArgs(args);
            testResultSet.updateFloat(0, (float) 0.0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateFloat(java.lang.String, float)}.
     */
    public final void testUpdateFloatStringFloat() { //NOPMD
        try {
            final Object[] args = {null, (float) 0.0};
            setArgs(args);
            testResultSet.updateFloat(null, (float) 0.0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateInt(int, int)}.
     */
    public final void testUpdateIntIntInt() { //NOPMD
        try {
            final Object[] args = {0, 0};
            setArgs(args);
            testResultSet.updateInt(0, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateInt(java.lang.String, int)}.
     */
    public final void testUpdateIntStringInt() { //NOPMD
        try {
            final Object[] args = {null, 0};
            setArgs(args);
            testResultSet.updateInt(null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateLong(int, long)}.
     */
    public final void testUpdateLongIntLong() { //NOPMD
        try {
            final Object[] args = {0, (long) 0};
            setArgs(args);
            testResultSet.updateLong(0, (long) 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateLong(java.lang.String, long)}.
     */
    public final void testUpdateLongStringLong() { //NOPMD
        try {
            final Object[] args = {null, (long) 0};
            setArgs(args);
            testResultSet.updateLong(null, (long) 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateNull(int)}.
     */
    public final void testUpdateNullInt() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testResultSet.updateNull(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateNull(java.lang.String)}.
     */
    public final void testUpdateNullString() { //NOPMD
        try {
            final Object[] args = {null};
            setArgs(args);
            testResultSet.updateNull(null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateObject(int, java.lang.Object)}.
     */
    public final void testUpdateObjectIntObject() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            testResultSet.updateObject(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateObject(java.lang.String, java.lang.Object)}.
     */
    public final void testUpdateObjectStringObject() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            testResultSet.updateObject(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateObject(int, java.lang.Object, int)}.
     */
    public final void testUpdateObjectIntObjectInt() { //NOPMD
        try {
            final Object[] args = {0, null, 0};
            setArgs(args);
            testResultSet.updateObject(0, null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateObject(java.lang.String, java.lang.Object, int)}.
     */
    public final void testUpdateObjectStringObjectInt() { //NOPMD
        try {
            final Object[] args = {null, null, 0};
            setArgs(args);
            testResultSet.updateObject(null, null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateRef(int, java.sql.Ref)}.
     */
    public final void testUpdateRefIntRef() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            testResultSet.updateRef(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateRef(java.lang.String, java.sql.Ref)}.
     */
    public final void testUpdateRefStringRef() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            testResultSet.updateRef(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateRow()}.
     */
    public final void testUpdateRow() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.updateRow();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateShort(int, short)}.
     */
    public final void testUpdateShortIntShort() { //NOPMD
        try {
            final Object[] args = {0, (short) 0};
            setArgs(args);
            testResultSet.updateShort(0, (short) 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateShort(java.lang.String, short)}.
     */
    public final void testUpdateShortStringShort() { //NOPMD
        try {
            final Object[] args = {null, (short) 0};
            setArgs(args);
            testResultSet.updateShort(null, (short) 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateString(int, java.lang.String)}.
     */
    public final void testUpdateStringIntString() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            testResultSet.updateString(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateString(java.lang.String, java.lang.String)}.
     */
    public final void testUpdateStringStringString() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            testResultSet.updateString(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateTime(int, java.sql.Time)}.
     */
    public final void testUpdateTimeIntTime() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            testResultSet.updateTime(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateTime(java.lang.String, java.sql.Time)}.
     */
    public final void testUpdateTimeStringTime() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            testResultSet.updateTime(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateTimestamp(int, java.sql.Timestamp)}.
     */
    public final void testUpdateTimestampIntTimestamp() { //NOPMD
        try {
            final Object[] args = {0, null};
            setArgs(args);
            testResultSet.updateTimestamp(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      updateTimestamp(java.lang.String, java.sql.Timestamp)}.
     */
    public final void testUpdateTimestampStringTimestamp() { //NOPMD
        try {
            final Object[] args = {null, null};
            setArgs(args);
            testResultSet.updateTimestamp(null, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ResultSetListener#
     *      wasNull()}.
     */
    public final void testWasNull() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testResultSet.wasNull();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }
}
