/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import junit.framework.TestCase;

/**
 * Simple test of DriverListener connect when connected using forName.
 */
public class DriverListenerNoSetupTest extends TestCase {

    /**
     * Holds oracle connection url under test.
     */
    static final String ORA_CONNECT_URL =
        "jdbc:oracle:thin:@localhost:1521:xe";

    /**
     * Test of using normal forName, DriverManager.getConnection when
     * no properties are set.
     */
    public final void testConnectNoProperties() {
        Connection conn = null;
        try {
            Class.forName(DriverListener.class.getName());
            conn = DriverManager.getConnection("jdbc:test:db", "hr",
                    "hr");
            fail("Connection should have failed");
        } catch (ClassNotFoundException e) {
            fail("Class should be found");
        } catch (SQLException e) {
            assertNull("Connection has failed", conn);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    fail("Problem closing connection");
                }
            }
        }
    }
}
