/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.Clob;
import java.sql.SQLException;

import com.facdatum.mock.jdbc.MockClob;

/**
 * Test of ClobListener.
 */
public class ClobListenerTest extends AbstractJDBCTestCase {

    /**
     * ClobListener under test.
     */
    protected ClobListener testClob = null; //NOPMD NOCS

    /**
     * Sets up the test.
     * @throws Exception if problem setting up test
     */
    protected void setUp() throws Exception { //NOPMD NOCS
        super.setUp();
        testClob = new ClobListener(new MockClob());
    }

    /**
     * Cleans up the test.
     * @throws Exception if problem cleaning up test
     */
    protected final void tearDown() throws Exception { //NOPMD
        super.tearDown();
        testClob = null;
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ClobListener#
     *      getAsciiStream()}.
     */
    public final void testGetAsciiStream() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testClob.getAsciiStream();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ClobListener#
     *      getCharacterStream()}.
     */
    public final void testGetCharacterStream() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testClob.getCharacterStream();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ClobListener#
     *      getSubString(long, int)}.
     */
    public final void testGetSubString() { //NOPMD
        try {
            final Object[] args = {Long.valueOf(0), 0};
            setArgs(args);
            testClob.getSubString(0, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ClobListener#length()}.
     */
    public final void testLength() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testClob.length();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ClobListener#position(
     *      java.lang.String, long)}.
     */
    public final void testPositionStringLong() { //NOPMD
        try {
            final Object[] args = {(String) null, Long.valueOf(0)};
            setArgs(args);
            testClob.position((String) null, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ClobListener#position(
     *      java.sql.Clob, long)}.
     */
    public final void testPositionClobLong() { //NOPMD
        try {
            final Object[] args = {(Clob) null, Long.valueOf(0)};
            setArgs(args);
            testClob.position((Clob) null, Long.valueOf(0));
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ClobListener#
     *      setAsciiStream(long)}.
     */
    public final void testSetAsciiStream() { //NOPMD
        try {
            final Object[] args = {Long.valueOf(0)};
            setArgs(args);
            testClob.setAsciiStream(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ClobListener#
     *      setCharacterStream(long)}.
     */
    public final void testSetCharacterStream() { //NOPMD
        try {
            final Object[] args = {Long.valueOf(0)};
            setArgs(args);
            testClob.setCharacterStream(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ClobListener#
     *      setString(long, java.lang.String)}.
     */
    public final void testSetStringLongString() { //NOPMD
        try {
            final Object[] args = {Long.valueOf(0), null};
            setArgs(args);
            testClob.setString(0, null);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ClobListener#
     *      setString(long, java.lang.String, int, int)}.
     */
    public final void testSetStringLongStringIntInt() { //NOPMD
        try {
            final Object[] args = {Long.valueOf(0), null, 0, 0};
            setArgs(args);
            testClob.setString(0, null, 0, 0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.ClobListener#
     *      truncate(long)}.
     */
    public final void testTruncate() { //NOPMD
        try {
            final Object[] args = {Long.valueOf(0)};
            setArgs(args);
            testClob.truncate(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }
}
