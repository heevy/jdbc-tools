/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.SQLException;

import com.facdatum.mock.jdbc.MockXAConnection;

/**
 * Test of XAConnectionListener.
 */
public class XAConnectionListenerTest extends PooledConnectionListenerTest {

    /**
     * Sets up the test.
     * @throws Exception if problem setting up test
     */
    protected void setUp() throws Exception { //NOPMD NOCS
        super.setUp();
        testConn = new XAConnectionListener(new MockXAConnection());
    }

    /**
     * Cleans up the test.
     * @throws Exception if problem cleaning up test
     */
    protected final void tearDown() throws Exception { //NOPMD
        super.tearDown();
    }

    /**
     * Test method for {@link
     *  com.facdatum.jdbc.listener.driver.XAConnectionListener#
     *      getXAResource()}.
     */
    public final void testGetXAResource() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            ((XAConnectionListener) testConn).getXAResource();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }
}
