/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.SQLException;

import com.facdatum.mock.jdbc.MockParameterMetaData;

/**
 * Test of ParameterMetaDataListener.
 */
public class ParameterMetaDataListenerTest extends AbstractJDBCTestCase {

    /**
     * ParameterMetaDataListener under test.
     */
    protected ParameterMetaDataListener testMetaData = null; //NOPMD NOCS

    /**
     * Sets up the test.
     * @throws Exception if problem setting up test
     */
    protected void setUp() throws Exception { //NOPMD NOCS
        testMetaData = new ParameterMetaDataListener(
                new MockParameterMetaData());
        super.setUp();
    }

    /**
     * Cleans up the test.
     * @throws Exception if problem cleaning up test
     */
    protected final void tearDown() throws Exception { //NOPMD
        super.tearDown();
        testMetaData = null;
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ParameterMetaDataListener
     * #getParameterClassName(int)}.
     */
    public final void testGetParameterClassName() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.getParameterClassName(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ParameterMetaDataListener
     * #getParameterCount()}.
     */
    public final void testGetParameterCount() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getParameterCount();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ParameterMetaDataListener
     * #getParameterMode(int)}.
     */
    public final void testGetParameterMode() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.getParameterMode(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ParameterMetaDataListener
     * #getParameterType(int)}.
     */
    public final void testGetParameterType() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.getParameterType(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ParameterMetaDataListener
     * #getParameterTypeName(int)}.
     */
    public final void testGetParameterTypeName() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.getParameterTypeName(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ParameterMetaDataListener
     * #getPrecision(int)}.
     */
    public final void testGetPrecision() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.getPrecision(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ParameterMetaDataListener
     * #getScale(int)}.
     */
    public final void testGetScale() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.getScale(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ParameterMetaDataListener
     * #isNullable(int)}.
     */
    public final void testIsNullable() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.isNullable(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ParameterMetaDataListener
     * #isSigned(int)}.
     */
    public final void testIsSigned() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.isSigned(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }
}
