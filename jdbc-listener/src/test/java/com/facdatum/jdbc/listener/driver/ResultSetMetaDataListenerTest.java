/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.SQLException;

import com.facdatum.mock.jdbc.MockResultSetMetaData;

/**
 * Test of ResultSetMetaDataListener.
 */
public class ResultSetMetaDataListenerTest extends AbstractJDBCTestCase {

    /**
     * ResultSetMetaDataListener under test.
     */
    protected ResultSetMetaDataListener testMetaData = null; //NOPMD NOCS

    /**
     * Sets up the test.
     * @throws Exception if problem setting up test
     */
    protected void setUp() throws Exception { //NOPMD NOCS
        testMetaData = new ResultSetMetaDataListener(
                new MockResultSetMetaData());
        super.setUp();
    }

    /**
     * Cleans up the test.
     * @throws Exception if problem cleaning up test
     */
    protected final void tearDown() throws Exception { //NOPMD
        super.tearDown();
        testMetaData = null;
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ResultSetMetaDataListener
     * #getCatalogName(int)}.
     */
    public final void testGetCatalogName() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.getCatalogName(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ResultSetMetaDataListener
     * #getColumnClassName(int)}.
     */
    public final void testGetColumnClassName() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.getColumnClassName(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ResultSetMetaDataListener
     * #getColumnCount()}.
     */
    public final void testGetColumnCount() { //NOPMD
        try {
            final Object[] args = {};
            setArgs(args);
            testMetaData.getColumnCount();
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ResultSetMetaDataListener
     * #getColumnDisplaySize(int)}.
     */
    public final void testGetColumnDisplaySize() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.getColumnDisplaySize(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ResultSetMetaDataListener
     * #getColumnLabel(int)}.
     */
    public final void testGetColumnLabel() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.getColumnLabel(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ResultSetMetaDataListener
     * #getColumnName(int)}.
     */
    public final void testGetColumnName() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.getColumnName(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ResultSetMetaDataListener
     * #getColumnType(int)}.
     */
    public final void testGetColumnType() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.getColumnType(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ResultSetMetaDataListener
     * #getColumnTypeName(int)}.
     */
    public final void testGetColumnTypeName() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.getColumnTypeName(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ResultSetMetaDataListener
     * #getPrecision(int)}.
     */
    public final void testGetPrecision() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.getPrecision(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ResultSetMetaDataListener
     * #getScale(int)}.
     */
    public final void testGetScale() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.getScale(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ResultSetMetaDataListener
     * #getSchemaName(int)}.
     */
    public final void testGetSchemaName() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.getSchemaName(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ResultSetMetaDataListener
     * #getTableName(int)}.
     */
    public final void testGetTableName() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.getTableName(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ResultSetMetaDataListener
     * #isAutoIncrement(int)}.
     */
    public final void testIsAutoIncrement() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.isAutoIncrement(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ResultSetMetaDataListener
     * #isCaseSensitive(int)}.
     */
    public final void testIsCaseSensitive() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.isCaseSensitive(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ResultSetMetaDataListener
     * #isCurrency(int)}.
     */
    public final void testIsCurrency() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.isCurrency(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ResultSetMetaDataListener
     * #isDefinitelyWritable(int)}.
     */
    public final void testIsDefinitelyWritable() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.isDefinitelyWritable(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ResultSetMetaDataListener
     * #isNullable(int)}.
     */
    public final void testIsNullable() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.isNullable(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ResultSetMetaDataListener
     * #isReadOnly(int)}.
     */
    public final void testIsReadOnly() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.isReadOnly(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ResultSetMetaDataListener
     * #isSearchable(int)}.
     */
    public final void testIsSearchable() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.isSearchable(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ResultSetMetaDataListener
     * #isSigned(int)}.
     */
    public final void testIsSigned() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.isSigned(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.listener.driver.ResultSetMetaDataListener
     * #isWritable(int)}.
     */
    public final void testIsWritable() { //NOPMD
        try {
            final Object[] args = {0};
            setArgs(args);
            testMetaData.isWritable(0);
            checkLog();
        } catch (SQLException e) {
            checkLog();
        }
    }
}
