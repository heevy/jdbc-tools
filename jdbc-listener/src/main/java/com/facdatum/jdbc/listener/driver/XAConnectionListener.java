/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.SQLException;

import javax.sql.PooledConnection;
import javax.sql.XAConnection;
import javax.transaction.xa.XAResource;

import com.facdatum.jdbc.listener.JDBCCallListener;


/**
 * XAConnectionListener wraps around a delegate XAConnection object
 * and passes all calls to the listener.
 */
public class XAConnectionListener extends PooledConnectionListener implements
        XAConnection {

    /**
     * Creates a new XAConnectionListener that wraps around the specified
     * delegate connection.
     * @param conn the delegate connection
     */
    public XAConnectionListener(final XAConnection conn) {
        super((PooledConnection) conn);
    }

    /**
     * {@inheritDoc}
     * @see javax.sql.XAConnection#getXAResource()
     */
    public final XAResource getXAResource() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        XAResource result = null;
        try {
            if (delegateConn != null) {
                result = ((XAConnection) delegateConn).getXAResource();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }
}
