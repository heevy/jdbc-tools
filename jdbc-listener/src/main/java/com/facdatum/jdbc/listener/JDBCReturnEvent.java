/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener;

/**
 * A JDBCReturnEvent holds the information related to the return from the JDBC
 * layer that is being listened to.
 */
public class JDBCReturnEvent extends JDBCEvent implements ListenerReturnEvent {

    /**
     * Holds the result obtained from the JDBC method call.
     */
    private final Object result; //NOPMD

    /**
     * Holds the wrapped result obtained from the JDBC method call.
     */
    private final Object wrappedResult; //NOPMD

    /**
     * Holds the duration of the JDBC method call.
     */
    private final long duration; //NOPMD

    /**
     * Creates a JDBCReturnEvent.
     * @param eventTime the time of the event
     * @param callObject the object called in JDBC Layer
     * @param callStackTrace the stack trace of calls into the JDBC Layer
     * @param callResult the result from the call to the JDBC method
     * @param callDuration the duration of the JDBC method call
     * @param callWrappedResult the wrapped result from the call to JDBC method
     */
    public JDBCReturnEvent(final long eventTime, final Object callObject,
            final StackTraceElement[] callStackTrace, final Object callResult,
            final long callDuration, final Object callWrappedResult) {
        super(eventTime, callObject, callStackTrace);
        this.result = callResult;
        this.duration = callDuration;
        this.wrappedResult = callWrappedResult;
    }

    /**
     * Returns the result of the call to the JDBC method.
     * @return the return value
     */
    public final Object getResult() {
        return this.result;
    }

    /**
     * Returns the duration of the JDBC call.
     * @return the call duration
     */
    public final long getDuration() {
        return this.duration;
    }

    /**
     * Returns the wrapped result of the call to the JDBC method.
     * @return the wrapped return value
     */
    public final Object getWrappedResult() {
        return this.wrappedResult;
    }
}
