/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver; //NOPMD

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCDelegator;

/**
 * PreparedStatementListener wraps around a delegate PreparedStatement object
 * and passes all calls to the listener.
 */
public class PreparedStatementListener extends StatementListener // NOPMD
		implements PreparedStatement, JDBCDelegator {

	/**
	 * Creates a new PreparedStatementListener that wraps around the specified
	 * delegate prepared statement.
	 * 
	 * @param preparedStatement
	 *            the delegate prepared statement
	 */
	PreparedStatementListener(final PreparedStatement preparedStatement) {
		super((Statement) preparedStatement);
	}

	/**
	 * Sets the delegate prepared statement for the listener prepared statement.
	 * 
	 * @param statement
	 *            the delegate prepared statement
	 */
	public final void setDelegateStatement(final PreparedStatement statement) {
		this.delegateStatement = (Statement) statement;
	}

	/**
	 * @return the delegatePreparedStatement
	 */
	public PreparedStatement getDelegateStatement() { // NOCS
		return (PreparedStatement) delegateStatement;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#addBatch()
	 */
	public final void addBatch() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).addBatch();
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#clearParameters()
	 */
	public final void clearParameters() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).clearParameters();
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#execute()
	 */
	public final boolean execute() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		boolean result = false;
		try {
			if (delegateStatement != null) {
				result = ((PreparedStatement) delegateStatement).execute();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#executeQuery()
	 */
	public final ResultSet executeQuery() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		ResultSetListener result = null; // NOPMD
		ResultSet delegateResult = null; // NOPMD
		try {
			if (delegateStatement != null) {
				delegateResult = ((PreparedStatement) delegateStatement).executeQuery();
				result = new ResultSetListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#executeUpdate()
	 */
	public final int executeUpdate() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		int result = 0;
		try {
			if (delegateStatement != null) {
				result = ((PreparedStatement) delegateStatement).executeUpdate();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#getMetaData()
	 */
	public final ResultSetMetaData getMetaData() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		ResultSetMetaDataListener result = null;
		ResultSetMetaData delegateResult = null;
		try {
			if (delegateStatement != null) {
				delegateResult = ((PreparedStatement) delegateStatement).getMetaData();
				result = new ResultSetMetaDataListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#getParameterMetaData()
	 */
	public final ParameterMetaData getParameterMetaData() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		ParameterMetaDataListener result = null;
		ParameterMetaData delegateResult = null;
		try {
			if (delegateStatement != null) {
				delegateResult = ((PreparedStatement) delegateStatement).getParameterMetaData();
				result = new ParameterMetaDataListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setArray(int, java.sql.Array)
	 */
	public final void setArray(final int parameterIndex, final Array val) throws SQLException {
		Array wrappedArray = null;
		if (val != null) {
			wrappedArray = new ArrayListener(val);
		}
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setArray(parameterIndex, wrappedArray);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setAsciiStream(int, java.io.InputStream, int)
	 */
	public final void setAsciiStream(final int parameterIndex, final InputStream stream, final int length) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, stream, length });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setAsciiStream(parameterIndex, stream, length);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setBigDecimal(int, java.math.BigDecimal)
	 */
	public final void setBigDecimal(final int parameterIndex, final BigDecimal val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setBigDecimal(parameterIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setBinaryStream(int, java.io.InputStream, int)
	 */
	public final void setBinaryStream(final int parameterIndex, final InputStream stream, final int length) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, stream, length });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setBinaryStream(parameterIndex, stream, length);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setBlob(int, java.sql.Blob)
	 */
	public final void setBlob(final int parameterIndex, final Blob val) throws SQLException {
		Blob wrappedBlob = null;
		if (val != null) {
			wrappedBlob = new BlobListener(val);
		}
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setBlob(parameterIndex, wrappedBlob);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setBoolean(int, boolean)
	 */
	public final void setBoolean(final int parameterIndex, final boolean val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setBoolean(parameterIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setByte(int, byte)
	 */
	public final void setByte(final int parameterIndex, final byte val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setByte(parameterIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setBytes(int, byte[])
	 */
	public final void setBytes(final int parameterIndex, final byte[] val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setBytes(parameterIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setCharacterStream(int, java.io.Reader, int)
	 */
	public final void setCharacterStream(final int parameterIndex, final Reader reader, final int length) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, reader, length });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setCharacterStream(parameterIndex, reader, length);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setClob(int, java.sql.Clob)
	 */
	public final void setClob(final int parameterIndex, final Clob val) throws SQLException {
		Clob wrappedClob = null;
		if (val != null) {
			wrappedClob = new ClobListener(val);
		}
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setClob(parameterIndex, wrappedClob);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setDate(int, java.sql.Date)
	 */
	public final void setDate(final int parameterIndex, final Date val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setDate(parameterIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setDate(int, java.sql.Date, java.util.Calendar)
	 */
	public final void setDate(final int parameterIndex, final Date val, final Calendar cal) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val, cal });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setDate(parameterIndex, val, cal);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setDouble(int, double)
	 */
	public final void setDouble(final int parameterIndex, final double val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setDouble(parameterIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setFloat(int, float)
	 */
	public final void setFloat(final int parameterIndex, final float val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setFloat(parameterIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setInt(int, int)
	 */
	public final void setInt(final int parameterIndex, final int val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setInt(parameterIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setLong(int, long)
	 */
	public final void setLong(final int parameterIndex, final long val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setLong(parameterIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setNull(int, int)
	 */
	public final void setNull(final int parameterIndex, final int sqlType) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, sqlType });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setNull(parameterIndex, sqlType);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setNull(int, int, java.lang.String)
	 */
	public final void setNull(final int paramIndex, final int sqlType, final String typeName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { paramIndex, sqlType, typeName });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setNull(paramIndex, sqlType, typeName);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setObject(int, java.lang.Object)
	 */
	public final void setObject(final int parameterIndex, final Object val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setObject(parameterIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setObject(int, java.lang.Object, int)
	 */
	public final void setObject(final int parameterIndex, final Object val, final int targetSqlType) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val, targetSqlType });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setObject(parameterIndex, val, targetSqlType);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setObject(int, java.lang.Object, int, int)
	 */
	public final void setObject(final int parameterIndex, final Object val, final int targetSqlType, final int scale) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val, targetSqlType, scale });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setObject(parameterIndex, val, targetSqlType, scale);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setRef(int, java.sql.Ref)
	 */
	public final void setRef(final int parameterIndex, final Ref val) throws SQLException {
		Ref wrappedRef = null;
		if (val != null) {
			wrappedRef = new RefListener(val);
		}
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setRef(parameterIndex, wrappedRef);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setShort(int, short)
	 */
	public final void setShort(final int parameterIndex, final short val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setShort(parameterIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setString(int, java.lang.String)
	 */
	public final void setString(final int parameterIndex, final String val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setString(parameterIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setTime(int, java.sql.Time)
	 */
	public final void setTime(final int parameterIndex, final Time val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setTime(parameterIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setTime(int, java.sql.Time, java.util.Calendar)
	 */
	public final void setTime(final int parameterIndex, final Time val, final Calendar cal) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val, cal });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setTime(parameterIndex, val, cal);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setTimestamp(int, java.sql.Timestamp)
	 */
	public final void setTimestamp(final int parameterIndex, final Timestamp val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setTimestamp(parameterIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setTimestamp(int, java.sql.Timestamp, java.util.Calendar)
	 */
	public final void setTimestamp(final int parameterIndex, final Timestamp val, final Calendar cal) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val, cal });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setTimestamp(parameterIndex, val, cal);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setURL(int, java.net.URL)
	 */
	public final void setURL(final int parameterIndex, final URL val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setURL(parameterIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.PreparedStatement#setUnicodeStream(int, java.io.InputStream, int)
	 */
	@SuppressWarnings("deprecation")
	public final void setUnicodeStream(final int parameterIndex, final InputStream val, final int length) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, val, length });
		final Object result = null;
		try {
			((PreparedStatement) delegateStatement).setUnicodeStream(parameterIndex, val, length);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	// TODO Joudek implement correctly

	@Override
	public void setRowId(int parameterIndex, RowId x) throws SQLException {
		if (delegateStatement != null) {
			((PreparedStatement) delegateStatement).setRowId(parameterIndex, x);
		}

	}

	@Override
	public void setNString(int parameterIndex, String value) throws SQLException {
		if (delegateStatement != null) {
			((PreparedStatement) delegateStatement).setNString(parameterIndex, value);
		}

	}

	@Override
	public void setNCharacterStream(int parameterIndex, Reader value, long length) throws SQLException {
		if (delegateStatement != null) {
			((PreparedStatement) delegateStatement).setNCharacterStream(parameterIndex, value, length);
		}

	}

	@Override
	public void setNClob(int parameterIndex, NClob value) throws SQLException {
		if (delegateStatement != null) {
			((PreparedStatement) delegateStatement).setNClob(parameterIndex, value);
		}

	}

	@Override
	public void setClob(int parameterIndex, Reader reader, long length) throws SQLException {
		if (delegateStatement != null) {
			((PreparedStatement) delegateStatement).setClob(parameterIndex, reader, length);
		}

	}

	@Override
	public void setBlob(int parameterIndex, InputStream inputStream, long length) throws SQLException {
		if (delegateStatement != null) {
			((PreparedStatement) delegateStatement).setBlob(parameterIndex, inputStream, length);
		}

	}

	@Override
	public void setNClob(int parameterIndex, Reader reader, long length) throws SQLException {
		if (delegateStatement != null) {
			((PreparedStatement) delegateStatement).setNClob(parameterIndex, reader, length);
		}

	}

	@Override
	public void setSQLXML(int parameterIndex, SQLXML xmlObject) throws SQLException {
		if (delegateStatement != null) {
			((PreparedStatement) delegateStatement).setSQLXML(parameterIndex, xmlObject);
		}

	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x, long length) throws SQLException {
		if (delegateStatement != null) {
			((PreparedStatement) delegateStatement).setAsciiStream(parameterIndex, x, length);
		}

	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x, long length) throws SQLException {
		if (delegateStatement != null) {
			((PreparedStatement) delegateStatement).setBinaryStream(parameterIndex, x, length);
		}

	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader, long length) throws SQLException {
		if (delegateStatement != null) {
			((PreparedStatement) delegateStatement).setCharacterStream(parameterIndex, reader, length);
		}

	}

	@Override
	public void setAsciiStream(int parameterIndex, InputStream x) throws SQLException {
		if (delegateStatement != null) {
			((PreparedStatement) delegateStatement).setAsciiStream(parameterIndex, x);
		}

	}

	@Override
	public void setBinaryStream(int parameterIndex, InputStream x) throws SQLException {
		if (delegateStatement != null) {
			((PreparedStatement) delegateStatement).setBinaryStream(parameterIndex, x);
		}

	}

	@Override
	public void setCharacterStream(int parameterIndex, Reader reader) throws SQLException {
		if (delegateStatement != null) {
			((PreparedStatement) delegateStatement).setCharacterStream(parameterIndex, reader);
		}

	}

	@Override
	public void setNCharacterStream(int parameterIndex, Reader value) throws SQLException {
		if (delegateStatement != null) {
			((PreparedStatement) delegateStatement).setNCharacterStream(parameterIndex, value);
		}

	}

	@Override
	public void setClob(int parameterIndex, Reader reader) throws SQLException {
		if (delegateStatement != null) {
			((PreparedStatement) delegateStatement).setClob(parameterIndex, reader);
		}

	}

	@Override
	public void setBlob(int parameterIndex, InputStream inputStream) throws SQLException {
		if (delegateStatement != null) {
			((PreparedStatement) delegateStatement).setBlob(parameterIndex, inputStream);
		}

	}

	@Override
	public void setNClob(int parameterIndex, Reader reader) throws SQLException {
		if (delegateStatement != null) {
			((PreparedStatement) delegateStatement).setNClob(parameterIndex, reader);
		}

	}
}
