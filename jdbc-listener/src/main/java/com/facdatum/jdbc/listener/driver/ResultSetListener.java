//NOCS (for file length check)
/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver; //NOPMD

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.NClob;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Map;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCDelegator;

/**
 * ResultSetListener wraps around a delegate ResultSet object
 * and passes all calls to the listener.
 */
public class ResultSetListener implements ResultSet, JDBCDelegator { // NOPMD
	/**
	 * Holds the delegate resultset that calls are passed through to.
	 */
	private ResultSet delegateResultSet;

	/**
	 * Creates a new ResultSetListener that wraps around the specified
	 * delegate resultset.
	 * 
	 * @param resultSet
	 *            the delegate resultset
	 */
	ResultSetListener(final ResultSet resultSet) {
		this.delegateResultSet = resultSet;
	}

	/**
	 * Sets the delegate resultset for the listener resultset.
	 * 
	 * @param resultSet
	 *            the delegate resultset
	 */
	public final void setDelegateResultSet(final ResultSet resultSet) {
		this.delegateResultSet = resultSet;
	}

	/**
	 * Gets the delegate resultset for the listener resultset.
	 * 
	 * @return the delegateResultSet
	 */
	public final ResultSet getDelegateResultSet() {
		return delegateResultSet;
	}

	/**
	 * Returns the delegate class name.
	 * 
	 * @return the delegate class name
	 */
	public final String getDelegateClassName() {
		return delegateResultSet.getClass().getName();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#absolute(int)
	 */
	public final boolean absolute(final int row) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { row });
		boolean result = false;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.absolute(row);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#afterLast()
	 */
	public final void afterLast() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		final Object result = null;
		try {
			delegateResultSet.afterLast();
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#beforeFirst()
	 */
	public final void beforeFirst() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		final Object result = null;
		try {
			delegateResultSet.beforeFirst();
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#cancelRowUpdates()
	 */
	public final void cancelRowUpdates() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		final Object result = null;
		try {
			delegateResultSet.cancelRowUpdates();
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#clearWarnings()
	 */
	public final void clearWarnings() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		final Object result = null;
		try {
			delegateResultSet.clearWarnings();
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#close()
	 */
	public final void close() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		final Object result = null;
		try {
			delegateResultSet.close();
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#deleteRow()
	 */
	public final void deleteRow() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		final Object result = null;
		try {
			delegateResultSet.deleteRow();
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#findColumn(java.lang.String)
	 */
	public final int findColumn(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		int result = 0;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.findColumn(columnName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#first()
	 */
	public final boolean first() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		boolean result = false;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.first();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getArray(int)
	 */
	public final Array getArray(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		ArrayListener result = null;
		Array delegateResult = null;
		try {
			if (delegateResultSet != null) {
				delegateResult = delegateResultSet.getArray(columnIndex);
				result = new ArrayListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getArray(java.lang.String)
	 */
	public final Array getArray(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		ArrayListener result = null;
		Array delegateResult = null;
		try {
			if (delegateResultSet != null) {
				delegateResult = delegateResultSet.getArray(columnName);
				result = new ArrayListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getAsciiStream(int)
	 */
	public final InputStream getAsciiStream(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		InputStream result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getAsciiStream(columnIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getAsciiStream(java.lang.String)
	 */
	public final InputStream getAsciiStream(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		InputStream result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getAsciiStream(columnName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getBigDecimal(int)
	 */
	public final BigDecimal getBigDecimal(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		BigDecimal result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getBigDecimal(columnIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getBigDecimal(java.lang.String)
	 */
	public final BigDecimal getBigDecimal(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		BigDecimal result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getBigDecimal(columnName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getBigDecimal(int, int)
	 */
	@SuppressWarnings("deprecation")
	public final// NOPMD
	BigDecimal getBigDecimal(final int columnIndex, final int scale) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, scale });
		BigDecimal result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getBigDecimal(columnIndex, scale);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getBigDecimal(java.lang.String, int)
	 */
	@SuppressWarnings("deprecation")
	public final BigDecimal getBigDecimal(final String columnName, final int scale) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, scale });
		BigDecimal result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getBigDecimal(columnName, scale);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getBinaryStream(int)
	 */
	public final InputStream getBinaryStream(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		InputStream result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getBinaryStream(columnIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getBinaryStream(java.lang.String)
	 */
	public final InputStream getBinaryStream(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		InputStream result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getBinaryStream(columnName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getBlob(int)
	 */
	public final Blob getBlob(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		BlobListener result = null;
		Blob delegateResult = null;
		try {
			if (delegateResultSet != null) {
				delegateResult = delegateResultSet.getBlob(columnIndex);
				result = new BlobListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getBlob(java.lang.String)
	 */
	public final Blob getBlob(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		BlobListener result = null;
		Blob delegateResult = null;
		try {
			if (delegateResultSet != null) {
				delegateResult = delegateResultSet.getBlob(columnName);
				result = new BlobListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getBoolean(int)
	 */
	public final boolean getBoolean(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		boolean result = false;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getBoolean(columnIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getBoolean(java.lang.String)
	 */
	public final boolean getBoolean(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		boolean result = false;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getBoolean(columnName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getByte(int)
	 */
	public final byte getByte(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		byte result = 0;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getByte(columnIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getByte(java.lang.String)
	 */
	public final byte getByte(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		byte result = 0;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getByte(columnName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getBytes(int)
	 */
	public final byte[] getBytes(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		byte[] result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getBytes(columnIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getBytes(java.lang.String)
	 */
	public final byte[] getBytes(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		byte[] result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getBytes(columnName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getCharacterStream(int)
	 */
	public final Reader getCharacterStream(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		Reader result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getCharacterStream(columnIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getCharacterStream(java.lang.String)
	 */
	public final Reader getCharacterStream(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		Reader result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getCharacterStream(columnName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getClob(int)
	 */
	public final Clob getClob(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		ClobListener result = null;
		Clob delegateResult = null;
		try {
			if (delegateResultSet != null) {
				delegateResult = delegateResultSet.getClob(columnIndex);
				result = new ClobListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getClob(java.lang.String)
	 */
	public final Clob getClob(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		ClobListener result = null;
		Clob delegateResult = null;
		try {
			if (delegateResultSet != null) {
				delegateResult = delegateResultSet.getClob(columnName);
				result = new ClobListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getConcurrency()
	 */
	public final int getConcurrency() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		int result = 0;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getConcurrency();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getCursorName()
	 */
	public final String getCursorName() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		String result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getCursorName();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getDate(int)
	 */
	public final Date getDate(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		Date result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getDate(columnIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getDate(java.lang.String)
	 */
	public final Date getDate(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		Date result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getDate(columnName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getDate(int, java.util.Calendar)
	 */
	public final Date getDate(final int columnIndex, final Calendar cal) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, cal });
		Date result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getDate(columnIndex, cal);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getDate(java.lang.String, java.util.Calendar)
	 */
	public final Date getDate(final String columnName, final Calendar cal) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, cal });
		Date result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getDate(columnName, cal);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getDouble(int)
	 */
	public final double getDouble(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		double result = 0;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getDouble(columnIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getDouble(java.lang.String)
	 */
	public final double getDouble(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		double result = 0;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getDouble(columnName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getFetchDirection()
	 */
	public final int getFetchDirection() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		int result = 0;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getFetchDirection();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getFetchSize()
	 */
	public final int getFetchSize() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		int result = 0;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getFetchSize();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getFloat(int)
	 */
	public final float getFloat(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		float result = 0;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getFloat(columnIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getFloat(java.lang.String)
	 */
	public final float getFloat(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		float result = 0;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getFloat(columnName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getInt(int)
	 */
	public final int getInt(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		int result = 0;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getInt(columnIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getInt(java.lang.String)
	 */
	public final int getInt(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		int result = 0;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getInt(columnName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getLong(int)
	 */
	public final long getLong(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		long result = 0;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getLong(columnIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getLong(java.lang.String)
	 */
	public final long getLong(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		long result = 0;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getLong(columnName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getMetaData()
	 */
	public final ResultSetMetaData getMetaData() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		ResultSetMetaDataListener result = null;
		ResultSetMetaData delegateResult = null;
		try {
			if (delegateResultSet != null) {
				delegateResult = delegateResultSet.getMetaData();
				result = new ResultSetMetaDataListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getObject(int)
	 */
	public final Object getObject(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		Object result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getObject(columnIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getObject(java.lang.String)
	 */
	public final Object getObject(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		Object result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getObject(columnName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getObject(int, java.util.Map)
	 */
	public final Object getObject(final int columnIndex, final Map<String, Class<?>> map) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, map });
		Object result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getObject(columnIndex, map);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getObject(java.lang.String, java.util.Map)
	 */
	public final Object getObject(final String columnName, final Map<String, Class<?>> map) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, map });
		Object result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getObject(columnName, map);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getRef(int)
	 */
	public final Ref getRef(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		RefListener result = null;
		Ref delegateResult = null;
		try {
			if (delegateResultSet != null) {
				delegateResult = delegateResultSet.getRef(columnIndex);
				result = new RefListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getRef(java.lang.String)
	 */
	public final Ref getRef(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		RefListener result = null;
		Ref delegateResult = null;
		try {
			if (delegateResultSet != null) {
				delegateResult = delegateResultSet.getRef(columnName);
				result = new RefListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getRow()
	 */
	public final int getRow() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		int result = 0;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getRow();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getShort(int)
	 */
	public final short getShort(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		short result = 0;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getShort(columnIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getShort(java.lang.String)
	 */
	public final short getShort(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		short result = 0;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getShort(columnName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getStatement()
	 */
	public final Statement getStatement() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		StatementListener result = null; // NOPMD
		Statement delegateResult = null; // NOPMD
		try {
			if (delegateResultSet != null) {
				delegateResult = delegateResultSet.getStatement();
				result = new StatementListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getString(int)
	 */
	public final String getString(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		String result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getString(columnIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getString(java.lang.String)
	 */
	public final String getString(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		String result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getString(columnName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getTime(int)
	 */
	public final Time getTime(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		Time result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getTime(columnIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getTime(java.lang.String)
	 */
	public final Time getTime(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		Time result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getTime(columnName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getTime(int, java.util.Calendar)
	 */
	public final Time getTime(final int columnIndex, final Calendar cal) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, cal });
		Time result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getTime(columnIndex, cal);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getTime(java.lang.String, java.util.Calendar)
	 */
	public final Time getTime(final String columnName, final Calendar cal) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, cal });
		Time result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getTime(columnName, cal);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getTimestamp(int)
	 */
	public final Timestamp getTimestamp(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		Timestamp result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getTimestamp(columnIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getTimestamp(java.lang.String)
	 */
	public final Timestamp getTimestamp(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		Timestamp result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getTimestamp(columnName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getTimestamp(int, java.util.Calendar)
	 */
	public final Timestamp getTimestamp(final int columnIndex, final Calendar cal) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, cal });
		Timestamp result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getTimestamp(columnIndex, cal);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getTimestamp(java.lang.String, java.util.Calendar)
	 */
	public final Timestamp getTimestamp(final String columnName, final Calendar cal) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, cal });
		Timestamp result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getTimestamp(columnName, cal);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getType()
	 */
	public final int getType() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		int result = 0;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getType();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getURL(int)
	 */
	public final URL getURL(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		URL result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getURL(columnIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getURL(java.lang.String)
	 */
	public final URL getURL(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		URL result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getURL(columnName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getUnicodeStream(int)
	 */
	@SuppressWarnings("deprecation")
	public final InputStream getUnicodeStream(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		InputStream result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getUnicodeStream(columnIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getUnicodeStream(java.lang.String)
	 */
	@SuppressWarnings("deprecation")
	public final InputStream getUnicodeStream(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		InputStream result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getUnicodeStream(columnName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#getWarnings()
	 */
	public final SQLWarning getWarnings() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		SQLWarning result = null;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.getWarnings();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#insertRow()
	 */
	public final void insertRow() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		final Object result = null;
		try {
			delegateResultSet.insertRow();
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#isAfterLast()
	 */
	public final boolean isAfterLast() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		boolean result = false;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.isAfterLast();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#isBeforeFirst()
	 */
	public final boolean isBeforeFirst() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		boolean result = false;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.isBeforeFirst();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#isFirst()
	 */
	public final boolean isFirst() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		boolean result = false;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.isFirst();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#isLast()
	 */
	public final boolean isLast() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		boolean result = false;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.isLast();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#last()
	 */
	public final boolean last() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		boolean result = false;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.last();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#moveToCurrentRow()
	 */
	public final void moveToCurrentRow() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		final Object result = null;
		try {
			delegateResultSet.moveToCurrentRow();
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#moveToInsertRow()
	 */
	public final void moveToInsertRow() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		final Object result = null;
		try {
			delegateResultSet.moveToInsertRow();
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#next()
	 */
	public final boolean next() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		boolean result = false;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.next();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#previous()
	 */
	public final boolean previous() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		boolean result = false;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.previous();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#refreshRow()
	 */
	public final void refreshRow() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		final Object result = null;
		try {
			delegateResultSet.refreshRow();
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#relative(int)
	 */
	public final boolean relative(final int rows) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { rows });
		boolean result = false;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.relative(rows);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#rowDeleted()
	 */
	public final boolean rowDeleted() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		boolean result = false;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.rowDeleted();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#rowInserted()
	 */
	public final boolean rowInserted() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		boolean result = false;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.rowInserted();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#rowUpdated()
	 */
	public final boolean rowUpdated() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		boolean result = false;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.rowUpdated();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#setFetchDirection(int)
	 */
	public final void setFetchDirection(final int direction) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { direction });
		final Object result = null;
		try {
			delegateResultSet.setFetchDirection(direction);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#setFetchSize(int)
	 */
	public final void setFetchSize(final int rows) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { rows });
		final Object result = null;
		try {
			delegateResultSet.setFetchSize(rows);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateArray(int, java.sql.Array)
	 */
	public final void updateArray(final int columnIndex, final Array val) throws SQLException {
		Array wrappedArray = null;
		if (val != null) {
			wrappedArray = new ArrayListener(val);
		}
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val });
		final Object result = null;
		try {
			delegateResultSet.updateArray(columnIndex, wrappedArray);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateArray(java.lang.String, java.sql.Array)
	 */
	public final void updateArray(final String columnName, final Array val) throws SQLException {
		Array wrappedArray = null;
		if (val != null) {
			wrappedArray = new ArrayListener(val);
		}
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, val });
		final Object result = null;
		try {
			delegateResultSet.updateArray(columnName, wrappedArray);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateAsciiStream(int, java.io.InputStream, int)
	 */
	public final void updateAsciiStream(final int columnIndex, final InputStream val, final int length) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val, length });
		final Object result = null;
		try {
			delegateResultSet.updateAsciiStream(columnIndex, val, length);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateAsciiStream(java.lang.String, java.io.InputStream, int)
	 */
	public final void updateAsciiStream(final String columnName, final InputStream val, final int length) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, val, length });
		final Object result = null;
		try {
			delegateResultSet.updateAsciiStream(columnName, val, length);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateBigDecimal(int, java.math.BigDecimal)
	 */
	public final void updateBigDecimal(final int columnIndex, final BigDecimal val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val });
		final Object result = null;
		try {
			delegateResultSet.updateBigDecimal(columnIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateBigDecimal(java.lang.String, java.math.BigDecimal)
	 */
	public final void updateBigDecimal(final String columnName, final BigDecimal val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, val });
		final Object result = null;
		try {
			delegateResultSet.updateBigDecimal(columnName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateBinaryStream(int, java.io.InputStream, int)
	 */
	public final void updateBinaryStream(final int columnIndex, final InputStream val, final int length) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val, length });
		final Object result = null;
		try {
			delegateResultSet.updateBinaryStream(columnIndex, val, length);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateBinaryStream(java.lang.String, java.io.InputStream, int)
	 */
	public final void updateBinaryStream(final String columnName, final InputStream val, final int length) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, val, length });
		final Object result = null;
		try {
			delegateResultSet.updateBinaryStream(columnName, val, length);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateBlob(int, java.sql.Blob)
	 */
	public final void updateBlob(final int columnIndex, final Blob val) throws SQLException {
		Blob wrappedBlob = null;
		if (val != null) {
			wrappedBlob = new BlobListener(val);
		}
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val });
		final Object result = null;
		try {
			delegateResultSet.updateBlob(columnIndex, wrappedBlob);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateBlob(java.lang.String, java.sql.Blob)
	 */
	public final void updateBlob(final String columnName, final Blob val) throws SQLException {
		Blob wrappedBlob = null;
		if (val != null) {
			wrappedBlob = new BlobListener(val);
		}
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, val });
		final Object result = null;
		try {
			delegateResultSet.updateBlob(columnName, wrappedBlob);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateBoolean(int, boolean)
	 */
	public final void updateBoolean(final int columnIndex, final boolean val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val });
		final Object result = null;
		try {
			delegateResultSet.updateBoolean(columnIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateBoolean(java.lang.String, boolean)
	 */
	public final void updateBoolean(final String columnName, final boolean val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, val });
		final Object result = null;
		try {
			delegateResultSet.updateBoolean(columnName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateByte(int, byte)
	 */
	public final void updateByte(final int columnIndex, final byte val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val });
		final Object result = null;
		try {
			delegateResultSet.updateByte(columnIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateByte(java.lang.String, byte)
	 */
	public final void updateByte(final String columnName, final byte val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, val });
		final Object result = null;
		try {
			delegateResultSet.updateByte(columnName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateBytes(int, byte[])
	 */
	public final void updateBytes(final int columnIndex, final byte[] val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val });
		final Object result = null;
		try {
			delegateResultSet.updateBytes(columnIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateBytes(java.lang.String, byte[])
	 */
	public final void updateBytes(final String columnName, final byte[] val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, val });
		final Object result = null;
		try {
			delegateResultSet.updateBytes(columnName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateCharacterStream(int, java.io.Reader, int)
	 */
	public final void updateCharacterStream(final int columnIndex, final Reader val, final int length) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val, length });
		final Object result = null;
		try {
			delegateResultSet.updateCharacterStream(columnIndex, val, length);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateCharacterStream(java.lang.String, java.io.Reader, int)
	 */
	public final void updateCharacterStream(final String columnName, final Reader reader, final int length) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, reader, length });
		final Object result = null;
		try {
			delegateResultSet.updateCharacterStream(columnName, reader, length);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateClob(int, java.sql.Clob)
	 */
	public final void updateClob(final int columnIndex, final Clob val) throws SQLException {
		Clob wrappedClob = null;
		if (val != null) {
			wrappedClob = new ClobListener(val);
		}
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val });
		final Object result = null;
		try {
			delegateResultSet.updateClob(columnIndex, wrappedClob);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateClob(java.lang.String, java.sql.Clob)
	 */
	public final void updateClob(final String columnName, final Clob val) throws SQLException {
		Clob wrappedClob = null;
		if (val != null) {
			wrappedClob = new ClobListener(val);
		}
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, val });
		final Object result = null;
		try {
			delegateResultSet.updateClob(columnName, wrappedClob);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateDate(int, java.sql.Date)
	 */
	public final void updateDate(final int columnIndex, final Date val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val });
		final Object result = null;
		try {
			delegateResultSet.updateDate(columnIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateDate(java.lang.String, java.sql.Date)
	 */
	public final void updateDate(final String columnName, final Date val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, val });
		final Object result = null;
		try {
			delegateResultSet.updateDate(columnName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateDouble(int, double)
	 */
	public final void updateDouble(final int columnIndex, final double val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val });
		final Object result = null;
		try {
			delegateResultSet.updateDouble(columnIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateDouble(java.lang.String, double)
	 */
	public final void updateDouble(final String columnName, final double val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, val });
		final Object result = null;
		try {
			delegateResultSet.updateDouble(columnName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateFloat(int, float)
	 */
	public final void updateFloat(final int columnIndex, final float val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val });
		final Object result = null;
		try {
			delegateResultSet.updateFloat(columnIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateFloat(java.lang.String, float)
	 */
	public final void updateFloat(final String columnName, final float val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, val });
		final Object result = null;
		try {
			delegateResultSet.updateFloat(columnName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateInt(int, int)
	 */
	public final void updateInt(final int columnIndex, final int val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val });
		final Object result = null;
		try {
			delegateResultSet.updateInt(columnIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateInt(java.lang.String, int)
	 */
	public final void updateInt(final String columnName, final int val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, val });
		final Object result = null;
		try {
			delegateResultSet.updateInt(columnName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateLong(int, long)
	 */
	public final void updateLong(final int columnIndex, final long val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val });
		final Object result = null;
		try {
			delegateResultSet.updateLong(columnIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateLong(java.lang.String, long)
	 */
	public final void updateLong(final String columnName, final long val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, val });
		final Object result = null;
		try {
			delegateResultSet.updateLong(columnName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateNull(int)
	 */
	public final void updateNull(final int columnIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex });
		final Object result = null;
		try {
			delegateResultSet.updateNull(columnIndex);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateNull(java.lang.String)
	 */
	public final void updateNull(final String columnName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName });
		final Object result = null;
		try {
			delegateResultSet.updateNull(columnName);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateObject(int, java.lang.Object)
	 */
	public final void updateObject(final int columnIndex, final Object val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val });
		final Object result = null;
		try {
			delegateResultSet.updateObject(columnIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateObject(java.lang.String, java.lang.Object)
	 */
	public final void updateObject(final String columnName, final Object val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, val });
		final Object result = null;
		try {
			delegateResultSet.updateObject(columnName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateObject(int, java.lang.Object, int)
	 */
	public final void updateObject(final int columnIndex, final Object val, final int scale) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val, scale });
		final Object result = null;
		try {
			delegateResultSet.updateObject(columnIndex, val, scale);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateObject(java.lang.String, java.lang.Object, int)
	 */
	public final void updateObject(final String columnName, final Object val, final int scale) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, val, scale });
		final Object result = null;
		try {
			delegateResultSet.updateObject(columnName, val, scale);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateRef(int, java.sql.Ref)
	 */
	public final void updateRef(final int columnIndex, final Ref val) throws SQLException {
		Ref wrappedRef = null;
		if (val != null) {
			wrappedRef = new RefListener(val);
		}
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val });
		final Object result = null;
		try {
			delegateResultSet.updateRef(columnIndex, wrappedRef);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateRef(java.lang.String, java.sql.Ref)
	 */
	public final void updateRef(final String columnName, final Ref val) throws SQLException {
		Ref wrappedRef = null;
		if (val != null) {
			wrappedRef = new RefListener(val);
		}
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, val });
		final Object result = null;
		try {
			delegateResultSet.updateRef(columnName, wrappedRef);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateRow()
	 */
	public final void updateRow() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		final Object result = null;
		try {
			delegateResultSet.updateRow();
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateShort(int, short)
	 */
	public final void updateShort(final int columnIndex, final short val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val });
		final Object result = null;
		try {
			delegateResultSet.updateShort(columnIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateShort(java.lang.String, short)
	 */
	public final void updateShort(final String columnName, final short val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, val });
		final Object result = null;
		try {
			delegateResultSet.updateShort(columnName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateString(int, java.lang.String)
	 */
	public final void updateString(final int columnIndex, final String val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val });
		final Object result = null;
		try {
			delegateResultSet.updateString(columnIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateString(java.lang.String, java.lang.String)
	 */
	public final void updateString(final String columnName, final String val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, val });
		final Object result = null;
		try {
			delegateResultSet.updateString(columnName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateTime(int, java.sql.Time)
	 */
	public final void updateTime(final int columnIndex, final Time val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val });
		final Object result = null;
		try {
			delegateResultSet.updateTime(columnIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateTime(java.lang.String, java.sql.Time)
	 */
	public final void updateTime(final String columnName, final Time val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, val });
		final Object result = null;
		try {
			delegateResultSet.updateTime(columnName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateTimestamp(int, java.sql.Timestamp)
	 */
	public final void updateTimestamp(final int columnIndex, final Timestamp val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnIndex, val });
		final Object result = null;
		try {
			delegateResultSet.updateTimestamp(columnIndex, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#updateTimestamp(java.lang.String, java.sql.Timestamp)
	 */
	public final void updateTimestamp(final String columnName, final Timestamp val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { columnName, val });
		final Object result = null;
		try {
			delegateResultSet.updateTimestamp(columnName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.ResultSet#wasNull()
	 */
	public final boolean wasNull() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		boolean result = false;
		try {
			if (delegateResultSet != null) {
				result = delegateResultSet.wasNull();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	// TODO Joudek Implement correctly

	@Override
	public RowId getRowId(int columnIndex) throws SQLException {
		if (delegateResultSet != null) {
			return delegateResultSet.getRowId(columnIndex);
		}
		return null;
	}

	@Override
	public RowId getRowId(String columnLabel) throws SQLException {
		if (delegateResultSet != null) {
			return delegateResultSet.getRowId(columnLabel);
		}
		return null;
	}

	@Override
	public void updateRowId(int columnIndex, RowId x) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateRowId(columnIndex, x);
		}

	}

	@Override
	public void updateRowId(String columnLabel, RowId x) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateRowId(columnLabel, x);
		}

	}

	@Override
	public int getHoldability() throws SQLException {
		return 0;
	}

	@Override
	public boolean isClosed() throws SQLException {
		return false;
	}

	@Override
	public void updateNString(int columnIndex, String nString) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateNString(columnIndex, nString);
		}

	}

	@Override
	public void updateNString(String columnLabel, String nString) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateNString(columnLabel, nString);
		}

	}

	@Override
	public void updateNClob(int columnIndex, NClob nClob) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateNClob(columnIndex, nClob);
		}

	}

	@Override
	public void updateNClob(String columnLabel, NClob nClob) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateNClob(columnLabel, nClob);
		}

	}

	@Override
	public NClob getNClob(int columnIndex) throws SQLException {
		if (delegateResultSet != null) {
			return delegateResultSet.getNClob(columnIndex);
		}
		return null;
	}

	@Override
	public NClob getNClob(String columnLabel) throws SQLException {
		if (delegateResultSet != null) {
			return delegateResultSet.getNClob(columnLabel);
		}
		return null;
	}

	@Override
	public SQLXML getSQLXML(int columnIndex) throws SQLException {
		if (delegateResultSet != null) {
			return delegateResultSet.getSQLXML(columnIndex);
		}
		return null;
	}

	@Override
	public SQLXML getSQLXML(String columnLabel) throws SQLException {
		if (delegateResultSet != null) {
			return delegateResultSet.getSQLXML(columnLabel);
		}
		return null;
	}

	@Override
	public void updateSQLXML(int columnIndex, SQLXML xmlObject) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateSQLXML(columnIndex, xmlObject);
		}

	}

	@Override
	public void updateSQLXML(String columnLabel, SQLXML xmlObject) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateSQLXML(columnLabel, xmlObject);
		}

	}

	@Override
	public String getNString(int columnIndex) throws SQLException {
		if (delegateResultSet != null) {
			return delegateResultSet.getNString(columnIndex);
		}
		return null;
	}

	@Override
	public String getNString(String columnLabel) throws SQLException {
		if (delegateResultSet != null) {
			return delegateResultSet.getNString(columnLabel);
		}
		return null;
	}

	@Override
	public Reader getNCharacterStream(int columnIndex) throws SQLException {
		if (delegateResultSet != null) {
			return delegateResultSet.getNCharacterStream(columnIndex);
		}
		return null;
	}

	@Override
	public Reader getNCharacterStream(String columnLabel) throws SQLException {
		if (delegateResultSet != null) {
			return delegateResultSet.getNCharacterStream(columnLabel);
		}
		return null;
	}

	@Override
	public void updateNCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateNCharacterStream(columnIndex, x, length);
		}

	}

	@Override
	public void updateNCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateNCharacterStream(columnLabel, reader, length);
		}

	}

	@Override
	public void updateAsciiStream(int columnIndex, InputStream x, long length) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateAsciiStream(columnIndex, x, length);
		}

	}

	@Override
	public void updateBinaryStream(int columnIndex, InputStream x, long length) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateBinaryStream(columnIndex, x, length);
		}

	}

	@Override
	public void updateCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateCharacterStream(columnIndex, x, length);
		}

	}

	@Override
	public void updateAsciiStream(String columnLabel, InputStream x, long length) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateAsciiStream(columnLabel, x, length);
		}

	}

	@Override
	public void updateBinaryStream(String columnLabel, InputStream x, long length) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateBinaryStream(columnLabel, x, length);
		}

	}

	@Override
	public void updateCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateCharacterStream(columnLabel, reader, length);
		}

	}

	@Override
	public void updateBlob(int columnIndex, InputStream inputStream, long length) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateBlob(columnIndex, inputStream, length);
		}

	}

	@Override
	public void updateBlob(String columnLabel, InputStream inputStream, long length) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateBlob(columnLabel, inputStream, length);
		}

	}

	@Override
	public void updateClob(int columnIndex, Reader reader, long length) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateClob(columnIndex, reader, length);
		}

	}

	@Override
	public void updateClob(String columnLabel, Reader reader, long length) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateClob(columnLabel, reader, length);
		}

	}

	@Override
	public void updateNClob(int columnIndex, Reader reader, long length) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateNClob(columnIndex, reader, length);
		}

	}

	@Override
	public void updateNClob(String columnLabel, Reader reader, long length) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateNClob(columnLabel, reader, length);
		}

	}

	@Override
	public void updateNCharacterStream(int columnIndex, Reader x) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateNCharacterStream(columnIndex, x);
		}

	}

	@Override
	public void updateNCharacterStream(String columnLabel, Reader reader) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateNCharacterStream(columnLabel, reader);
		}

	}

	@Override
	public void updateAsciiStream(int columnIndex, InputStream x) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateAsciiStream(columnIndex, x);
		}

	}

	@Override
	public void updateBinaryStream(int columnIndex, InputStream x) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateBinaryStream(columnIndex, x);
		}

	}

	@Override
	public void updateCharacterStream(int columnIndex, Reader x) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateCharacterStream(columnIndex, x);
		}

	}

	@Override
	public void updateAsciiStream(String columnLabel, InputStream x) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateAsciiStream(columnLabel, x);
		}

	}

	@Override
	public void updateBinaryStream(String columnLabel, InputStream x) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateBinaryStream(columnLabel, x);
		}

	}

	@Override
	public void updateCharacterStream(String columnLabel, Reader reader) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateCharacterStream(columnLabel, reader);
		}

	}

	@Override
	public void updateBlob(int columnIndex, InputStream inputStream) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateBlob(columnIndex, inputStream);
		}

	}

	@Override
	public void updateBlob(String columnLabel, InputStream inputStream) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateBlob(columnLabel, inputStream);
		}

	}

	@Override
	public void updateClob(int columnIndex, Reader reader) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateClob(columnIndex, reader);
		}

	}

	@Override
	public void updateClob(String columnLabel, Reader reader) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateClob(columnLabel, reader);
		}

	}

	@Override
	public void updateNClob(int columnIndex, Reader reader) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateNClob(columnIndex, reader);
		}

	}

	@Override
	public void updateNClob(String columnLabel, Reader reader) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.updateNClob(columnLabel, reader);
		}

	}

	@Override
	public <T> T getObject(int columnIndex, Class<T> type) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.getObject(columnIndex, type);
		}
		return null;
	}

	@Override
	public <T> T getObject(String columnLabel, Class<T> type) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.getObject(columnLabel, type);
		}
		return null;
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.unwrap(iface);
		}
		return null;
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		if (delegateResultSet != null) {
			delegateResultSet.isWrapperFor(iface);
		}
		return false;
	}
}
