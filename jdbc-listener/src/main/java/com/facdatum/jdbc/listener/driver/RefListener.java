/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.Ref;
import java.sql.SQLException;
import java.util.Map;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCDelegator;


/**
 * RefListener wraps around a delegate Ref object
 * and passes all calls to the listener.
 */
public class RefListener implements Ref, JDBCDelegator {
    /**
     * Holds the delegate ref that calls are passed through to.
     */
    private Ref delegateRef;

    /**
     * Creates a new RefListener that wraps around the specified
     * delegate ref.
     * @param ref the delegate ref
     */
    RefListener(final Ref ref) {
        this.delegateRef = ref;
    }

    /**
     * Sets the delegate ref for the listener ref.
     * @param ref the delegate ref
     */
    public final void setDelegateRef(final Ref ref) {
        this.delegateRef = ref;
    }

    /**
     * Gets the delegate ref for the listener ref.
     * @return the delegateRef
     */
    public final Ref getDelegateRef() {
        return delegateRef;
    }

    /**
     * Returns the delegate class name.
     * @return the delegate class name
     */
    public final String getDelegateClassName() {
        return delegateRef.getClass().getName();
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Ref#getBaseTypeName()
     */
    public final String getBaseTypeName() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        String result = null;
        try {
            if (delegateRef != null) {
                result = delegateRef.getBaseTypeName();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Ref#getObject()
     */
    public final Object getObject() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        Object result = null;
        try {
            if (delegateRef != null) {
                result = delegateRef.getObject();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Ref#getObject(java.util.Map)
     */
    public final Object getObject(final Map < String, Class < ? > > map)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {map});
        Object result = null;
        try {
            if (delegateRef != null) {
                result = delegateRef.getObject(map);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Ref#setObject(java.lang.Object)
     */
    public final void setObject(final Object val) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {val});
        final Object result = null;
        try {
            delegateRef.setObject(val);
            JDBCCallListener.callEnd(begin, this, result);
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }
}
