/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

/**
 * Provides access to all the properties that can be used to control the
 * JDBCListener. These can either be pass in as java properties at the
 * command line of loaded via the logging driver properties file
 * jdbc-listener.properties.
  */
public final class JDBCListenerProperties {

    /**
     * The properties bundle that can be used to override the property file
     * name.
     */
    public static final String FILE_BUNDLE_REF =
            "com.facdatum.jdbc.config.JDBCPropertiesFileName";

    /**
     * Contains the key used to pick up the list of listeners that should
     * be created.
     */
    public static final String LISTENERS_KEY = "jdbc.listener.classes";

    /**
     * Contains the configured array of listeners.
     */
    public static String[] listeners; //NOCS

    /**
     * Contains the key used to pick up the listener property directory path.
     */
    public static final String PROP_PATH_KEY = "jdbc.listener.prop.path";

    /**
     * Contains the configured listener property directory path.
     */
    private static String propPath;

    /**
     * Contains the key used to pick up the listener property file name.
     */
    public static final String PROP_FILE_KEY = "jdbc.listener.prop.filename";

    /**
     * Contains the key used to pick up the shared property file name.
     */
    public static final String SHARED_FILE_KEY = "prop.filename";

    /**
     * Contains the default listener property file name.
     */
    public static final String PROP_FILE_DEF =
            "jdbc-listener";

    /**
     * Contains the configured listener property file name.
     */
    private static String propFile;

    /**
     * Contains the key used to pick up the delegate driver class.
     */
    public static final String DRIVER_CLASS_KEY = "driver.delegate";

    /**
     * Contains the configured delegate driver class name.
     */
    private static String driverClass;

    /**
     * Contains the key used to pick up the delegate datasource.
     */
    public static final String DS_NAME_KEY = "datasource.delegate";

    /**
     * Contains the configured delegate datasource name.
     */
    private static String datasourceName;

    /**
     * Contains the key used to pick up the delegate datasource properties.
     */
    public static final String DS_PROPS_KEY = "datasource.props";

    /**
     * Contains the configured delegate datasource properties.
     */
    private static Properties datasourceProps;

    /**
     * Contains the key used to pick up the jndi context factory if configured.
     */
    public static final String NAMINGFACTORY_KEY = "jndi.factory";

    /**
     * Contains the configured jndi factory.
     */
    private static String namingFactory;

    /**
     * Contains the key used to pick up the jndi context provider url.
     */
    public static final String NAMINGURL_KEY = "jndi.context.url";

    /**
     * Contains the configured context provider url.
     */
    private static String namingUrl;

    /**
     * Contains the key used to pick up the jndi context provider custom
     * properties.
     */
    public static final String NAMINGPROPS_KEY = "jndi.context.props";

    /**
     * Contains the configured delegate jndi context provider custom properties.
     */
    private static Properties namingProps;

    static {
        JDBCListenerProperties.init();
    }

    /**
     * Private default constructor to stops creation of instances.
     */
    private JDBCListenerProperties() {
        super();
    }

    /**
     * Creates an instance of the logging properties by reading the
     * defined logging properties file.
     */
    public static void init() {
        propFile = System.getProperty(PROP_FILE_KEY,
                PROP_FILE_DEF);
        try {
            final Properties fileProperties = loadBundle(FILE_BUNDLE_REF);
            propFile = fileProperties.getProperty(SHARED_FILE_KEY, propFile);
        } catch (MissingResourceException e) { //NOPMD NOCS
            //use default
        }

        propPath = System.getProperty(PROP_PATH_KEY);
        Properties tempProperties = new Properties();
        try {
            if (propPath == null) {
                tempProperties = loadBundle(propFile);
            } else {
                tempProperties.load(new FileInputStream(new File(propPath,
                        propFile + ".properties")));
            }
        } catch (Exception e) { //NOPMD NOCS
            //defaults will be used
        }
        String listenerList = tempProperties.getProperty(LISTENERS_KEY);
        driverClass = tempProperties.getProperty(DRIVER_CLASS_KEY);
        datasourceName = tempProperties.getProperty(DS_NAME_KEY);
        namingFactory = tempProperties.getProperty(NAMINGFACTORY_KEY);
        namingUrl = tempProperties.getProperty(NAMINGURL_KEY);
        String tempNamingProps = tempProperties.getProperty(NAMINGPROPS_KEY);
        String tempDSProps = tempProperties.getProperty(DS_PROPS_KEY);

        listenerList = System.getProperty(LISTENERS_KEY, listenerList); //NOPMD
        if (listenerList != null) {
            listeners = convertListToArray(listenerList);
        }
        driverClass = System.getProperty(DRIVER_CLASS_KEY, driverClass);
        datasourceName = System.getProperty(DS_NAME_KEY, datasourceName);
        namingFactory = System.getProperty(NAMINGFACTORY_KEY, namingFactory);
        namingUrl = System.getProperty(NAMINGURL_KEY, namingUrl);
        tempNamingProps = System.getProperty(NAMINGPROPS_KEY, //NOPMD
                tempNamingProps);
        if (tempNamingProps != null) { //NOPMD
            namingProps = JDBCListenerProperties.convertListToProperties(
                    tempNamingProps);
        } else {
            namingProps = new Properties();
        }
        tempDSProps = System.getProperty(DS_PROPS_KEY, //NOPMD
                tempDSProps);
        if (tempDSProps != null) { //NOPMD
            datasourceProps = JDBCListenerProperties.convertListToProperties(
                    tempDSProps);
        } else {
            datasourceProps = new Properties();
        }
    }

    /**
     * Loads properties from a ResourceBundle.
     * @param bundleName name of bundle to load
     * @return properties in bundle
     */
    protected static Properties loadBundle(final String bundleName) {
        final ResourceBundle bundle = ResourceBundle.getBundle(bundleName);
        final Properties props = new Properties();
        final Enumeration < String > keys = bundle.getKeys();
        while (keys.hasMoreElements()) {
            final String key = (String) keys.nextElement();
            props.put(key, bundle.getString(key));
        }
        return props;
    }

    /**
     * Clears all the system properties related to the JDBCLogger to
     * their defaults.
     */
    public static void clearProperties() {
        System.clearProperty(LISTENERS_KEY);
        System.clearProperty(PROP_PATH_KEY);
        System.clearProperty(PROP_FILE_KEY);
        System.clearProperty(DRIVER_CLASS_KEY);
        System.clearProperty(DS_NAME_KEY);
        System.clearProperty(NAMINGFACTORY_KEY);
        System.clearProperty(NAMINGURL_KEY);
        System.clearProperty(NAMINGPROPS_KEY);
        System.clearProperty(DS_PROPS_KEY);
    }

    /**
     * Gets the properties file name.
     * @return properties file name
     */
    public static String getPropFile() {
        return propFile;
    }

    /**
     * Gets the properties file path.
     * @return properties file path
     */
    public static String getPropPath() {
        return propPath;
    }

    /**
     * Gets the list of configured listener classes.
     * @return the listeners
     */
    public static String[] getListeners() {
        return listeners.clone();
    }

    /**
     * Gets the configured delegate driver.
     * @return the delegate driver class name
     */
    public static String getDelegateDriver() {
        return driverClass;
    }

    /**
     * Gets the configured delegate data source name.
     * @return the delegate data source name
     */
    public static String getDataSourceName() {
        return datasourceName;
    }

    /**
     * Gets the configured delegate data source properties.
     * @return the delegate data source properties
     */
    public static Properties getDataSourceProperties() {
        return datasourceProps;
    }

    /**
     * Gets the configured jndi context factory.
     * @return the context factory
     */
    public static String getContextFactory() {
        return namingFactory;
    }

    /**
     * Gets the configured jndi context provider url.
     * @return the jndi context provider url
     */
    public static String getContextProviderURL() {
        return namingUrl;
    }

    /**
     * Gets the configured jndi context provider properties.
     * @return the jndi context provider custom properties
     */
    public static Properties getContextProviderProperties() {
        return namingProps;
    }

    /**
     * Converts a list of properties in a string into the equivalent properties.
     * The list should be a set of comma separated name:value pairs (that is,
     * with colon (:) separator).
     * @param propsAsString the properties list as a string
     * @return the properties
     */
    public static Properties convertListToProperties(
            final String propsAsString) {
        final Properties props = new Properties();
        final StringTokenizer tokens = new StringTokenizer(propsAsString, ",",
                false);
        while (tokens.hasMoreElements()) {
            final String pair = tokens.nextToken();
            final String[] pst = pair.split(":", 2);
            if (pst.length == 2) {
                final String name = pst[0].toLowerCase().trim(); //NOPMD
                final String value = pst[1].trim();
                props.put(name, value);
            }
        }
        return props;
    }

    /**
     * Converts a list of items in a string into the equivalent String array.
     * @param listAsString the properties list as a string
     * @return the array
     */
    public static String[] convertListToArray(
            final String listAsString) {
        final ArrayList < String > items = new ArrayList < String > (); //NOCS
        final StringTokenizer tokens = new StringTokenizer(listAsString, ",",
                false);
        while (tokens.hasMoreElements()) {
            items.add(tokens.nextToken().trim());
        }
        return items.toArray(new String[] {});
    }
}
