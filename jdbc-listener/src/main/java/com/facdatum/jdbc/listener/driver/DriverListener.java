/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.logging.Logger;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCDelegator;
import com.facdatum.jdbc.listener.JDBCListenerProperties;

/**
 * DriverListener wraps around a delegate Driver object
 * and passes all calls to the listener.
 */

public class DriverListener implements Driver, JDBCDelegator {

    /**
     * Records whether the driver has been registered already.
     */
    private static boolean registered = false;

    /**
     * If there is an error registering driver then this variable is set to
     * throw when creating driver.
     */
    private static SQLException registerError = null;

    /**
     * Holds the reference to the driver that all calls are delegated to.
     */

    private Driver delegateDriver = null;

    /**
     * Tag used for indirect URL to perform listening.
     */
    private static final String LISTENER_TAG = "jdbc:listener:";

    /**
     * Standard jdbc tag at start of connection string.
     */
    private static final String JDBC_TAG = "jdbc:";

    /*
     * static clause that is called when forName is executed on this Driver
     * class as part of the JDBC registration process.
     */
    static {
        try {
            if (!registered) {
                DriverListener.registerDriver();
                registerError = null;
                registered = true;
            }
        } catch (SQLException e) {
            registerError = e;
        }
    }

    /**
     * Creates driver.
     * @throws SQLException if there was a previous problem registering driver
     */
    public DriverListener() throws SQLException {
        super();
        if (registerError != null) {
            //DriverListener.registerDriver();
            throw new SQLException(registerError.getMessage());
        }
    }

    /**
     * Creates driver directly. Used for testing.
     * @param driver delegate driver
     * @throws SQLException if there was a previous problem registering driver
     */
    public DriverListener(final Driver driver) throws SQLException {
        delegateDriver = driver;
    }

    /**
     * Used in testing to clear any previous failures to register driver.
     */
    protected static final void clearDriver() {
        registered = false;
        registerError = null;
    }

    /**
     * Registers the DriverListener and the base driver with JDBC DriverManager.
     * The base driver details are obtained from the DriverListener properties.
     * @return the delegate driver
     * @throws SQLException if there is a problem registering driver
     */
    public static Driver registerDriver() throws SQLException {
        String driverClassName = null;
        Driver driver = null;
        try {
            // register driver listener
            driverClassName =
                    "com.facdatum.jdbc.listener.driver.DriverListener";
            driver = new DriverListener();
            DriverManager.registerDriver(driver);
            final DriverListener driverListener = (DriverListener) driver;

            // register delegate driver
            driverClassName = JDBCListenerProperties.getDelegateDriver();
            driver = (Driver) Class.forName(driverClassName).newInstance();
            DriverManager.registerDriver(driver);

            // set delegate driver
            driverListener.setDelegateDriver(driver);
            return driver;
        } catch (Exception e) {
            final String err = "Error registering: " + driverClassName
                + " for: " + driver + ": " + e;
            throw new SQLException(err); //NOPMD
        }
    }

    /**
     * Setter for the delegate driver.
     * @param driver Delegate driver
     */
    public final void setDelegateDriver(final Driver driver) {
        this.delegateDriver = driver;
    }

    /**
     * Getter for the delegate driver.
     * @return delegateDriver
     */
    public final Driver getDelegateDriver() {
        return this.delegateDriver;
    }

    /**
     * Returns the delegate class name.
     * @return the delegate class name
     */
    public final String getDelegateClassName() {
        if (delegateDriver != null) {
            return delegateDriver.getClass().getName();
        }
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Driver#acceptsURL(java.lang.String)
     */
    public final boolean acceptsURL(final String arg0) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                arg0});
        boolean result = false;
        try {
            if (delegateDriver == null) {
                delegateDriver = DriverListener.registerDriver();
            }
            String url = arg0;
            if (arg0 != null && arg0.startsWith(LISTENER_TAG)) {
                url = arg0.replaceFirst(LISTENER_TAG, JDBC_TAG);
            }
            if (delegateDriver != null) {
                result = delegateDriver.acceptsURL(url);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Driver#connect(java.lang.String, java.util.Properties)
     */
    public final Connection connect(final String arg0, final Properties arg1)
    throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                arg0, arg1});
        ConnectionListener result = null; //NOPMD
        Connection delegateResult = null; //NOPMD
        try {
            if (delegateDriver == null) {
                delegateDriver = DriverListener.registerDriver();
            }
            String url = arg0;
            if (arg0 != null && arg0.startsWith(LISTENER_TAG)) {
                url = arg0.replaceFirst(LISTENER_TAG, JDBC_TAG);
            }
            if (delegateDriver != null) {
                delegateResult = //NOPMD
                    delegateDriver.connect(url, arg1);
                if (delegateResult != null) {
                    result = new ConnectionListener(delegateResult);
                }
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Driver#getMajorVersion()
     */
    public final int getMajorVersion() {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        if (delegateDriver != null) {
            result = delegateDriver.getMajorVersion();
        }
        JDBCCallListener.callEnd(begin, this, result);
        return result;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Driver#getMinorVersion()
     */
    public final int getMinorVersion() {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        if (delegateDriver != null) {
            result = delegateDriver.getMinorVersion();
        }
        JDBCCallListener.callEnd(begin, this, result);
        return result;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Driver#getPropertyInfo(java.lang.String,
     * java.util.Properties)
     */
    public final DriverPropertyInfo[] getPropertyInfo(final String arg0,
            final Properties arg1)
    throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                arg0, arg1});
        DriverPropertyInfo[] result = null;
        try {
            if (delegateDriver != null) {
                result = delegateDriver.getPropertyInfo(arg0, arg1);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Driver#jdbcCompliant()
     */
    public final boolean jdbcCompliant() {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        if (delegateDriver != null) {
            result = delegateDriver.jdbcCompliant();
        }
        JDBCCallListener.callEnd(begin, this, result);
        return result;
    }

    //TODO Joudek Implement correctly

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        if (delegateDriver != null) {
            return  delegateDriver.getParentLogger();
        }
        return null;
    }
}
