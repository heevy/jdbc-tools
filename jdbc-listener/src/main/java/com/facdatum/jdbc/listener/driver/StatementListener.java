/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCDelegator;


/**
 * StatementListener wraps around a delegate Statement object
 * and passes all calls to the listener.
 */
public class StatementListener implements Statement, JDBCDelegator {

    /**
     * Holds the delegate statement that will be listened to.
     */
    protected Statement delegateStatement; //NOCS

    /**
     * Creates a StatementListener from the defined Statement.
     * @param statement the delegate statement to set
     */
    StatementListener(final Statement statement) {
        this.delegateStatement = statement;
    }

    /**
     * Get the delegate statement.
     * @return the delegateStatement
     */
    public Statement getDelegateStatement() { //NOCS
        return delegateStatement;
    }

    /**
     * Set the delegate statement.
     * @param statement the delegate statement to set
     */
    public void setDelegateStatement(final Statement statement) { //NOCS
        this.delegateStatement = statement;
    }

    /**
     * Returns the delegate class name.
     * @return the delegate class name
     */
    public final String getDelegateClassName() {
        return delegateStatement.getClass().getName();
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#addBatch(java.lang.String)
     */
    public final void addBatch(final String sql) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        final Object result = null;
        try {
            delegateStatement.addBatch(sql);
            JDBCCallListener.callEnd(begin, this, result);
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#cancel()
     */
    public final void cancel() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        final Object result = null;
        try {
            delegateStatement.cancel();
            JDBCCallListener.callEnd(begin, this, result);
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#clearBatch()
     */
    public final void clearBatch() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        final Object result = null;
        try {
            delegateStatement.clearBatch();
            JDBCCallListener.callEnd(begin, this, result);
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#clearWarnings()
     */
    public final void clearWarnings() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        final Object result = null;
        try {
            delegateStatement.clearWarnings();
            JDBCCallListener.callEnd(begin, this, result);
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#close()
     */
    public final void close() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        final Object result = null;
        try {
            delegateStatement.close();
            JDBCCallListener.callEnd(begin, this, result);
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#execute(java.lang.String)
     */
    public final boolean execute(final String sql) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {sql});
        boolean result = false;
        try {
            if (delegateStatement != null) {
                result = delegateStatement.execute(sql);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#execute(java.lang.String, int)
     */
    public final boolean execute(final String sql, final int autoGeneratedKeys)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {sql,
                autoGeneratedKeys});
        boolean result = false;
        try {
            if (delegateStatement != null) {
                result = delegateStatement.execute(sql, autoGeneratedKeys);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#execute(java.lang.String, int[])
     */
    public final boolean execute(final String sql, final int[] columnIndexes)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {sql,
                columnIndexes});
        boolean result = false;
        try {
            if (delegateStatement != null) {
                result = delegateStatement.execute(sql, columnIndexes);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#execute(java.lang.String, java.lang.String[])
     */
    public final boolean execute(final String sql, final String[] columnNames)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {sql,
                columnNames});
        boolean result = false;
        try {
            if (delegateStatement != null) {
                result = delegateStatement.execute(sql, columnNames);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#executeBatch()
     */
    public final int[] executeBatch() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int[] result = null;
        try {
            if (delegateStatement != null) {
                result = delegateStatement.executeBatch();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#executeQuery(java.lang.String)
     */
    public final ResultSet executeQuery(final String sql) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {sql});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateStatement != null) {
                delegateResult = delegateStatement.executeQuery(sql);
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#executeUpdate(java.lang.String)
     */
    public final int executeUpdate(final String sql) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {sql});
        int result = 0;
        try {
            if (delegateStatement != null) {
                result = delegateStatement.executeUpdate(sql);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#executeUpdate(java.lang.String, int)
     */
    public final int executeUpdate(final String sql,
            final int autoGeneratedKeys) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {sql,
                autoGeneratedKeys});
        int result = 0;
        try {
            if (delegateStatement != null) {
                result = delegateStatement.executeUpdate(sql,
                        autoGeneratedKeys);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#executeUpdate(java.lang.String, int[])
     */
    public final int executeUpdate(final String sql,
            final int[] columnIndexes) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {sql,
                columnIndexes});
        int result = 0;
        try {
            if (delegateStatement != null) {
                result = delegateStatement.executeUpdate(sql, columnIndexes);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#executeUpdate(java.lang.String,
     *      java.lang.String[])
     */
    public final int executeUpdate(final String sql, final String[] columnNames)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {sql,
                columnNames});
        int result = 0;
        try {
            if (delegateStatement != null) {
                result = delegateStatement.executeUpdate(sql, columnNames);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getConnection()
     */
    public final Connection getConnection() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        ConnectionListener result = null; //NOPMD
        Connection delegateResult = null; //NOPMD
        try {
            if (delegateStatement != null) {
                delegateResult = delegateStatement.getConnection();
                result = new ConnectionListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getFetchDirection()
     */
    public final int getFetchDirection() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateStatement != null) {
                result = delegateStatement.getFetchDirection();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getFetchSize()
     */
    public final int getFetchSize() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateStatement != null) {
                result = delegateStatement.getFetchSize();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getGeneratedKeys()
     */
    public final ResultSet getGeneratedKeys() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateStatement != null) {
                delegateResult = delegateStatement.getGeneratedKeys();
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getMaxFieldSize()
     */
    public final int getMaxFieldSize() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateStatement != null) {
                result = delegateStatement.getMaxFieldSize();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getMaxRows()
     */
    public final int getMaxRows() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateStatement != null) {
                result = delegateStatement.getMaxRows();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getMoreResults()
     */
    public final boolean getMoreResults() throws SQLException { //NOPMD
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateStatement != null) {
                result = delegateStatement.getMoreResults();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getMoreResults(int)
     */
    public final boolean getMoreResults(final int current) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                current});
        boolean result = false;
        try {
            if (delegateStatement != null) {
                result = delegateStatement.getMoreResults(current);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getQueryTimeout()
     */
    public final int getQueryTimeout() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateStatement != null) {
                result = delegateStatement.getQueryTimeout();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getResultSet()
     */
    public final ResultSet getResultSet() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateStatement != null) {
                delegateResult = delegateStatement.getResultSet();
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getResultSetConcurrency()
     */
    public final int getResultSetConcurrency() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateStatement != null) {
                result = delegateStatement.getResultSetConcurrency();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getResultSetHoldability()
     */
    public final int getResultSetHoldability() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateStatement != null) {
                result = delegateStatement.getResultSetHoldability();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }


    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getResultSetType()
     */
    public final int getResultSetType() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateStatement != null) {
                result = delegateStatement.getResultSetType();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getUpdateCount()
     */
    public final int getUpdateCount() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateStatement != null) {
                result = delegateStatement.getUpdateCount();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getWarnings()
     */
    public final SQLWarning getWarnings() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        SQLWarning result = null;
        try {
            if (delegateStatement != null) {
                result = delegateStatement.getWarnings();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#setCursorName(java.lang.String)
     */
    public final void setCursorName(final String name) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                name});
        final Object result = null;
        try {
            delegateStatement.setCursorName(name);
            JDBCCallListener.callEnd(begin, this, result);
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#setEscapeProcessing(boolean)
     */
    public final void setEscapeProcessing(final boolean enable)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                enable});
        final Object result = null;
        try {
            delegateStatement.setEscapeProcessing(enable);
            JDBCCallListener.callEnd(begin, this, result);
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#setFetchDirection(int)
     */
    public final void setFetchDirection(final int direction)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                direction});
        final Object result = null;
        try {
            delegateStatement.setFetchDirection(direction);
            JDBCCallListener.callEnd(begin, this, result);
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#setFetchSize(int)
     */
    public final void setFetchSize(final int rows) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                rows});
        final Object result = null;
        try {
            delegateStatement.setFetchSize(rows);
            JDBCCallListener.callEnd(begin, this, result);
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#setMaxFieldSize(int)
     */
    public final void setMaxFieldSize(final int max) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {max});
        final Object result = null;
        try {
            delegateStatement.setMaxFieldSize(max);
            JDBCCallListener.callEnd(begin, this, result);
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#setMaxRows(int)
     */
    public final void setMaxRows(final int max) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {max});
        final Object result = null;
        try {
            delegateStatement.setMaxRows(max);
            JDBCCallListener.callEnd(begin, this, result);
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#setQueryTimeout(int)
     */
    public final void setQueryTimeout(final int seconds) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                seconds});
        final Object result = null;
        try {
            delegateStatement.setQueryTimeout(seconds);
            JDBCCallListener.callEnd(begin, this, result);
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    //TODO Joudek implement correctly

    @Override
    public boolean isClosed() throws SQLException {
        return delegateStatement.isClosed();
    }

    @Override
    public void setPoolable(boolean poolable) throws SQLException {
        delegateStatement.setPoolable(poolable);
    }

    @Override
    public boolean isPoolable() throws SQLException {
        return delegateStatement.isPoolable();
    }

    @Override
    public void closeOnCompletion() throws SQLException {
        delegateStatement.closeOnCompletion();
    }

    @Override
    public boolean isCloseOnCompletion() throws SQLException {
        return delegateStatement.isCloseOnCompletion();
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return  delegateStatement.unwrap(iface);
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return delegateStatement.isWrapperFor(iface);
    }
}
