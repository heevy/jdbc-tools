/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.ConnectionEventListener;
import javax.sql.PooledConnection;
import javax.sql.StatementEventListener;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCDelegator;


/**
 * PooledConnectionListener wraps around a delegate PooledConnection object
 * and passes all calls to the listener.
 */
public class PooledConnectionListener implements PooledConnection,
        JDBCDelegator {

    /**
     * Holds the delegate connection that calls are passed through to.
     */
    protected PooledConnection delegateConn; //NOCS

    /**
     * Creates a new ConnectionListener that wraps around the specified
     * delegate connection.
     * @param connection the delegate connection
     */
    PooledConnectionListener(final PooledConnection connection) {
        this.delegateConn = connection;
    }

    /**
     * Sets the delegate connection for the listener connection.
     * @param connection the delegate connection
     */
    public final void setDelegateConn(final PooledConnection connection) {
        this.delegateConn = connection;
    }

    /**
     * Gets the delegate connection for the listener connection.
     * @return the delegateConn
     */
    public final PooledConnection getDelegateConn() {
        return delegateConn;
    }

    /**
     * Returns the delegate class name.
     * @return the delegate class name
     */
    public final String getDelegateClassName() {
        return delegateConn.getClass().getName();
    }

    /**
     * {@inheritDoc}
     * @see javax.sql.PooledConnection#addConnectionEventListener(
     *      javax.sql.ConnectionEventListener)
     */
    public final void addConnectionEventListener(
            final ConnectionEventListener listener) {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                listener});
        final Object result = null;
        if (delegateConn != null) {
            delegateConn.addConnectionEventListener(listener);
        }
        JDBCCallListener.callEnd(begin, this, result);
    }

    /**
     * {@inheritDoc}
     * @see javax.sql.PooledConnection#close()
     */
    public final void close() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        final Object result = null;
        try {
            if (delegateConn != null) {
                delegateConn.close();
            }
            JDBCCallListener.callEnd(begin, this, result);
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see javax.sql.PooledConnection#getConnection()
     */
    public final Connection getConnection() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        ConnectionListener result = null; //NOPMD
        Connection delegateResult = null; //NOPMD
        try {
            if (delegateConn != null) {
                delegateResult = delegateConn.getConnection();
                result =
                    new ConnectionListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see javax.sql.PooledConnection#removeConnectionEventListener(
     *      javax.sql.ConnectionEventListener)
     */
    public final void removeConnectionEventListener(
            final ConnectionEventListener listener) {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                listener});
        final Object result = null;
        if (delegateConn != null) {
            delegateConn.removeConnectionEventListener(listener);
        }
        JDBCCallListener.callEnd(begin, this, result);
    }

    //TODO Joudek implement correctly

    @Override
    public void addStatementEventListener(StatementEventListener listener) {
        if (delegateConn != null) {
            delegateConn.addStatementEventListener(listener);
        }
    }

    @Override
    public void removeStatementEventListener(StatementEventListener listener) {
        if (delegateConn != null) {
            delegateConn.removeStatementEventListener(listener);
        }
    }
}
