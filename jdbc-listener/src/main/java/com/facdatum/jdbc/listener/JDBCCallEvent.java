/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener;

/**
 * A JDBCCallEvent holds the information related to the call into the JDBC
 * layer that is being listened to.
 */
public class JDBCCallEvent extends JDBCEvent implements ListenerCallEvent {

    /**
     * Holds the arguments that were passed to the JDBC Method.
     */
    private final Object[] args; //NOPMD

    /**
     * Creates a JDBCCallEvent.
     * @param eventTime the time of the event
     * @param callObject the object called in JDBC Layer
     * @param callStackTrace the stack trace of calls into the JDBC Layer
     * @param callArgs the arguments passed to the JDBC Method
     */
    public JDBCCallEvent(final long eventTime, final Object callObject,
            final StackTraceElement[] callStackTrace, final Object[] callArgs) {
        super(eventTime, callObject, callStackTrace);
        this.args = callArgs.clone();
    }

    /**
     * Returns the arguments to the JDBC method.
     * @return the arguments
     */
    public final Object[] getArgs() {
        return this.args.clone();
    }
}
