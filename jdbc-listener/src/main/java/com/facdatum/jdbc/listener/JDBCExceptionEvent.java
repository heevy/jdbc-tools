/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener;

/**
 * A JDBCExceptionEvent holds the information related to the call into the JDBC
 * layer that is being listened to.
 */
public class JDBCExceptionEvent extends JDBCEvent
        implements ListenerExceptionEvent {

    /**
     * Holds the exception thrown by the JDBC Method.
     */
    private final Exception exception; //NOPMD

    /**
     * Holds the duration of the JDBC method call.
     */
    private final long duration; //NOPMD

    /**
     * Creates a JDBCCallEvent.
     * @param eventTime the time of the event
     * @param callObject the object called in JDBC Layer
     * @param callStackTrace the stack trace of calls into the JDBC Layer
     * @param callException the exception thrown by the JDBC layer
     * @param callDuration the duration of the JDBC method call
     */
    public JDBCExceptionEvent(final long eventTime, final Object callObject,
            final StackTraceElement[] callStackTrace,
            final Exception callException, final long callDuration) {
        super(eventTime, callObject, callStackTrace);
        this.exception = callException;
        this.duration = callDuration;
    }

    /**
     * Returns the exception thrown by the JDBC method.
     * @return the exception
     */
    public final Exception getException() {
        return this.exception;
    }

    /**
     * Returns the duration of the JDBC call.
     * @return the call duration
     */
    public final long getDuration() {
        return this.duration;
    }
}
