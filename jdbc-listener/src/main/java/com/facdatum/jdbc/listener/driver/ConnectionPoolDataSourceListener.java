/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.lang.reflect.Method;
import java.sql.SQLException;

import javax.sql.ConnectionPoolDataSource;
import javax.sql.DataSource;
import javax.sql.PooledConnection;

import com.facdatum.jdbc.listener.JDBCCallListener;

/**
 * ConnectionPoolDataSourceListener wraps around a delegate
 * ConnectionPoolDataSource object and passes all calls to the listener.
 */
public class ConnectionPoolDataSourceListener extends DataSourceListener
        implements ConnectionPoolDataSource {

    /**
     * Compiler generated serialVersionUID for serialization. Update if
     * structural revisions.
     */
    private static final long serialVersionUID = -7343328733710012962L;

    /**
     * No arg constructor for Serialization/Referenceable.
     * @throws SQLException if there was a previous problem registering driver
     */
    public ConnectionPoolDataSourceListener() throws SQLException {
      super();
    }

    /**
     * Creates a listener data source with given delegate data source.
     * @param dataSource delegate data source
     */
    public ConnectionPoolDataSourceListener(final DataSource dataSource) {
      super(dataSource);
    }

    /**
     * {@inheritDoc}
     * @see javax.sql.ConnectionPoolDataSource#getPooledConnection()
     */
    public PooledConnection getPooledConnection() throws SQLException { //NOCS
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        PooledConnectionListener result = null; //NOPMD
        PooledConnection delegateResult = null; //NOPMD
        try {
            if (delegateDataSource == null) {
                bindDataSource();
            }
            if (delegateDataSource != null) {
                delegateResult = getPooledConnectionHackForWebSphere(
                        new Object[] {}, new Class[] {});
                if (delegateResult == null) {
                    delegateResult = ((ConnectionPoolDataSource)
                            delegateDataSource).getPooledConnection();
                }
                result =
                    new PooledConnectionListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see javax.sql.ConnectionPoolDataSource#getPooledConnection(
     *      java.lang.String, java.lang.String)
     */
    public PooledConnection getPooledConnection(final String username, //NOCS
            final String password) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                username, password});
        PooledConnectionListener result = null; //NOPMD
        PooledConnection delegateResult = null; //NOPMD
        try {
            if (delegateDataSource == null) {
                bindDataSource();
            }
            if (delegateDataSource != null) {
                delegateResult = getPooledConnectionHackForWebSphere(
                        new Object[] {username, password},
                        new Class[] {String.class, String.class});
                if (delegateResult == null) {
                    delegateResult = ((ConnectionPoolDataSource)
                                delegateDataSource).getPooledConnection(
                                        username, password);
                }
                result =
                    new PooledConnectionListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * This method is always called but only has any effect in a websphere
     * environment. Because Websphere wraps the JDBC classes itself, a bit
     * of magic is needed to get things working.
     * @param args arguments to be passed to getPooledConnection
     * @param types types of the arguments
     * @return the pooled connection
     */
    @SuppressWarnings("unchecked")
    private PooledConnection getPooledConnectionHackForWebSphere(
            final Object[] args, final Class[] types) {
        PooledConnection result = null;
        try {
            final Class helperClass = Class.
                forName("com.ibm.websphere.rsadapter.WSCallHelper");
            final Method helperMethod = helperClass.getMethod("jdbcCall",
                new Class[] {Class.class, Object.class, String.class,
                Object[].class, Class[].class});
            result = (PooledConnection) helperMethod.
                invoke(helperClass, null, delegateDataSource,
                        "getPooledConnection", args, types);
            return result;
        } catch (Exception e) {
            return null;
        } catch (Error e) {
            return null;
        }
    }
}
