/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener;

/**
 * JDBCListener interface that must be implemented if configuring a class
 * as a listener for JDBC events.
 */
public interface JDBCListener {

    /**
     * Method called before listener is used to set up any necessary
     * infrastructure.
     */
    void init();

    /**
     * Method called before a JDBC method is called on a JDBC interface.
     * @param event the details of the method call
     */
    void acceptBegin(ListenerCallEvent event);

    /**
     * Method called after a JDBC method is called on a JDBC interface.
     * @param event the details of the method return
     */
    void acceptEnd(ListenerReturnEvent event);

    /**
     * Method called when an exception is thrown by a JDBC method
     * on a JDBC interface.
     * @param event the details of the exception thrown by the method
     */
    void acceptException(ListenerExceptionEvent event);
}
