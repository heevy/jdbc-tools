/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.SQLException;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCDelegator;


/**
 * BlobListener wraps around a delegate Blob object
 * and passes all calls to the listener.
 */
public class BlobListener implements Blob, JDBCDelegator {
    /**
     * Holds the delegate blob that calls are passed through to.
     */
    private Blob delegateBlob;

    /**
     * Creates a new BlobListener that wraps around the specified
     * delegate blob.
     * @param blob the delegate blob
     */
    BlobListener(final Blob blob) {
        this.delegateBlob = blob;
    }

    /**
     * Sets the delegate blob for the listener blob.
     * @param blob the delegate blob
     */
    public final void setDelegateBlob(final Blob blob) {
        this.delegateBlob = blob;
    }

    /**
     * Gets the delegate blob for the listener blob.
     * @return the delegateBlob
     */
    public final Blob getDelegateBlob() {
        return delegateBlob;
    }


    /**
     * Returns the delegate class name.
     * @return the delegate class name
     */
    public final String getDelegateClassName() {
        return delegateBlob.getClass().getName();
    }
    /**
     * {@inheritDoc}
     * @see java.sql.Blob#getBinaryStream()
     */
    public final InputStream getBinaryStream() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        InputStream result = null;
        try {
            if (delegateBlob != null) {
                result = delegateBlob.getBinaryStream();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Blob#getBytes(long, int)
     */
    public final byte[] getBytes(final long pos, final int length)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                pos, length});
        byte[] result = null;
        try {
            if (delegateBlob != null) {
                result = delegateBlob.getBytes(pos, length);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Blob#length()
     */
    public final long length() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        long result = 0;
        try {
            if (delegateBlob != null) {
                result = delegateBlob.length();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Blob#position(byte[], long)
     */
    public final long position(final byte[] pattern, final long start)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                pattern, start});
        long result = 0;
        try {
            if (delegateBlob != null) {
                result = delegateBlob.position(pattern, start);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Blob#position(java.sql.Blob, long)
     */
    public final long position(final Blob pattern, final long start)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                pattern, start});
        long result = 0;
        try {
            if (delegateBlob != null) {
                result = delegateBlob.position(pattern, start);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Blob#setBinaryStream(long)
     */
    public final OutputStream setBinaryStream(final long pos)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {pos});
        OutputStream result = null;
        try {
            if (delegateBlob != null) {
                result = delegateBlob.setBinaryStream(pos);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Blob#setBytes(long, byte[])
     */
    public final int setBytes(final long pos, final byte[] bytes)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                pos, bytes});
        int result = 0;
        try {
            if (delegateBlob != null) {
                result = delegateBlob.setBytes(pos, bytes);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Blob#setBytes(long, byte[], int, int)
     */
    public final int setBytes(final long pos, final byte[] bytes,
            final int offset, final int len) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this,
                new Object[] {pos, bytes, offset, len});
        int result = 0;
        try {
            if (delegateBlob != null) {
                result = delegateBlob.setBytes(pos, bytes, offset, len);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Blob#truncate(long)
     */
    public final void truncate(final long len) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        final Object result = null;
        try {
            delegateBlob.truncate(len);
            JDBCCallListener.callEnd(begin, this, result);
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    //TODO Joudek Implement correctly

    @Override
    public void free() throws SQLException {
        delegateBlob.free();
    }

    @Override
    public InputStream getBinaryStream(long pos, long length) throws SQLException {
        return delegateBlob.getBinaryStream();
    }
}
