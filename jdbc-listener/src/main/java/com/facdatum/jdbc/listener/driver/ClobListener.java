/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.sql.Clob;
import java.sql.SQLException;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCDelegator;


/**
 * ClobListener wraps around a delegate Clob object
 * and passes all calls to the listener.
 */
public class ClobListener implements Clob, JDBCDelegator {
    /**
     * Holds the delegate clob that calls are passed through to.
     */
    private Clob delegateClob;

    /**
     * Creates a new ClobListener that wraps around the specified
     * delegate clob.
     * @param clob the delegate clob
     */
    ClobListener(final Clob clob) {
        this.delegateClob = clob;
    }

    /**
     * Sets the delegate clob for the listener clob.
     * @param clob the delegate clob
     */
    public final void setDelegateClob(final Clob clob) {
        this.delegateClob = clob;
    }

    /**
     * Gets the delegate clob for the listener clob.
     * @return the delegateClob
     */
    public final Clob getDelegateClob() {
        return delegateClob;
    }

    /**
     * Returns the delegate class name.
     * @return the delegate class name
     */
    public final String getDelegateClassName() {
        return delegateClob.getClass().getName();
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#getAsciiStream()
     */
    public final InputStream getAsciiStream() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        InputStream result = null;
        try {
            if (delegateClob != null) {
                result = delegateClob.getAsciiStream();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#getCharacterStream()
     */
    public final Reader getCharacterStream() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        Reader result = null;
        try {
            if (delegateClob != null) {
                result = delegateClob.getCharacterStream();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#getSubString(long, int)
     */
    public final String getSubString(final long pos, final int length)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                pos, length});
        String result = null;
        try {
            if (delegateClob != null) {
                result = delegateClob.getSubString(pos, length);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#length()
     */
    public final long length() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        long result = 0;
        try {
            if (delegateClob != null) {
                result = delegateClob.length();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#position(java.lang.String, long)
     */
    public final long position(final String searchstr, final long start)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                searchstr, start});
        long result = 0;
        try {
            if (delegateClob != null) {
                result = delegateClob.position(searchstr, start);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#position(java.sql.Clob, long)
     */
    public final long position(final Clob searchstr, final long start)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                searchstr, start});
        long result = 0;
        try {
            if (delegateClob != null) {
                result = delegateClob.position(searchstr, start);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#setAsciiStream(long)
     */
    public final OutputStream setAsciiStream(final long pos)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {pos});
        OutputStream result = null;
        try {
            if (delegateClob != null) {
                result = delegateClob.setAsciiStream(pos);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#setCharacterStream(long)
     */
    public final Writer setCharacterStream(final long pos) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {pos});
        Writer result = null;
        try {
            if (delegateClob != null) {
                result = delegateClob.setCharacterStream(pos);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#setString(long, java.lang.String)
     */
    public final int setString(final long pos, final String str)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                pos, str});
        int result = 0;
        try {
            if (delegateClob != null) {
                result = delegateClob.setString(pos, str);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#setString(long, java.lang.String, int, int)
     */
    public final int setString(final long pos, final String str,
            final int offset, final int len) throws SQLException {
        int result = 0;
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                pos, str, offset, len});
        try {
            if (delegateClob != null) {
                result = delegateClob.setString(pos, str, offset, len);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Clob#truncate(long)
     */
    public final void truncate(final long len) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {len});
        final Object result = null;
        try {
            delegateClob.truncate(len);
            JDBCCallListener.callEnd(begin, this, result);
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    //TODO Joudek Implement correctly

    @Override
    public void free() throws SQLException {
        delegateClob.free();
    }

    @Override
    public Reader getCharacterStream(long pos, long length) throws SQLException {
        return delegateClob.getCharacterStream(pos,length);
    }
}
