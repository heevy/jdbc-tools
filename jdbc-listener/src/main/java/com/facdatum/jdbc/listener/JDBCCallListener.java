/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener;

import java.io.PrintStream;

/**
 * JDBCCallListener is called before and after each JDBC call is made and
 * enables listener code to be plugged in to perform actions on the call
 * and exit from the JDBC method.
 */
public final class JDBCCallListener {

    /**
     * Identifies listener package name.
     */
    private static final String LISTENER_PACKAGE =
            "com.facdatum.jdbc.listener.driver";

    /**
     * Records whether the JDBCCallListener has been initialized.
     */
    private static boolean initialized = false;

    /**
     * Records whether the calls to the listeners should be skipped. This allows
     * listeners to filter calls to subsequent listeners and is updated via
     * the JDBCEvents.
     */
    private static boolean skip = false;

    /**
     * List of listeners that have been created to accept listener events.
     */
    private static JDBCListener[] listeners;

    /**
     * Output print stream for error messages.
     */
    private static PrintStream errLog = System.err;

    static {
        JDBCCallListener.init();
    }

    /**
     * Hidden constructor to stop instances of class being created.
     */
    private JDBCCallListener() {
        //do nothing
    }
    /**
     * Returns the listeners that have been configured.
     * @return the JDBC listeners
     */
    public static JDBCListener[] getListeners() {
        return listeners; //NOPMD
    }

    /**
     * Called on initialization of class and initializes all configured
     * listeners.
     */
    public static void init() {
        if (!initialized) {
            JDBCListenerProperties.init();
            final Thread currentThread = Thread.currentThread(); //NOPMD
            final ClassLoader classLoader = currentThread.
                    getContextClassLoader();
            final String[] listenerNames =
                    JDBCListenerProperties.getListeners();
            listeners = new JDBCListener[listenerNames.length];
            for (int i = 0; i < listenerNames.length; i++) {
                try {
                    listeners[i] = (JDBCListener) classLoader.loadClass(
                            listenerNames[i]).newInstance();
                    listeners[i].init();
                } catch (Exception e) {
                    errLog.println("Cannot load JDBCListener '"
                            + listenerNames[i] + "'.");
                    e.printStackTrace(errLog);
                }
            }
            initialized = true;
        }
    }

    /**
     * Sets the state of the JDBCCallListener to be un-initialized. (Used for
     * testing.)
     */
    public static void resetInit() {
        initialized = false;
    }

    /**
     * Calls all configured listeners acceptBegin methods.
     * @param instance the object on which the JDBC method was called
     * @param args the arguments to the JDBC method
     * @return the time that the call was made
     */
    public static long callBegin(final Object instance,
            final Object[] args) {
        final long start = System.currentTimeMillis();
        final StackTraceElement[] cleanStackTrace = getCleanStackTrace();
        if (listeners == null) {
            JDBCCallListener.init();
        }
        final JDBCCallEvent event = new JDBCCallEvent(start, instance,
                cleanStackTrace, args);
        skip = event.getFilterListeners();
        for (int i = 0; i < listeners.length; i++) {
            if (!skip) {
                listeners[i].acceptBegin(event);
                skip = event.getFilterListeners();
            }
        }
        return start;
    }


    /**
     * Calls all configured listeners acceptException methods.
     * @param beginTime time that the callBegin method was called
     * @param instance the object on which the JDBC method was called
     * @param exception the exception thrown by the JDBC method
     */
    public static void callException(final long beginTime,
            final Object instance, final Exception exception) {
        final long start = System.currentTimeMillis();
        final StackTraceElement[] cleanStackTrace = getCleanStackTrace();
        if (listeners == null) {
            JDBCCallListener.init();
        }
        final JDBCExceptionEvent event = new JDBCExceptionEvent(start, instance,
                cleanStackTrace, exception, (start - beginTime));
        for (int i = 0; i < listeners.length; i++) {
            if (!skip) {
                listeners[i].acceptException(event);
                skip = event.getFilterListeners();
            }
        }
    }

    /**
     * Calls all configured listeners acceptEnd methods.
     * @param beginTime time that the callBegin method was called
     * @param instance the object on which the JDBC method was called
     * @param result the return from the called JDBC method
     */
    public static void callEnd(final long beginTime,
            final Object instance, final Object result) {
        JDBCCallListener.callEnd(beginTime, instance, result, null);
    }

    /**
     * Calls all configured listeners acceptEnd methods.
     * @param beginTime time that the callBegin method was called
     * @param instance the object on which the JDBC method was called
     * @param result the return from the called JDBC method
     * @param wrappedResult the wrapped return from the called JDBC method
     */
    public static void callEnd(final long beginTime,
            final Object instance, final Object result,
            final Object wrappedResult) {
        final long start = System.currentTimeMillis();
        final StackTraceElement[] cleanStackTrace = getCleanStackTrace();
        if (listeners == null) {
            JDBCCallListener.init();
        }
        final JDBCReturnEvent event = new JDBCReturnEvent(start, instance,
                cleanStackTrace, result, (start - beginTime), wrappedResult);
        for (int i = 0; i < listeners.length; i++) {
            if (!skip) {
                listeners[i].acceptEnd(event);
                skip = event.getFilterListeners();
            }
        }
    }

    /**
     * Trims the top off of the stack trace removing layers STACK_DEPTH
     * below where the listener call appears in the stack trace.
     * @return the clean stack trace.
     */
    private static StackTraceElement[] getCleanStackTrace() {
        final Exception e = new Exception(); //NOPMD
        final StackTraceElement[] stackTrace = e.getStackTrace();
        int traceElement = 0;
        boolean done = false;
        for (int i = 0; i < stackTrace.length && !done; i++) {
            final String className = stackTrace[i].getClassName();
            if (className.startsWith(LISTENER_PACKAGE)) {
                traceElement = i;
                done = true;
            }
        }
        final StackTraceElement[] cleanStackTrace =
            new StackTraceElement[stackTrace.length - traceElement];
        System.arraycopy(stackTrace, traceElement,
                cleanStackTrace, 0, cleanStackTrace.length);
        return cleanStackTrace;
    }
}
