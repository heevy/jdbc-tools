/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener;


/**
 * Holds the information captured at the time that the JDBCEvent occurs.
 * Sub-classed by JDBCCallEvent and JDBCReturnEvent.
 */
public class JDBCEvent implements ListenerEvent {

    /**
     * Holds depth of JDBC Call in stack trace.
     */
    private static final int JDBC_DEPTH = 0;

    /**
     * Holds depth of the method that the JDBC Layer was called from in
     * stack trace.
     */
    private static final int CALL_DEPTH = 1;

    /**
     * The JDBC java.sql package name.
     */
    private static final String JAVA_SQL_PREFIX = "java.sql";

    /**
     * The JDBC javax.sql package name.
     */
    private static final String JAVAX_SQL_PREFIX = "javax.sql";

    /**
     * Holds the time that the JDBC event occurred.
     */
    private final long time; //NOPMD

    /**
     * Holds the object instance that the JDBC call was made on.
     */
    private final Object object; //NOPMD

    /**
     * Holds the stack trace of calls into the JDBC Layer.
     */
    private final StackTraceElement[] stackTrace; //NOPMD

    /**
     * Holds the filter value that can be set to stop any further listeners
     * being called.
     */
    private boolean filterListeners = false; //NOPMD

    /**
     * Creates a JDBCEvent.
     * @param eventTime the time of the event
     * @param callObject the object called in JDBC Layer
     * @param callStackTrace the stack trace of calls into the JDBC Layer
     */
    public JDBCEvent(final long eventTime, final Object callObject,
            final StackTraceElement[] callStackTrace) {
        this.time = eventTime;
        this.object = callObject;
        this.stackTrace = callStackTrace.clone();
    }

    /**
     * Returns the time of the jdbc event.
     * @return the event time
     */
    public final long getTime() {
        return this.time;
    }

    /**
     * Returns the object that the jdbc method was called.
     * @return the called object
     */
    public final Object getObject() {
        return this.object;
    }

    /**
     * Returns the stack trace of calls into the JDBC layer.
     * @return the stack trace
     */
    public final StackTraceElement[] getStackTrace() {
        return this.stackTrace.clone();
    }

    /**
     * Returns the JDBC method call.
     * @return the method call
     */
    public final String getJDBCMethod() {
        final StringBuffer jdbcCall = new StringBuffer("");
        if  (((JDBCDelegator) object).getDelegateClassName() == null) {
            jdbcCall.append(stackTrace[JDBC_DEPTH].getClassName());
        } else {
            jdbcCall.append(((JDBCDelegator) object).getDelegateClassName());
        }
        jdbcCall.append('.');
        jdbcCall.append(stackTrace[JDBC_DEPTH].getMethodName());
        return  jdbcCall.toString();
    }

    /**
     * Returns the JDBC method call on interface.
     * @return the JDBC interface method call
     */
    @SuppressWarnings("unchecked")
    public final String getJDBCInterfaceMethod() {
        final String  methodName = stackTrace[JDBC_DEPTH].getMethodName();
        final Class[] interfaces =
                ((JDBCDelegator) object).getClass().getInterfaces();
        for (int i = 0; i < interfaces.length; i++) {
            final String interfaceName = interfaces[i].getCanonicalName();
            if (interfaceName.startsWith(JAVA_SQL_PREFIX)
                    || interfaceName.startsWith(
                            JAVAX_SQL_PREFIX)) {
                return interfaceName + "." + methodName;
            }
        }
        return null;
    }

    /**
     * Returns the method that called into the JDBC layer.
     * @return the calling method
     */
    public final String getCallingMethod() {
        return stackTrace[CALL_DEPTH].getClassName()
            + "." + stackTrace[CALL_DEPTH].getMethodName();
    }

    /**
     * Sets the value of filterListeners.
     * @param filter the value to set filterListeners to
     */
    public final void setFilterListeners(final boolean filter) {
        this.filterListeners = filter;
    }

    /**
     * Returns whether further listeners should be skipped.
     * @return true if listeners should be filtered; false otherwise
     */
    public final boolean getFilterListeners() { //NOPMD
        return filterListeners;
    }
}
