//NOCS (for file length check)
/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver; //NOPMD

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Date;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Map;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCDelegator;

/**
 * CallableStatementListener wraps around a delegate CallableStatement object
 * and passes all calls to the listener.
 */
public class CallableStatementListener extends PreparedStatementListener // NOPMD
		implements CallableStatement, JDBCDelegator {

	/**
	 * Creates a new CallableStatementListener that wraps around the specified
	 * delegate callable statement.
	 * 
	 * @param callableStatement
	 *            the delegate callable statement
	 */
	CallableStatementListener(final CallableStatement callableStatement) {
		super((PreparedStatement) callableStatement);
	}

	/**
	 * Sets the delegate callable statement for the listener callable statement.
	 * 
	 * @param statement
	 *            the delegate callable statement
	 */
	public final void setDelegateStatement(final CallableStatement statement) {
		this.delegateStatement = (Statement) statement;
	}

	/**
	 * @return the delegateCallableStatement
	 */
	public final CallableStatement getDelegateStatement() {
		return (CallableStatement) delegateStatement;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getArray(int)
	 */
	public final Array getArray(final int parameterIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex });
		ArrayListener result = null;
		Array delegateResult = null;
		try {
			if (delegateStatement != null) {
				delegateResult = ((CallableStatement) delegateStatement).getArray(parameterIndex);
				result = new ArrayListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getArray(java.lang.String)
	 */
	public final Array getArray(final String parameterName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName });
		ArrayListener result = null;
		Array delegateResult = null;
		try {
			if (delegateStatement != null) {
				delegateResult = ((CallableStatement) delegateStatement).getArray(parameterName);
				result = new ArrayListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getBigDecimal(int)
	 */
	public final BigDecimal getBigDecimal(final int parameterIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex });
		BigDecimal result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getBigDecimal(parameterIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getBigDecimal(java.lang.String)
	 */
	public final BigDecimal getBigDecimal(final String parameterName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName });
		BigDecimal result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getBigDecimal(parameterName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getBigDecimal(int, int)
	 */
	@SuppressWarnings("deprecation")
	public final BigDecimal getBigDecimal(final int parameterIndex, final int scale) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, scale });
		BigDecimal result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getBigDecimal(parameterIndex, scale);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getBlob(int)
	 */
	public final Blob getBlob(final int parameterIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex });
		BlobListener result = null;
		Blob delegateResult = null;
		try {
			if (delegateStatement != null) {
				delegateResult = ((CallableStatement) delegateStatement).getBlob(parameterIndex);
				result = new BlobListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getBlob(java.lang.String)
	 */
	public final Blob getBlob(final String parameterName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName });
		BlobListener result = null;
		Blob delegateResult = null;
		try {
			if (delegateStatement != null) {
				delegateResult = ((CallableStatement) delegateStatement).getBlob(parameterName);
				result = new BlobListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getBoolean(int)
	 */
	public final boolean getBoolean(final int parameterIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex });
		boolean result = false;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getBoolean(parameterIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getBoolean(java.lang.String)
	 */
	public final boolean getBoolean(final String parameterName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName });
		boolean result = false;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getBoolean(parameterName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getByte(int)
	 */
	public final byte getByte(final int parameterIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex });
		byte result = 0;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getByte(parameterIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getByte(java.lang.String)
	 */
	public final byte getByte(final String parameterName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName });
		byte result = 0;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getByte(parameterName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getBytes(int)
	 */
	public final byte[] getBytes(final int parameterIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex });
		byte[] result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getBytes(parameterIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getBytes(java.lang.String)
	 */
	public final byte[] getBytes(final String parameterName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName });
		byte[] result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getBytes(parameterName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getClob(int)
	 */
	public final Clob getClob(final int parameterIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex });
		ClobListener result = null;
		Clob delegateResult = null;
		try {
			if (delegateStatement != null) {
				delegateResult = ((CallableStatement) delegateStatement).getClob(parameterIndex);
				result = new ClobListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getClob(java.lang.String)
	 */
	public final Clob getClob(final String parameterName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName });
		ClobListener result = null;
		Clob delegateResult = null;
		try {
			if (delegateStatement != null) {
				delegateResult = ((CallableStatement) delegateStatement).getClob(parameterName);
				result = new ClobListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getDate(int)
	 */
	public final Date getDate(final int parameterIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex });
		Date result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getDate(parameterIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getDate(java.lang.String)
	 */
	public final Date getDate(final String parameterName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName });
		Date result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getDate(parameterName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getDate(int, java.util.Calendar)
	 */
	public final Date getDate(final int parameterIndex, final Calendar cal) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, cal });
		Date result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getDate(parameterIndex, cal);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getDate(java.lang.String, java.util.Calendar)
	 */
	public final Date getDate(final String parameterName, final Calendar cal) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, cal });
		Date result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getDate(parameterName, cal);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getDouble(int)
	 */
	public final double getDouble(final int parameterIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex });
		double result = 0;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getDouble(parameterIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getDouble(java.lang.String)
	 */
	public final double getDouble(final String parameterName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName });
		double result = 0;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getDouble(parameterName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getFloat(int)
	 */
	public final float getFloat(final int parameterIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex });
		float result = 0;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getFloat(parameterIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getFloat(java.lang.String)
	 */
	public final float getFloat(final String parameterName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName });
		float result = 0;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getFloat(parameterName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getInt(int)
	 */
	public final int getInt(final int parameterIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex });
		int result = 0;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getInt(parameterIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getInt(java.lang.String)
	 */
	public final int getInt(final String parameterName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName });
		int result = 0;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getInt(parameterName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getLong(int)
	 */
	public final long getLong(final int parameterIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex });
		long result = 0;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getLong(parameterIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getLong(java.lang.String)
	 */
	public final long getLong(final String parameterName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName });
		long result = 0;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getLong(parameterName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getObject(int)
	 */
	public final Object getObject(final int parameterIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex });
		Object result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getObject(parameterIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getObject(java.lang.String)
	 */
	public final Object getObject(final String parameterName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName });
		Object result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getObject(parameterName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getObject(int, java.util.Map)
	 */
	public final Object getObject(final int parameterIndex, final Map<String, Class<?>> map) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, map });
		Object result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getObject(parameterIndex, map);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getObject(java.lang.String, java.util.Map)
	 */
	public final Object getObject(final String parameterName, final Map<String, Class<?>> map) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, map });
		Object result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getObject(parameterName, map);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getRef(int)
	 */
	public final Ref getRef(final int parameterIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex });
		RefListener result = null;
		Ref delegateResult = null;
		try {
			if (delegateStatement != null) {
				delegateResult = ((CallableStatement) delegateStatement).getRef(parameterIndex);
				result = new RefListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getRef(java.lang.String)
	 */
	public final Ref getRef(final String parameterName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName });
		RefListener result = null;
		Ref delegateResult = null;
		try {
			if (delegateStatement != null) {
				delegateResult = ((CallableStatement) delegateStatement).getRef(parameterName);
				result = new RefListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getShort(int)
	 */
	public final short getShort(final int parameterIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex });
		Short result = 0;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getShort(parameterIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getShort(java.lang.String)
	 */
	public final short getShort(final String parameterName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName });
		Short result = 0;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getShort(parameterName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getString(int)
	 */
	public final String getString(final int parameterIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex });
		String result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getString(parameterIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getString(java.lang.String)
	 */
	public final String getString(final String parameterName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName });
		String result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getString(parameterName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getTime(int)
	 */
	public final Time getTime(final int parameterIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex });
		Time result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getTime(parameterIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getTime(java.lang.String)
	 */
	public final Time getTime(final String parameterName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName });
		Time result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getTime(parameterName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getTime(int, java.util.Calendar)
	 */
	public final Time getTime(final int parameterIndex, final Calendar cal) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, cal });
		Time result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getTime(parameterIndex, cal);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getTime(java.lang.String, java.util.Calendar)
	 */
	public final Time getTime(final String parameterName, final Calendar cal) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, cal });
		Time result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getTime(parameterName, cal);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getTimestamp(int)
	 */
	public final Timestamp getTimestamp(final int parameterIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex });
		Timestamp result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getTimestamp(parameterIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getTimestamp(java.lang.String)
	 */
	public final Timestamp getTimestamp(final String parameterName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName });
		Timestamp result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getTimestamp(parameterName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getTimestamp(int, java.util.Calendar)
	 */
	public final Timestamp getTimestamp(final int parameterIndex, final Calendar cal) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, cal });
		Timestamp result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getTimestamp(parameterIndex, cal);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getTimestamp(java.lang.String, java.util.Calendar)
	 */
	public final Timestamp getTimestamp(final String parameterName, final Calendar cal) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, cal });
		Timestamp result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getTimestamp(parameterName, cal);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getURL(int)
	 */
	public final URL getURL(final int parameterIndex) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex });
		URL result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getURL(parameterIndex);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#getURL(java.lang.String)
	 */
	public final URL getURL(final String parameterName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName });
		URL result = null;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).getURL(parameterName);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#registerOutParameter(int, int)
	 */
	public final void registerOutParameter(final int parameterIndex, final int sqlType) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, sqlType });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).registerOutParameter(parameterIndex, sqlType);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#registerOutParameter(java.lang.String, int)
	 */
	public final void registerOutParameter(final String parameterName, final int sqlType) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, sqlType });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).registerOutParameter(parameterName, sqlType);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#registerOutParameter(int, int, int)
	 */
	public final void registerOutParameter(final int parameterIndex, final int sqlType, final int scale) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterIndex, sqlType, scale });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).registerOutParameter(parameterIndex, sqlType, scale);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#registerOutParameter(int, int, java.lang.String)
	 */
	public final void registerOutParameter(final int paramIndex, final int sqlType, final String typeName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { paramIndex, sqlType, typeName });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).registerOutParameter(paramIndex, sqlType, typeName);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#registerOutParameter(java.lang.String, int, int)
	 */
	public final void registerOutParameter(final String parameterName, final int sqlType, final int scale) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, sqlType, scale });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).registerOutParameter(parameterName, sqlType, scale);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#registerOutParameter(java.lang.String, int, java.lang.String)
	 */
	public final void registerOutParameter(final String parameterName, final int sqlType, final String typeName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, sqlType, typeName });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).registerOutParameter(parameterName, sqlType, typeName);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setAsciiStream(java.lang.String, java.io.InputStream, int)
	 */
	public final void setAsciiStream(final String parameterName, final InputStream val, final int length) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val, length });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setAsciiStream(parameterName, val, length);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setBigDecimal(java.lang.String, java.math.BigDecimal)
	 */
	public final void setBigDecimal(final String parameterName, final BigDecimal val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setBigDecimal(parameterName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setBinaryStream(java.lang.String, java.io.InputStream, int)
	 */
	public final void setBinaryStream(final String parameterName, final InputStream val, final int length) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val, length });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setBinaryStream(parameterName, val, length);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setBoolean(java.lang.String, boolean)
	 */
	public final void setBoolean(final String parameterName, final boolean val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setBoolean(parameterName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setByte(java.lang.String, byte)
	 */
	public final void setByte(final String parameterName, final byte val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setByte(parameterName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setBytes(java.lang.String, byte[])
	 */
	public final void setBytes(final String parameterName, final byte[] val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setBytes(parameterName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setCharacterStream(java.lang.String, java.io.Reader, int)
	 */
	public final void setCharacterStream(final String parameterName, final Reader reader, final int length) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, reader, length });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setCharacterStream(parameterName, reader, length);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setDate(java.lang.String, java.sql.Date)
	 */
	public final void setDate(final String parameterName, final Date val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setDate(parameterName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setDate(java.lang.String, java.sql.Date, java.util.Calendar)
	 */
	public final void setDate(final String parameterName, final Date val, final Calendar cal) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setDate(parameterName, val, cal);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setDouble(java.lang.String, double)
	 */
	public final void setDouble(final String parameterName, final double val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setDouble(parameterName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setFloat(java.lang.String, float)
	 */
	public final void setFloat(final String parameterName, final float val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setFloat(parameterName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setInt(java.lang.String, int)
	 */
	public final void setInt(final String parameterName, final int val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setInt(parameterName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setLong(java.lang.String, long)
	 */
	public final void setLong(final String parameterName, final long val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setLong(parameterName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setNull(java.lang.String, int)
	 */
	public final void setNull(final String parameterName, final int sqlType) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, sqlType });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setNull(parameterName, sqlType);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setNull(java.lang.String, int, java.lang.String)
	 */
	public final void setNull(final String parameterName, final int sqlType, final String typeName) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, sqlType, typeName });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setNull(parameterName, sqlType, typeName);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setObject(java.lang.String, java.lang.Object)
	 */
	public final void setObject(final String parameterName, final Object val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setObject(parameterName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setObject(java.lang.String, java.lang.Object, int)
	 */
	public final void setObject(final String parameterName, final Object val, final int targetSqlType) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val, targetSqlType });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setObject(parameterName, val, targetSqlType);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setObject(java.lang.String, java.lang.Object, int, int)
	 */
	public final void setObject(final String parameterName, final Object val, final int targetSqlType, final int scale) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val, targetSqlType, scale });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setObject(parameterName, val, targetSqlType, scale);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setShort(java.lang.String, short)
	 */
	public final void setShort(final String parameterName, final short val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setShort(parameterName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setString(java.lang.String, java.lang.String)
	 */
	public final void setString(final String parameterName, final String val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setString(parameterName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setTime(java.lang.String, java.sql.Time)
	 */
	public final void setTime(final String parameterName, final Time val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setTime(parameterName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setTime(java.lang.String, java.sql.Time, java.util.Calendar)
	 */
	public final void setTime(final String parameterName, final Time val, final Calendar cal) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val, cal });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setTime(parameterName, val, cal);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setTimestamp(java.lang.String, java.sql.Timestamp)
	 */
	public final void setTimestamp(final String parameterName, final Timestamp val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setTimestamp(parameterName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setTimestamp(java.lang.String, java.sql.Timestamp, java.util.Calendar)
	 */
	public final void setTimestamp(final String parameterName, final Timestamp val, final Calendar cal) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val, cal });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setTimestamp(parameterName, val, cal);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#setURL(java.lang.String, java.net.URL)
	 */
	public final void setURL(final String parameterName, final URL val) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { parameterName, val });
		final Object result = null;
		try {
			((CallableStatement) delegateStatement).setURL(parameterName, val);
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.CallableStatement#wasNull()
	 */
	public final boolean wasNull() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		boolean result = false;
		try {
			if (delegateStatement != null) {
				result = ((CallableStatement) delegateStatement).wasNull();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	// TODO Joudek implement correctly

	@Override
	public RowId getRowId(int parameterIndex) throws SQLException {
		if (delegateStatement != null) {
			return ((CallableStatement) delegateStatement).getRowId(parameterIndex);
		}
		return null;
	}

	@Override
	public RowId getRowId(String parameterName) throws SQLException {
		if (delegateStatement != null) {
			return ((CallableStatement) delegateStatement).getRowId(parameterName);
		}
		return null;
	}

	@Override
	public void setRowId(String parameterName, RowId x) throws SQLException {
		if (delegateStatement != null) {
			((CallableStatement) delegateStatement).setRowId(parameterName, x);
		}

	}

	@Override
	public void setNString(String parameterName, String value) throws SQLException {
		if (delegateStatement != null) {
			((CallableStatement) delegateStatement).setNString(parameterName, value);
		}

	}

	@Override
	public void setNCharacterStream(String parameterName, Reader value, long length) throws SQLException {
		if (delegateStatement != null) {
			((CallableStatement) delegateStatement).setNCharacterStream(parameterName, value, length);
		}

	}

	@Override
	public void setNClob(String parameterName, NClob value) throws SQLException {
		if (delegateStatement != null) {
			((CallableStatement) delegateStatement).setNClob(parameterName, value);
		}

	}

	@Override
	public void setClob(String parameterName, Reader reader, long length) throws SQLException {
		if (delegateStatement != null) {
			((CallableStatement) delegateStatement).setClob(parameterName, reader, length);
		}

	}

	@Override
	public void setBlob(String parameterName, InputStream inputStream, long length) throws SQLException {
		if (delegateStatement != null) {
			((CallableStatement) delegateStatement).setBlob(parameterName, inputStream, length);
		}

	}

	@Override
	public void setNClob(String parameterName, Reader reader, long length) throws SQLException {
		if (delegateStatement != null) {
			((CallableStatement) delegateStatement).setNClob(parameterName, reader, length);
		}

	}

	@Override
	public NClob getNClob(int parameterIndex) throws SQLException {
		if (delegateStatement != null) {
			return ((CallableStatement) delegateStatement).getNClob(parameterIndex);
		}
		return null;
	}

	@Override
	public NClob getNClob(String parameterName) throws SQLException {
		if (delegateStatement != null) {
			return ((CallableStatement) delegateStatement).getNClob(parameterName);
		}
		return null;
	}

	@Override
	public void setSQLXML(String parameterName, SQLXML xmlObject) throws SQLException {
		if (delegateStatement != null) {
			((CallableStatement) delegateStatement).setSQLXML(parameterName, xmlObject);
		}

	}

	@Override
	public SQLXML getSQLXML(int parameterIndex) throws SQLException {
		if (delegateStatement != null) {
			return ((CallableStatement) delegateStatement).getSQLXML(parameterIndex);
		}
		return null;
	}

	@Override
	public SQLXML getSQLXML(String parameterName) throws SQLException {
		if (delegateStatement != null) {
			return ((CallableStatement) delegateStatement).getSQLXML(parameterName);
		}
		return null;
	}

	@Override
	public String getNString(int parameterIndex) throws SQLException {
		if (delegateStatement != null) {
			return ((CallableStatement) delegateStatement).getNString(parameterIndex);
		}
		return null;
	}

	@Override
	public String getNString(String parameterName) throws SQLException {
		if (delegateStatement != null) {
			return ((CallableStatement) delegateStatement).getNString(parameterName);
		}
		return null;
	}

	@Override
	public Reader getNCharacterStream(int parameterIndex) throws SQLException {
		if (delegateStatement != null) {
			return ((CallableStatement) delegateStatement).getNCharacterStream(parameterIndex);
		}
		return null;
	}

	@Override
	public Reader getNCharacterStream(String parameterName) throws SQLException {
		if (delegateStatement != null) {
			return ((CallableStatement) delegateStatement).getNCharacterStream(parameterName);
		}
		return null;
	}

	@Override
	public Reader getCharacterStream(int parameterIndex) throws SQLException {
		if (delegateStatement != null) {
			return ((CallableStatement) delegateStatement).getCharacterStream(parameterIndex);
		}
		return null;
	}

	@Override
	public Reader getCharacterStream(String parameterName) throws SQLException {
		if (delegateStatement != null) {
			return ((CallableStatement) delegateStatement).getCharacterStream(parameterName);
		}
		return null;
	}

	@Override
	public void setBlob(String parameterName, Blob x) throws SQLException {
		if (delegateStatement != null) {
			((CallableStatement) delegateStatement).setBlob(parameterName, x);
		}

	}

	@Override
	public void setClob(String parameterName, Clob x) throws SQLException {
		if (delegateStatement != null) {
			((CallableStatement) delegateStatement).setClob(parameterName, x);
		}

	}

	@Override
	public void setAsciiStream(String parameterName, InputStream x, long length) throws SQLException {
		if (delegateStatement != null) {
			((CallableStatement) delegateStatement).setAsciiStream(parameterName, x, length);
		}

	}

	@Override
	public void setBinaryStream(String parameterName, InputStream x, long length) throws SQLException {
		if (delegateStatement != null) {
			((CallableStatement) delegateStatement).setBinaryStream(parameterName, x, length);
		}

	}

	@Override
	public void setCharacterStream(String parameterName, Reader reader, long length) throws SQLException {
		if (delegateStatement != null) {
			((CallableStatement) delegateStatement).setCharacterStream(parameterName, reader, length);
		}

	}

	@Override
	public void setAsciiStream(String parameterName, InputStream x) throws SQLException {
		if (delegateStatement != null) {
			((CallableStatement) delegateStatement).setAsciiStream(parameterName, x);
		}

	}

	@Override
	public void setBinaryStream(String parameterName, InputStream x) throws SQLException {
		if (delegateStatement != null) {
			((CallableStatement) delegateStatement).setBinaryStream(parameterName, x);
		}

	}

	@Override
	public void setCharacterStream(String parameterName, Reader reader) throws SQLException {
		if (delegateStatement != null) {
			((CallableStatement) delegateStatement).setCharacterStream(parameterName, reader);
		}

	}

	@Override
	public void setNCharacterStream(String parameterName, Reader value) throws SQLException {
		if (delegateStatement != null) {
			((CallableStatement) delegateStatement).setNCharacterStream(parameterName, value);
		}

	}

	@Override
	public void setClob(String parameterName, Reader reader) throws SQLException {
		if (delegateStatement != null) {
			((CallableStatement) delegateStatement).setClob(parameterName, reader);
		}

	}

	@Override
	public void setBlob(String parameterName, InputStream inputStream) throws SQLException {
		if (delegateStatement != null) {
			((CallableStatement) delegateStatement).setBlob(parameterName, inputStream);
		}

	}

	@Override
	public void setNClob(String parameterName, Reader reader) throws SQLException {
		if (delegateStatement != null) {
			((CallableStatement) delegateStatement).setNClob(parameterName, reader);
		}

	}

	@Override
	public <T> T getObject(int parameterIndex, Class<T> type) throws SQLException {
        if (delegateStatement != null) {
            return ((CallableStatement) delegateStatement).getObject(parameterIndex, type);
        }
        return null;
	}

	@Override
	public <T> T getObject(String parameterName, Class<T> type) throws SQLException {
        if (delegateStatement != null) {
            return ((CallableStatement) delegateStatement).getObject(parameterName,type);
        }
        return null;
	}
}
