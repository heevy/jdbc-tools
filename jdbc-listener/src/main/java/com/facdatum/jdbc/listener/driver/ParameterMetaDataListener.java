/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.ParameterMetaData;
import java.sql.SQLException;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCDelegator;


/**
 * ParameterMetaDataListener wraps around a delegate ParameterMetaData object
 * and passes all calls to the listener.
 */
public class ParameterMetaDataListener implements ParameterMetaData,
        JDBCDelegator {
    /**
     * Holds the delegate metadata that calls are passed through to.
     */
    private ParameterMetaData delegateMetaData;

    /**
     * Creates a new ParameterMetaDataListener that wraps around the specified
     * delegate metadata.
     * @param metaData the delegate parameter metadata
     */
    ParameterMetaDataListener(final ParameterMetaData metaData) {
        this.delegateMetaData = metaData;
    }

    /**
     * Sets the delegate metadata for the listener parameter metadata.
     * @param metaData the delegate parameter metadata
     */
    public final void setDelegateMetaData(final ParameterMetaData metaData) {
        this.delegateMetaData = metaData;
    }

    /**
     * Gets the delegate parameter metadata for the listener metadata.
     * @return the delegateMetaData
     */
    public final ParameterMetaData getDelegateMetaData() {
        return delegateMetaData;
    }

    /**
     * Returns the delegate class name.
     * @return the delegate class name
     */
    public final String getDelegateClassName() {
        return delegateMetaData.getClass().getName();
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ParameterMetaData#getParameterClassName(int)
     */
    public final String getParameterClassName(final int param)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                param});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getParameterClassName(param);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     */
    public final int getParameterCount() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getParameterCount();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ParameterMetaData#getParameterMode(int)
     */
    public final int getParameterMode(final int param) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                param});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getParameterMode(param);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ParameterMetaData#getParameterType(int)
     */
    public final int getParameterType(final int param) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                param});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getParameterType(param);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ParameterMetaData#getParameterTypeName(int)
     */
    public final String getParameterTypeName(final int param)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                param});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getParameterTypeName(param);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ParameterMetaData#getPrecision(int)
     */
    public final int getPrecision(final int param) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                param});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getPrecision(param);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ParameterMetaData#getScale(int)
     */
    public final int getScale(final int param) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                param});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getScale(param);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ParameterMetaData#isNullable(int)
     */
    public final int isNullable(final int param) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                param});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.isNullable(param);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ParameterMetaData#isSigned(int)
     */
    public final boolean isSigned(final int param) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                param});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.isSigned(param);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    //TODO Joudek implement correctly

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        if (delegateMetaData != null) {
            return  delegateMetaData.unwrap(iface);
        }
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        if (delegateMetaData != null) {
            return  delegateMetaData.isWrapperFor(iface);
        }
        return false;
    }
}
