/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.Reference;
import javax.naming.spi.ObjectFactory;

/**
 * Creates instances of DataSourceListeners from object references.
 */
public class DataSourceListenerFactory implements ObjectFactory {

    /**
     * Holds the name of the DataSourceListener class.
     */
    protected static final String DS_CLASS_NAME =
            DataSourceListener.class.getName();

    /**
     * Holds the name of the ConnectionPoolDataSourceListener class.
     */
    protected static final String POOL_DS_CLASS_NAME = //NOPMD
            ConnectionPoolDataSourceListener.class.getName();

    /**
     * Holds the name of the XADataSourceListener class.
     */
    protected static final String XA_DS_CLASS_NAME =
            XADataSourceListener.class.getName();

    /**
     * Creates data source instance from a naming reference.
     * {@inheritDoc}
     * @see javax.naming.spi.ObjectFactory#getObjectInstance(java.lang.Object,
     *      javax.naming.Name, javax.naming.Context, java.util.Hashtable)
     */
    public final Object getObjectInstance(final Object refObj, final Name name,
            final Context context, final Hashtable < ? , ? > env)
            throws Exception { //NOPMD
        final Reference ref = (Reference) refObj;
        final String className = ref.getClassName();
        if (className != null && (className.equals(DS_CLASS_NAME)
                || className.equals(POOL_DS_CLASS_NAME)
                || className.equals(XA_DS_CLASS_NAME))) {
            DataSourceListener dataSource = null;
            try {
                dataSource = (DataSourceListener)
                        Class.forName(className).newInstance();
            } catch (Exception e) {
                throw new RuntimeException(//NOPMD
                        "Unable to create DataSource of class '"
                        + className + "': " + e);
            }
            dataSource.setDelegateClassName(
                    (String) ref.get("dataSourceName").getContent());
            return dataSource;
        }
        return null;
    }
}
