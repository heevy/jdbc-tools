/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener;

/**
 * Interface describing the ListenerReturnEvent that is passed to a JDBCListener
 * acceptEnd method.
 */
public interface ListenerReturnEvent extends ListenerEvent {

    /**
     * Returns the result from the call to the JDBC method.
     * @return the return value
     */
    Object getResult();

    /**
     * Returns the wrapped result from the call to the JDBC method. (Added in
     * jdbc-tools version 1.1.)
     * @return the wrapped return value; or null if result is not wrapped
     */
    Object getWrappedResult();

    /**
     * Returns the duration of the JDBC method call.
     * @return the duration
     */
    long getDuration();
}
