/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCDelegator;


/**
 * ResultSetMetaDataListener wraps around a delegate ResultSetMetaData object
 * and passes all calls to the listener.
 */
public class ResultSetMetaDataListener implements ResultSetMetaData,
        JDBCDelegator {
    /**
     * Holds the delegate metadata that calls are passed through to.
     */
    private ResultSetMetaData delegateMetaData;

    /**
     * Creates a new ResultSetMetaDataListener that wraps around the specified
     * delegate metadata.
     * @param metaData the delegate parameter metadata
     */
    ResultSetMetaDataListener(final ResultSetMetaData metaData) {
        this.delegateMetaData = metaData;
    }

    /**
     * Sets the delegate metadata for the listener resultset metadata.
     * @param metaData the delegate result metadata
     */
    public final void setDelegateMetaData(final ResultSetMetaData metaData) {
        this.delegateMetaData = metaData;
    }

    /**
     * Gets the delegate resultset metadata for the listener metadata.
     * @return the delegateMetaData
     */
    public final ResultSetMetaData getDelegateMetaData() {
        return delegateMetaData;
    }

    /**
     * Returns the delegate class name.
     * @return the delegate class name
     */
    public final String getDelegateClassName() {
        return delegateMetaData.getClass().getName();
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getCatalogName(int)
     */
    public final String getCatalogName(final int column) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                column});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getCatalogName(column);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getColumnClassName(int)
     */
    public final String getColumnClassName(final int column)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                column});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getColumnClassName(column);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getColumnCount()
     */
    public final int getColumnCount() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getColumnCount();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getColumnDisplaySize(int)
     */
    public final int getColumnDisplaySize(final int column)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                column});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getColumnDisplaySize(column);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getColumnLabel(int)
     */
    public final String getColumnLabel(final int column) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                column});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getColumnLabel(column);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getColumnName(int)
     */
    public final String getColumnName(final int column) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                column});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getColumnName(column);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getColumnType(int)
     */
    public final int getColumnType(final int column) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                column});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getColumnType(column);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getColumnTypeName(int)
     */
    public final String getColumnTypeName(final int column)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                column});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getColumnTypeName(column);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getPrecision(int)
     */
    public final int getPrecision(final int column) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                column});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getPrecision(column);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getScale(int)
     */
    public final int getScale(final int column) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                column});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getScale(column);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getSchemaName(int)
     */
    public final String getSchemaName(final int column) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                column});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getSchemaName(column);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#getTableName(int)
     */
    public final String getTableName(final int column) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                column});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getTableName(column);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#isAutoIncrement(int)
     */
    public final boolean isAutoIncrement(final int column)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                column});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.isAutoIncrement(column);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#isCaseSensitive(int)
     */
    public final boolean isCaseSensitive(final int column) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                column});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.isCaseSensitive(column);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#isCurrency(int)
     */
    public final boolean isCurrency(final int column) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                column});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.isCurrency(column);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#isDefinitelyWritable(int)
     */
    public final boolean isDefinitelyWritable(final int column)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                column});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.isDefinitelyWritable(column);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#isNullable(int)
     */
    public final int isNullable(final int column) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                column});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.isNullable(column);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#isReadOnly(int)
     */
    public final boolean isReadOnly(final int column) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                column});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.isReadOnly(column);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#isSearchable(int)
     */
    public final boolean isSearchable(final int column) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                column});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.isSearchable(column);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#isSigned(int)
     */
    public final boolean isSigned(final int column) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                column});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.isSigned(column);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.ResultSetMetaData#isWritable(int)
     */
    public final boolean isWritable(final int column) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                column});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.isWritable(column);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    //TODO Joudek Implement correctly

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }
}
