/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCDelegator;

/**
 * ConnectionListener wraps around a delegate Connection object
 * and passes all calls to the listener.
 */
public class ConnectionListener implements Connection, JDBCDelegator {

	/**
	 * Holds the delegate connection that calls are passed through to.
	 */
	private Connection delegateConn;

	/**
	 * Creates a new ConnectionListener that wraps around the specified
	 * delegate connection.
	 * 
	 * @param connection
	 *            the delegate connection
	 */
	ConnectionListener(final Connection connection) {
		this.delegateConn = connection;
	}

	/**
	 * Sets the delegate connection for the listener connection.
	 * 
	 * @param connection
	 *            the delegate connection
	 */
	public final void setDelegateConn(final Connection connection) {
		this.delegateConn = connection;
	}

	/**
	 * Gets the delegate connection for the listener connection.
	 * 
	 * @return the delegateConn
	 */
	public final Connection getDelegateConn() {
		return delegateConn;
	}

	/**
	 * Returns the delegate class name.
	 * 
	 * @return the delegate class name
	 */
	public final String getDelegateClassName() {
		return delegateConn.getClass().getName();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#clearWarnings()
	 */
	public final void clearWarnings() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		final Object result = null;
		try {
			delegateConn.clearWarnings();
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#close()
	 */
	public final void close() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		final Object result = null;
		try {
			if (delegateConn != null) {
				delegateConn.close();
			}
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#commit()
	 */
	public final void commit() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		final Object result = null;
		try {
			if (delegateConn != null) {
				delegateConn.commit();
			}
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#createStatement()
	 */
	public final Statement createStatement() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		StatementListener result = null; // NOPMD
		Statement delegateResult = null; // NOPMD
		try {
			if (delegateConn != null) {
				delegateResult = delegateConn.createStatement();
				result = new StatementListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#createStatement(int, int)
	 */
	public final Statement createStatement(final int resultSetType, final int rsConcurrency) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { resultSetType, rsConcurrency });
		StatementListener result = null; // NOPMD
		Statement delegateResult = null; // NOPMD
		try {
			if (delegateConn != null) {
				delegateResult = delegateConn.createStatement(resultSetType, rsConcurrency);
				result = new StatementListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#createStatement(int, int, int)
	 */
	public final Statement createStatement(final int resultSetType, final int rsConcurrency, final int rsHoldability) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { resultSetType, rsConcurrency, rsHoldability });
		StatementListener result = null; // NOPMD
		Statement delegateResult = null; // NOPMD
		try {
			if (delegateConn != null) {
				delegateResult = delegateConn.createStatement(resultSetType, rsConcurrency, rsHoldability);
				result = new StatementListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#getAutoCommit()
	 */
	public final boolean getAutoCommit() throws SQLException { // NOPMD
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		boolean result = false;
		try {
			if (delegateConn != null) {
				result = delegateConn.getAutoCommit();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#getCatalog()
	 */
	public final String getCatalog() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		String result = null;
		try {
			if (delegateConn != null) {
				result = delegateConn.getCatalog();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#getHoldability()
	 */
	public final int getHoldability() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		int result = 0;
		try {
			if (delegateConn != null) {
				result = delegateConn.getHoldability();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#getMetaData()
	 */
	public final DatabaseMetaData getMetaData() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		DatabaseMetaDataListener result = null;
		DatabaseMetaData delegateResult = null;
		try {
			if (delegateConn != null) {
				delegateResult = delegateConn.getMetaData();
				result = new DatabaseMetaDataListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#getTransactionIsolation()
	 */
	public final int getTransactionIsolation() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		int result = 0;
		try {
			if (delegateConn != null) {
				result = delegateConn.getTransactionIsolation();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#getTypeMap()
	 */
	public final Map<String, Class<?>> getTypeMap() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		Map<String, Class<?>> result = null;
		try {
			if (delegateConn != null) {
				result = delegateConn.getTypeMap();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#getWarnings()
	 */
	public final SQLWarning getWarnings() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		SQLWarning result = null;
		try {
			if (delegateConn != null) {
				result = delegateConn.getWarnings();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#isClosed()
	 */
	public final boolean isClosed() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		boolean result = false;
		try {
			if (delegateConn != null) {
				result = delegateConn.isClosed();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#isReadOnly()
	 */
	public final boolean isReadOnly() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		boolean result = false;
		try {
			if (delegateConn != null) {
				result = delegateConn.isReadOnly();
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#nativeSQL(java.lang.String)
	 */
	public final String nativeSQL(final String sql) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { sql });
		String result = null;
		try {
			if (delegateConn != null) {
				result = delegateConn.nativeSQL(sql);
			}
			JDBCCallListener.callEnd(begin, this, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#prepareCall(java.lang.String)
	 */
	public final CallableStatement prepareCall(final String sql) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { sql });
		CallableStatementListener result = null;
		CallableStatement delegateResult = null;
		try {
			if (delegateConn != null) {
				delegateResult = delegateConn.prepareCall(sql);
				result = new CallableStatementListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#prepareCall(java.lang.String, int, int)
	 */
	public final CallableStatement prepareCall(final String sql, final int rsType, final int rsConcurrency) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { sql, rsType, rsConcurrency });
		CallableStatementListener result = null;
		CallableStatement delegateResult = null;
		try {
			if (delegateConn != null) {
				delegateResult = delegateConn.prepareCall(sql, rsType, rsConcurrency);
				result = new CallableStatementListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#prepareCall(java.lang.String, int, int, int)
	 */
	public final CallableStatement prepareCall(final String sql, final int rsType, final int rsConcurrency, final int rsHoldability) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { sql, rsType, rsConcurrency, rsHoldability });
		CallableStatementListener result = null;
		CallableStatement delegateResult = null;
		try {
			if (delegateConn != null) {
				delegateResult = delegateConn.prepareCall(sql, rsType, rsConcurrency, rsHoldability);
				result = new CallableStatementListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#prepareStatement(java.lang.String)
	 */
	public final PreparedStatement prepareStatement(final String sql) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { sql });
		PreparedStatementListener result = null;
		PreparedStatement delegateResult = null;
		try {
			if (delegateConn != null) {
				delegateResult = delegateConn.prepareStatement(sql);
				result = new PreparedStatementListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#prepareStatement(java.lang.String, int)
	 */
	public final PreparedStatement prepareStatement(final String sql, final int autoGeneratedKeys) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { sql, autoGeneratedKeys });
		PreparedStatementListener result = null;
		PreparedStatement delegateResult = null;
		try {
			if (delegateConn != null) {
				delegateResult = delegateConn.prepareStatement(sql, autoGeneratedKeys);
				result = new PreparedStatementListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#prepareStatement(java.lang.String, int[])
	 */
	public final PreparedStatement prepareStatement(final String sql, final int[] columnIndexes) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { sql, columnIndexes });
		PreparedStatementListener result = null;
		PreparedStatement delegateResult = null;
		try {
			if (delegateConn != null) {
				delegateResult = delegateConn.prepareStatement(sql, columnIndexes);
				result = new PreparedStatementListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#prepareStatement(java.lang.String, java.lang.String[])
	 */
	public final PreparedStatement prepareStatement(final String sql, final String[] columnNames) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { sql, columnNames });
		PreparedStatementListener result = null;
		PreparedStatement delegateResult = null;
		try {
			if (delegateConn != null) {
				delegateResult = delegateConn.prepareStatement(sql, columnNames);
				result = new PreparedStatementListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#prepareStatement(java.lang.String, int, int)
	 */
	public final PreparedStatement prepareStatement(final String sql, final int rsType, final int rsConcurrency) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { sql, rsType, rsConcurrency });
		PreparedStatementListener result = null;
		PreparedStatement delegateResult = null;
		try {
			if (delegateConn != null) {
				delegateResult = delegateConn.prepareStatement(sql, rsType, rsConcurrency);
				result = new PreparedStatementListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#prepareStatement(java.lang.String, int, int, int)
	 */
	public final PreparedStatement prepareStatement(final String sql, final int rsType, final int rsConcurrency, final int rsHoldability) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { sql, rsType, rsConcurrency, rsHoldability });
		PreparedStatementListener result = null;
		PreparedStatement delegateResult = null;
		try {
			if (delegateConn != null) {
				delegateResult = delegateConn.prepareStatement(sql, rsType, rsConcurrency, rsHoldability);
				result = new PreparedStatementListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#releaseSavepoint(java.sql.Savepoint)
	 */
	public final void releaseSavepoint(final Savepoint savepoint) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { savepoint });
		final Object result = null;
		try {
			if (delegateConn != null) {
				delegateConn.releaseSavepoint(savepoint);
			}
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#rollback()
	 */
	public final void rollback() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		final Object result = null;
		try {
			if (delegateConn != null) {
				delegateConn.rollback();
			}
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#rollback(java.sql.Savepoint)
	 */
	public final void rollback(final Savepoint savepoint) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { savepoint });
		final Object result = null;
		try {
			if (delegateConn != null) {
				delegateConn.rollback(savepoint);
			}
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#setAutoCommit(boolean)
	 */
	public final void setAutoCommit(final boolean autoCommit) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { autoCommit });
		final Object result = null;
		try {
			if (delegateConn != null) {
				delegateConn.setAutoCommit(autoCommit);
			}
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#setCatalog(java.lang.String)
	 */
	public final void setCatalog(final String catalog) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { catalog });
		final Object result = null;
		try {
			if (delegateConn != null) {
				delegateConn.setCatalog(catalog);
			}
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#setHoldability(int)
	 */
	public final void setHoldability(final int holdability) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { holdability });
		final Object result = null;
		try {
			if (delegateConn != null) {
				delegateConn.setHoldability(holdability);
			}
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#setReadOnly(boolean)
	 */
	public final void setReadOnly(final boolean readOnly) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { readOnly });
		final Object result = null;
		try {
			if (delegateConn != null) {
				delegateConn.setReadOnly(readOnly);
			}
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#setSavepoint()
	 */
	public final Savepoint setSavepoint() throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] {});
		SavepointListener result = null;
		Savepoint delegateResult = null;
		try {
			if (delegateConn != null) {
				delegateResult = delegateConn.setSavepoint();
				result = new SavepointListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#setSavepoint(java.lang.String)
	 */
	public final Savepoint setSavepoint(final String name) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { name });
		SavepointListener result = null;
		Savepoint delegateResult = null;
		try {
			if (delegateConn != null) {
				delegateResult = delegateConn.setSavepoint(name);
				result = new SavepointListener(delegateResult);
			}
			JDBCCallListener.callEnd(begin, this, delegateResult, result);
			return result;
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#setTransactionIsolation(int)
	 */
	public final void setTransactionIsolation(final int level) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { level });
		final Object result = null;
		try {
			if (delegateConn != null) {
				delegateConn.setTransactionIsolation(level);
			}
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.sql.Connection#setTypeMap(java.util.Map)
	 */
	public final void setTypeMap(final Map<String, Class<?>> map) throws SQLException {
		final long begin = JDBCCallListener.callBegin(this, new Object[] { map });
		final Object result = null;
		try {
			if (delegateConn != null) {
				delegateConn.setTypeMap(map);
			}
			JDBCCallListener.callEnd(begin, this, result);
		} catch (SQLException e) {
			JDBCCallListener.callException(begin, this, e);
			throw e;
		}
	}

	// TODO Joudek Implement correctly

	@Override
	public Clob createClob() throws SQLException {
		if (delegateConn != null) {
			return delegateConn.createClob();
		}
		return null;
	}

	@Override
	public Blob createBlob() throws SQLException {
		if (delegateConn != null) {
			return delegateConn.createBlob();
		}
		return null;
	}

	@Override
	public NClob createNClob() throws SQLException {
		if (delegateConn != null) {
			return delegateConn.createNClob();
		}
		return null;
	}

	@Override
	public SQLXML createSQLXML() throws SQLException {
		if (delegateConn != null) {
			return delegateConn.createSQLXML();
		}
		return null;
	}

	@Override
	public boolean isValid(int timeout) throws SQLException {
        if (delegateConn != null) {
            return delegateConn.isValid(timeout);
        }
		return false;
	}

	@Override
	public void setClientInfo(String name, String value) throws SQLClientInfoException {
        if (delegateConn != null) {
            delegateConn.setClientInfo(name,value);
        }
	}

	@Override
	public void setClientInfo(Properties properties) throws SQLClientInfoException {
        if (delegateConn != null) {
            delegateConn.setClientInfo(properties);
        }
	}

	@Override
	public String getClientInfo(String name) throws SQLException {
        if (delegateConn != null) {
            return delegateConn.getClientInfo(name);
        }
		return null;
	}

	@Override
	public Properties getClientInfo() throws SQLException {
		if (delegateConn != null) {
			return delegateConn.getClientInfo();
		}
		return null;
	}

	@Override
	public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
        if (delegateConn != null) {
            return delegateConn.createArrayOf(typeName,elements);
        }
		return null;
	}

	@Override
	public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
        if (delegateConn != null) {
            return delegateConn.createStruct(typeName,attributes);
        }
		return null;
	}

	@Override
	public void setSchema(String schema) throws SQLException {
        if (delegateConn != null) {
            delegateConn.setSchema(schema);
        }
	}

	@Override
	public String getSchema() throws SQLException {
		if (delegateConn != null) {
			return delegateConn.getSchema();
		}
		return null;
	}

	@Override
	public void abort(Executor executor) throws SQLException {
        if (delegateConn != null) {
             delegateConn.abort(executor);
        }
	}

	@Override
	public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {

	}

	@Override
	public int getNetworkTimeout() throws SQLException {
		if (delegateConn != null) {
			return delegateConn.getNetworkTimeout();
		}
		return 0;
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
        if (delegateConn != null) {
            return delegateConn.unwrap(iface);
        }
		return null;
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
        if (delegateConn != null) {
            return delegateConn.isWrapperFor(iface);
        }
		return false;
	}
}
