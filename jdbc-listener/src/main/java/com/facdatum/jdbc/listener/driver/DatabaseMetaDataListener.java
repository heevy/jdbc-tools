//NOCS (for file length check)
/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver; //NOPMD

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.RowIdLifetime;
import java.sql.SQLException;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCDelegator;


/**
 * DatabaseMetaDataListener wraps around a delegate DatabaseMetaData object
 * and passes all calls to the listener.
 */
public class DatabaseMetaDataListener implements DatabaseMetaData, //NOPMD
        JDBCDelegator {

    /**
     * Holds the delegate database meta data that will be listened to.
     */
    private DatabaseMetaData delegateMetaData;

    /**
     * Creates a DatabaseMetaDataListener from the defined DatabaseMetaData.
     * @param metaData the delegate database meta data to set
     */
    DatabaseMetaDataListener(final DatabaseMetaData metaData) {
        this.delegateMetaData = metaData;
    }

    /**
     * Get the delegate database meta data.
     * @return the delegateDbMetaData
     */
    public final DatabaseMetaData getDelegateMetaData() {
        return this.delegateMetaData;
    }

    /**
     * Set the delegate meta data.
     * @param metaData the delegate database meta data to set
     */
    public final void setDelegateMetaData(
            final DatabaseMetaData metaData) {
        this.delegateMetaData = metaData;
    }

    /**
     * Returns the delegate class name.
     * @return the delegate class name
     */
    public final String getDelegateClassName() {
        return delegateMetaData.getClass().getName();
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#allProceduresAreCallable()
     */
    public final boolean allProceduresAreCallable() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.allProceduresAreCallable();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#allTablesAreSelectable()
     */
    public final boolean allTablesAreSelectable() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.allTablesAreSelectable();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#dataDefinitionCausesTransactionCommit()
     */
    public final boolean dataDefinitionCausesTransactionCommit()
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.
                        dataDefinitionCausesTransactionCommit();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#dataDefinitionIgnoredInTransactions()
     */
    public final boolean dataDefinitionIgnoredInTransactions()
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.dataDefinitionIgnoredInTransactions();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#deletesAreDetected(int)
     */
    public final boolean deletesAreDetected(final int type)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                type});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.deletesAreDetected(type);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#doesMaxRowSizeIncludeBlobs()
     */
    public final boolean doesMaxRowSizeIncludeBlobs() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.doesMaxRowSizeIncludeBlobs();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getAttributes(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    public final ResultSet getAttributes(final String catalog,
            final String schemaPattern, final String typeNamePattern,
            final String attNamePattern) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                catalog, schemaPattern, typeNamePattern, attNamePattern});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.getAttributes(
                        catalog, schemaPattern,
                        typeNamePattern, attNamePattern);
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getBestRowIdentifier(java.lang.String,
     *      java.lang.String, java.lang.String, int, boolean)
     */
    public final ResultSet getBestRowIdentifier(final String catalog,
            final String schema, final String table, final int scope,
            final boolean nullable) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                catalog, schema, table, scope, nullable});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.
                        getBestRowIdentifier(catalog, schema,
                                table, scope, nullable);
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getCatalogSeparator()
     */
    public final String getCatalogSeparator() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getCatalogSeparator();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getCatalogTerm()
     */
    public final String getCatalogTerm() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getCatalogTerm();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getCatalogs()
     */
    public final ResultSet getCatalogs() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.getCatalogs();
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getColumnPrivileges(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    public final ResultSet getColumnPrivileges(final String catalog,
            final String schema, final String table,
            final String columnNamePattern) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                catalog, schema, table, columnNamePattern});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.
                        getColumnPrivileges(catalog, schema, table,
                                columnNamePattern);
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getColumns(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    public final ResultSet getColumns(final String catalog,
            final String schemaPattern, final String tableNamePattern,
            final String columnNamePattern) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                catalog, schemaPattern, tableNamePattern, columnNamePattern});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.getColumns(
                        catalog, schemaPattern, tableNamePattern,
                        columnNamePattern);
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getConnection()
     */
    public final Connection getConnection() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        ConnectionListener result = null; //NOPMD
        Connection delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.getConnection();
                result = new ConnectionListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getCrossReference(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public final ResultSet getCrossReference(final String primaryCatalog,
            final String primarySchema, final String primaryTable,
            final String foreignCatalog,
            final String foreignSchema, final String foreignTable)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                primaryCatalog, primarySchema, primaryTable, foreignCatalog,
                foreignSchema, foreignTable});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.
                        getCrossReference(primaryCatalog, primarySchema,
                                primaryTable, foreignCatalog, foreignSchema,
                                foreignTable);
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getDatabaseMajorVersion()
     */
    public final int getDatabaseMajorVersion() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getDatabaseMajorVersion();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getDatabaseMinorVersion()
     */
    public final int getDatabaseMinorVersion() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getDatabaseMinorVersion();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getDatabaseProductName()
     */
    public final String getDatabaseProductName() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getDatabaseProductName();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getDatabaseProductVersion()
     */
    public final String getDatabaseProductVersion() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getDatabaseProductVersion();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getDefaultTransactionIsolation()
     */
    public final int getDefaultTransactionIsolation() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getDefaultTransactionIsolation();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getDriverMajorVersion()
     */
    public final int getDriverMajorVersion() {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        if (delegateMetaData != null) {
            result = delegateMetaData.getDriverMajorVersion();
        }
        JDBCCallListener.callEnd(begin, this, result);
        return result;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getDriverMinorVersion()
     */
    public final int getDriverMinorVersion() {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        if (delegateMetaData != null) {
            result = delegateMetaData.getDriverMinorVersion();
        }
        JDBCCallListener.callEnd(begin, this, result);
        return result;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getDriverName()
     */
    public final String getDriverName() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getDriverName();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getDriverVersion()
     */
    public final String getDriverVersion() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getDriverVersion();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getExportedKeys(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public final ResultSet getExportedKeys(final String catalog,
            final String schema, final String table) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                catalog, schema, table});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.getExportedKeys(
                        catalog, schema, table);
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getExtraNameCharacters()
     */
    public final String getExtraNameCharacters() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getExtraNameCharacters();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getIdentifierQuoteString()
     */
    public final String getIdentifierQuoteString() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getIdentifierQuoteString();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getImportedKeys(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public final ResultSet getImportedKeys(final String catalog,
            final String schema, final String table) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                catalog, schema, table});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.getImportedKeys(
                        catalog, schema, table);
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getIndexInfo(java.lang.String,
     *      java.lang.String, java.lang.String, boolean, boolean)
     */
    public final ResultSet getIndexInfo(final String catalog,
            final String schema, final String table, final boolean unique,
            final boolean approximate) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                catalog, schema, table, unique, approximate});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.getIndexInfo(
                        catalog, schema, table, unique, approximate);
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getJDBCMajorVersion()
     */
    public final int getJDBCMajorVersion() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getJDBCMajorVersion();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getJDBCMinorVersion()
     */
    public final int getJDBCMinorVersion() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getJDBCMinorVersion();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxBinaryLiteralLength()
     */
    public final int getMaxBinaryLiteralLength() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getMaxBinaryLiteralLength();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxCatalogNameLength()
     */
    public final int getMaxCatalogNameLength() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getMaxCatalogNameLength();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxCharLiteralLength()
     */
    public final int getMaxCharLiteralLength() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getMaxCharLiteralLength();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxColumnNameLength()
     */
    public final int getMaxColumnNameLength() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getMaxColumnNameLength();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxColumnsInGroupBy()
     */
    public final int getMaxColumnsInGroupBy() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getMaxColumnsInGroupBy();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxColumnsInIndex()
     */
    public final int getMaxColumnsInIndex() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getMaxColumnsInIndex();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxColumnsInOrderBy()
     */
    public final int getMaxColumnsInOrderBy() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getMaxColumnsInOrderBy();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxColumnsInSelect()
     */
    public final int getMaxColumnsInSelect() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getMaxColumnsInSelect();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxColumnsInTable()
     */
    public final int getMaxColumnsInTable() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getMaxColumnsInTable();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxConnections()
     */
    public final int getMaxConnections() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getMaxConnections();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxCursorNameLength()
     */
    public final int getMaxCursorNameLength() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getMaxCursorNameLength();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxIndexLength()
     */
    public final int getMaxIndexLength() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getMaxIndexLength();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxProcedureNameLength()
     */
    public final int getMaxProcedureNameLength() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getMaxProcedureNameLength();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxRowSize()
     */
    public final int getMaxRowSize() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getMaxRowSize();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxSchemaNameLength()
     */
    public final int getMaxSchemaNameLength() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getMaxSchemaNameLength();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxStatementLength()
     */
    public final int getMaxStatementLength() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getMaxStatementLength();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxStatements()
     */
    public final int getMaxStatements() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getMaxStatements();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxTableNameLength()
     */
    public final int getMaxTableNameLength() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getMaxTableNameLength();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxTablesInSelect()
     */
    public final int getMaxTablesInSelect() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getMaxTablesInSelect();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getMaxUserNameLength()
     */
    public final int getMaxUserNameLength() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getMaxUserNameLength();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getNumericFunctions()
     */
    public final String getNumericFunctions() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getNumericFunctions();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getPrimaryKeys(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public final ResultSet getPrimaryKeys(final String catalog,
            final String schema, final String table) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                catalog, schema, table});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.getPrimaryKeys(
                        catalog, schema, table);
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getProcedureColumns(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    public final ResultSet getProcedureColumns(final String catalog,
            final String schemaPattern, final String procNamePattern,
            final String columnNamePattern) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                catalog, schemaPattern, procNamePattern, columnNamePattern});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.
                        getProcedureColumns(catalog, schemaPattern,
                                procNamePattern, columnNamePattern);
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getProcedureTerm()
     */
    public final String getProcedureTerm() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getProcedureTerm();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getProcedures(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public final ResultSet getProcedures(final String catalog,
            final String schemaPattern, final String procNamePattern)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                catalog, schemaPattern, procNamePattern});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.getProcedures(
                        catalog, schemaPattern, procNamePattern);
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getResultSetHoldability()
     */
    public final int getResultSetHoldability() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getResultSetHoldability();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getSQLKeywords()
     */
    public final String getSQLKeywords() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getSQLKeywords();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getSQLStateType()
     */
    public final int getSQLStateType() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getSQLStateType();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getSchemaTerm()
     */
    public final String getSchemaTerm() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getSchemaTerm();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getSchemas()
     */
    public final ResultSet getSchemas() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.getSchemas();
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getSearchStringEscape()
     */
    public final String getSearchStringEscape() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getSearchStringEscape();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getStringFunctions()
     */
    public final String getStringFunctions() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getStringFunctions();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getSuperTables(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public final ResultSet getSuperTables(final String catalog,
            final String schemaPattern, final String tableNamePattern)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                catalog, schemaPattern, tableNamePattern});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.getSuperTables(
                        catalog, schemaPattern, tableNamePattern);
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getSuperTypes(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public final ResultSet getSuperTypes(final String catalog,
            final String schemaPattern, final String typeNamePattern)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                catalog, schemaPattern, typeNamePattern});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.getSuperTypes(
                        catalog, schemaPattern, typeNamePattern);
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getSystemFunctions()
     */
    public final String getSystemFunctions() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getSystemFunctions();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getTablePrivileges(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public final ResultSet getTablePrivileges(final String catalog,
            final String schemaPattern, final String tableNamePattern)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                catalog, schemaPattern, tableNamePattern});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.
                        getTablePrivileges(catalog, schemaPattern,
                                tableNamePattern);
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getTableTypes()
     */
    public final ResultSet getTableTypes() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.getTableTypes();
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getTables(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String[])
     */
    public final ResultSet getTables(final String catalog,
            final String schemaPattern, final String tableNamePattern,
            final String[] types) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                catalog, schemaPattern, tableNamePattern, types});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.getTables(
                        catalog, schemaPattern, tableNamePattern, types);
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getTimeDateFunctions()
     */
    public final String getTimeDateFunctions() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getTimeDateFunctions();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getTypeInfo()
     */
    public final ResultSet getTypeInfo() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.getTypeInfo();
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getUDTs(java.lang.String,
     *      java.lang.String, java.lang.String, int[])
     */
    public final ResultSet getUDTs(final String catalog,
            final String schemaPattern, final String typeNamePattern,
            final int[] types) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                catalog, schemaPattern, typeNamePattern, types});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.getUDTs(
                        catalog, schemaPattern, typeNamePattern, types);
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getURL()
     */
    public final String getURL() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getURL();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getUserName()
     */
    public final String getUserName() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        String result = null;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.getUserName();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#getVersionColumns(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public final ResultSet getVersionColumns(final String catalog,
            final String schema, final String table)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                catalog, schema, table});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateMetaData != null) {
                delegateResult = delegateMetaData.
                        getVersionColumns(catalog, schema, table);
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#insertsAreDetected(int)
     */
    public final boolean insertsAreDetected(final int type)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                type});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.insertsAreDetected(type);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#isCatalogAtStart()
     */
    public final boolean isCatalogAtStart() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.isCatalogAtStart();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#isReadOnly()
     */
    public final boolean isReadOnly() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.isReadOnly();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#locatorsUpdateCopy()
     */
    public final boolean locatorsUpdateCopy() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.locatorsUpdateCopy();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#nullPlusNonNullIsNull()
     */
    public final boolean nullPlusNonNullIsNull() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.nullPlusNonNullIsNull();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#nullsAreSortedAtEnd()
     */
    public final boolean nullsAreSortedAtEnd() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.nullsAreSortedAtEnd();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#nullsAreSortedAtStart()
     */
    public final boolean nullsAreSortedAtStart() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.nullsAreSortedAtStart();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#nullsAreSortedHigh()
     */
    public final boolean nullsAreSortedHigh() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.nullsAreSortedHigh();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#nullsAreSortedLow()
     */
    public final boolean nullsAreSortedLow() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.nullsAreSortedLow();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#othersDeletesAreVisible(int)
     */
    public final boolean othersDeletesAreVisible(final int type)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                type});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.othersDeletesAreVisible(type);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#othersInsertsAreVisible(int)
     */
    public final boolean othersInsertsAreVisible(final int type)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                type});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.othersInsertsAreVisible(type);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#othersUpdatesAreVisible(int)
     */
    public final boolean othersUpdatesAreVisible(final int type)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                type});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.othersUpdatesAreVisible(type);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#ownDeletesAreVisible(int)
     */
    public final boolean ownDeletesAreVisible(final int type)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                type});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.ownDeletesAreVisible(type);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#ownInsertsAreVisible(int)
     */
    public final boolean ownInsertsAreVisible(final int type)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                type});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.ownInsertsAreVisible(type);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#ownUpdatesAreVisible(int)
     */
    public final boolean ownUpdatesAreVisible(final int type)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                type});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.ownUpdatesAreVisible(type);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#storesLowerCaseIdentifiers()
     */
    public final boolean storesLowerCaseIdentifiers() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.storesLowerCaseIdentifiers();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#storesLowerCaseQuotedIdentifiers()
     */
    public final boolean storesLowerCaseQuotedIdentifiers()
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.storesLowerCaseQuotedIdentifiers();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#storesMixedCaseIdentifiers()
     */
    public final boolean storesMixedCaseIdentifiers() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.storesMixedCaseIdentifiers();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#storesMixedCaseQuotedIdentifiers()
     */
    public final boolean storesMixedCaseQuotedIdentifiers()
        throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.storesMixedCaseQuotedIdentifiers();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#storesUpperCaseIdentifiers()
     */
    public final boolean storesUpperCaseIdentifiers() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.storesUpperCaseIdentifiers();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#storesUpperCaseQuotedIdentifiers()
     */
    public final boolean storesUpperCaseQuotedIdentifiers()
        throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.storesUpperCaseQuotedIdentifiers();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsANSI92EntryLevelSQL()
     */
    public final boolean supportsANSI92EntryLevelSQL() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsANSI92EntryLevelSQL();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsANSI92FullSQL()
     */
    public final boolean supportsANSI92FullSQL() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsANSI92FullSQL();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsANSI92IntermediateSQL()
     */
    public final boolean supportsANSI92IntermediateSQL() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsANSI92IntermediateSQL();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsAlterTableWithAddColumn()
     */
    public final boolean supportsAlterTableWithAddColumn() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsAlterTableWithAddColumn();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsAlterTableWithDropColumn()
     */
    public final boolean supportsAlterTableWithDropColumn()
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsAlterTableWithDropColumn();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsBatchUpdates()
     */
    public final boolean supportsBatchUpdates() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsBatchUpdates();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsCatalogsInDataManipulation()
     */
    public final boolean supportsCatalogsInDataManipulation()
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsCatalogsInDataManipulation();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsCatalogsInIndexDefinitions()
     */
    public final boolean supportsCatalogsInIndexDefinitions()
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsCatalogsInIndexDefinitions();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsCatalogsInPrivilegeDefinitions()
     */
    public final boolean supportsCatalogsInPrivilegeDefinitions()
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.
                        supportsCatalogsInPrivilegeDefinitions();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsCatalogsInProcedureCalls()
     */
    public final boolean supportsCatalogsInProcedureCalls()
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsCatalogsInProcedureCalls();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsCatalogsInTableDefinitions()
     */
    public final boolean supportsCatalogsInTableDefinitions()
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsCatalogsInTableDefinitions();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsColumnAliasing()
     */
    public final boolean supportsColumnAliasing() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsColumnAliasing();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsConvert()
     */
    public final boolean supportsConvert() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsConvert();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsConvert(int, int)
     */
    public final boolean supportsConvert(final int fromType,
            final int toType) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                fromType, toType});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsConvert(fromType, toType);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsCoreSQLGrammar()
     */
    public final boolean supportsCoreSQLGrammar() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsCoreSQLGrammar();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsCorrelatedSubqueries()
     */
    public final boolean supportsCorrelatedSubqueries() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsCorrelatedSubqueries();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData
     *      #supportsDataDefinitionAndDataManipulationTransactions()
     */
    public final boolean supportsDataDefinitionAndDataManipulationTransactions()
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.
                        supportsDataDefinitionAndDataManipulationTransactions();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsDataManipulationTransactionsOnly()
     */
    public final boolean supportsDataManipulationTransactionsOnly()
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.
                        supportsDataManipulationTransactionsOnly();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsDifferentTableCorrelationNames()
     */
    public final boolean supportsDifferentTableCorrelationNames()
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.
                        supportsDifferentTableCorrelationNames();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsExpressionsInOrderBy()
     */
    public final boolean supportsExpressionsInOrderBy() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsExpressionsInOrderBy();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsExtendedSQLGrammar()
     */
    public final boolean supportsExtendedSQLGrammar() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsExtendedSQLGrammar();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsFullOuterJoins()
     */
    public final boolean supportsFullOuterJoins() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsFullOuterJoins();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsGetGeneratedKeys()
     */
    public final boolean supportsGetGeneratedKeys() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsGetGeneratedKeys();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsGroupBy()
     */
    public final boolean supportsGroupBy() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsGroupBy();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsGroupByBeyondSelect()
     */
    public final boolean supportsGroupByBeyondSelect() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsGroupByBeyondSelect();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsGroupByUnrelated()
     */
    public final boolean supportsGroupByUnrelated() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsGroupByUnrelated();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsIntegrityEnhancementFacility()
     */
    public final boolean supportsIntegrityEnhancementFacility()
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.
                        supportsIntegrityEnhancementFacility();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsLikeEscapeClause()
     */
    public final boolean supportsLikeEscapeClause() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsLikeEscapeClause();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsLimitedOuterJoins()
     */
    public final boolean supportsLimitedOuterJoins() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsLimitedOuterJoins();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsMinimumSQLGrammar()
     */
    public final boolean supportsMinimumSQLGrammar() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsMinimumSQLGrammar();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsMixedCaseIdentifiers()
     */
    public final boolean supportsMixedCaseIdentifiers() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsMixedCaseIdentifiers();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsMixedCaseQuotedIdentifiers()
     */
    public final boolean supportsMixedCaseQuotedIdentifiers()
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsMixedCaseQuotedIdentifiers();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsMultipleOpenResults()
     */
    public final boolean supportsMultipleOpenResults() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsMultipleOpenResults();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsMultipleResultSets()
     */
    public final boolean supportsMultipleResultSets() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsMultipleResultSets();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsMultipleTransactions()
     */
    public final boolean supportsMultipleTransactions() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsMultipleTransactions();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsNamedParameters()
     */
    public final boolean supportsNamedParameters() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsNamedParameters();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsNonNullableColumns()
     */
    public final boolean supportsNonNullableColumns() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsNonNullableColumns();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsOpenCursorsAcrossCommit()
     */
    public final boolean supportsOpenCursorsAcrossCommit() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsOpenCursorsAcrossCommit();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsOpenCursorsAcrossRollback()
     */
    public final boolean supportsOpenCursorsAcrossRollback()
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsOpenCursorsAcrossRollback();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsOpenStatementsAcrossCommit()
     */
    public final boolean supportsOpenStatementsAcrossCommit()
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsOpenStatementsAcrossCommit();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsOpenStatementsAcrossRollback()
     */
    public final boolean supportsOpenStatementsAcrossRollback()
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.
                        supportsOpenStatementsAcrossRollback();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsOrderByUnrelated()
     */
    public final boolean supportsOrderByUnrelated() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsOrderByUnrelated();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsOuterJoins()
     */
    public final boolean supportsOuterJoins() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsOuterJoins();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsPositionedDelete()
     */
    public final boolean supportsPositionedDelete() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsPositionedDelete();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsPositionedUpdate()
     */
    public final boolean supportsPositionedUpdate() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsPositionedUpdate();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsResultSetConcurrency(int, int)
     */
    public final boolean supportsResultSetConcurrency(final int type,
            final int concurrency) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {type,
                concurrency});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsResultSetConcurrency(type,
                        concurrency);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsResultSetHoldability(int)
     */
    public final boolean supportsResultSetHoldability(final int holdability)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                holdability});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.
                        supportsResultSetHoldability(holdability);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsResultSetType(int)
     */
    public final boolean supportsResultSetType(final int type)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                type});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsResultSetType(type);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSavepoints()
     */
    public final boolean supportsSavepoints() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsSavepoints();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSchemasInDataManipulation()
     */
    public final boolean supportsSchemasInDataManipulation()
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsSchemasInDataManipulation();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSchemasInIndexDefinitions()
     */
    public final boolean supportsSchemasInIndexDefinitions()
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsSchemasInIndexDefinitions();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSchemasInPrivilegeDefinitions()
     */
    public final boolean supportsSchemasInPrivilegeDefinitions()
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.
                        supportsSchemasInPrivilegeDefinitions();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSchemasInProcedureCalls()
     */
    public final boolean supportsSchemasInProcedureCalls() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsSchemasInProcedureCalls();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSchemasInTableDefinitions()
     */
    public final boolean supportsSchemasInTableDefinitions()
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsSchemasInTableDefinitions();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSelectForUpdate()
     */
    public final boolean supportsSelectForUpdate() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsSelectForUpdate();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsStatementPooling()
     */
    public final boolean supportsStatementPooling() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsStatementPooling();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }



    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsStoredProcedures()
     */
    public final boolean supportsStoredProcedures() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsStoredProcedures();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSubqueriesInComparisons()
     */
    public final boolean supportsSubqueriesInComparisons() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsSubqueriesInComparisons();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSubqueriesInExists()
     */
    public final boolean supportsSubqueriesInExists() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsSubqueriesInExists();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSubqueriesInIns()
     */
    public final boolean supportsSubqueriesInIns() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsSubqueriesInIns();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsSubqueriesInQuantifieds()
     */
    public final boolean supportsSubqueriesInQuantifieds() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsSubqueriesInQuantifieds();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsTableCorrelationNames()
     */
    public final boolean supportsTableCorrelationNames() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsTableCorrelationNames();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsTransactionIsolationLevel(int)
     */
    public final boolean supportsTransactionIsolationLevel(final int level)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                level});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.
                        supportsTransactionIsolationLevel(level);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsTransactions()
     */
    public final boolean supportsTransactions() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsTransactions();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsUnion()
     */
    public final boolean supportsUnion() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsUnion();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#supportsUnionAll()
     */
    public final boolean supportsUnionAll() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.supportsUnionAll();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#updatesAreDetected(int)
     */
    public final boolean updatesAreDetected(final int type)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                type});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.updatesAreDetected(type);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#usesLocalFilePerTable()
     */
    public final boolean usesLocalFilePerTable() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.usesLocalFilePerTable();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.DatabaseMetaData#usesLocalFiles()
     */
    public final boolean usesLocalFiles() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        boolean result = false;
        try {
            if (delegateMetaData != null) {
                result = delegateMetaData.usesLocalFiles();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    //TODO Joudek Implement Correctly

    @Override
    public RowIdLifetime getRowIdLifetime() throws SQLException {
        if (delegateMetaData != null) {
            return delegateMetaData.getRowIdLifetime();
        }
        return null;
    }

    @Override
    public ResultSet getSchemas(String catalog, String schemaPattern) throws SQLException {
        if (delegateMetaData != null) {
            return delegateMetaData.getSchemas(catalog,schemaPattern);
        }
        return null;
    }

    @Override
    public boolean supportsStoredFunctionsUsingCallSyntax() throws SQLException {
        if (delegateMetaData != null) {
            return delegateMetaData.supportsStoredFunctionsUsingCallSyntax();
        }
        return false;
    }

    @Override
    public boolean autoCommitFailureClosesAllResultSets() throws SQLException {
        if (delegateMetaData != null) {
            return delegateMetaData.autoCommitFailureClosesAllResultSets();
        }
        return false;
    }

    @Override
    public ResultSet getClientInfoProperties() throws SQLException {
        if (delegateMetaData != null) {
            return delegateMetaData.getClientInfoProperties();
        }
        return null;
    }

    @Override
    public ResultSet getFunctions(String catalog, String schemaPattern, String functionNamePattern) throws SQLException {
        if (delegateMetaData != null) {
            return delegateMetaData.getFunctions(catalog,schemaPattern,functionNamePattern);
        }
        return null;
    }

    @Override
    public ResultSet getFunctionColumns(String catalog, String schemaPattern, String functionNamePattern, String columnNamePattern) throws SQLException {
        if (delegateMetaData != null) {
            return delegateMetaData.getFunctionColumns(catalog,schemaPattern,functionNamePattern,columnNamePattern);
        }
        return null;
    }

    @Override
    public ResultSet getPseudoColumns(String catalog, String schemaPattern, String tableNamePattern, String columnNamePattern) throws SQLException {
        if (delegateMetaData != null) {
            return delegateMetaData.getPseudoColumns(catalog,schemaPattern,tableNamePattern,columnNamePattern);
        }
        return null;
    }

    @Override
    public boolean generatedKeyAlwaysReturned() throws SQLException {
        if (delegateMetaData != null) {
            return delegateMetaData.generatedKeyAlwaysReturned();
        }
        return false;
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        if (delegateMetaData != null) {
            return delegateMetaData.unwrap(iface);
        }
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        if (delegateMetaData != null) {
            return delegateMetaData.isWrapperFor(iface);
        }
        return false;
    }
}
