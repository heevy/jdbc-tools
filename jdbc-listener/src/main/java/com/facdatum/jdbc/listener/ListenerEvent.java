/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener;

/**
 * Interface describing the ListenerEvent that is passed to a JDBCListener
 * acceptXXXX method.
 */
public interface ListenerEvent {

    /**
     * Returns the time of the JDBC event.
     * @return the event time
     */
    long getTime();

    /**
     * Returns the object called on the JDBC Layer.
     * @return the call object
     */
    Object getObject();

    /**
     * Returns the stack trace of calls into the JDBC layer.
     * @return the stack trace
     */
    StackTraceElement[] getStackTrace();

    /**
     * Helper method to return JDBC Method call from the stack trace.
     * @return the JDBC Method call
     */
    String getJDBCMethod();

    /**
     * Helper method to return the JDBC Method call from the JDBC interface
     * that has been called in the stack trace.
     * @return the method call on the JDBC Interface
     */
    @SuppressWarnings("unchecked")
    String getJDBCInterfaceMethod();

    /**
     * Helper method to return the method that called into the JDBC Layer
     * from the stack trace.
     * @return the calling method
     */
    String getCallingMethod();

    /**
     * Sets whether subsequent calls to listeners should be filtered or not.
     * @param filter set to true to filter all calls to subsequent listeners
     */
    void setFilterListeners(boolean filter);

    /**
     * Returns whether subsequent calls to listeners for this call should
     * be filtered or not.
     * @return true if listeners should be filtered; false otherwise
     */
    boolean getFilterListeners(); //NOPMD
}
