/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCDelegator;


/**
 * ArrayListener wraps around a delegate Array object
 * and passes all calls to the listener.
 */
public class ArrayListener implements Array, JDBCDelegator {
    /**
     * Holds the delegate array that calls are passed through to.
     */
    private Array delegateArray;

    /**
     * Creates a new ArrayListener that wraps around the specified
     * delegate array.
     * @param array the delegate array
     */
    ArrayListener(final Array array) {
        this.delegateArray = array;
    }

    /**
     * Sets the delegate array for the listener array.
     * @param array the delegate array
     */
    public final void setDelegateArray(final Array array) {
        this.delegateArray = array;
    }

    /**
     * Gets the delegate array for the listener array.
     * @return the delegateArray
     */
    public final Array getDelegateArray() {
        return delegateArray;
    }

    /**
     * Returns the delegate class name.
     * @return the delegate class name
     */
    public final String getDelegateClassName() {
        return delegateArray.getClass().getName();
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Array#getArray()
     */
    public final Object getArray() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        Object result = null;
        try {
            if (delegateArray != null) {
                result = delegateArray.getArray();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Array#getArray(java.util.Map)
     */
    public final Object getArray(final Map < String, Class < ? > > map)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {map});
        Object result = null;
        try {
            if (delegateArray != null) {
                result = delegateArray.getArray(map);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Array#getArray(long, int)
     */
    public final Object getArray(final long index, final int count)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this,
                new Object[] {index, count});
        Object result = null;
        try {
            if (delegateArray != null) {
                result = delegateArray.getArray(index, count);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Array#getArray(long, int, java.util.Map)
     */
    public final Object getArray(final long index, final int count,
            final Map < String, Class < ? > > map) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this,
                new Object[] {index, count, map});
        Object result = null;
        try {
            if (delegateArray != null) {
                result = delegateArray.getArray(index, count, map);
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Array#getBaseType()
     */
    public final int getBaseType() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateArray != null) {
                result = delegateArray.getBaseType();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Array#getBaseTypeName()
     */
    public final String getBaseTypeName() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        String result = null;
        try {
            if (delegateArray != null) {
                result = delegateArray.getBaseTypeName();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Array#getResultSet()
     */
    public final ResultSet getResultSet() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateArray != null) {
                delegateResult = delegateArray.getResultSet();
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Array#getResultSet(java.util.Map)
     */
    public final ResultSet getResultSet(final Map < String, Class < ? > > map)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {map});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateArray != null) {
                delegateResult = delegateArray.getResultSet(map);
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Array#getResultSet(long, int)
     */
    public final ResultSet getResultSet(final long index, final int count)
            throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                index, count});
        ResultSetListener result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateArray != null) {
                delegateResult = delegateArray.getResultSet(index, count);
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Array#getResultSet(long, int, java.util.Map)
     */
    public final ResultSet getResultSet(final long index, final int count,
            final Map < String, Class < ? > > map) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this,
                new Object[] {index, count, map});
        ResultSet result = null; //NOPMD
        ResultSet delegateResult = null; //NOPMD
        try {
            if (delegateArray != null) {
                delegateResult = delegateArray.getResultSet(index, count, map);
                result = new ResultSetListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    //TODO Joudek Implement correctly

    @Override
    public void free() throws SQLException {
        delegateArray.free();
    }
}
