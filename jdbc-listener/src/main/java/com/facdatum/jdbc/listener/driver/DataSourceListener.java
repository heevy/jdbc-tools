/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.io.PrintWriter;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.Reference;
import javax.naming.Referenceable;
import javax.naming.StringRefAddr;
import javax.sql.DataSource;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCDelegator;
import com.facdatum.jdbc.listener.JDBCListenerProperties;

/**
 * DataSourceListener wraps around a delegate DataSource object
 * and passes all calls to the listener.
 */
public class DataSourceListener implements DataSource, JDBCDelegator,
        Referenceable, Serializable {

    /**
     * Compiler generated serialVersionUID for serialization. Update if
     * structural revisions.
     */
    private static final long serialVersionUID = 2785661508581518972L;

    /**
     * Records whether the driver has been registered already.
     */
    private static boolean registered = false;

    /**
     * If there is an error registering driver then this variable is set to
     * throw when creating driver.
     */
    private static SQLException registerError = null;

    /**
     * Holds the reference to the datasource that all calls are delegated to.
     */
    protected DataSource delegateDataSource = null; //NOPMD NOCS

    /**
     * Holds the delegate data source class name.
     */
    protected String delegateClassName; //NOCS

    /*
     * static clause that is called when forName is executed on this Driver
     * class as part of the JDBC registration process.
     */
    static {
        try {
            if (!registered) {
                DriverListener.registerDriver();
                registerError = null;
                registered = true;
            }
        } catch (SQLException e) {
            registerError = e;
        }
    }

    /**
     * No arg constructor for Serialization/Referenceable.
     * @throws SQLException if there was a previous problem registering driver
     */
    public DataSourceListener() throws SQLException {
        super();
        if (registerError != null) {
            //DriverListener.registerDriver();
            throw new SQLException(registerError.getMessage());
        }
    }

    /**
     * Creates a listener data source with given delegate data source.
     * @param datasource delegate data source
     */
    public DataSourceListener(final DataSource datasource) {
        delegateDataSource = datasource;
    }

    /**
     * Used in testing to clear any previous failures to register driver.
     */
    protected static final void clearDataSource() {
        registered = false;
        registerError = null;
    }

    /**
     * Setter for the delegate datasource.
     * @param datasource Delegate datasource
     */
    public final void setDelegateDataSource(final DataSource datasource) {
        this.delegateDataSource = datasource;
    }

    /**
     * Getter for the delegate datasource.
     * @return delegateDatasource
     */
    public final DataSource getDelegateDataSource() {
        return this.delegateDataSource;
    }

    /**
     * Sets the delegate class name.
     * @param className the delegate data source class name
     * @see com.facdatum.jdbc.listener.driver.JDBCDelegator
     * #getDelegateClassName()
     */
    public void setDelegateClassName(final String className) { //NOCS
        delegateClassName = className;
    }

    /**
     * Returns the delegate data source class name.
     * @return the delegate data source class name
     * @see com.facdatum.jdbc.listener.driver.JDBCDelegator
     * #getDelegateClassName()
     */
    public String getDelegateClassName() { //NOCS
        if (delegateDataSource != null) {
            return delegateDataSource.getClass().getName();
        }
        return null;
    }

    /**
     * Binds the datasource through JNDI setting any additionally specified
     * properties. Uses a custom context factory if configured.
     * @throws SQLException if problem creating datasource.
     */
    protected void bindDataSource() throws SQLException { //NOCS
        if (delegateClassName == null) {
            delegateClassName = JDBCListenerProperties.
                    getDataSourceName();
        }
        if (delegateClassName == null) {
            throw new SQLException("DataSourceListener: no value for delegate"
                    + " data source name, cannot perform jndi lookup");
        }
        try {
            Properties env = null;
            final String factory = JDBCListenerProperties.getContextFactory();
            if (factory  != null) {
                env = JDBCListenerProperties.getContextProviderProperties();
                env.put(Context.INITIAL_CONTEXT_FACTORY, factory);
                final String url = JDBCListenerProperties.
                        getContextProviderURL();
                if (url != null) {
                    env.put(Context.PROVIDER_URL, url);
                }
            }
            InitialContext ctx;
            if (env != null) { //NOPMD
                ctx = new InitialContext(env);
            } else {
                ctx = new InitialContext();
            }
            delegateDataSource = (DataSource) ctx.lookup(delegateClassName);

            setDataSourceProperties(JDBCListenerProperties.
                    getDataSourceProperties());
        } catch (NamingException e) {
            throw new SQLException("DataSourceListener: naming" //NOPMD
                    + " exception during jndi lookup of delegate data source"
                    + "Name of '" + delegateClassName + "'. " + e.getMessage());
        }
    }

    /**
     * This method sets the properties that have been provided on the delegate
     * data source.
     * @param dataSourceProps the datasource properties to set
     * @throws SQLException if there is a problem setting the properties
     */
    @SuppressWarnings("unchecked")
    private void setDataSourceProperties(final Properties
            dataSourceProps) throws SQLException {
        if (!dataSourceProps.isEmpty()) {
            final Class klass = delegateDataSource.getClass();

            // find the setter methods in the class and set from properties
            final Method[] methods = klass.getMethods();
            for (int i = 0; methods != null && i < methods.length; i++) {
                final Method method = methods[i];
                final String methodName = method.getName();
                // see if the method is a setXXX
                if (methodName.startsWith("set")) {
                    final String propertyname = methodName.substring(
                            "set".length()).toLowerCase();
                    final String value = (String) dataSourceProps.getProperty(
                            propertyname);
                    if (value != null) {
                        try {
                            executeMethod(method, value);
                        } catch (IllegalAccessException e) {
                            throw (new SQLException(//NOPMD
                                    "Access denied to method "
                                    + this.getDelegateClassName() + "."
                                    + methodName));
                        } catch (InvocationTargetException e2) {
                            throw (new SQLException(//NOPMD
                                    "Call to method "
                                    + this.getDelegateClassName() + "."
                                    + methodName + " failed."));
                        }
                    }
                }
            }
        }
    }

    /**
     * This method executes the provided method with the supplied arguments.
     * @param method method to execute
     * @param argValue argument value
     * @throws IllegalAccessException if error accessing method
     * @throws InvocationTargetException if error executing method
     * @throws SQLException if unsupported argument type
     */
    @SuppressWarnings("unchecked")
    private void executeMethod(final Method method, final String argValue)
            throws IllegalAccessException, InvocationTargetException,
            SQLException {
        final Class[] types = method.getParameterTypes();
        if (types[0].getName().equals(argValue.getClass().getName())) {
            final String[] args = new String[1]; // NOPMD
            args[0] = argValue;
            method.invoke(delegateDataSource, (Object[]) args);
        } else if (types[0].isPrimitive() && types[0].getName().equals("int")) {
            final Integer[] args = new Integer[1]; // NOPMD
            args[0] = Integer.valueOf(argValue);
            method.invoke(delegateDataSource, (Object[]) args);
        }
    }

    /**
     * {@inheritDoc}
     * @see javax.sql.DataSource#getConnection()
     */
    public Connection getConnection() throws SQLException { // NOCS
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        ConnectionListener result = null; //NOPMD
        Connection delegateResult = null; //NOPMD
        try {
            if (delegateDataSource == null) {
                bindDataSource();
            }
            if (delegateDataSource != null) {
                delegateResult = delegateDataSource.getConnection();
                result =
                    new ConnectionListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see javax.sql.DataSource#getConnection(java.lang.String,
     *      java.lang.String)
     */
    public Connection getConnection(final String username, //NOCS
            final String password) throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                username, password});
        ConnectionListener result = null; //NOPMD
        Connection delegateResult = null; //NOPMD
        try {
            if (delegateDataSource == null) {
                bindDataSource();
            }
            if (delegateDataSource != null) {
                delegateResult = delegateDataSource.getConnection(
                            username, password);
                result =
                    new ConnectionListener(delegateResult);
            }
            JDBCCallListener.callEnd(begin, this, delegateResult, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see javax.sql.DataSource#getLogWriter()
     */
    public PrintWriter getLogWriter() throws SQLException { //NOCS
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        PrintWriter result = null;
        try {
            if (delegateDataSource == null) {
                bindDataSource();
            }
            if (delegateDataSource != null) {
                result = delegateDataSource.getLogWriter();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see javax.sql.DataSource#getLoginTimeout()
     */
    public int getLoginTimeout() throws SQLException { //NOCS
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateDataSource == null) {
                bindDataSource();
            }
            if (delegateDataSource != null) {
                result = delegateDataSource.getLoginTimeout();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see javax.sql.DataSource#setLogWriter(java.io.PrintWriter)
     */
    public void setLogWriter(final PrintWriter out) throws SQLException { //NOCS
        final long begin = JDBCCallListener.callBegin(this, new Object[] {out});
        final Object result = null;
        try {
            if (delegateDataSource == null) {
                bindDataSource();
            }
            if (delegateDataSource != null) {
                delegateDataSource.setLogWriter(out);
            }
            JDBCCallListener.callEnd(begin, this, result);
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see javax.sql.DataSource#setLoginTimeout(int)
     */
    public void setLoginTimeout(final int seconds) throws SQLException { //NOCS
        final long begin = JDBCCallListener.callBegin(this, new Object[] {
                seconds});
        final Object result = null;
        try {
            if (delegateDataSource == null) {
                bindDataSource();
            }
            if (delegateDataSource != null) {
                delegateDataSource.setLoginTimeout(seconds);
            }
            JDBCCallListener.callEnd(begin, this, result);
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * Method to enable class to be referenceable.
     * @return reference to datasource
     * @throws NamingException if there is a problem creating reference.
     * @see javax.naming.Referenceable#getReference()
     */
    public Reference getReference() throws NamingException { //NOCS
        final String factoryName = DataSourceListenerFactory.class.getName();
        final Reference ref = new Reference(getClass().getName(), factoryName,
                null);
        ref.add(new StringRefAddr("dataSourceName", getDelegateClassName()));
        return ref;
    }

    //TODO Joudek implement correctly

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        if (delegateDataSource != null) {
            return  delegateDataSource.getParentLogger();
        }
        return null;
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        if (delegateDataSource != null) {
            return  delegateDataSource.unwrap(iface);
        }
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        if (delegateDataSource != null) {
            return  delegateDataSource.isWrapperFor(iface);
        }
        return false;
    }
}
