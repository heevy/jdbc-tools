/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.SQLException;
import java.sql.Savepoint;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCDelegator;


/**
 * SavepointListener wraps around a delegate Savepoint object
 * and passes all calls to the listener.
 */
public class SavepointListener implements Savepoint, JDBCDelegator {

    /**
     * Holds the delegate savepoint that calls are passed through to.
     */
    private Savepoint delegateSavepoint;

    /**
     * Creates a new SavepointListener that wraps around the specified
     * delegate savepoint.
     * @param savepoint the delegate savepoint
     */
    SavepointListener(final Savepoint savepoint) {
        this.delegateSavepoint = savepoint;
    }

    /**
     * Sets the delegate savepoint for the listener savepoint.
     * @param savepoint the delegate savepoint
     */
    public final void setDelegateSavepoint(final Savepoint savepoint) {
        this.delegateSavepoint = savepoint;
    }

    /**
     * @return the delegateSavepoint
     */
    public final Savepoint getDelegateSavepoint() {
        return delegateSavepoint;
    }

    /**
     * Returns the delegate class name.
     * @return the delegate class name
     */
    public final String getDelegateClassName() {
        return delegateSavepoint.getClass().getName();
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Savepoint#getSavepointId()
     */
    public final int getSavepointId() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        int result = 0;
        try {
            if (delegateSavepoint != null) {
                result = delegateSavepoint.getSavepointId();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Savepoint#getSavepointName()
     */
    public final String getSavepointName() throws SQLException {
        final long begin = JDBCCallListener.callBegin(this, new Object[] {});
        String result = null;
        try {
            if (delegateSavepoint != null) {
                result = delegateSavepoint.getSavepointName();
            }
            JDBCCallListener.callEnd(begin, this, result);
            return result;
        } catch (SQLException e) {
            JDBCCallListener.callException(begin, this, e);
            throw e;
        }
    }
}
