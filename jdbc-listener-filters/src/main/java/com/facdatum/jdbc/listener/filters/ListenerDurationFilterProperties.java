/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.filters;

import java.io.File;
import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

import com.facdatum.jdbc.listener.JDBCListenerProperties;

/**
 * Provides access to all the properties that can be used to control the
 * ListenerDurationFilter. These can either be pass in as java properties at the
 * command line of loaded via the properties file
 * jdbc-duration-filter.properties.
  */
public final class ListenerDurationFilterProperties {

    /**
     * The properties bundle that can be used to override the property file
     * name.
     */
    public static final String FILE_BUNDLE_REF =
            "com.facdatum.jdbc.config.JDBCPropertiesFileName";

    /**
     * Contains the key used to pick up the property directory path.
     */
    public static final String PROP_PATH_KEY = "jdbc.duration.filter.prop.path";

    /**
     * Contains the configured property directory path.
     */
    private static String propPath;

    /**
     * Contains the key used to pick up the property file name.
     */
    public static final String PROP_FILE_KEY =
        "jdbc.duration.filter.prop.filename";

    /**
     * Contains the default property file name.
     */
    public static final String PROP_FILE_DEF =
            "jdbc-duration-filter";

    /**
     * Contains the configured property file name.
     */
    private static String propFile;

    /**
     * Contains the key used to pick up the duration limit.
     */
    public static final String TIME_LIMIT_KEY =
        "jdbc.duration.filter.limit";

    /**
     * Contains the default time limit which means to ignore this check.
     */
    public static final long TIME_LIMIT_DEF = -1;

    /**
     * The configured duration limit.
     */
    private static long durationLimit;

    /**
     * Contains the key used to pick up the duration change threshold.
     */
    public static final String TIME_CHANGE_KEY =
        "jdbc.duration.filter.change";

    /**
     * Contains the default change threshold which means to ignore this check.
     */
    public static final double TIME_CHANGE_DEF = -1;

    /**
     * The configured duration change threshold.
     */
    private static double durationChange;

    /**
     * Contains the key used to pick up the stored durations file path.
     */
    public static final String TIME_FILE_KEY = "jdbc.duration.store.filepath";

    /**
     * The configured stored durations file path (default is null).
     */
    private static String durationFilePath;

    static {
        ListenerDurationFilterProperties.init();
    }

    /**
     * Private default constructor to stops creation of instances.
     */
    private ListenerDurationFilterProperties() {
        super();
    }

    /**
     * Creates an instance of the duration filter properties by reading the
     * defined properties file.
     */
    public static void init() {
        propFile = System.getProperty(PROP_FILE_KEY,
                PROP_FILE_DEF);
        try {
            final Properties fileProperties = loadBundle(FILE_BUNDLE_REF);
            propFile = fileProperties.getProperty(
                    JDBCListenerProperties.SHARED_FILE_KEY, propFile);
        } catch (MissingResourceException e) { //NOPMD NOCS
            //use default
        }

        propPath = System.getProperty(PROP_PATH_KEY);
        Properties tempProperties = new Properties();
        try {
            if (propPath == null) {
                tempProperties = loadBundle(propFile);
            } else {
                tempProperties.load(new FileInputStream(new File(propPath,
                        propFile + ".properties")));
            }
        } catch (Exception e) { //NOPMD NOCS
            //defaults will be used
        }
        durationLimit = Long.parseLong(
                tempProperties.getProperty(TIME_LIMIT_KEY,
                String.valueOf(TIME_LIMIT_DEF)));
        durationChange = Double.parseDouble(
                tempProperties.getProperty(TIME_CHANGE_KEY,
                String.valueOf(TIME_CHANGE_DEF)));
        durationFilePath = tempProperties.getProperty(
                TIME_FILE_KEY);

        durationLimit = Long.parseLong(System.getProperty(TIME_LIMIT_KEY,
                String.valueOf(durationLimit)));
        durationChange = Double.parseDouble(System.getProperty(
                TIME_CHANGE_KEY, String.valueOf(durationChange)));
        durationFilePath = System.getProperty(TIME_FILE_KEY,
                durationFilePath);
    }

    /**
     * Loads properties from a ResourceBundle.
     * @param bundleName name of bundle to load
     * @return properties in bundle
     */
    protected static Properties loadBundle(final String bundleName) {
        final ResourceBundle bundle = ResourceBundle.getBundle(bundleName);
        final Properties props = new Properties();
        final Enumeration < String > keys = bundle.getKeys();
        while (keys.hasMoreElements()) {
            final String key = (String) keys.nextElement();
            props.put(key, bundle.getString(key));
        }
        return props;
    }

    /**
     * Clears all the system properties related to the ListenerDurationFilter
     * to their defaults.
     */
    public static void clearProperties() {
        System.clearProperty(PROP_PATH_KEY);
        System.clearProperty(PROP_FILE_KEY);
        System.clearProperty(TIME_LIMIT_KEY);
        System.clearProperty(TIME_CHANGE_KEY);
    }

    /**
     * Returns the duration limit.
     * @return the duration limit
     */
    public static long getDurationLimit() {
        return durationLimit;
    }

    /**
     * Returns the duration percentage change threshold.
     * @return the duration change threshold
     */
    public static double getDurationChange() {
        return durationChange;
    }

    /**
     * Returns the file path to where pre-seeded duration times are stored.
     * @return the stored durations file path
     */
    public static String getDurationFilePath() {
        if (durationFilePath != null
                && durationFilePath.trim().length() == 0) { //NOPMD
            return null;
        }
        return durationFilePath;
    }
}
