/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.filters;

import java.io.File;
import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

import com.facdatum.jdbc.listener.JDBCListenerProperties;

/**
 * Provides access to all the properties that can be used to control the
 * ListenerCallFilter. These can either be pass in as java properties at the
 * command line of loaded via the properties file jdbc-call-filter.properties.
  */
public final class ListenerCallFilterProperties {

    /**
     * The properties bundle that can be used to override the property file
     * name.
     */
    public static final String FILE_BUNDLE_REF =
            "com.facdatum.jdbc.config.JDBCPropertiesFileName";

    /**
     * Contains the key used to pick up the property directory path.
     */
    public static final String PROP_PATH_KEY = "jdbc.call.filter.prop.path";

    /**
     * Contains the configured property directory path.
     */
    private static String propPath;

    /**
     * Contains the key used to pick up the property file name.
     */
    public static final String PROP_FILE_KEY = "jdbc.call.filter.prop.filename";

    /**
     * Contains the default property file name.
     */
    public static final String PROP_FILE_DEF =
            "jdbc-call-filter";

    /**
     * Contains the configured property file name.
     */
    private static String propFile;

    /**
     * Contains the key used to pick up the jdbc classes and methods to allow
     * through filter.
     */
    public static final String JDBC_INCLUDE_KEY = "jdbc.include";

    /**
     * Contains the default pattern to identify the jdbc classes and methods to
     * allow through filter.
     */
    public static final String JDBC_INCLUDE_DEF = "*";

    /**
     * Contains the configured pattern to identify the jdbc classes and methods
     * to allow through filter.
     */
    private static String jdbcInclude;

    /**
     * Contains the key used to pick up the jdbc classes and methods to exclude
     * with filter.
     */
    public static final String JDBC_EXCLUDE_KEY = "jdbc.exclude";

    /**
     * Contains the default pattern to identify the jdbc classes and methods
     * to exclude with filter.
     */
    public static final String JDBC_EXCLUDE_DEF = "";

    /**
     * Contains the configured pattern to identify the jdbc classes and methods
     * to exclude with filter.
     */
    private static String jdbcExclude;

    /**
     * Contains the key used to pick up the application classes and methods
     * to allow through filter.
     */
    public static final String APP_INCLUDE_KEY = "app.include";

    /**
     * Contains the default pattern to identify the application classes and
     * methods to allow through filter.
     */
    public static final String APP_INCLUDE_DEF = "*";

    /**
     * Contains the configured pattern to identify the application classes and
     * methods to allow through filter.
     */
    private static String appInclude;

    /**
     * Contains the key used to pick up the application classes and methods to
     * exclude with filter.
     */
    public static final String APP_EXCLUDE_KEY = "app.exclude";

    /**
     * Contains the default pattern to identify the application classes and
     * methods to exclude with filter.
     */
    public static final String APP_EXCLUDE_DEF = "";

    /**
     * Contains the configured pattern to identify the application classes and
     * methods to exclude with filter.
     */
    private static String appExclude;

    static {
        ListenerCallFilterProperties.init();
    }

    /**
     * Private default constructor to stops creation of instances.
     */
    private ListenerCallFilterProperties() {
        super();
    }

    /**
     * Creates an instance of the call filter properties by reading the
     * defined properties file.
     */
    public static void init() {
        propFile = System.getProperty(PROP_FILE_KEY,
                PROP_FILE_DEF);
        try {
            final Properties fileProperties = loadBundle(FILE_BUNDLE_REF);
            propFile = fileProperties.getProperty(
                    JDBCListenerProperties.SHARED_FILE_KEY, propFile);
        } catch (MissingResourceException e) { //NOPMD NOCS
            //use default
        }

        propPath = System.getProperty(PROP_PATH_KEY);
        Properties tempProperties = new Properties();
        try {
            if (propPath == null) {
                tempProperties = loadBundle(propFile);
            } else {
                tempProperties.load(new FileInputStream(new File(propPath,
                        propFile + ".properties")));
            }
        } catch (Exception e) { //NOPMD NOCS
            //defaults will be used
        }
        jdbcInclude = tempProperties.getProperty(JDBC_INCLUDE_KEY,
                JDBC_INCLUDE_DEF);
        jdbcExclude = tempProperties.getProperty(JDBC_EXCLUDE_KEY,
                JDBC_EXCLUDE_DEF);
        appInclude = tempProperties.getProperty(APP_INCLUDE_KEY,
                APP_INCLUDE_DEF);
        appExclude = tempProperties.getProperty(APP_EXCLUDE_KEY,
                APP_EXCLUDE_DEF);

        jdbcInclude = System.getProperty(JDBC_INCLUDE_KEY, jdbcInclude);
        jdbcExclude = System.getProperty(JDBC_EXCLUDE_KEY, jdbcExclude);
        appInclude = System.getProperty(APP_INCLUDE_KEY, appInclude);
        appExclude = System.getProperty(APP_EXCLUDE_KEY, appExclude);
    }

    /**
     * Loads properties from a ResourceBundle.
     * @param bundleName name of bundle to load
     * @return properties in bundle
     */
    protected static Properties loadBundle(final String bundleName) {
        final ResourceBundle bundle = ResourceBundle.getBundle(bundleName);
        final Properties props = new Properties();
        final Enumeration < String > keys = bundle.getKeys();
        while (keys.hasMoreElements()) {
            final String key = (String) keys.nextElement();
            props.put(key, bundle.getString(key));
        }
        return props;
    }

    /**
     * Clears all the system properties related to the ListenerCallFilter to
     * their defaults.
     */
    public static void clearProperties() {
        System.clearProperty(PROP_PATH_KEY);
        System.clearProperty(PROP_FILE_KEY);
        System.clearProperty(JDBC_INCLUDE_KEY);
        System.clearProperty(JDBC_EXCLUDE_KEY);
        System.clearProperty(APP_INCLUDE_KEY);
        System.clearProperty(APP_EXCLUDE_KEY);
    }

    /**
     * Gets the configured properties file path.
     * @return the properties file path
     */
    public static String getPropPath() {
        return propPath;
    }

    /**
     * Gets the configured properties file name.
     * @return the properties file name
     */
    public static String getPropFile() {
        return propFile;
    }

    /**
     * Gets the configured include pattern for jdbc classes and methods.
     * @return the jdbc class and method inclusion pattern
     */
    public static String getJDBCInclude() {
        return jdbcInclude;
    }

    /**
     * Gets the configured exclude pattern for jdbc classes and methods.
     * @return the jdbc class and method exclusion pattern
     */
    public static String getJDBCExclude() {
        return jdbcExclude;
    }

    /**
     * Gets the configured include pattern for application classes and methods.
     * @return the application class and method inclusion pattern
     */
    public static String getAppInclude() {
        return appInclude;
    }

    /**
     * Gets the configured exclude pattern for application classes and methods.
     * @return the application class and method exclusion pattern
     */
    public static String getAppExclude() {
        return appExclude;
    }
}
