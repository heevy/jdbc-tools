/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.filters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.facdatum.jdbc.listener.JDBCListener;
import com.facdatum.jdbc.listener.ListenerCallEvent;
import com.facdatum.jdbc.listener.ListenerExceptionEvent;
import com.facdatum.jdbc.listener.ListenerReturnEvent;

/**
 * ListenerDurationFilter provides methods for filtering listeners based
 * on duration of executed SQL statements.
 */
public class ListenerDurationFilter implements JDBCListener {

    /**
     * Holds the set of jdbc methods that prepare queries.
     */
    private final Set < String > prepareMethods = //NOPMD
        new HashSet < String > (); //NOCS

    /**
     * Holds the set of jdbc methods that execute queries.
     */
    private final Set < String > executeMethods = //NOPMD
        new HashSet < String > (); //NOCS

    /**
     * Contains set of sql statements that have been prepared.
     */
    private final  Map < PreparedStatement, String > prepSQL = //NOPMD
        new HashMap < PreparedStatement, String > (); //NOCS

    /**
     * Contains set of sql statements that have been executed and their
     * timings.
     */
    private final  Map < String, Long > sqlDuration = //NOPMD
        new HashMap < String, Long > (); //NOCS

    /**
     * Holds the event passed into acceptBegin for later use in acceptEnd.
     */
    private ListenerCallEvent callEvent; //NOPMD

    /**
     * Configured duration limit below which listeners will be filtered.
     */
    private long durationLimit; //NOPMD

    /**
     * Configured duration change percentage below which listeners will
     * be filtered.
     */
    private double durationChange; //NOPMD

    /**
     * Output print stream for error messages.
     */
    private static PrintStream errLog = System.err;

    /**
     * Initialises the filter using the properties class.
     */
    public final void init() {
        ListenerDurationFilterProperties.init();
        durationLimit = ListenerDurationFilterProperties.getDurationLimit();
        durationChange = ListenerDurationFilterProperties.getDurationChange();
        loadStoredDurations(
                ListenerDurationFilterProperties.getDurationFilePath());
        prepareMethods.add("java.sql.Connection.prepareStatement");
        executeMethods.add("java.sql.Statement.executeQuery");
        executeMethods.add("java.sql.Statement.executeUpdate");
        executeMethods.add("java.sql.PreparedStatement.execute");
        executeMethods.add("java.sql.PreparedStatement.executeQuery");
        executeMethods.add("java.sql.PreparedStatement.executeUpdate");
    }

    /**
     * Loads pre-seed stored durations if the property has been set.
     * @param durationsFilePath the configured file location
     */
    private void loadStoredDurations(final String durationsFilePath) {
        if (durationsFilePath != null) {
            try {
                final BufferedReader durationsFile = new BufferedReader(
                        new FileReader(new File(durationsFilePath)));
                String line;
                while ((line = durationsFile.readLine()) != null) {
                    final int fieldBreak = line.lastIndexOf(':');
                    final String sql = line.substring(0, fieldBreak);
                    final long duration = Long.parseLong(line.substring(
                            fieldBreak + 1));
                    sqlDuration.put(sql, duration);
                }
            } catch (IOException e) {
                errLog.println(e);
                e.printStackTrace(errLog);
            }
        }
    }

    /**
     * Records the call event for use in acceptEnd.
     * @param event the call event
     */
    public final void acceptBegin(final ListenerCallEvent event) {
        callEvent = event;
    }

    /**
     * Runs the checks on call duration.
     * @param event the return event
     */
    public final void acceptEnd(final ListenerReturnEvent event) {
        if ((durationLimit < 0 && durationChange < 0)
                || (!prepareMethods.contains(event.getJDBCInterfaceMethod())
                        && !executeMethods.contains(
                                event.getJDBCInterfaceMethod()))) {
            return;
        } else if (prepareMethods.contains(event.getJDBCInterfaceMethod())) {
            final Object[] args = callEvent.getArgs();
            prepSQL.put((PreparedStatement) event.getWrappedResult(),
                            (String) args[0]);
            return;
        }
        final long duration = event.getDuration();

        if (durationLimit >= 0 && duration < durationLimit) {
            event.setFilterListeners(true);
            return;
        }
        if (durationChange >= 0) {
            checkChangeDuration(event, duration);
        }
    }

    /**
     * Checks whether the new duration is above percentage change threshold.
     * @param event the return event
     * @param duration the duration of call
     */
    private void checkChangeDuration(final ListenerReturnEvent event,
            final long duration) {
        String sql;
        if (callEvent.getArgs().length > 0) {
            final Object[] args = callEvent.getArgs();
            sql = (String) args[0];
        } else {
            sql = (String) prepSQL.get(event.getObject());
        }

        final Long savedDuration = sqlDuration.get(sql);
        if (savedDuration != null
            && (duration < savedDuration * (1 + durationChange))) {
            event.setFilterListeners(true);
            return;
        }
        sqlDuration.put(sql, duration);
    }

    /**
     * Not used in this filter.
     * @param event exception listener event
     */
    public void acceptException(final ListenerExceptionEvent event) {
        // do nothing
    }
}
