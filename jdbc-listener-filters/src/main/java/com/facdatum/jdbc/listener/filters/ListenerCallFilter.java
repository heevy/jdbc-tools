/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.filters;


import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.facdatum.jdbc.listener.JDBCListener;
import com.facdatum.jdbc.listener.ListenerCallEvent;
import com.facdatum.jdbc.listener.ListenerExceptionEvent;
import com.facdatum.jdbc.listener.ListenerReturnEvent;

/**
 * ListenerCallFilter class provides the methods for filtering listeners
 * based on call stack contents for integration with the DriverListener.
 */
public final class ListenerCallFilter implements JDBCListener {

    /**
     * Array of regular expression patterns that will be matched against
     * JDBC method for inclusion.
     */
    private static Pattern[] jdbcIncludes;

    /**
     * Array of regular expression patterns that will be matched against
     * JDBC method for exclusion.
     */
    private static Pattern[] jdbcExcludes;

    /**
     * Array of regular expression patterns that will be matched against
     * application call stack for inclusion.
     */
    private static Pattern[] appIncludes;

    /**
     * Array of regular expression patterns that will be matched against
     * application call stack for exclusion.
     */
    private static Pattern[] appExcludes;

    /**
     * Configure the filter based on defined properties.
     */
    public void init() {
        ListenerCallFilterProperties.init();
        jdbcIncludes = getPatterns(
                ListenerCallFilterProperties.getJDBCInclude());
        jdbcExcludes = getPatterns(
                ListenerCallFilterProperties.getJDBCExclude());
        appIncludes = getPatterns(
                ListenerCallFilterProperties.getAppInclude());
        appExcludes = getPatterns(
                ListenerCallFilterProperties.getAppExclude());
    }

    /**
     * No argument constructor to enable JDBCListener to create instances
     * of ListenerCallFilter.
     */
    public ListenerCallFilter() {
        //nothing to do
    }

    /**
     * Checks whether listeners should be filtered based on call stack
     * information.
     * @param event the record of the JDBC call
     */
    public void acceptBegin(final ListenerCallEvent event) {
        if (filterCall(event.getJDBCMethod(),
                    event.getJDBCInterfaceMethod())) {
            event.setFilterListeners(true);
        }
    }

    /**
     * Checks whether listeners should be filtered based on call stack
     * information.
     * @param event the record of the JDBC return
     */
    public void acceptEnd(final ListenerReturnEvent event) {
        if (filterCall(event.getJDBCMethod(),
                event.getJDBCInterfaceMethod())) {
            event.setFilterListeners(true);
        }
    }

    /**
     * Checks whether listeners should be filtered based on call stack
     * information.
     * @param event the record of the JDBC exception
     */
    public void acceptException(final ListenerExceptionEvent event) {
        if (filterCall(event.getJDBCMethod(),
                event.getJDBCInterfaceMethod())) {
            event.setFilterListeners(true);
        }
    }

    /**
     * Tests whether call should be filtered based on jdbc method and
     * application call.
     * @param jdbcMethod the jdbc method
     * @param jdbcIntMethod the implemented jdbc interface
     * @return true if call should be filtered; false otherwise
     */
    protected static boolean filterCall(final String jdbcMethod,
            final String jdbcIntMethod) {
        return !(filterJDBCMethod(jdbcMethod, jdbcIntMethod)
                && filterApplicationCallStack());

    }

    /**
     * Converts a comma separated list of regular expressions into an array
     * of Pattern elements.
     * @param patternsList comma separated list of patterns
     * @return array of regular expression patterns
     */
    protected static Pattern[] getPatterns(final String patternsList) {
        final ArrayList < Pattern > patterns =
                new ArrayList < Pattern > (); //NOCS
        if (patternsList != null) {
            final StringTokenizer tokens =
                    new StringTokenizer(patternsList, ",");
            while (tokens.hasMoreElements()) {
                patterns.add(Pattern.compile(convertToRegExp(
                        tokens.nextToken().trim())));
            }
        }
        return patterns.toArray(new Pattern[0]); //NOPMD
    }

    /**
     * Converts a simple search string using standard * to match multiple
     * characters into a regular expression.
     * @param simpleExp the simple search expression
     * @return the regular expression equivalent of simple search expression
     */
    protected static String convertToRegExp(final String simpleExp) {
        return simpleExp.replace("$", "\\$").replace(".", "\\.").
                replace("*", ".*");
    }

    /**
     * Checks whether the input string matches any of the regular expressions
     * in the supplied patterns.
     * @param patterns the array of regular expressions to match
     * @param method the input method to match against
     * @return true if there is a match; false otherwise
     */
    protected static boolean matches(final Pattern[] patterns,
            final String method) {
        for (int i = 0; i < patterns.length; i++) {
            final Matcher matcher = patterns[i].matcher(method);
            if (matcher.matches()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks whether the JDBC method that is being called should be filtered
     * or not according to current configuration.
     * @param jdbcMethod the jdbc method name and class definition
     * @param jdbcIntMethod the jdbc method name and interface name
     * @return true if method should be not be filtered; false otherwise
     */
    protected static boolean filterJDBCMethod(final String jdbcMethod,
            final String jdbcIntMethod) {
        if ((matches(jdbcIncludes, jdbcMethod)
                        || matches(jdbcIncludes, jdbcIntMethod))
                && !(matches(jdbcExcludes, jdbcMethod)
                        || matches(jdbcExcludes, jdbcIntMethod))) {
            return true;
        }
        return false;
    }

    /**
     * Checks whether the call stack that led to the listener being called
     * contains anything that should not be filtered.
     * @return true if method should not be filtered; false otherwise
     */
    protected static boolean filterApplicationCallStack() {
        final Exception e = new Exception(); //NOPMD
        final StackTraceElement[] stackTrace = e.getStackTrace();
        boolean included = false;
        for (int i = 0; i < stackTrace.length; i++) {
            final String callStack = stackTrace[i].getClassName() + "."
                    + stackTrace[i].getMethodName();
            if (matches(appIncludes, callStack)) {
                included = true;
            }
            if (matches(appExcludes, callStack)) {
                return false;
            }
        }
        return included;
    }
}
