/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.listener;

import com.facdatum.jdbc.listener.ListenerCallEvent;

/**
 * Simple mock call event used in testing.
 */
public class MockCallEvent implements ListenerCallEvent {

    /**
     * Stored interface method.
     */
    private final String interfaceMethod; //NOPMD

    /**
     * Stored arguments.
     */
    private final Object[] args; //NOPMD

    /**
     * Creates call event with specified interface method and arguments.
     * @param thisMethod the JDBC interface method
     * @param thisArgs the arguments
     */
    public MockCallEvent(final String thisMethod,
            final Object[] thisArgs) {
        interfaceMethod = thisMethod;
        args = thisArgs.clone();
    }

    /**
     * Returns JDBC Method under test.
     * @return jdbc method
     */
    public final String getJDBCMethod() {
        return "com.facdatum.jdbc.Connection.prepareStatement";
    }

    /**
     * Returns JDBC interface method under test.
     * @return jdbc interface method
     */
    public final String getJDBCInterfaceMethod() {
        return interfaceMethod;
    }

    /**
     * Returns the arguments under test with SQL string.
     * @return the test method arguments
     */
    public final Object[] getArgs() {
        return args.clone();
    }

    /**
     * Not needed for test.
     * @return null
     */
    public final Object getObject() {
        return null;
    }

    /**
     * Not needed for test.
     * @return current time millis
     */
    public final long getTime() {
        return System.currentTimeMillis();
    }

    /**
     * Not needed for test.
     * @return null
     */
    public final StackTraceElement[] getStackTrace() {
        return null;
    }

    /**
     * Not needed for test.
     * @return null
     */
    public final String getCallingMethod() {
        return null;
    }

    /**
     * Not needed for test.
     * @param filter filter the listeners
     */
    public final void setFilterListeners(final boolean filter) {
        //do nothing
    }

    /**
     * Not needed for test.
     * @return false
     */
    public final boolean getFilterListeners() { //NOPMD
        return false;
    }
}
