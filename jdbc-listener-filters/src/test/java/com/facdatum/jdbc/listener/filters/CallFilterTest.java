/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.filters;

import java.sql.SQLException;
import java.sql.Savepoint;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCDelegator;
import com.facdatum.jdbc.listener.JDBCListener;
import com.facdatum.jdbc.listener.JDBCListenerProperties;
import com.facdatum.jdbc.listener.driver.SavepointHelper;
import com.facdatum.mock.listener.MockListener;

import junit.framework.TestCase;

/**
 * Tests for the ListenerCallFilter class.
 */
public class CallFilterTest extends TestCase {

    /**
     * Test driver that will get registered (although is not used).
     */
    private static final String DRIVER_CLASS = "oracle.jdbc.OracleDriver";

    /**
     * Error message when problem logging begin.
     */
    private static final String TESTING_CALLS_MSG =
        "Checking right number of calls made";

    /**
     * Expected number of calls.
     */
    private static final int EXPECTED_CALLS = 3;

    /**
     * Savepoint failed message.
     */
    private static final String SAVEPOINT_FAILED =
            "Error creating mock jdbc savepoint";

    /**
     * Savepoint under test.
     */
    private Savepoint testSavepoint; //NOPMD

    /**
     * Setup test of logger class by overriding some of the default properties.
     * @throws Exception if there is a problem setting up test
     */
    protected final void setUp() throws Exception {
        super.setUp();
        System.setProperty(JDBCListenerProperties.LISTENERS_KEY,
                ListenerCallFilter.class.getName() + ","
                + MockListener.class.getName());
        System.setProperty(JDBCListenerProperties.DRIVER_CLASS_KEY,
                DRIVER_CLASS);
        // intentional space in following list to check these are stripped
        System.setProperty(ListenerCallFilterProperties.JDBC_INCLUDE_KEY,
                "java.sql.PreparedStatement.*, java.sql.ResultSet.get*,"
                + this.getClass().getName()
                        + "$MockJDBCSavepoint.*");
        System.setProperty(ListenerCallFilterProperties.JDBC_EXCLUDE_KEY,
                "java.sql.PreparedStatement.addBatch,"
                + "java.sql.ResultSet.getMetaData,"
                + "java.sql.Savepoint.getSavepointName");
        System.setProperty(ListenerCallFilterProperties.APP_INCLUDE_KEY,
                "com.facdatum.jdbc.listener.filters.CallFilterTest.*");
        System.setProperty(ListenerCallFilterProperties.APP_EXCLUDE_KEY
                , "*.testAppExcludeLog*");
        JDBCCallListener.init();
        testSavepoint = SavepointHelper.getMockSavepoint(
                new MockJDBCSavepoint());
    }

    /**
     * Tidy up after test by deleted generated log file.
     * @throws Exception if there is a problem tidying up test
     */
    protected final void tearDown() throws Exception {
        super.tearDown();
        ListenerCallFilterProperties.clearProperties();
        JDBCCallListener.resetInit();
    }

    /**
     * Gets the number of times the listener has been called.
     * @return the number of listener calls
     */
    private int countCalls() {
        final JDBCListener[] listeners = JDBCCallListener.getListeners();
        final MockListener mockListener = (MockListener) listeners[1];
        return mockListener.getCallCount();
    }

    /**
     * Test of logBegin when calling method is included in filter list.
     */
    public final void testJDBCInclude() {
        try {
            testSavepoint.getSavepointId();
        } catch (SQLException e) {
            fail(SAVEPOINT_FAILED);
        }
        assertEquals(TESTING_CALLS_MSG, EXPECTED_CALLS, countCalls());
    }

    /**
     * Test of logBegin when calling method is excluded from filter list.
     */
    public final void testJDBCExclude() {
        try {
            testSavepoint.getSavepointName();
        } catch (SQLException e) {
            fail(SAVEPOINT_FAILED);
        }
        assertEquals(TESTING_CALLS_MSG, 1, countCalls());
    }

    /**
     * Test of logBegin when calling method is included in application
     * filter list.
     */
    public final void testAppIncludeLogBegin() {
        try {
            testSavepoint.getSavepointId();
        } catch (SQLException e) {
            fail(SAVEPOINT_FAILED);
        }
        assertEquals(TESTING_CALLS_MSG, EXPECTED_CALLS, countCalls());
    }

    /**
     * Test of logBegin when calling method is excluded from application
     * filter list.
     */
    public final void testAppExcludeLogBegin() {
        try {
            testSavepoint.getSavepointId();
        } catch (SQLException e) {
            fail(SAVEPOINT_FAILED);
        }
        assertEquals(TESTING_CALLS_MSG, 1, countCalls());
    }

    /**
     * Test for the filter JDBC method check method.
     */
    public final void testLogJDBCMethod() {
        assertTrue("Testing when method included",
                ListenerCallFilter.filterJDBCMethod(this.getClass().getName()
                        + "$DummyJDBCPreparedStatement.execute",
                        "java.sql.PreparedStatement.execute"));
        assertFalse("Testing when class excluded",
                ListenerCallFilter.filterJDBCMethod(this.getClass().getName()
                        + "$DummyJDBCCallableStatement.execute",
                        "java.sql.CallableStatement.execute"));
        assertFalse("Testing when method excluded",
                ListenerCallFilter.filterJDBCMethod(this.getClass().getName()
                        + "$DummyJDBCPreparedStatement.addBatch",
                        "java.sql.PreparedStatement.addBatch"));
        assertTrue("Testing when multiple method included",
                ListenerCallFilter.filterJDBCMethod(this.getClass().getName()
                        + "$DummyJDBCResutSet.getInt",
                        "java.sql.ResultSet.getInt"));
        assertFalse("Testing when method excluded from multiple match",
                ListenerCallFilter.filterJDBCMethod(this.getClass().getName()
                        + "$DummyJDBCResultSet.getMetaData",
                        "java.sql.ResultSet.getMetaData"));
    }

    /**
     * Mock JDBC Savepoint class to allow easy testing.
     */
    private class MockJDBCSavepoint implements JDBCDelegator, Savepoint {

        /**
         * Default constructor.
         */
        public MockJDBCSavepoint() {
            //do nothing
        }

        /**
         * Implemented method to enable testing.
         * @return the delegate class name
         */
        public String getDelegateClassName() {
            return this.getClass().getName();
        }

        /**
         * {@inheritDoc}
         * @see java.sql.Savepoint#getSavepointId()
         */
        public final int getSavepointId() throws SQLException {
            //do nothing
            return 0;
        }

        /**
         * {@inheritDoc}
         * @see java.sql.Savepoint#getSavepointName()
         */
        public final String getSavepointName() throws SQLException {
            //do nothing
            return null;
        }
    }
}
