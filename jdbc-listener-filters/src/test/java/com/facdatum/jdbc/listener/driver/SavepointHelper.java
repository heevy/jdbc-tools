/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.Savepoint;

/**
 * This class allows instances of mock savepoints to be obtained with
 * a listener wrapped around them.
 */
public final class SavepointHelper {

    /**
     * Hide default constructor to stop instance being created.
     */
    private SavepointHelper() {
        //do nothing
    }

    /**
     * Creates a SavepointListener that is wrapped around the supplied
     * Savepoint.
     * @param mock the mock savepoint
     * @return the mock savepoint listener
     */
    public static Savepoint getMockSavepoint(
            final Savepoint mock) {
        return new SavepointListener(mock);
    }
}
