/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.filters;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCListener;
import com.facdatum.jdbc.listener.JDBCListenerProperties;
import com.facdatum.mock.listener.MockCallEvent;
import com.facdatum.mock.listener.MockListener;
import com.facdatum.mock.listener.MockReturnEvent;

import junit.framework.TestCase;

/**
 * Tests for the ListenerDurationFilter class.
 */
public class DurationFilterTest extends TestCase {

    /**
     * Initial execution time.
     */
    private static final int BASE_TIME = 20000;

    /**
     * Execution time below threshold.
     */
    private static final int BELOW_TIME = 25000;

    /**
     * Execution time above threshold.
     */
    private static final int ABOVE_TIME = 35000;

    /**
     * Test driver that will get registered (although is not used).
     */
    private static final String DRIVER_CLASS = "oracle.jdbc.OracleDriver";

    /**
     * Execute method under test.
     */
    private static final String EXECUTE_METHOD =
        "java.sql.Statement.executeQuery";

    /**
     * Prepare statement method under test.
     */
    private static final String PREP_METHOD =
        "java.sql.Connection.prepareStatement";

    /**
     * Execute for prepared statement under test.
     */
    private static final String PREP_EXE_METHOD =
        "java.sql.PreparedStatement.execute";

    /**
     * SQL under test.
     */
    private static final String TEST_SQL =
        "select * from hr.employees where last_name = ?";

    /**
     * Configured duration limit.
     */
    private static final long TEST_LIMIT = 10000;

    /**
     * Configured duration change threshold.
     */
    private static final double TEST_CHANGE = 0.5;

    /**
     * Setup test of logger class by overriding some of the default properties.
     * @throws Exception if there is a problem setting up test
     */
    protected final void setUp() throws Exception {
        super.setUp();
        System.setProperty(JDBCListenerProperties.LISTENERS_KEY,
                ListenerDurationFilter.class.getName() + ","
                + MockListener.class.getName());
        System.setProperty(JDBCListenerProperties.DRIVER_CLASS_KEY,
                DRIVER_CLASS);
        System.setProperty(ListenerDurationFilterProperties.TIME_LIMIT_KEY,
                String.valueOf(TEST_LIMIT));
        System.setProperty(ListenerDurationFilterProperties.TIME_CHANGE_KEY,
                String.valueOf(TEST_CHANGE));
        JDBCCallListener.init();
    }

    /**
     * Tidy up after test by deleted generated log file.
     * @throws Exception if there is a problem tidying up test
     */
    protected final void tearDown() throws Exception {
        super.tearDown();
        ListenerDurationFilterProperties.clearProperties();
        JDBCCallListener.resetInit();
    }


    /**
     * Returns the configured ListenerDurationFilter.
     * @return the ListenerDurationFilter
     */
    private ListenerDurationFilter getFilter() {
        final JDBCListener[] listeners = JDBCCallListener.getListeners();
        for (int i = 0; i < listeners.length; i++) {
            if (listeners[i] instanceof ListenerDurationFilter) {
                return (ListenerDurationFilter) listeners[i];
            }
        }
        return null;
    }

    /**
     * Test when limit should not be filtered.
     */
    public final void testLimitNotFiltered() {
        final ListenerDurationFilter filter = getFilter();
        filter.acceptBegin(new MockCallEvent(PREP_METHOD,
                new Object[] {TEST_SQL}));
        final MockReturnEvent event = new MockReturnEvent(
                PREP_EXE_METHOD, 20000);
        filter.acceptEnd(event);
        assertFalse("test listener not filtered", event.getFilterListeners());
    }


    /**
     * Test when limit should be filtered.
     */
    public final void testLimitFiltered() {
        final ListenerDurationFilter filter = getFilter();
        filter.acceptBegin(new MockCallEvent(PREP_METHOD,
                new Object[] {TEST_SQL}));
        final MockReturnEvent event = new MockReturnEvent(
                PREP_EXE_METHOD, 5000);
        filter.acceptEnd(event);
        assertTrue("test listener filtered", event.getFilterListeners());
    }

    /**
     * Test execution time changes are filtered correctly for direct
     * execute method.
     */
    public final void testChangesFiltered() {
        final ListenerDurationFilter filter = getFilter();
        filter.acceptBegin(new MockCallEvent(EXECUTE_METHOD,
                new Object[] {TEST_SQL}));
        filter.acceptEnd(new MockReturnEvent(EXECUTE_METHOD, BASE_TIME));
        MockReturnEvent event = new MockReturnEvent(
                EXECUTE_METHOD, BELOW_TIME);
        filter.acceptEnd(event);
        assertTrue("test change too small", event.getFilterListeners());
        filter.acceptBegin(new MockCallEvent(EXECUTE_METHOD,
                new Object[] {TEST_SQL}));
        event = new MockReturnEvent(EXECUTE_METHOD, ABOVE_TIME);
        filter.acceptEnd(event);
        assertFalse("test change exceeds limit", event.getFilterListeners());
    }

    /**
     * Test execution time changes are filtered correctly for prepare
     * and execute methods.
     */
    public final void testPrepChangesFiltered() {
        final ListenerDurationFilter filter = getFilter();
        filter.acceptBegin(new MockCallEvent(PREP_METHOD,
                new Object[] {TEST_SQL}));
        filter.acceptEnd(new MockReturnEvent(PREP_METHOD, 0));
        filter.acceptBegin(new MockCallEvent(PREP_EXE_METHOD,
                new Object[] {}));
        MockReturnEvent event = new MockReturnEvent(
                PREP_EXE_METHOD, BASE_TIME);
        filter.acceptEnd(event);
        assertFalse("initial exceeds limit", event.getFilterListeners());
        filter.acceptBegin(new MockCallEvent(PREP_EXE_METHOD,
                new Object[] {}));
        event = new MockReturnEvent(PREP_EXE_METHOD, ABOVE_TIME);
        filter.acceptEnd(event);
        assertFalse("test change exceeds limit", event.getFilterListeners());
    }
}
