/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.filters;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCListener;
import com.facdatum.jdbc.listener.JDBCListenerProperties;
import com.facdatum.mock.listener.MockCallEvent;
import com.facdatum.mock.listener.MockListener;
import com.facdatum.mock.listener.MockReturnEvent;

import junit.framework.TestCase;

/**
 * Tests for changes in duration with no limit set.
 */
public class DurationChangeOnlyTest extends TestCase {

    /**
     * Initial time for execution.
     */
    private static final int BASE_TIME = 100;

    /**
     * Execution time below threshold.
     */
    private static final int BELOW_TIME = 120;

    /**
     * Execution time above threshold.
     */
    private static final int ABOVE_TIME = 200;

    /**
     * Test driver that will get registered (although is not used).
     */
    private static final String DRIVER_CLASS = "oracle.jdbc.OracleDriver";

    /**
     * Method under test.
     */
    private static final String EXECUTE_METHOD =
        "java.sql.Statement.executeQuery";

    /**
     * SQL under test.
     */
    private static final String TEST_SQL =
        "select * from hr.employees where last_name = ?";

    /**
     * Change threshold used in test.
     */
    private static final double TEST_CHANGE = 0.5;

    /**
     * Setup test of logger class by overriding some of the default properties.
     * @throws Exception if there is a problem setting up test
     */
    protected final void setUp() throws Exception {
        super.setUp();
        System.setProperty(JDBCListenerProperties.LISTENERS_KEY,
                ListenerDurationFilter.class.getName() + ","
                + MockListener.class.getName());
        System.setProperty(JDBCListenerProperties.DRIVER_CLASS_KEY,
                DRIVER_CLASS);
        System.setProperty(ListenerDurationFilterProperties.TIME_CHANGE_KEY,
                String.valueOf(TEST_CHANGE));
        JDBCCallListener.init();
    }

    /**
     * Tidy up after test by deleted generated log file.
     * @throws Exception if there is a problem tidying up test
     */
    protected final void tearDown() throws Exception {
        super.tearDown();
        ListenerDurationFilterProperties.clearProperties();
        JDBCCallListener.resetInit();
    }

    /**
     * Returns the configured ListenerDurationFilter.
     * @return the ListenerDurationFilter
     */
    private ListenerDurationFilter getFilter() {
        final JDBCListener[] listeners = JDBCCallListener.getListeners();
        for (int i = 0; i < listeners.length; i++) {
            if (listeners[i] instanceof ListenerDurationFilter) {
                return (ListenerDurationFilter) listeners[i];
            }
        }
        return null;
    }

    /**
     * Tests changed execution times are filtered correctly.
     */
    public final void testChangesFiltered() {
        final ListenerDurationFilter filter = getFilter();
        filter.acceptBegin(new MockCallEvent(EXECUTE_METHOD,
                new Object[] {TEST_SQL}));
        filter.acceptEnd(new MockReturnEvent(EXECUTE_METHOD, BASE_TIME));
        MockReturnEvent event = new MockReturnEvent(
                EXECUTE_METHOD, BELOW_TIME);
        filter.acceptEnd(event);
        assertTrue("test change too small", event.getFilterListeners());
        filter.acceptBegin(new MockCallEvent(EXECUTE_METHOD,
                new Object[] {TEST_SQL}));
        event = new MockReturnEvent(EXECUTE_METHOD, ABOVE_TIME);
        filter.acceptEnd(event);
        assertFalse("test change exceeds limit", event.getFilterListeners());
    }
}
