/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.filters;

import java.io.File;
import java.io.PrintStream;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCListener;
import com.facdatum.jdbc.listener.JDBCListenerProperties;
import com.facdatum.mock.listener.MockCallEvent;
import com.facdatum.mock.listener.MockListener;
import com.facdatum.mock.listener.MockReturnEvent;

import junit.framework.TestCase;

/**
 * Test of reading pre-seeded durations.
 */
public class StoredDurationTest extends TestCase {

    /**
     * Test driver that will get registered (although is not used).
     */
    private static final String DRIVER_CLASS = "oracle.jdbc.OracleDriver";

    /**
     * Method under test.
     */
    private static final String EXECUTE_METHOD =
        "java.sql.Statement.executeQuery";

    /**
     * First stored sql under test.
     */
    private static final String TEST_SQL_1 =
        "select * from hr.employees where last_name = ?";

    /**
     * First stored duration under test.
     */
    private static final long TEST_TIME_1 = 10000;

    /**
     * Second stored sql under test.
     */
    private static final String TEST_SQL_2 =
        "select * from hr.employees";

    /**
     * Second stored duration under test.
     */
    private static final long TEST_TIME_2 = 5000;

    /**
     * Third sql under test which is not stored.
     */
    private static final String TEST_SQL_3 =
        "select * from hr.jobs";

    /**
     * Base execution time for third sql under test.
     */
    private static final long TEST_TIME_3 = 4000;

    /**
     * Configured change threshold.
     */
    private static final double TEST_CHANGE = 0.5;

    /**
     * Configured location for stored durations.
     */
    private static final String TEST_FILE = "/tmp/timings.store";

    /**
     * Setup test of logger class by overriding some of the default properties.
     * @throws Exception if there is a problem setting up test
     */
    protected final void setUp() throws Exception {
        super.setUp();
        final PrintStream durationsFile = new PrintStream(new File(
                TEST_FILE));
        durationsFile.println(TEST_SQL_1 + ":" + TEST_TIME_1);
        durationsFile.println(TEST_SQL_2 + ":" + TEST_TIME_2);
        durationsFile.close();
        System.setProperty(JDBCListenerProperties.LISTENERS_KEY,
                ListenerDurationFilter.class.getName() + ","
                + MockListener.class.getName());
        System.setProperty(JDBCListenerProperties.DRIVER_CLASS_KEY,
                DRIVER_CLASS);
        System.setProperty(ListenerDurationFilterProperties.TIME_CHANGE_KEY,
                String.valueOf(TEST_CHANGE));
        System.setProperty(ListenerDurationFilterProperties.TIME_FILE_KEY,
                TEST_FILE);
        JDBCCallListener.init();
    }

    /**
     * Tidy up after test by deleted generated log file.
     * @throws Exception if there is a problem tidying up test
     */
    protected final void tearDown() throws Exception {
        super.tearDown();
        ListenerDurationFilterProperties.clearProperties();
        JDBCCallListener.resetInit();
        new File(TEST_FILE).delete();
    }

    /**
     * Returns the configured ListenerDurationFilter.
     * @return the ListenerDurationFilter
     */
    private ListenerDurationFilter getFilter() {
        final JDBCListener[] listeners = JDBCCallListener.getListeners();
        for (int i = 0; i < listeners.length; i++) {
            if (listeners[i] instanceof ListenerDurationFilter) {
                return (ListenerDurationFilter) listeners[i];
            }
        }
        return null;
    }

    /**
     * Test changes in stored durations.
     */
    public final void testStoredDurations() { //NOPMD
        final ListenerDurationFilter filter = getFilter();
        runFilterTest(filter, TEST_SQL_1, TEST_TIME_1 + 1, true);
        runFilterTest(filter, TEST_SQL_1,
                (long) (TEST_TIME_1 * (1 + TEST_CHANGE)) + 1, false);
        runFilterTest(filter, TEST_SQL_2,
                (long) (TEST_TIME_1 * (1 + TEST_CHANGE)) + 1, false);
        runFilterTest(filter, TEST_SQL_3, TEST_TIME_3, false);
        runFilterTest(filter, TEST_SQL_3, TEST_TIME_3 + 1, true);
        runFilterTest(filter, TEST_SQL_3,
                (long) (TEST_TIME_3 * (1 + TEST_CHANGE)) + 1, false);
    }

    /**
     * Steps to run a filter test.
     * @param filter the duration filter
     * @param sql the sql to be tested
     * @param duration the duration to be tested
     * @param result the expected result
     */
    protected final void runFilterTest(final ListenerDurationFilter filter,
            final String sql, final long duration, final boolean result) {
        filter.acceptBegin(new MockCallEvent(EXECUTE_METHOD,
                new Object[] {sql}));
        final MockReturnEvent event = new MockReturnEvent(
                EXECUTE_METHOD, duration);
        filter.acceptEnd(event);
        assertEquals("test listener filtered on stored value", result,
                event.getFilterListeners());
    }
}
