/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.PreparedStatement;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCListenerProperties;
import com.facdatum.jdbc.listener.driver.ListenerHelper;

import junit.framework.TestCase;

/**
 * Tests that logging of PreparedStatement works correctly for logging the
 * parameters set on this prepared statement.
 */
public class LogPreparedStatementTest extends TestCase {

    /**
     * Statement under test.
     */
    private PreparedStatement statement; //NOPMD

    /**
     * Number of parameters in statement created.
     */
    private static final int NO_PARAMS = 2;

    /**
     * Test log file name.
     */
    private static final String TEST_LOG_FILE = "/tmp/test.log";

    /**
     * Setup test of logger class by overriding some of the default properties.
     * @throws Exception if there is a problem setting up test
     */
    protected final void setUp() throws Exception {
        statement = ListenerHelper.getMockPreparedStatement(NO_PARAMS);
        super.setUp();
        System.setProperty(JDBCListenerProperties.LISTENERS_KEY,
                JDBCLogger.class.getName());
        System.setProperty(JDBCLoggerProperties.LOG_PATH_KEY,
                new File(TEST_LOG_FILE).getParent());
        System.setProperty(JDBCLoggerProperties.LOG_FILE_KEY,
                new File(TEST_LOG_FILE).getName());
        JDBCCallListener.init();
    }

    /**
     * Tidy up after test by deleted generated log file.
     * @throws Exception if there is a problem tidying up test
     */
    protected final void tearDown() throws Exception {
        super.tearDown();
        (new File(TEST_LOG_FILE)).delete();
        JDBCLoggerProperties.clearProperties();
        JDBCCallListener.resetInit();
    }

    /**
     * Tests parameters are correctly logged.
     */
    public final void testSetParameters() {
        try {
            statement.setInt(1, Integer.valueOf("100"));
            statement.setString(2, "Test");
            statement.execute();
            final BufferedReader testFile = new BufferedReader(
                    new FileReader(new File(TEST_LOG_FILE)));
            testFile.readLine();
            testFile.readLine();
            testFile.readLine();
            testFile.readLine();
            final String logEntry = testFile.readLine();
            assertTrue("Testing a message written with arguments output: "
                    + logEntry, logEntry.endsWith(":100,Test"));
        } catch (Exception e) {
            fail("Problem running setParameterTest");
        }
    }
}
