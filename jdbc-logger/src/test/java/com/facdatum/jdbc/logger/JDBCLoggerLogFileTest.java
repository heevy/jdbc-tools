/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.logger;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintStream;

import junit.framework.TestCase;

/**
 * Tests for the createLogFile method of JDBCLogger.
 */
public class JDBCLoggerLogFileTest extends TestCase {

    /**
     * Test log file path.
     */
    private static final String TEST_PATH = "/tmp";

    /**
     * Test log file name.
     */
    private static final String TEST_FILENAME = "test";

    /**
     * Test log file extension.
     */
    private static final String TEST_FILEEXT = "log";

    /**
     * Logger under test.
     */
    private JDBCLogger logger; //NOPMD

    /**
     * Set up test by creating logger under test.
     * @throws Exception if there is a problem creating logger.
     */
    protected final void setUp() throws Exception {
        logger = new JDBCLogger();
        final File testDir = new File(TEST_PATH);
        final String[] files = testDir.list(new ArchiveFileFilter());
        for (int i = 0; i < files.length; i++) {
            new File(TEST_PATH, files[i]).delete(); //NOPMD
        }
    }

    /**
     * Cleans up after test.
     * @throws Exception if there is a problem cleaning up test
     */
    protected final void tearDown() throws Exception {
        logger = null;
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.logger.JDBCLogger#createLogFile(
     * java.lang.String, java.lang.String)}.
     *
     * Tests when file does not exist.
     */
    public final void testCreateNewLogFile() {
        try {
            final String testFilename = TEST_FILENAME + "." + TEST_FILEEXT;
            final File testFile = logger.createLogFile(
                    TEST_PATH, testFilename);
            assertEquals("checking file created", testFilename,
                    testFile.getName());
            final File testDir = new File(TEST_PATH);
            final String[] files = testDir.list(new ArchiveFileFilter());
            assertEquals("should be no matches", 0, files.length);
        } catch (IOException e) {
            fail("Problem creating log file");
        }
    }

    /**
     * Test method for
     * {@link com.facdatum.jdbc.logger.JDBCLogger#createLogFile(
     * java.lang.String, java.lang.String)}.
     *
     * Tests when file does exist.
     */
    public final void testCreateExistingLogFile() {
        try {
            final String testFilename = TEST_FILENAME + "." + TEST_FILEEXT;
            new PrintStream(new File(TEST_PATH, testFilename)).println();
            final File testFile = logger.createLogFile(
                    TEST_PATH, testFilename);
            assertEquals("checking file created", testFilename,
                    testFile.getName());
            final File testDir = new File(TEST_PATH);
            final String[] files = testDir.list(new ArchiveFileFilter());
            assertEquals("should be matches", 1, files.length);
            for (int i = 0; i < files.length; i++) {
                new File(TEST_PATH, files[i]).delete(); //NOPMD
            }
            testFile.delete();
        } catch (IOException e) {
            fail("Problem creating log file");
        }
    }

    /**
     * Simple FilenameFilter that picks up files that match archive files
     * for the file under test.
     */
    private class ArchiveFileFilter implements FilenameFilter {

        /**
         * Checks filename to see if it match archive format for the file
         * under test.
         * @param file the file
         * @param filename the filename
         * @return true if the filename matches; false otherwise
         */
        public boolean accept(final File file, final String filename) {
            return filename.matches(TEST_FILENAME + ".*.." + TEST_FILEEXT);
        }
    }
}
