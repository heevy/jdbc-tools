/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import junit.framework.TestCase;

/**
 * Test class for setting logger properties through properties file.
 */
public class JDBCLoggerPropertiesFileTest extends TestCase {

    /**
     * Log file location that is set in properties.
     */
    private static final String LOG_PATH = "/tmp";

    /**
     * Log file name that is set in properties.
     */
    private static final String LOG_FILE = "test.log";

    /**
     * Holds the properties file reference.
     */
    private File testFile; //NOPMD

    /**
     * Properties that are tested.
     */
    private Properties testProps; //NOPMD
    /**
     * Sets up the tests by creating the properties file under test.
     * @throws Exception if there is problem setting up test
     */
    protected final void setUp() throws Exception {
        super.setUp();
        testProps = new Properties();
        testProps.setProperty(JDBCLoggerProperties.LOG_PATH_KEY, LOG_PATH);
        testProps.setProperty(JDBCLoggerProperties.LOG_FILE_KEY, LOG_FILE);
        clearResourceBundleCache();
    }

    /**
     * Tidies up test by removing properties file under test.
     * @throws Exception if there is a problem tidying up test
     */
    protected final void tearDown() throws Exception {
        super.tearDown();
        testFile.delete();
        JDBCLoggerProperties.clearProperties();
        clearResourceBundleCache();
    }

    /**
     * This will clear the ResourceBundle cache to allow new version to
     * be loaded.
     */
    @SuppressWarnings("unchecked")
    private void clearResourceBundleCache() {
        try {
            final Class < ResourceBundle > type = ResourceBundle.class;
            final Field cacheList = type.getDeclaredField("cacheList");
            cacheList.setAccessible(true);
            ((Map) cacheList.get(ResourceBundle.class)).clear();
        } catch (Exception e) {
            fail("Problem cleaning ResourceBundle");
        }
    }

    /**
     * Test the log path set in properties file.
     */
    public final void testPropertiesOnClasspath() {
        try {
            testFile = new File("config/"
                    + JDBCLoggerProperties.PROP_FILE_DEF + ".properties");
            testProps.store(new FileOutputStream(testFile), "Test properties");
            System.setProperty(JDBCLoggerProperties.PROP_FILE_KEY,
                    JDBCLoggerProperties.PROP_FILE_DEF);
            JDBCLoggerProperties.init();
            assertEquals("Testing loaded log path",
                    LOG_PATH, JDBCLoggerProperties.getLogPath());
            assertEquals("Testing loaded log filename",
                    LOG_FILE, JDBCLoggerProperties.getLogFile());
        } catch (IOException e) {
            fail("Failed to write properties file (" + e.toString() + ")");
        }
    }

    /**
     * Test the log path set in properties file.
     */
    public final void testPropertiesFilenameSet() {
        try {
            final String propsName = "jdbcTest";
            testFile = new File("config/" + propsName + ".properties");
            testProps.store(new FileOutputStream(testFile), "Test properties");
            System.setProperty(JDBCLoggerProperties.PROP_FILE_KEY,
                    propsName);
            JDBCLoggerProperties.init();
            assertEquals("Testing loaded log path",
                    LOG_PATH, JDBCLoggerProperties.getLogPath());
            assertEquals("Testing loaded log filename",
                    LOG_FILE, JDBCLoggerProperties.getLogFile());
        } catch (IOException e) {
            fail("Failed to write properties file (" + e.toString() + ")");
        }
    }

    /**
     * Test the log file name set in properties file.
     */
    public final void testPropertiesInLocation() {
        try {
            testFile = new File("/tmp/"
                    + JDBCLoggerProperties.PROP_FILE_DEF
                    + ".properties");
            testProps.store(new FileOutputStream(testFile), "Test properties");
            System.setProperty(JDBCLoggerProperties.PROP_PATH_KEY, "/tmp");
            JDBCLoggerProperties.init();
            assertEquals("Testing loaded log path",
                    LOG_PATH, JDBCLoggerProperties.getLogPath());
            assertEquals("Testing loaded log filename",
                    LOG_FILE, JDBCLoggerProperties.getLogFile());
        } catch (IOException e) {
            fail("Failed to write properties file (" + e.toString() + ")");
        }
    }
}
