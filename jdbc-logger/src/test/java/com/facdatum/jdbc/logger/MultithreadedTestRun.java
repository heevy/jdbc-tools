/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;

import junit.framework.TestCase;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCListenerProperties;
import com.facdatum.jdbc.listener.driver.DriverListener;

/**
 * Runs some simple steps against JDBC listener driver to generate some
 * log output. Assumes running against the default Oracle test HR database.
 */
public class MultithreadedTestRun extends TestCase {

    /**
     * Holds oracle connection url under test.
     */
    protected static final String ORA_CONNECT_URL =
        "jdbc:oracle:thin:@localhost:1521:xe";

    /**
     * Number of threads that will be started in test.
     */
    protected static final int THREADS = 4;

    /**
     * Number of records that should be written by each thread.
     */
    protected static final int CALLS = 230;

    /**
     * Wait period (ms) between check for thread completion.
     */
    protected static final int PAUSE_PERIOD = 500;

    /**
     * Test log file name.
     */
    private static final String TEST_LOG_FILE = "/tmp/test.log";

    /**
     * Prepares the test by setting up the logger properties and creating
     * the connection.
     * @throws Exception if there is a problem creating the connection
     */
    protected void setUp() throws Exception { //NOCS
        System.setProperty(JDBCListenerProperties.LISTENERS_KEY,
                JDBCLogger.class.getName());
        System.setProperty(JDBCListenerProperties.DRIVER_CLASS_KEY,
                "oracle.jdbc.OracleDriver");
        System.setProperty(JDBCLoggerProperties.LOG_PATH_KEY,
                new File(TEST_LOG_FILE).getParent());
        System.setProperty(JDBCLoggerProperties.LOG_FILE_KEY,
                new File(TEST_LOG_FILE).getName());
        JDBCCallListener.init();
        Class.forName(DriverListener.class.getName());
        DriverListener.registerDriver();
    }


    /**
     * Cleans up after test by removing generated log file and closing
     * connection.
     * @throws Exception if there is a problem closing connection
     */
    protected void tearDown() throws Exception { //NOCS
        (new File(TEST_LOG_FILE)).delete();
        JDBCListenerProperties.clearProperties();
        JDBCLoggerProperties.clearProperties();
        JDBCCallListener.resetInit();
        final Enumeration < Driver > drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            DriverManager.deregisterDriver(drivers.nextElement());
        }
    }

    /**
     * Executes the test steps.
     */
    public final void testRun() {
        final ThreadGroup group = new ThreadGroup("Test threads"); //NOPMD
        for (int i = 0; i < THREADS; i++) {
            final Runnable job = new Runnable() { //NOPMD
                public void run() {
                    executeSteps();
                }
            };
            new Thread(group, job).start(); //NOPMD
        }
        while (group.activeCount() > 0) {
            try {
                Thread.sleep(PAUSE_PERIOD);
            } catch (InterruptedException e) { //NOPMD NOCS
                //do nothing
            }
        }
        final int lines = readLog();
        assertEquals("Checking steps executed were logged", THREADS * CALLS,
                lines); // *2 for begin/end +1 for init
    }

    /**
     * Executes the jdbc steps that are to be performed.
     */
    private void executeSteps() {
        Connection conn = null;
        PreparedStatement sql = null;
        try {
            final Properties props = new Properties();
            props.setProperty("user", "hr");
            props.setProperty("password", "hr");
            conn = DriverManager.getConnection(ORA_CONNECT_URL, props);
            sql = conn.prepareStatement("select * from hr.employees");
            runQuery(sql);
        } catch (SQLException e) {
            fail("problem running test: " + e);
        } finally {
            if (sql != null) {
                try {
                    sql.close();
                } catch (SQLException e2) { //NOPMD NOCS
                    // do nothing
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e3) { //NOPMD NOCS
                    // do nothing
                }
            }
        }
    }

    /**
     * Runs the query and skips through the results.
     * @param sql the prepared statement of the query
     */
    private void runQuery(final PreparedStatement sql) {
        ResultSet results = null;
        try {
            if (sql.execute()) {
                results = sql.getResultSet();
                while (results.next()) { //NOPMD NOCS
                    // just skipping through results
                }
            }
        } catch (SQLException e) {
            fail("problem running test: " + e);
        } finally {
            if (results != null) {
                try {
                    results.close();
                } catch (SQLException e2) { //NOPMD NOCS
                    //do nothing
                }
            }
        }
    }

    /**
     * Reads the number of calls made to the MockListener.
     * @return the number of calls
     */
    private int readLog() {
        try {
            final BufferedReader testFile = new BufferedReader(
                    new FileReader(new File(TEST_LOG_FILE)));
            int lines = 0;
            String logEntry = testFile.readLine();
            while (logEntry != null) {
                lines++;
                logEntry = testFile.readLine();
            }
            return lines;
        } catch (IOException e) {
            fail("Error reading file");
            return 0;
        }
    }
}
