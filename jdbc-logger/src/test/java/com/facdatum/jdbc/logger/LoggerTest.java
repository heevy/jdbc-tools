/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import junit.framework.TestCase;

/**
 * Test of the standard logging methods of the JDBCLogger.
 */
public class LoggerTest extends TestCase {

    /**
     * Error message used in tests.
     */
    private static final String ERROR_READ_LOG = "Error reading log file";

    /**
     * End of file expected message.
     */
    private static final String END_OF_FILE = "End of file";

    /**
     * Test log file name.
     */
    private static final String TEST_LOG_FILE = "/tmp/test.log";

    /**
     * Logger under test.
     */
    private JDBCLogger logger; //NOPMD

    /**
     * Setup test of logger class by overriding some of the default properties.
     * @throws Exception if there is a problem setting up test
     */
    protected final void setUp() throws Exception {
        super.setUp();
        System.setProperty(JDBCLoggerProperties.LOG_PATH_KEY,
                new File(TEST_LOG_FILE).getParent());
        System.setProperty(JDBCLoggerProperties.LOG_FILE_KEY,
                new File(TEST_LOG_FILE).getName());
        logger = new JDBCLogger();
        logger.init();
    }

    /**
     * Tidy up after test by deleted generated log file.
     * @throws Exception if there is a problem tidying up test
     */
    protected final void tearDown() throws Exception {
        super.tearDown();
        (new File(TEST_LOG_FILE)).delete();
        JDBCLoggerProperties.clearProperties();
    }

    /**
     * Test method for {@link com.facdatum.jdbc.logger.JDBCLogger#
     * logDebug(java.lang.String)}.
     */
    public final void testLogDebug() {
        System.setProperty(JDBCLoggerProperties.LOG_JDBC_KEY, "debug");
        logger.init();
        final String debugMessage = "This is a debug message.";
        logger.logDebug(debugMessage);
        try {
            final BufferedReader testFile = new BufferedReader(
                    new FileReader(new File(TEST_LOG_FILE)));
            String logEntry = testFile.readLine();
            assertTrue("Testing properties", logEntry.contains(
                    JDBCLoggerProperties.class.getName()));
            logEntry = testFile.readLine();
            assertTrue("Testing debug", logEntry.contains(debugMessage));
            assertNull(END_OF_FILE, testFile.readLine());
        } catch (IOException e) {
            fail(ERROR_READ_LOG);
        }
    }

    /**
     * Test method for {@link com.facdatum.jdbc.logger.JDBCLogger#
     * logDebug(java.lang.String)}.
     */
    public final void testLogNoDebug() {
        System.setProperty(JDBCLoggerProperties.LOG_JDBC_KEY, "info");
        logger.init();
        final String debugMessage = "This is a debug message.";
        logger.logDebug(debugMessage);
        try {
            final BufferedReader testFile = new BufferedReader(
                    new FileReader(new File(TEST_LOG_FILE)));
            assertNull(END_OF_FILE, testFile.readLine());
        } catch (IOException e) {
            fail(ERROR_READ_LOG);
        }
    }

    /**
     * Test method for {@link com.facdatum.jdbc.logger.JDBCLogger#
     * logError(java.lang.String, java.lang.Throwable)}.
     */
    public final void testLogError() {
        System.setProperty(JDBCLoggerProperties.LOG_JDBC_KEY, "error");
        logger.init();
        final String errorMessage = "This is a error message.";
        logger.logError(errorMessage, null);
        try {
            final BufferedReader testFile = new BufferedReader(
                    new FileReader(new File(TEST_LOG_FILE)));
            final String logEntry = testFile.readLine();
            assertTrue("Testing error", logEntry.contains(errorMessage));
            assertNull(END_OF_FILE, testFile.readLine());
        } catch (IOException e) {
            fail(ERROR_READ_LOG);
        }
    }

    /**
     * Test method for {@link com.facdatum.jdbc.logger.JDBCLogger#
     * logError(java.lang.String, java.lang.Throwable)}.
     */
    public final void testLogNoError() {
        System.setProperty(JDBCLoggerProperties.LOG_JDBC_KEY, "info");
        logger.init();
        final String errorMessage = "This is a error message.";
        logger.logError(errorMessage, null);
        try {
            final BufferedReader testFile = new BufferedReader(
                    new FileReader(new File(TEST_LOG_FILE)));
            assertNull(END_OF_FILE, testFile.readLine());
        } catch (IOException e) {
            fail(ERROR_READ_LOG);
        }
    }

    /**
     * Test method for {@link com.facdatum.jdbc.logger.JDBCLogger#
     * logError(java.lang.String, java.lang.Throwable)}.
     */
    public final void testLogException() {
        System.setProperty(JDBCLoggerProperties.LOG_JDBC_KEY, "error");
        logger.init();
        final String errorMessage = "This is a error message.";
        final Exception eTest = new Exception("Test error");
        logger.logError(errorMessage, eTest);
        try {
            final BufferedReader testFile = new BufferedReader(
                    new FileReader(new File(TEST_LOG_FILE)));
            String logEntry = testFile.readLine();
            assertTrue("Testing error", logEntry.contains(errorMessage));
            logEntry = testFile.readLine();
            assertTrue("Testing exception message", logEntry.contains(
                    eTest.getLocalizedMessage()));
            final StackTraceElement[] stackTrace = eTest.getStackTrace();
            for (int i = 0; i < stackTrace.length; i++) {
                logEntry = testFile.readLine();
                assertTrue("Testing exception: " + logEntry, logEntry.contains(
                        stackTrace[i].toString()));
            }
            assertNull(END_OF_FILE, testFile.readLine());
        } catch (IOException e) {
            fail(ERROR_READ_LOG);
        }
    }
}
