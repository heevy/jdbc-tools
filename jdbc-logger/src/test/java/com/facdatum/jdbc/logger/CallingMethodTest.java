/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.logger;

import java.io.File;

import junit.framework.TestCase;

/**
 * Test of the getCallingMethod method of the Logger.
 */
public class CallingMethodTest extends TestCase {

    /**
     * Test log file name.
     */
    private static final String TEST_LOG_FILE = "/tmp/test.log";

    /**
     * Logger under test.
     */
    private JDBCLogger logger; //NOPMD

    /**
     * Setup test of logger class by overriding some of the default properties.
     * @throws Exception if there is a problem setting up test
     */
    protected final void setUp() throws Exception {
        super.setUp();
        JDBCLoggerProperties.clearProperties();
        System.setProperty(JDBCLoggerProperties.LOG_PATH_KEY,
                new File(TEST_LOG_FILE).getParent());
        System.setProperty(JDBCLoggerProperties.LOG_FILE_KEY,
                new File(TEST_LOG_FILE).getName());
        logger = new JDBCLogger();
        logger.init();
    }

    /**
     * Tidy up after test by deleted generated log file.
     * @throws Exception if there is a problem tidying up test
     */
    protected final void tearDown() throws Exception {
        super.tearDown();
        (new File(TEST_LOG_FILE)).delete();
        JDBCLoggerProperties.clearProperties();
        logger.init();
    }

    /**
     * Test when defaults are set that last calling method is logged.
     */
    public final void testDefaultCallingMethod() {
        logger.init();
        assertEquals("test delegate call is skipped",
                CallingMethodTest.class.getName() + ".delegateCallMethod"
                        + "(CallingMethodTest.java:105)",
                delegateCallMethod());
    }

    /**
     * Test when exclude is set that it is not logged.
     */
    public final void testExcludeCallingMethod() {
        System.setProperty(JDBCLoggerProperties.CALL_EXCLUDE_KEY,
                CallingMethodTest.class.getName()
                + ".delegateCallMethod");
        logger.init();
        final String callMethod = delegateCallMethod();
        assertEquals("test delegate call is skipped",
                CallingMethodTest.class.getName() + ".testExcludeCallingMethod"
                        + "(CallingMethodTest.java:82)", callMethod);
    }

    /**
     * Test when all are excluded that first call is logged.
     */
    public final void testExcludeAllCallingMethod() {
        System.setProperty(JDBCLoggerProperties.CALL_EXCLUDE_KEY, "*");
        logger.init();
        assertEquals("test delegate call is skipped",
                CallingMethodTest.class.getName() + ".delegateCallMethod"
                        + "(CallingMethodTest.java:105)",
                delegateCallMethod());
    }

    /**
     * Simulates a delegate call that could be skipped.
     * @return the call method
     */
    private String delegateCallMethod() {
        return logLayerCall();
    }

    /**
     * Simulates the log layer within logging framework.
     * @return the call method
     */
    private String logLayerCall() {
        return jdbcLayerCall();
    }

    /**
     * Simulates the jdbc layer within logging framework.
     * @return the call method
     */
    private String jdbcLayerCall() {
        final Exception e = new Exception(); //NOPMD
        final StackTraceElement[] stackTrace = e.getStackTrace();
        final StackTraceElement[] cleanStackTrace =
            new StackTraceElement[stackTrace.length - 1];
        System.arraycopy(stackTrace, 1, cleanStackTrace, 0,
                cleanStackTrace.length);
        return JDBCLogger.getCallingMethod(cleanStackTrace);
    }
}
