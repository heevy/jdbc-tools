/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.logger;

import junit.framework.TestCase;

/**
 * Test classes for default logging properties.
 */
public class JDBCLoggerPropertiesTest extends TestCase {

    /**
     * Setting up test by re-initializing properties.
     * @throws Exception if there is a problem setting up test
     */
    protected final void setUp() throws Exception {
        super.setUp();
        System.setProperty(JDBCLoggerProperties.LOG_PATH_KEY,
                JDBCLoggerProperties.LOG_PATH_DEF);
        System.setProperty(JDBCLoggerProperties.LOG_FILE_KEY,
                JDBCLoggerProperties.LOG_FILE_DEF);
        JDBCLoggerProperties.init();
    }

    /**
     * Tidies up test by cleaning up properties.
     * @throws Exception if there is a problem tidying up test
     */
    protected final void tearDown() throws Exception {
        super.tearDown();
        JDBCLoggerProperties.clearProperties();
    }

    /**
     * Test the default log path property.
     */
    public final void testGetLogPath() {
        assertEquals("Testing default log path",
                JDBCLoggerProperties.getLogPath(), ".");
    }

    /**
     * Test the default log filename property.
     */
    public final void testGetLogFile() {
        assertEquals("Testing default log filename",
                JDBCLoggerProperties.getLogFile(), "jdbc-logging.log");
   }
}
