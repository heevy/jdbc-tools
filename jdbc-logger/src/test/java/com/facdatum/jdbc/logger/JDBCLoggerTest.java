/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.Properties;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCListenerProperties;
import com.facdatum.jdbc.listener.driver.ListenerHelper;

import junit.framework.TestCase;

/**
 * Tests for the JDBCLogger class.
 */
public class JDBCLoggerTest extends TestCase {

    /**
     * Error message when problem logging begin.
     */
    private static final String ERR_IN_APP_BEGIN =
        "Error writing to includeAppLogBegin log";

    /**
     * Call message type string.
     */
    private static final String CALL_MSG_TYPE = "CALL";

    /**
     * Message for logging begin testing.
     */
    private static final String TESTING_BEGIN_MSG =
        "Testing a BEGIN message written";

    /**
     * Test log file name.
     */
    private static final String TEST_LOG_FILE = "/tmp/test.log";

    /**
     * Setup test of logger class by overriding some of the default properties.
     * @throws Exception if there is a problem setting up test
     */
    protected final void setUp() throws Exception {
        super.setUp();
        System.setProperty(JDBCListenerProperties.LISTENERS_KEY,
                JDBCLogger.class.getName());
        System.setProperty(JDBCLoggerProperties.LOG_PATH_KEY,
                new File(TEST_LOG_FILE).getParent());
        System.setProperty(JDBCLoggerProperties.LOG_FILE_KEY,
                new File(TEST_LOG_FILE).getName());
        System.setProperty(JDBCLoggerProperties.CALL_EXCLUDE_KEY,
                "com.facdatum.jdbc.logger.JDBCLoggerTest$MockDAO.*");
        JDBCCallListener.init();
    }

    /**
     * Tidy up after test by deleted generated log file.
     * @throws Exception if there is a problem tidying up test
     */
    protected final void tearDown() throws Exception {
        super.tearDown();
        (new File(TEST_LOG_FILE)).delete();
        JDBCLoggerProperties.clearProperties();
        JDBCCallListener.resetInit();
    }

    /**
     * Test for the get args method.
     */
    public final void testGetArgs() {
        final Object[] args = {1, "hello", 3.4, 'c'};
        final String argsList = "1,\"hello\",3.4,'c'";
        assertEquals("Testing get args", argsList, JDBCLogger.getArgs(
                "java.sql.PreparedStatement.execute", args));
    }

    /**
     * Test hiding a password in a URL string.
     */
    public final void testHideURLPassword() {
        assertEquals("Testing URL with password at end is hidden",
                "jdbc:oracle:thin:@localhost:1521:xe,userid=hr,password=****",
                JDBCLogger.hidePasswordInURL(
                        "jdbc:oracle:thin:@localhost:1521:xe,"
                        + "userid=hr,password=hr"));
        assertEquals("Testing URL with password in middle is hidden",
                "jdbc:oracle:thin:@localhost:1521:xe,password=****,userid=hr",
                JDBCLogger.hidePasswordInURL(
                        "jdbc:oracle:thin:@localhost:1521:xe"
                        + ",password=hr,userid=hr"));
    }

    /**
     * Test hiding a password in a URL string.
     */
    public final void testHidePropsPassword() {
        final Properties inProps = new Properties();
        inProps.setProperty("user", "hr");
        inProps.setProperty("password", "hr");
        final Properties outProps = JDBCLogger.hidePasswordInProps(inProps);
        assertEquals("Same number of properties", inProps.size(),
                outProps.size());
        assertEquals("Testing user not changed", inProps.getProperty("user"),
                outProps.getProperty("user"));
        assertEquals("Testing pass is hidden", "****",
                outProps.getProperty("password"));
    }

    /**
     * Test of logging call.
     */
    public final void testCallMethod() {
        try {
            final MockDAO dao = new MockDAO();
            dao.run();
            final BufferedReader testFile = new BufferedReader(
                    new FileReader(new File(TEST_LOG_FILE)));
            String logEntry = testFile.readLine();
            assertTrue(TESTING_BEGIN_MSG,
                    logEntry.contains(CALL_MSG_TYPE));
            assertTrue("Testing App method call message written (Mock skipped):"
                    + logEntry,
                   logEntry.contains(JDBCLoggerTest.class.getName()
                            + ".testCallMethod"));
            logEntry = testFile.readLine();
            assertTrue("Testing a EXIT message written",
                    logEntry.contains("EXIT"));
            assertTrue("Testing App method exit message written (Mock skipped)",
                    logEntry.contains(JDBCLoggerTest.class.getName()
                            + ".testCallMethod"));
        } catch (IOException e) {
            fail(ERR_IN_APP_BEGIN);
        }
    }

    /**
     * Test of logging when exception raised.
     */
    public final void testCallFailMethod() {
        try {
            final MockDAO dao = new MockDAO();
            dao.runFail();
            final BufferedReader testFile = new BufferedReader(
                    new FileReader(new File(TEST_LOG_FILE)));
            String logEntry = testFile.readLine();
            assertTrue(TESTING_BEGIN_MSG,
                    logEntry.contains(CALL_MSG_TYPE));
            assertTrue("Testing App method call message written (Mock skipped):"
                    + logEntry,
                    logEntry.contains(JDBCLoggerTest.class.getName()
                            + ".testCallFailMethod"));
            logEntry = testFile.readLine();
            assertTrue("Testing a FAIL message written",
                    logEntry.contains("FAIL"));
            assertTrue("Testing App method fail message exception written",
                    logEntry.contains("SQLException"));
            assertTrue("Testing App method fail message exception message",
                    logEntry.contains("Test Error"));
        } catch (IOException e) {
            fail(ERR_IN_APP_BEGIN);
        }
    }

    /**
     * MockDAO is a small class to run log statement from another
     * place than directly in test code.
     */
    private class MockDAO {
        /**
         * Run method that executes begin and end logging methods.
         */
        public final void run() {
            final Savepoint savepoint = ListenerHelper.getMockSavepoint(
                    false);
            try {
                savepoint.getSavepointId();
            } catch (SQLException e) {
                fail("error creating savepoint");
            }
        }

        /**
         * Run method that executes begin and exception logging methods.
         */
        public final void runFail() {
            final Savepoint savepoint = ListenerHelper.getMockSavepoint(true);
            try {
                savepoint.getSavepointId();
            } catch (SQLException e) { //NOPMD NOCS
                //do nothing
            }
        }
    }
}
