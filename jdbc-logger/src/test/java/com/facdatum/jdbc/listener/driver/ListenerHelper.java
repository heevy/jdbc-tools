/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.listener.driver;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.Savepoint;

import com.facdatum.mock.jdbc.MockCallableStatement;
import com.facdatum.mock.jdbc.MockPreparedStatement;
import com.facdatum.mock.jdbc.MockSavepoint;

/**
 * This class allows instances of mock jdbc classes to be obtained with
 * a listener wrapped around them.
 */
public final class ListenerHelper {

    /**
     * Hide default constructor to stop instance being created.
     */
    private ListenerHelper() {
        //do nothing
    }

    /**
     * Creates a SavepointListener that is wrapped around a MockSavepoint.
     * @param fail determines if mock should throw an exception or not
     * @return the mock savepoint
     */
    public static Savepoint getMockSavepoint(final boolean fail) {
        return new SavepointListener(new MockSavepoint(fail));
    }

    /**
     * Creates a PreparedStatementListener that is wrapped around a
     * MockPreparedStatement.
     * @param params number of parameters in mock statement
     * @return the mock prepared statement
     */
    public static PreparedStatement getMockPreparedStatement(final int params) {
        return new PreparedStatementListener(new MockPreparedStatement(params));
    }

    /**
     * Creates a CallableStatementListener that is wrapped around a
     * MockCallableStatement.
     * @param params number of parameters in mock statement
     * @return the mock prepared statement
     */
    public static CallableStatement getMockCallableStatement(final int params) {
        return new CallableStatementListener(new MockCallableStatement(params));
    }
}
