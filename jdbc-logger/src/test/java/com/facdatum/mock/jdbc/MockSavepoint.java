/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.jdbc;

import java.sql.SQLException;
import java.sql.Savepoint;

import com.facdatum.jdbc.listener.JDBCDelegator;

/**
 * Mock of the Savepoint class that checks each call to ensure that it
 * has been called from delegating listener class.
 */
public class MockSavepoint implements JDBCDelegator, Savepoint {

    /**
     * Is set then calls to JDBC methods will throw exceptions.
     */
    private boolean callFail = false; //NOPMD

    /**
     * Default constructor.
     */
    public MockSavepoint() {
        this.callFail = false;
    }

    /**
     * Constructor that enables client call to decide when calls should
     * fail or succeed.
     * @param fail if true then exceptions will be thrown from calls.
     */
    public MockSavepoint(final boolean fail) {
        this.callFail = fail;
    }

    /**
     * Implemented method to enable testing.
     * @return the delegate class name
     */
    public final String getDelegateClassName() {
        return this.getClass().getName();
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Savepoint#getSavepointId()
     */
    public final int getSavepointId() throws SQLException {
        if (callFail) {
            throw new SQLException("Test Error");
        }
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Savepoint#getSavepointName()
     */
    public final String getSavepointName() throws SQLException {
        if (callFail) {
            throw new SQLException("Test Error");
        }
        return null;
    }
}
