/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;

/**
 * Mock of the Statement class that checks each call to ensure that it
 * has been called from delegating listener class.
 */
public class MockStatement implements Statement {

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#addBatch(java.lang.String)
     */
    public final void addBatch(final String arg0) throws SQLException {
        //do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#cancel()
     */
    public final void cancel() throws SQLException {
        //do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#clearBatch()
     */
    public final void clearBatch() throws SQLException {
        //do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#clearWarnings()
     */
    public final void clearWarnings() throws SQLException {
        //do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#close()
     */
    public final void close() throws SQLException {
        //do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#execute(java.lang.String)
     */
    public final boolean execute(final String arg0) throws SQLException {
        //do nothing
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#execute(java.lang.String, int)
     */
    public final boolean execute(final String arg0, final int arg1)
            throws SQLException {
        //do nothing
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#execute(java.lang.String, int[])
     */
    public final boolean execute(final String arg0, final int[] arg1)
            throws SQLException {
        //do nothing
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#execute(java.lang.String, java.lang.String[])
     */
    public final boolean execute(final String arg0, final String[] arg1)
            throws SQLException {
        //do nothing
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#executeBatch()
     */
    public final int[] executeBatch() throws SQLException {
        //do nothing
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#executeQuery(java.lang.String)
     */
    public final ResultSet executeQuery(final String arg0) throws SQLException {
        //do nothing
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#executeUpdate(java.lang.String)
     */
    public final int executeUpdate(final String arg0) throws SQLException {
        //do nothing
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#executeUpdate(java.lang.String, int)
     */
    public final int executeUpdate(final String arg0, final int arg1)
            throws SQLException {
        //do nothing
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#executeUpdate(java.lang.String, int[])
     */
    public final int executeUpdate(final String arg0, final int[] arg1)
            throws SQLException {
        //do nothing
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#executeUpdate(java.lang.String,
     *      java.lang.String[])
     */
    public final int executeUpdate(final String arg0, final String[] arg1)
            throws SQLException {
        //do nothing
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getConnection()
     */
    public final Connection getConnection() throws SQLException {
        //do nothing
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getFetchDirection()
     */
    public final int getFetchDirection() throws SQLException {
        //do nothing
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getFetchSize()
     */
    public final int getFetchSize() throws SQLException {
        //do nothing
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getGeneratedKeys()
     */
    public final ResultSet getGeneratedKeys() throws SQLException {
        //do nothing
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getMaxFieldSize()
     */
    public final int getMaxFieldSize() throws SQLException {
        //do nothing
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getMaxRows()
     */
    public final int getMaxRows() throws SQLException {
        //do nothing
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getMoreResults()
     */
    public final boolean getMoreResults() throws SQLException { //NOPMD
        //do nothing
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getMoreResults(int)
     */
    public final boolean getMoreResults(final int arg0) throws SQLException {
        //do nothing
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getQueryTimeout()
     */
    public final int getQueryTimeout() throws SQLException {
        //do nothing
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getResultSet()
     */
    public final ResultSet getResultSet() throws SQLException {
        //do nothing
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getResultSetConcurrency()
     */
    public final int getResultSetConcurrency() throws SQLException {
        //do nothing
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getResultSetHoldability()
     */
    public final int getResultSetHoldability() throws SQLException {
        //do nothing
        return 0;
    }

    @Override
    public boolean isClosed() throws SQLException {
        return false;
    }

    @Override
    public void setPoolable(boolean poolable) throws SQLException {

    }

    @Override
    public boolean isPoolable() throws SQLException {
        return false;
    }

    @Override
    public void closeOnCompletion() throws SQLException {

    }

    @Override
    public boolean isCloseOnCompletion() throws SQLException {
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getResultSetType()
     */
    public final int getResultSetType() throws SQLException {
        //do nothing
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getUpdateCount()
     */
    public final int getUpdateCount() throws SQLException {
        //do nothing
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#getWarnings()
     */
    public final SQLWarning getWarnings() throws SQLException {
        //do nothing
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#setCursorName(java.lang.String)
     */
    public final void setCursorName(final String arg0) throws SQLException {
        //do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#setEscapeProcessing(boolean)
     */
    public final void setEscapeProcessing(final boolean arg0)
            throws SQLException {
        //do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#setFetchDirection(int)
     */
    public final void setFetchDirection(final int arg0) throws SQLException {
        //do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#setFetchSize(int)
     */
    public final void setFetchSize(final int arg0) throws SQLException {
        //do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#setMaxFieldSize(int)
     */
    public final void setMaxFieldSize(final int arg0) throws SQLException {
        //do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#setMaxRows(int)
     */
    public final void setMaxRows(final int arg0) throws SQLException {
        //do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.Statement#setQueryTimeout(int)
     */
    public final void setQueryTimeout(final int arg0) throws SQLException {
        //do nothing
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }
}
