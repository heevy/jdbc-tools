/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.mock.jdbc;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Mock of the PreparedStatement class that checks each call to ensure that it
 * has been called from delegating listener class.
 */
public class MockPreparedStatement extends MockStatement implements
        PreparedStatement {

    /**
     * Holds number of parameters in mock prepared statement SQL.
     */
    protected final int params; //NOPMD NOCS

    /**
     * Creates a mock prepared statement with a defined number of parameters.
     * @param thisParams the number of parameters in statement
     */
    public MockPreparedStatement(final int thisParams) {
        this.params = thisParams;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#getParameterMetaData()
     */
    public final ParameterMetaData getParameterMetaData() throws SQLException {
        return new MockParameterMetaData(params);
    }

    @Override
    public void setRowId(int parameterIndex, RowId x) throws SQLException {

    }

    @Override
    public void setNString(int parameterIndex, String value) throws SQLException {

    }

    @Override
    public void setNCharacterStream(int parameterIndex, Reader value, long length) throws SQLException {

    }

    @Override
    public void setNClob(int parameterIndex, NClob value) throws SQLException {

    }

    @Override
    public void setClob(int parameterIndex, Reader reader, long length) throws SQLException {

    }

    @Override
    public void setBlob(int parameterIndex, InputStream inputStream, long length) throws SQLException {

    }

    @Override
    public void setNClob(int parameterIndex, Reader reader, long length) throws SQLException {

    }

    @Override
    public void setSQLXML(int parameterIndex, SQLXML xmlObject) throws SQLException {

    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#addBatch()
     */
    public final void addBatch() throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#clearParameters()
     */
    public final void clearParameters() throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#execute()
     */
    public final boolean execute() throws SQLException {
        // do nothing
        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#executeQuery()
     */
    public final ResultSet executeQuery() throws SQLException {
        // do nothing
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#executeUpdate()
     */
    public final int executeUpdate() throws SQLException {
        // do nothing
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#getMetaData()
     */
    public final ResultSetMetaData getMetaData() throws SQLException {
        // do nothing
        return null;
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setArray(int, java.sql.Array)
     */
    public final void setArray(final int arg0, final Array arg1)
            throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setAsciiStream(int, java.io.InputStream,
     *      int)
     */
    public final void setAsciiStream(final int arg0, final InputStream arg1,
            final int arg2) throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setBigDecimal(int, java.math.BigDecimal)
     */
    public final void setBigDecimal(final int arg0, final BigDecimal arg1)
            throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setBinaryStream(int, java.io.InputStream,
     *      int)
     */
    public final void setBinaryStream(final int arg0, final InputStream arg1,
            final int arg2) throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setBlob(int, java.sql.Blob)
     */
    public final void setBlob(final int arg0, final Blob arg1)
            throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setBoolean(int, boolean)
     */
    public final void setBoolean(final int arg0, final boolean arg1)
            throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setByte(int, byte)
     */
    public final void setByte(final int arg0, final byte arg1)
            throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setBytes(int, byte[])
     */
    public final void setBytes(final int arg0, final byte[] arg1)
            throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setCharacterStream(int, java.io.Reader,
     *      int)
     */
    public final void setCharacterStream(final int arg0, final Reader arg1,
            final int arg2) throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setClob(int, java.sql.Clob)
     */
    public final void setClob(final int arg0, final Clob arg1)
            throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setDate(int, java.sql.Date)
     */
    public final void setDate(final int arg0, final Date arg1)
            throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setDate(int, java.sql.Date,
     *      java.util.Calendar)
     */
    public final void setDate(final int arg0, final Date arg1,
            final Calendar arg2)
            throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setDouble(int, double)
     */
    public final void setDouble(final int arg0, final double arg1)
            throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setFloat(int, float)
     */
    public final void setFloat(final int arg0, final float arg1)
            throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setInt(int, int)
     */
    public final void setInt(final int arg0, final int arg1)
            throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setLong(int, long)
     */
    public final void setLong(final int arg0, final long arg1)
            throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setNull(int, int)
     */
    public final void setNull(final int arg0, final int arg1)
            throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setNull(int, int, java.lang.String)
     */
    public final void setNull(final int arg0, final int arg1,
            final String arg2) throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setObject(int, java.lang.Object)
     */
    public final void setObject(final int arg0, final Object arg1)
            throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setObject(int, java.lang.Object, int)
     */
    public final void setObject(final int arg0, final Object arg1,
            final int arg2) throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setObject(int, java.lang.Object, int,
     *      int)
     */
    public final void setObject(final int arg0, final Object arg1,
            final int arg2, final int arg3) throws SQLException {
        // do nothing
    }

    @Override
    public void setAsciiStream(int parameterIndex, InputStream x, long length) throws SQLException {

    }

    @Override
    public void setBinaryStream(int parameterIndex, InputStream x, long length) throws SQLException {

    }

    @Override
    public void setCharacterStream(int parameterIndex, Reader reader, long length) throws SQLException {

    }

    @Override
    public void setAsciiStream(int parameterIndex, InputStream x) throws SQLException {

    }

    @Override
    public void setBinaryStream(int parameterIndex, InputStream x) throws SQLException {

    }

    @Override
    public void setCharacterStream(int parameterIndex, Reader reader) throws SQLException {

    }

    @Override
    public void setNCharacterStream(int parameterIndex, Reader value) throws SQLException {

    }

    @Override
    public void setClob(int parameterIndex, Reader reader) throws SQLException {

    }

    @Override
    public void setBlob(int parameterIndex, InputStream inputStream) throws SQLException {

    }

    @Override
    public void setNClob(int parameterIndex, Reader reader) throws SQLException {

    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setRef(int, java.sql.Ref)
     */
    public final void setRef(final int arg0, final Ref arg1)
            throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setShort(int, short)
     */
    public final void setShort(final int arg0, final short arg1)
            throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setString(int, java.lang.String)
     */
    public final void setString(final int arg0, final String arg1)
            throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setTime(int, java.sql.Time)
     */
    public final void setTime(final int arg0, final Time arg1)
            throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setTime(int, java.sql.Time,
     *      java.util.Calendar)
     */
    public final void setTime(final int arg0, final Time arg1,
            final Calendar arg2) throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setTimestamp(int, java.sql.Timestamp)
     */
    public final void setTimestamp(final int arg0, final Timestamp arg1)
            throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setTimestamp(int, java.sql.Timestamp,
     *      java.util.Calendar)
     */
    public final void setTimestamp(final int arg0, final Timestamp arg1,
            final Calendar arg2) throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setURL(int, java.net.URL)
     */
    public final void setURL(final int arg0, final URL arg1)
            throws SQLException {
        // do nothing
    }

    /**
     * {@inheritDoc}
     * @see java.sql.PreparedStatement#setUnicodeStream(int,
     *      java.io.InputStream, int)
     */
    public final void setUnicodeStream(final int arg0, final InputStream arg1,
            final int arg2) throws SQLException {
        // do nothing
    }
}
