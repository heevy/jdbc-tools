/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.logger;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.File;
import java.io.PrintStream;
import java.io.IOException;

import com.facdatum.jdbc.listener.JDBCListener;
import com.facdatum.jdbc.listener.ListenerCallEvent;
import com.facdatum.jdbc.listener.ListenerExceptionEvent;
import com.facdatum.jdbc.listener.ListenerReturnEvent;

/**
 * JDBCLogger class provides the methods for logging to file for integration
 * with the DriverListener.
 */
public final class JDBCLogger implements JDBCListener {

    /**
     * Log time format.
     */
    public static final String TIME_FORMAT_STR =
            "EEE MMM d HH:mm:ss.SSS z yyyy";

    /**
     * Stack depth on calling method needed for logging.
     */
    private static final int STACK_CALL_DEPTH = 1;

    /**
     * Position of instance reference in split Object.toString() string.
     */
    private static final int OBJECT_REF_INDEX = 1;

    /**
     * Value used to indicate that no elapsed time should be printed.
     */
    private static final int NULL_ELAPSED = -1;

    /**
     * String in URL that identifies a password has been supplied.
     */
    private static final String PASSWORD_STRING = "password";

    /**
     * The string that will be used in place of URL.
     */
    private static final String HIDDEN_PASSWORD = "****";

    /**
     * Contains static reference to the log file stream that log messages
     * are written to.
     */
    private static PrintStream log;

    /**
     * Array of regular expression patterns that will be matched against
     * calling method to check if it should be reported.
     */
    private static Pattern[] callIncludes;

    /**
     * Array of regular expression patterns that will be matched against
     * calling method to check if it should not be reported.
     */
    private static Pattern[] callExcludes;

    /**
     * Holds the set of jdbc methods that set SQL parameters.
     */
    private final Set < String > setMethods = //NOPMD
        new HashSet < String > (); //NOCS

    /**
     * Holds the set of jdbc methods that execute SQL statements.
     */
    private final Set < String > executeMethods = //NOPMD
        new HashSet < String > (); //NOCS

    /**
     * Holds the parameters that have been set on the statement.
     */
    private final Map < Integer, String > savedParams = //NOPMD
        new HashMap < Integer, String > (); //NOCS

    /**
     * Configure the logger based on defined logging driver properties.
     */
    public void init() {
        JDBCLoggerProperties.init();
        try {
            setMethods.add("java.sql.PreparedStatement.setArray");
            setMethods.add("java.sql.PreparedStatement.setAsciiStream");
            setMethods.add("java.sql.PreparedStatement.setBigDecimal");
            setMethods.add("java.sql.PreparedStatement.setBinaryStream");
            setMethods.add("java.sql.PreparedStatement.setBlob");
            setMethods.add("java.sql.PreparedStatement.setBoolean");
            setMethods.add("java.sql.PreparedStatement.setByte");
            setMethods.add("java.sql.PreparedStatement.setBytes");
            setMethods.add("java.sql.PreparedStatement.setCharacterStream");
            setMethods.add("java.sql.PreparedStatement.setClob");
            setMethods.add("java.sql.PreparedStatement.setDate");
            setMethods.add("java.sql.PreparedStatement.setDouble");
            setMethods.add("java.sql.PreparedStatement.setFloat");
            setMethods.add("java.sql.PreparedStatement.setInt");
            setMethods.add("java.sql.PreparedStatement.setLong");
            setMethods.add("java.sql.PreparedStatement.setNull");
            setMethods.add("java.sql.PreparedStatement.setObject");
            setMethods.add("java.sql.PreparedStatement.setRef");
            setMethods.add("java.sql.PreparedStatement.setShort");
            setMethods.add("java.sql.PreparedStatement.setString");
            setMethods.add("java.sql.PreparedStatement.setTime");
            setMethods.add("java.sql.PreparedStatement.setTimestamp");
            setMethods.add("java.sql.PreparedStatement.setUnicodeStream");
            setMethods.add("java.sql.PreparedStatement.setURL");
            setMethods.add("java.sql.CallableStatement.setArray");
            setMethods.add("java.sql.CallableStatement.setAsciiStream");
            setMethods.add("java.sql.CallableStatement.setBigDecimal");
            setMethods.add("java.sql.CallableStatement.setBinaryStream");
            setMethods.add("java.sql.CallableStatement.setBlob");
            setMethods.add("java.sql.CallableStatement.setBoolean");
            setMethods.add("java.sql.CallableStatement.setByte");
            setMethods.add("java.sql.CallableStatement.setBytes");
            setMethods.add("java.sql.CallableStatement.setCharacterStream");
            setMethods.add("java.sql.CallableStatement.setClob");
            setMethods.add("java.sql.CallableStatement.setDate");
            setMethods.add("java.sql.CallableStatement.setDouble");
            setMethods.add("java.sql.CallableStatement.setFloat");
            setMethods.add("java.sql.CallableStatement.setInt");
            setMethods.add("java.sql.CallableStatement.setLong");
            setMethods.add("java.sql.CallableStatement.setNull");
            setMethods.add("java.sql.CallableStatement.setObject");
            setMethods.add("java.sql.CallableStatement.setRef");
            setMethods.add("java.sql.CallableStatement.setShort");
            setMethods.add("java.sql.CallableStatement.setString");
            setMethods.add("java.sql.CallableStatement.setTime");
            setMethods.add("java.sql.CallableStatement.setTimestamp");
            setMethods.add("java.sql.CallableStatement.setUnicodeStream");
            setMethods.add("java.sql.CallableStatement.setURL");

            executeMethods.add("java.sql.PreparedStatement.addBatch");
            executeMethods.add("java.sql.PreparedStatement.execute");
            executeMethods.add("java.sql.PreparedStatement.executeQuery");
            executeMethods.add("java.sql.PreparedStatement.executeUpdate");
            executeMethods.add("java.sql.CallableStatement.addBatch");
            executeMethods.add("java.sql.CallableStatement.execute");
            executeMethods.add("java.sql.CallableStatement.executeQuery");
            executeMethods.add("java.sql.CallableStatement.executeUpdate");

            final File logFile = createLogFile(
                    JDBCLoggerProperties.getLogPath(),
                    JDBCLoggerProperties.getLogFile());
            log = new PrintStream(logFile);
            callIncludes = getPatterns(
                    JDBCLoggerProperties.getCallInclude());
            callExcludes = getPatterns(
                    JDBCLoggerProperties.getCallExclude());
        } catch (IOException e) {
            log = System.out;
            log.println("Error accessing log file: "
                    + JDBCLoggerProperties.getLogFile());
            log.println("Writing messages to standard output stream");
        }
        writeProperties();
    }

    /**
     * No argument constructor to enable JDBCListener to create instances
     * of JDBClogger.
     */
    public JDBCLogger() {
        //nothing to do
    }

    /**
     * Outputs the properties as debug messages.
     */
    private void writeProperties() {
        final StringBuffer buf = new StringBuffer(100);
        buf.append(JDBCLoggerProperties.class.getName());
        buf.append(" {");
        buf.append(JDBCLoggerProperties.PROP_PATH_KEY);
        buf.append('=');
        buf.append(JDBCLoggerProperties.getPropPath());
        buf.append(", ");
        buf.append(JDBCLoggerProperties.PROP_FILE_KEY);
        buf.append('=');
        buf.append(JDBCLoggerProperties.getPropFile());
        buf.append(", ");
        buf.append(JDBCLoggerProperties.LOG_PATH_KEY);
        buf.append('=');
        buf.append(JDBCLoggerProperties.getLogPath());
        buf.append(", ");
        buf.append(JDBCLoggerProperties.LOG_FILE_KEY);
        buf.append('=');
        buf.append(JDBCLoggerProperties.getLogFile());
        buf.append(", ");
        buf.append(JDBCLoggerProperties.LOG_JDBC_KEY);
        buf.append('=');
        buf.append(JDBCLoggerProperties.getLogJDBC());
        buf.append(", ");
        buf.append(JDBCLoggerProperties.CALL_INCLUDE_KEY);
        buf.append('=');
        buf.append(JDBCLoggerProperties.getCallInclude());
        buf.append(", ");
        buf.append(JDBCLoggerProperties.CALL_EXCLUDE_KEY);
        buf.append('=');
        buf.append(JDBCLoggerProperties.getCallExclude());
        buf.append('}');
        this.logDebug(buf.toString());
    }

    /**
     * Creates a new log file. If one already exists then this is renamed.
     * @param path the path to new log file
     * @param filename the name of new log file
     * @return the log file
     * @throws IOException if there is a problem creating new log file
     */
    protected File createLogFile(final String path,
            final String filename) throws IOException {
        File logFile = new File(path, filename);
        if (logFile.exists()) {
            final SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyyMMddHHmmssSSS", Locale.getDefault());
            final StringBuffer newFilename = new StringBuffer();
            final int extPos = filename.lastIndexOf('.');
            if (extPos >= 0) {
                newFilename.append(filename.substring(0, extPos));
                newFilename.append('.');
                newFilename.append(dateFormat.format(new Date()));
                newFilename.append(filename.substring(extPos,
                        filename.length()));
            } else {
                newFilename.append(filename);
                newFilename.append('.');
                newFilename.append(dateFormat.format(new Date()));
            }
            final File archiveFile = new File(path, newFilename.toString());
             if (!logFile.renameTo(archiveFile)) {
                 throw new IOException("Unable to archive existing log file");
             }
            logFile = new File(path, filename);
        }
        return logFile;
    }

    /**
     * Logs the beginning of the execution of a method. Returns the logged
     * time so this can be passed into the log exit call.
     * @param event the record of the JDBC call
     */
    public void acceptBegin(final ListenerCallEvent event) {
        if (!JDBCLoggerProperties.isLoggingJDBC()) {
            return;
        }
        final long time = event.getTime();
        final String jdbcMethod = event.getJDBCMethod();
        final String jdbcIntMethod = event.getJDBCInterfaceMethod();
        final String argString = getArgs(jdbcIntMethod, event.getArgs());
        final String callingMethod = getCallingMethod(event.getStackTrace());
        final String instanceRef = getInstanceRef(event.getObject());
        doLog(time, NULL_ELAPSED, callingMethod, MsgTypes.CALL,  instanceRef
                + "@" + jdbcIntMethod + "/" + jdbcMethod
                + "(" + argString + ")" + addSQLParameters(
                        event.getObject(), event.getJDBCInterfaceMethod(),
                        event.getArgs()));
        checkCallSetsParameters(event.getObject(),
                event.getJDBCInterfaceMethod(), event.getArgs());
    }

    /**
     * Checks whether the call is a call to one of the setParam methods
     * on either an instance of a PreparedStatement or a CallableStatement.
     * If so then the parameter set is recorded.
     * @param instance the instance that call was made on
     * @param method the method name that was called on instance
     * @param args the arguments to the call.
     */
    private void checkCallSetsParameters(final Object instance,
            final String method, final Object[] args) {
        if ((instance instanceof PreparedStatement
                    || instance instanceof CallableStatement)
                && setMethods.contains(method)
                && args.length >= 2
                && args[0] instanceof Integer) {
            savedParams.put((Integer) args[0], args[1].toString());
        }
    }

    /**
     * Checks whether a SQL statement is being executed and if it is
     * then the sql string is output with parameters filled in.
     * @param instance the instance that call was made on
     * @param method the method name that was called on instance
     * @param args the arguments to the call.
     * @return the SQL string with set parameters filled in or empty string
     * if not an appropriate JDBC call
     */
    private String addSQLParameters(final Object instance,
            final String method, final Object[] args) {
        if ((instance instanceof PreparedStatement
                    || instance instanceof CallableStatement)
                && executeMethods.contains(method)
                && args.length == 0) {
            final StringBuffer result = new StringBuffer();
            final int noParams = savedParams.size();
            for (int i = 0; i < noParams; i++) {
                if (i == 0) {
                    result.append(':');
                } else {
                    result.append(',');
                }
                final String param =
                        savedParams.get(Integer.valueOf(i + 1));
                if (result == null) {
                    result.append("");
                } else {
                    result.append(param);
                }
            }
            return result.toString();
        } else {
            return "";
        }
    }

    /**
     * Logs the end of the execution of a method. Takes beginning time as
     * parameter to calculation duration of method call.
     * @param event the record of the JDBC return
     */
    public void acceptEnd(final ListenerReturnEvent event) {
        if (!JDBCLoggerProperties.isLoggingJDBC()) {
            return;
        }
        final StringBuffer resultString = new StringBuffer("");
        if (event.getResult() != null) {
            resultString.append(logObject(event.getResult()));
        }
        final String jdbcMethod = event.getJDBCMethod();
        final String jdbcIntMethod = event.getJDBCInterfaceMethod();
        final String callingMethod = getCallingMethod(event.getStackTrace());
        final String instanceRef = getInstanceRef(event.getObject());
        doLog(event.getTime(), event.getDuration(),
                callingMethod, MsgTypes.EXIT, instanceRef + "@"
                + jdbcIntMethod + "/" + jdbcMethod + ":"
                + resultString.toString());
    }

    /**
     * Exceptions are not currently logged by the logging framework.
     * @param event the record of the JDBC exception
     */
    public void acceptException(final ListenerExceptionEvent event) {
        if (!JDBCLoggerProperties.isLoggingJDBC()) {
            return;
        }
        final StringBuffer exceptionString = new StringBuffer("");
        if (event.getException() != null) {
            exceptionString.append(logObject(
                    event.getException().toString()));
//            final StackTraceElement[] stackTrace =
//                    event.getException().getStackTrace();
//            for (int i = 0; i < stackTrace.length; i++) {
//                exceptionString.append('^');
//                exceptionString.append(stackTrace[i].toString());
//            }
        }
        final String jdbcMethod = event.getJDBCMethod();
        final String jdbcIntMethod = event.getJDBCInterfaceMethod();
        final String callingMethod = getCallingMethod(event.getStackTrace());
        final String instanceRef = getInstanceRef(event.getObject());
        doLog(event.getTime(), event.getDuration(),
                callingMethod, MsgTypes.FAIL, instanceRef + "@"
                + jdbcIntMethod + "/" + jdbcMethod + ":"
                + exceptionString.toString());
    }

    /**
     * Logs the debug message.
     * @param message the debug message
     */
    public void logDebug(final String message) {
        if (!JDBCLoggerProperties.isLoggingDebug()) {
            return;
        }
        final long time = System.currentTimeMillis();
        doLog(time, NULL_ELAPSED, null, MsgTypes.DEBUG, message);
    }

    /**
     * Logs the message as an error message and writes out the Exception
     * stack trace. If exception is null then just message is written.
     * @param message the error message
     * @param throwable the exception that caused the error
     */
    public void logError(final String message, final Throwable throwable) {
        if (!JDBCLoggerProperties.isLoggingErrors()) {
            return;
        }
        final long time = System.currentTimeMillis();
        doLog(time, NULL_ELAPSED, null, MsgTypes.ERROR, message);
        if (throwable != null) {
            doLog(time, NULL_ELAPSED, null, MsgTypes.ERROR,
                    throwable.getLocalizedMessage());
            final StackTraceElement[] stackTrace = throwable.getStackTrace();
            for (int i = 0; i < stackTrace.length; i++) {
                doLog(time, NULL_ELAPSED, null, MsgTypes.ERROR,
                        stackTrace[i].toString());
            }
        }
    }

    /**
     * Generic logging method for writing time, elapsed time (optional), method
     * that called the logger, message type (CALL, EXIT, ERROR) and message.
     * @param time time to log
     * @param elapsed elapsed time to log
     * @param callingMethod calling method to log
     * @param messageType type of message to log
     * @param msg message to log
     */
    public void doLog(final long time, final long elapsed,
            final String callingMethod, final String messageType,
            final String msg) {
        if (log == null) {
            init();
        }
        final StringBuffer elapsedString = new StringBuffer("");
        if (elapsed != NULL_ELAPSED) {
            elapsedString.append(elapsed);
        }
        final SimpleDateFormat dateFormat =
                new SimpleDateFormat(TIME_FORMAT_STR,
                        Locale.getDefault());

        final String msgLine = dateFormat.format((Date) new Date(time))
                + "|" + elapsedString.toString() + "|" + callingMethod + "|"
                + messageType + "|" + msg;
            log.println(msgLine);
    }

    /**
     * Returns the instance reference from Object.toString().
     * @param instance the instance to get the reference from
     * @return the instance reference
     */
    protected static String getInstanceRef(final Object instance) {
        final String[] instanceDetails = instance.toString().split("@");
        return instanceDetails[OBJECT_REF_INDEX];
    }

    /**
     * Returns the name of the method that called into the JDBC Layer taking
     * inclusion and exclusion specification into account.
     * @param stackTrace the stack trace of calls into the JDBC layer
     * @return method name that called into the JDBC Layer
     */
    protected static String getCallingMethod(
            final StackTraceElement[] stackTrace) {
        for (int i = STACK_CALL_DEPTH; i < stackTrace.length; i++) {
            final StringBuffer candidateCall = new StringBuffer(); //NOPMD
            candidateCall.append(stackTrace[i].getClassName());
            candidateCall.append('.');
            candidateCall.append(stackTrace[i].getMethodName());
            if (matches(callIncludes, candidateCall.toString())
                    && !matches(callExcludes, candidateCall.toString())) {
                candidateCall.append(getCallDetails(stackTrace[i]));
                return candidateCall.toString();
            }
        }
        return stackTrace[STACK_CALL_DEPTH].getClassName()
                + "." + stackTrace[STACK_CALL_DEPTH].getMethodName()
                + getCallDetails(stackTrace[STACK_CALL_DEPTH]);
    }

    /**
     * Finds out the source file and line number of call and turns into string.
     * @param stackTrace stack trace to extract information from
     * @return Source File and Line Number as ':' separated String
     */
    private static String getCallDetails(final StackTraceElement stackTrace) {
        final StringBuffer callDetails = new StringBuffer("");
        if (stackTrace.getFileName() != null) {
            callDetails.append('(');
            callDetails.append(stackTrace.getFileName());
            if (stackTrace.getLineNumber() > 0) {
                callDetails.append(':');
                callDetails.append(stackTrace.getLineNumber());
            }
            callDetails.append(')');
        }
        return callDetails.toString();
    }

    /**
     * Returns the arguments as a comma separated list with passwords
     * hidden.
     * @param jdbcMethod the jdbc method call used to check whether passwords
     * need to be hidden
     * @param args the arguments to be turned into list
     * @return comma separated list of arguments
     */
    protected static String getArgs(final String jdbcMethod,
            final Object[] args) {
        final StringBuffer argsList = new StringBuffer();
        final Object[] safeArgs = hidePassword(jdbcMethod, args);
        for (int i = 0; i < safeArgs.length; i++) {
            if (i > 0) {
                argsList.append(',');
            }
            argsList.append(logObject(safeArgs[i]));
        }
        return argsList.toString();
    }

    /**
     * Formats an object for logging depending on it's type. String objects
     * get surrounded by double quotes (") and character objects get
     * surrounded by single quotes('). Other object types get formatted
     * as-is.
     * @param obj the object to format
     * @return the formatted object string
     */
    protected static String logObject(final Object obj) {
        final StringBuffer objString = new StringBuffer();
        if (obj != null) { //NOPMD
            if (obj instanceof String) {
                objString.append('"');
                objString.append(obj.toString());
                objString.append('"');
            } else if (obj instanceof Character) {
                objString.append('\'');
                objString.append(obj.toString());
                objString.append('\'');
            } else {
                objString.append(obj.toString());
            }
        } else {
            objString.append("null");
        }
        return objString.toString();
    }

    /**
     * Converts a comma separated list of regular expressions into an array
     * of Pattern elements.
     * @param patternsList comma separated list of patterns
     * @return array of regular expression patterns
     */
    protected static Pattern[] getPatterns(final String patternsList) {
        final ArrayList < Pattern > patterns =
                new ArrayList < Pattern > (); //NOCS
        if (patternsList != null) {
            final StringTokenizer tokens =
                    new StringTokenizer(patternsList, ",");
            while (tokens.hasMoreElements()) {
                patterns.add(Pattern.compile(convertToRegExp(
                        tokens.nextToken().trim())));
            }
        }
        return patterns.toArray(new Pattern[0]); //NOPMD
    }

    /**
     * Converts a simple search string using standard * to match multiple
     * characters into a regular expression.
     * @param simpleExp the simple search expression
     * @return the regular expression equivalent of simple search expression
     */
    protected static String convertToRegExp(final String simpleExp) {
        return simpleExp.replace("$", "\\$").replace(".", "\\.").
                replace("*", ".*");
    }

    /**
     * Checks whether the input string matches any of the regular expressions
     * in the supplied patterns.
     * @param patterns the array of regular expressions to match
     * @param method the input method to match against
     * @return true if there is a match; false otherwise
     */
    protected static boolean matches(final Pattern[] patterns,
            final String method) {
        for (int i = 0; i < patterns.length; i++) {
            final Matcher matcher = patterns[i].matcher(method);
            if (matcher.matches()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks the jdbcMethod to see if it takes a password as an argument
     * and hides it if so, otherwise arguments are returned as is.
     * @param jdbcMethod the jdbc method that was called
     * @param args the input arguments
     * @return the arguments with any passwords hidden
     */
    protected static Object[] hidePassword(final String jdbcMethod,
            final Object[] args) {
        final Object[] safeArgs = new Object[args.length];
        if (("javax.sql.ConnectionPoolDataSource.getPooledConnection".
                equals(jdbcMethod)
                || "javax.sql.DataSource.getConnection".equals(jdbcMethod)
                || "javax.sql.XADataSource.getXAConnection".equals(jdbcMethod))
            && args.length == 2) {
            safeArgs[0] = args[0];
            safeArgs[1] = hidePasswordString((String) args[1]);
        } else if ("java.sql.Driver.acceptsURL".equals(jdbcMethod)) {
            safeArgs[0] = hidePasswordInURL((String) args[0]);
        } else if ("java.sql.Driver.connect".equals(jdbcMethod)) {
            safeArgs[0] = hidePasswordInURL((String) args[0]);
            safeArgs[1] = hidePasswordInProps((Properties) args[1]);
        } else {
            System.arraycopy(args, 0, safeArgs, 0, args.length);
        }
        return safeArgs;
    }

    /**
     * Simply returns the hidden password string.
     * @param passwordString the password string
     * @return the hidden password string
     */
    protected static String hidePasswordString(final String passwordString) {
        return HIDDEN_PASSWORD;
    }

    /**
     * Reads the given url string to see if it contains a password, if it
     * does then this is hidden.
     * @param urlString the url string to check
     * @return the converted string or the input string if no password to hide
     */
    protected static String hidePasswordInURL(final String urlString) {
        if (urlString == null) {
            return null;
        }
        final StringBuffer outputURL = new StringBuffer();
        String[] urlComponents = urlString.split(PASSWORD_STRING + "=", 0);
        outputURL.append(urlComponents[0]);
        if (urlComponents.length > 1) {
            outputURL.append(PASSWORD_STRING + "=");
            urlComponents = urlComponents[1].split(",", 0); //NOPMD
            outputURL.append(HIDDEN_PASSWORD);
            if (urlComponents.length > 1) {
                outputURL.append(',');
                outputURL.append(urlComponents[1]);
            }
        }
        return outputURL.toString();
    }

    /**
     * Reads the given url properties to see if they contain a password, if
     * they do then this is hidden.
     * @param urlProps the url properties to check
     * @return the converted properties or the input if no password to hide
     */
    protected static Properties hidePasswordInProps(final Properties urlProps) {
        if (urlProps == null) {
            return null;
        }
        final Properties outputProps = (Properties) urlProps.clone();
        if (outputProps.containsKey(PASSWORD_STRING)) {
            outputProps.setProperty(PASSWORD_STRING, "****");
        }
        return outputProps;
    }
}
