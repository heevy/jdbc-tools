/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.logger;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import com.facdatum.jdbc.listener.JDBCListenerProperties;

/**
 * Provides access to all the properties that can be used to control the
 * JDBCLogger. These can either be pass in as java properties at the
 * command line of loaded via the logging driver properties file
 * jdbc-logging.properties.
  */
public final class JDBCLoggerProperties {

    /**
     * The properties bundle that can be used to override the property file
     * name.
     */
    public static final String FILE_BUNDLE_REF =
            "com.facdatum.jdbc.config.JDBCPropertiesFileName";

    /**
     * Specifies the different log levels.
     */
    public final class LogLevel {

        /**
         * Log level info mask.
         */
        public static final int INFO = 1;

        /**
         * Log level debug mask.
         */
        public static final int DEBUG = 2;

        /**
         * Log level error mask.
         */
        public static final int ERROR = 4;

        /**
         * Private constructor to stop instances being created.
         */
        private LogLevel() {
            //do nothing
        }
    }

    /**
     * Contains the key used to pick up the logger property directory path.
     */
    public static final String PROP_PATH_KEY = "jdbc.logger.prop.path";

    /**
     * Contains the configured logger property directory path.
     */
    private static String propPath;

    /**
     * Contains the key used to pick up the logger property file name.
     */
    public static final String PROP_FILE_KEY = "jdbc.logger.prop.filename";

    /**
     * Contains the default logger property file name.
     */
    public static final String PROP_FILE_DEF =
            "jdbc-logger";

    /**
     * Contains the configured logger property file name.
     */
    private static String propFile;

    /**
     * Contains the key used to pick up whether logging should occur or not.
     */
    public static final String LOG_JDBC_KEY = "log.levels";

    /**
     * Contains the default JDBC logging status (info only).
     */
    public static final String LOG_JDBC_DEF = "info";

    /**
     * Contains the configured JDBC logging status.
     */
    private static int logJDBC;

    /**
     * Contains the string list of configured logging levels.
     */
    private static String logJDBCString;

    /**
     * Contains the key used to pick up the path to where the log file is
     * placed.
     */
    public static final String LOG_PATH_KEY = "log.path";

    /**
     * Contains the default path of the log file.
     */
    public static final String LOG_PATH_DEF = ".";

    /**
     * Contains the actual path used to place log file.
     */
    private static String logPath;

    /**
     * Contains the key used to pick up the filename of the log file.
     */
    public static final String LOG_FILE_KEY = "log.filename";

    /**
     * Contains the default name of the log file is used to write all log
     * messages from logger to.
     */
    public static final String LOG_FILE_DEF = "jdbc-logging.log";

    /**
     * Contains the actual filename of the log file.
     */
    private static String logFile;

    /**
     * Contains the key used to pick up call methods to report
     * in logging.
     */
    public static final String CALL_INCLUDE_KEY = "call.include";

    /**
     * Contains the default pattern to identify the call methods to report
     * in logging.
     */
    public static final String CALL_INCLUDE_DEF = "*";

    /**
     * Contains the configured pattern to identify the call methods to report
     * in logging.
     */
    private static String callInclude;

    /**
     * Contains the key used to pick up the call methods not to report
     * in logging.
     */
    public static final String CALL_EXCLUDE_KEY = "call.exclude";

    /**
     * Contains the default pattern to identify the call methods
     * not to report in logging.
     */
    public static final String CALL_EXCLUDE_DEF = "";

    /**
     * Contains the configured pattern to identify the call methods
     * not to report in logging.
     */
    private static String callExclude;

    static {
        JDBCLoggerProperties.init();
    }

    /**
     * Private default constructor to stops creation of instances.
     */
    private JDBCLoggerProperties() {
        super();
    }

    /**
     * Creates an instance of the logging properties by reading the
     * defined logging properties file.
     */
    public static void init() {
        propFile = System.getProperty(PROP_FILE_KEY,
                PROP_FILE_DEF);
        try {
            final Properties fileProperties = loadBundle(FILE_BUNDLE_REF);
            propFile = fileProperties.getProperty(
                    JDBCListenerProperties.SHARED_FILE_KEY, propFile);
        } catch (MissingResourceException e) { //NOPMD NOCS
            //use default
        }

        propPath = System.getProperty(PROP_PATH_KEY);
        Properties tempProperties = new Properties();
        try {
            if (propPath == null) {
                tempProperties = loadBundle(propFile);
            } else {
                tempProperties.load(new FileInputStream(new File(propPath,
                        propFile + ".properties")));
            }
        } catch (Exception e) { //NOPMD NOCS
            //defaults will be used
        }
        logPath = tempProperties.getProperty(LOG_PATH_KEY, LOG_PATH_DEF);
        logFile = tempProperties.getProperty(LOG_FILE_KEY, LOG_FILE_DEF);
        logJDBCString = tempProperties.getProperty(LOG_JDBC_KEY,
                LOG_JDBC_DEF);
        callInclude = tempProperties.getProperty(CALL_INCLUDE_KEY,
                CALL_INCLUDE_DEF);
        callExclude = tempProperties.getProperty(CALL_EXCLUDE_KEY,
                CALL_EXCLUDE_DEF);

        logPath = System.getProperty(LOG_PATH_KEY, logPath);
        logFile = System.getProperty(LOG_FILE_KEY, logFile);
        logJDBCString = System.getProperty(LOG_JDBC_KEY, logJDBCString); //NOPMD
        logJDBC = readLogLevel(logJDBCString);
        callInclude = System.getProperty(CALL_INCLUDE_KEY, callInclude);
        callExclude = System.getProperty(CALL_EXCLUDE_KEY, callExclude);
    }

    /**
     * Converts a list of log levels into their corresponding log level
     * mask.
     * @param levelList log levels as comma separated list
     * @return the logging level mask
     */
    protected static int readLogLevel(final String levelList) {
        final String[] levels = convertListToArray(levelList);
        int level = 0;
        for (int i = 0; i < levels.length; i++) {
            if (levels[i].equalsIgnoreCase("info")) {
                level += level | LogLevel.INFO;
            }
            if (levels[i].equalsIgnoreCase("debug")) {
                level = level | LogLevel.DEBUG;
            }
            if (levels[i].equalsIgnoreCase("error")) {
                level = level | LogLevel.ERROR;
            }
        }
        return level;
    }

    /**
     * Converts a list of items in a string into the equivalent String array.
     * @param listAsString the properties list as a string
     * @return the array
     */
    public static String[] convertListToArray(
            final String listAsString) {
        final ArrayList < String > items = new ArrayList < String > (); //NOCS
        final StringTokenizer tokens = new StringTokenizer(listAsString, ",",
                false);
        while (tokens.hasMoreElements()) {
            items.add(tokens.nextToken());
        }
        return items.toArray(new String[] {});
    }

    /**
     * Loads properties from a ResourceBundle.
     * @param bundleName name of bundle to load
     * @return properties in bundle
     */
    protected static Properties loadBundle(final String bundleName) {
        final ResourceBundle bundle = ResourceBundle.getBundle(bundleName);
        final Properties props = new Properties();
        final Enumeration < String > keys = bundle.getKeys();
        while (keys.hasMoreElements()) {
            final String key = (String) keys.nextElement();
            props.put(key, bundle.getString(key));
        }
        return props;
    }

    /**
     * Clears all the system properties related to the JDBCLogger to
     * their defaults.
     */
    public static void clearProperties() {
        System.clearProperty(PROP_PATH_KEY);
        System.clearProperty(PROP_FILE_KEY);
        System.clearProperty(LOG_PATH_KEY);
        System.clearProperty(LOG_FILE_KEY);
        System.clearProperty(LOG_JDBC_KEY);
        System.clearProperty(CALL_INCLUDE_KEY);
        System.clearProperty(CALL_EXCLUDE_KEY);
    }

    /**
     * Gets the configured properties file path.
     * @return the properties file path
     */
    public static String getPropPath() {
        return propPath;
    }

    /**
     * Gets the configured properties file name.
     * @return the properties file name
     */
    public static String getPropFile() {
        return propFile;
    }

    /**
     * Gets the configured path to the log file.
     * @return the path to log file
     */
    public static String getLogPath() {
        return logPath;
    }

    /**
     * Gets the configured log file name.
     * @return the log file name
     */
    public static String getLogFile() {
        return logFile;
    }

    /**
     * Gets the configured log levels as string.
     * @return the log levels as string
     */
    public static String getLogJDBC() {
        return logJDBCString;
    }

    /**
     * Gets the status of logging JDBC calls.
     * @return true if JDBC logging is on; false otherwise
     */
    public static boolean isLoggingJDBC() {
        return ((logJDBC & LogLevel.INFO) != 0);
    }


    /**
     * Gets the status of logging debug calls.
     * @return true if debug logging is on; false otherwise
     */
    public static boolean isLoggingDebug() {
        return ((logJDBC & LogLevel.DEBUG) != 0);
    }


    /**
     * Gets the status of logging error calls.
     * @return true if error logging is on; false otherwise
     */
    public static boolean isLoggingErrors() {
        return ((logJDBC & LogLevel.ERROR) != 0);
    }

    /**
     * Gets the configured include pattern for call methods.
     * @return the call method inclusion pattern
     */
    public static String getCallInclude() {
        return callInclude;
    }

    /**
     * Gets the configured exclude pattern for call methods.
     * @return the call method exclusion pattern
     */
    public static String getCallExclude() {
        return callExclude;
    }
}
