/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.planner;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.Properties;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCListenerProperties;
import com.facdatum.jdbc.listener.driver.DriverListener;
import com.facdatum.jdbc.logger.JDBCLogger;
import com.facdatum.jdbc.logger.JDBCLoggerProperties;

import junit.framework.TestCase;

/**
 * Tests for different statement types through jdbc-planner.
 */
public class StatementsTest extends TestCase {

    /**
     * Database under test.
     */
    private static final int TEST_DB =
        0; //Oracle
        //1; //SQL Server
        //2; //MySQL

    /**
     * Driver library path under test.
     */
    private static final String[] DRIVER_PATH = {
        "/usr/lib/oracle/xe/app/oracle/product/10.2.0/server/"
        + "jdbc/lib/ojdbc14.jar",
        "/home/iain/workspace/jdbc-planner/lib/sqljdbc.jar", //SQL Server
        "/home/iain/workspace/jdbc-planner/lib/"
        + "mysql-connector-java-5.1.6-bin.jar"}; //MySQL

    /**
     * Driver class under test.
     */
    private static final String[] DRIVER_CLASS = {
        "oracle.jdbc.OracleDriver",
        "com.microsoft.sqlserver.jdbc.SQLServerDriver", //SQL Server
        "com.mysql.jdbc.Driver"}; //MySQL

    /**
     * Connection URL for planning.
     */
    private static final String[] DIRECT_DRIVER_URL = {
        "jdbc:oracle:thin:@localhost:1521:xe",
        "jdbc:sqlserver://192.168.0.5:50679", //SQL Server
        "jdbc:mysql://192.168.0.5:3306/world"}; //MySQL

    /**
     * Connection URL under test.
     */
    private static final String[] DRIVER_URL = {
        "jdbc:listener:oracle:thin:@localhost:1521:xe",
        "jdbc:listener:sqlserver://192.168.0.5:50679", //SQL Server
        "jdbc:listener:mysql://192.168.0.5:3306/world"}; //MySQL

    /**
     * Connection user name under test.
     */
    private static final String[] DRIVER_USER = {
        "hr",
        "sa", //SQL Server
        "user"}; //MySQL

    /**
     * Connection password under test.
     */
    private static final String[] DRIVER_PASSWORD = {
        "hr",
        "sa", //SQL Server
        "user"}; //MySQL

    /**
     * SQL query that is tested.
     */
    private static final String[] TEST_SQL = {
        "select * from hr.employees",
        "select * from northwind..employees", //SQL Server
        "select * from world.Country"}; //MySQL

    /**
     * Parameterized SQL query that is tested.
     */
    private static final String[] PARAM_TEST_SQL = {
        "select * from hr.employees where first_name = ?",
        "select * from northwind..employees where LastName = ?", //SQL Server
        "select * from world.Country where code = ?"}; //MySQL

    /**
     * Parameterized SQL query argument values (where required for query plan
     * generation).
     */
    private static final String[][] ARGS_TEST_SQL = {
        {"King"},
        {"King"}, //SQLServer
        {"GBR"}}; //MySQL

    /**
     * Parameterized SQL query that is tested with other data types.
     */
    private static final String[] TYPES_TEST_SQL = {
        "select * from hr.employees where hire_date > ? and manager_id > ?",
        "select * from northwind..employees"
            + " where hiredate > ? and reportsto > ?", //SQL Server
        "select * from world.Country"
            + " where founded > ? and population > ?"}; //MySQL

    /**
     * Parameterized SQL query argument values with other data types.
     */
    private static final Object[][] ARGS_TYPES_SQL = {
        {new Date(System.currentTimeMillis()), 100},
        {new Date(System.currentTimeMillis()), 100}, //SQLServer
        {new Date(System.currentTimeMillis()), 100000}}; //MySQL

    /**
     * Test log file name.
     */
    private static final String TEST_LOG_FILE = "/tmp/test.log";

    /**
     * Holds the connection used by the test.
     */
    protected Connection conn = null; //NOPMD NOCS

    /**
     * Holds the number of jdbc calls that should have been logged.
     */
    protected int steps; //NOPMD NOCS

    /**
     * Prepares the test by setting up the logger properties and creating
     * the connection.
     * @throws Exception if there is a problem creating the connection
     */
    protected void setUp() throws Exception { //NOCS
        super.setUp();
        System.setProperty(JDBCListenerProperties.LISTENERS_KEY,
                JDBCLogger.class.getName() + ","
                + QueryPlanListener.class.getName());
        System.setProperty(JDBCListenerProperties.DRIVER_CLASS_KEY,
                DRIVER_CLASS[TEST_DB]);
        System.setProperty(JDBCPlannerProperties.DRIVER_PATH_KEY,
                DRIVER_PATH[TEST_DB]);
        System.setProperty(JDBCPlannerProperties.DRIVER_CLASS_KEY,
                DRIVER_CLASS[TEST_DB]);
        System.setProperty(JDBCPlannerProperties.DRIVER_URL_KEY,
                DIRECT_DRIVER_URL[TEST_DB]);
        System.setProperty(JDBCPlannerProperties.DRIVER_USER_KEY,
                DRIVER_USER[TEST_DB]);
        System.setProperty(JDBCPlannerProperties.DRIVER_PWD_KEY,
                DRIVER_PASSWORD[TEST_DB]);
        System.setProperty(JDBCLoggerProperties.LOG_PATH_KEY,
                new File(TEST_LOG_FILE).getParent());
        System.setProperty(JDBCLoggerProperties.LOG_FILE_KEY,
                new File(TEST_LOG_FILE).getName());
        JDBCCallListener.init();
        Class.forName(DriverListener.class.getName());
        DriverListener.registerDriver();
        final Properties props = new Properties();
        props.setProperty("user", DRIVER_USER[TEST_DB]);
        props.setProperty("password", DRIVER_PASSWORD[TEST_DB]);
        conn = DriverManager.getConnection(DRIVER_URL[TEST_DB], props);
        steps = 2;
    }


    /**
     * Cleans up after test by removing generated log file and closing
     * connection.
     * @throws Exception if there is a problem closing connection
     */
    protected void tearDown() throws Exception { //NOCS
        conn.close();
        super.tearDown();
        (new File(TEST_LOG_FILE)).delete();
        JDBCCallListener.resetInit();
        JDBCListenerProperties.clearProperties();
        JDBCLoggerProperties.clearProperties();
        JDBCPlannerProperties.clearProperties();
        final Enumeration < Driver > drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            DriverManager.deregisterDriver(drivers.nextElement());
        }
    }

    /**
     * Tests different statement types to check they are planned
     * correctly.
     */
    public final void testStatements() {
        executeStatement();
        executePreparedStatement();
        executeStatementDataTypes();
        final int lines = readLog();
        assertEquals("Checking steps executed were logged", steps,
                lines); // *2 for each statement +1 for each query plan
    }

    /**
     * Runs the direct execute statement.
     */
    protected final void executeStatement() {
        Statement sql = null;
        ResultSet results = null;
        try {
            sql = conn.createStatement();
            steps += 2;
            results = sql.executeQuery(TEST_SQL[TEST_DB]);
            steps += 2 + 1;
        } catch (SQLException e) {
            fail("problem running test: " + e);
        } finally {
            if (results != null) {
                try {
                    results.close();
                    steps += 2;
                } catch (SQLException e2) { //NOPMD NOCS
                    //do nothing
                }
            }
            if (sql != null) {
                try {
                    sql.close();
                    steps += 2;
                } catch (SQLException e2) { //NOPMD NOCS
                    // do nothing
                }
            }
        }
    }

    /**
     * Runs the prepare and execute statements.
     */
    protected final void executePreparedStatement() {
        PreparedStatement sql = null;
        ResultSet results = null;
        try {
            sql = conn.prepareStatement(PARAM_TEST_SQL[TEST_DB]);
            steps += 2;
            sql.setString(1, ARGS_TEST_SQL[TEST_DB][0]);
            steps += 2;
            results = sql.executeQuery();
            steps += 2 + 1;
        } catch (SQLException e) {
            fail("problem running test: " + e);
        } finally {
            if (results != null) {
                try {
                    results.close();
                    steps += 2;
                } catch (SQLException e2) { //NOPMD NOCS
                    //do nothing
                }
            }
            if (sql != null) {
                try {
                    sql.close();
                    steps += 2;
                } catch (SQLException e2) { //NOPMD NOCS
                    // do nothing
                }
            }
        }
    }

    /**
     * Checks statements using different data types.
     */
    protected final void executeStatementDataTypes() {
        PreparedStatement sql = null;
        ResultSet results = null;
        try {
            sql = conn.prepareStatement(TYPES_TEST_SQL[TEST_DB]);
            steps += 2;
            sql.setDate(1, (Date) ARGS_TYPES_SQL[TEST_DB][0]);
            steps += 2;
            sql.setInt(2, ((Integer) ARGS_TYPES_SQL[TEST_DB][1]).intValue());
            steps += 2;
            results = sql.executeQuery();
            steps += 2 + 1;
        } catch (SQLException e) {
            fail("problem running test: " + e);
        } finally {
            if (results != null) {
                try {
                    results.close();
                    steps += 2;
                } catch (SQLException e2) { //NOPMD NOCS
                    //do nothing
                }
            }
            if (sql != null) {
                try {
                    sql.close();
                    steps += 2;
                } catch (SQLException e2) { //NOPMD NOCS
                    // do nothing
                }
            }
        }
    }
    /**
     * Reads the number of calls made to the MockListener.
     * @return the number of calls
     */
    private int readLog() {
        try {
            final BufferedReader testFile = new BufferedReader(
                    new FileReader(new File(TEST_LOG_FILE)));
            int count = 0;
            while (testFile.readLine() != null) {
                count++;
            }
            return count;
        } catch (IOException e) {
            fail("Error reading test file");
            return 0;
        }
    }
}
