/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.planner;

import java.io.File;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Iterator;

import junit.framework.TestCase;

/**
 * Test for running the query planner. Assumes connection to example Oracle
 * HR database.
 */
public class QueryPlanTest extends TestCase {

    /**
     * Database under test.
     */
    private static final int TEST_DB =
        0; //Oracle
        //1; //SQL Server
        //2; //MySQL

    /**
     * Driver library path under test.
     */
    private static final String[] DRIVER_PATH = {
        "/usr/lib/oracle/xe/app/oracle/product/10.2.0/server/"
        + "jdbc/lib/ojdbc14.jar",
        "/home/iain/workspace/jdbc-tools/lib/sqljdbc.jar", //SQL Server
        "/home/iain/workspace/jdbc-tools/lib/"
        + "mysql-connector-java-5.1.6-bin.jar"}; //MySQL

    /**
     * Driver class under test.
     */
    private static final String[] DRIVER_CLASS = {
        "oracle.jdbc.OracleDriver",
        "com.microsoft.sqlserver.jdbc.SQLServerDriver", //SQL Server
        "com.mysql.jdbc.Driver"}; //MySQL

    /**
     * Connection URL under test.
     */
    private static final String[] DRIVER_URL = {
        "jdbc:oracle:thin:@localhost:1521:xe",
        "jdbc:sqlserver://192.168.0.5:50679", //SQL Server
        "jdbc:mysql://192.168.0.5:3306/world"}; //MySQL

    /**
     * Connection user name under test.
     */
    private static final String[] DRIVER_USER = {
        "hr",
        "sa", //SQL Server
        "user"}; //MySQL

    /**
     * Connection password under test.
     */
    private static final String[] DRIVER_PASSWORD = {
        "hr",
        "sa", //SQL Server
        "user"}; //MySQL

    /**
     * SQL query that is tested.
     */
    private static final String[] TEST_SQL = {
        "select * from hr.employees",
        "select * from northwind..employees", //SQL Server
        "select * from world.Country"}; //MySQL

    /**
     * Parameterized SQL query that is tested.
     */
    private static final String[] PARAM_TEST_SQL = {
        "select * from hr.employees where salary > ?",
        "select * from northwind..employees where LastName = ?", //SQL Server
        "select * from world.Country where code = ?"}; //MySQL

    /**
     * Parameterized SQL query argument values (where required for query plan
     * generation).
     */
    private static final String[][] ARGS_TEST_SQL = {
        {},
        {"King"}, //SQLServer
        {"GBR"}}; //MySQL

    /**
     * Name to be used for file to store generated plans.
     */
    public static final String PLAN_FILE = "/tmp/testPlan001.plan";

    /**
     * The instance of the QueryPlanRunner used for testing.
     */
    private QueryPlanRunner queryRunner; //NOPMD

    /**
     * Sets up the test by creating the query runner.
     * @throws Exception if there is a problem setting up test
     */
    protected final void setUp() throws Exception {
        queryRunner = new QueryPlanRunner(DRIVER_PATH[TEST_DB],
                DRIVER_CLASS[TEST_DB], DRIVER_URL[TEST_DB],
                DRIVER_USER[TEST_DB], DRIVER_PASSWORD[TEST_DB]);
    }

    /**
     * Cleans up plan files generated after test.
     * @throws Exception if there is a problem cleaning up test
     */
    protected final void tearDown() throws Exception {
        super.tearDown();
        new File(PLAN_FILE).delete();
        final Enumeration < Driver > drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            DriverManager.deregisterDriver(drivers.nextElement());
        }
    }

    /**
     * Tests a connection can be created.
     */
    public final void testGetConnection() {
        Connection conn = null;
        try {
            conn = QueryPlanRunner.getConnection(
                    DRIVER_PATH[TEST_DB], DRIVER_CLASS[TEST_DB],
                    DRIVER_URL[TEST_DB], DRIVER_USER[TEST_DB],
                    DRIVER_PASSWORD[TEST_DB]);
            assertNotNull("Testing connection", conn);
        } catch (Exception e) {
            fail("Error testing connection (" + e + ")");
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    fail("Error closing test connection(" + e + ")");
                }
            }
        }
    }

    /**
     * Tests creating a set of plan statements.
     */
    public final void testPlanStatements() {
        Connection conn = null;
        try {
            conn = QueryPlanRunner.getConnection(
                    DRIVER_PATH[TEST_DB], DRIVER_CLASS[TEST_DB],
                    DRIVER_URL[TEST_DB], DRIVER_USER[TEST_DB],
                    DRIVER_PASSWORD[TEST_DB]);
            final IPlanStatements statements =
                PlanStatementFactory.getStatements(conn);
            assertNotNull("Test creating plan statements", statements);
        } catch (Exception e) {
            fail("Error testing creating plan statements (" + e + ")");
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    fail("Error closing test connection(" + e + ")");
                }
            }
        }
    }
    /**
     * Tests a simple unparameterized select statement.
     */
    public final void testSimpleSelect() {
        final String sql = TEST_SQL[TEST_DB];
        assertTrue("check sql", isValidSQL(sql));

        try {
            queryRunner.generatePlan(sql, new File(PLAN_FILE));
            assertTrue("plan contents written",
                    new File(PLAN_FILE).length() > 0);
        } catch (Exception e) {
            fail("error with simple plan(" + e + ")");
        }
    }

    /**
     * Tests a parameterized select statement.
     */
    public final void testParameterizedSelect() {
        final String sql = PARAM_TEST_SQL[TEST_DB];
        assertTrue("check sql", isValidSQL(sql));
        try {
            final PreparedStatement stmt = queryRunner.createPlanStatement(sql);
            for (int i = 0; i < ARGS_TEST_SQL[TEST_DB].length; i++) {
                stmt.setString(i + 1, ARGS_TEST_SQL[TEST_DB][i]);
            }
            final Iterator < String > planLines =
                queryRunner.generatePlan(stmt);
            assertTrue("plan contents written", planLines.hasNext());
        } catch (Exception e) {
            fail("error with parameterized plan(" + e + ")");
        }
    }

    /**
     * Checks that the given sql statement is valid by creating a prepared
     * statement from it.
     * @param sql the statement to test
     * @return true if valid; false otherwise
     */
    private boolean isValidSQL(final String sql) {
        Connection conn = null;
        PreparedStatement statement = null;
        try {
            conn = QueryPlanRunner.getConnection(
                    DRIVER_PATH[TEST_DB], DRIVER_CLASS[TEST_DB],
                    DRIVER_URL[TEST_DB], DRIVER_USER[TEST_DB],
                    DRIVER_PASSWORD[TEST_DB]);
            statement = conn.prepareStatement(sql);
        } catch (Exception e) {
            fail("test sql not valid sql");
            return false;
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e2) { //NOPMD NOCS
                    // do nothing
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e2) { //NOPMD NOCS
                    //do nothing
                }
            }
        }
        return true;
    }
}
