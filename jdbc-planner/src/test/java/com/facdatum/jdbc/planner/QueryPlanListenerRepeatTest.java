/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */package com.facdatum.jdbc.planner;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Driver;
import java.sql.DriverManager;
import java.util.Enumeration;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCListener;
import com.facdatum.jdbc.listener.JDBCListenerProperties;
import com.facdatum.jdbc.logger.JDBCLogger;
import com.facdatum.jdbc.logger.JDBCLoggerProperties;
import com.facdatum.mock.listener.MockCallEvent;
import com.facdatum.mock.listener.MockReturnEvent;

import junit.framework.TestCase;

/**
 * Test of query plan listener when query plan should be repeatedly generated.
 */
public class QueryPlanListenerRepeatTest extends TestCase {
    /**
     * Execute method under test.
     */
    private static final String EXECUTE_METHOD =
        "java.sql.Statement.executeQuery";

    /**
     * SQL under test.
     */
    private static final String TEST_SQL =
        "select * from hr.employees";

    /**
     * Test log file name.
     */
    private static final String TEST_LOG_FILE = "/tmp/test.log";

    /**
     * Sets up the test by setting the properties.
     * @throws Exception if there is a problem creating test
     */
    protected final void setUp() throws Exception {
        super.setUp();
        System.setProperty(JDBCListenerProperties.LISTENERS_KEY,
                JDBCLogger.class.getName() + ","
                + QueryPlanListener.class.getName());
        System.setProperty(JDBCPlannerProperties.DRIVER_PATH_KEY,
                "/usr/lib/oracle/xe/app/oracle/product/10.2.0/server/"
                + "jdbc/lib/ojdbc14.jar");
        System.setProperty(JDBCPlannerProperties.DRIVER_CLASS_KEY,
                "oracle.jdbc.OracleDriver");
        System.setProperty(JDBCPlannerProperties.DRIVER_URL_KEY,
                "jdbc:oracle:thin:@localhost:1521:xe");
        System.setProperty(JDBCPlannerProperties.DRIVER_USER_KEY, "hr");
        System.setProperty(JDBCPlannerProperties.DRIVER_PWD_KEY, "hr");
        System.setProperty(JDBCPlannerProperties.REPEAT_KEY, "true");
        System.setProperty(JDBCLoggerProperties.LOG_PATH_KEY,
                new File(TEST_LOG_FILE).getParent());
        System.setProperty(JDBCLoggerProperties.LOG_FILE_KEY,
                new File(TEST_LOG_FILE).getName());
        JDBCCallListener.init();
    }

    /**
     * Cleans up test by clearing properties and deleting test log file.
     * @throws Exception if problem cleaning up test
     * @see junit.framework.TestCase#tearDown()
     */
    protected final void tearDown() throws Exception {
        super.tearDown();
        (new File(TEST_LOG_FILE)).delete();
        JDBCCallListener.resetInit();
        JDBCListenerProperties.clearProperties();
        JDBCLoggerProperties.clearProperties();
        JDBCPlannerProperties.clearProperties();
        final Enumeration < Driver > drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            DriverManager.deregisterDriver(drivers.nextElement());
        }
    }

    /**
     * Test method for {@link com.facdatum.jdbc.planner.QueryPlanListener
     * #acceptBegin(com.facdatum.jdbc.listener.JDBCCallEvent)}.
     */
    public final void testAcceptBegin() {
        final JDBCListener[] listeners = JDBCCallListener.getListeners();
        QueryPlanListener planner = null;
        for (int i = 0; i < listeners.length; i++) {
            if (listeners[i] instanceof QueryPlanListener) {
                planner = (QueryPlanListener) listeners[i];
            }
        }
        planner.acceptBegin(new MockCallEvent(EXECUTE_METHOD,
                new Object[] {TEST_SQL}));
        planner.acceptEnd(new MockReturnEvent(EXECUTE_METHOD, 0));
        planner.acceptBegin(new MockCallEvent(EXECUTE_METHOD,
                new Object[] {TEST_SQL}));
        planner.acceptEnd(new MockReturnEvent(EXECUTE_METHOD, 0));
        BufferedReader testFile = null;
        try {
            testFile = new BufferedReader(
                    new FileReader(new File(TEST_LOG_FILE)));
            String logLine = testFile.readLine(); //skip begin log message
            assertTrue("Checking logged plan",
                    logLine.contains("SELECT STATEMENT"));
            logLine = testFile.readLine();
            assertTrue("Checking logged plan",
                    logLine.contains("SELECT STATEMENT"));
            assertNull("Should not be a third entry.", testFile.readLine());
        } catch (IOException e) {
            fail("problem reading log file");
        }
    }
}
