/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.planner;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;

/**
 * This class generates query plans against provided SQL and stores them
 * in a specified file.
 */
public class QueryPlanRunner {

    /**
     * Path to driver library that will be used to generate query plan.
     */
    private String driverPath = null; //NOPMD

    /**
     * Class name of driver that will be used to generate query plan.
     */
    private String driverClass = null; //NOPMD

    /**
     * Class url that will be used to generate query plan.
     */
    private String driverURL = null; //NOPMD

    /**
     * Class user id that will be used to generate query plan.
     */
    private String driverUserID = null; //NOPMD

    /**
     * Class password that will be used to generate query plan.
     */
    private String driverPassword = null; //NOPMD

    /**
     * Connection used to generate plans on.
     */
    private Connection conn = null; //NOPMD

    /**
     * Plan statements used to generate plans.
     */
    private IPlanStatements plan = null; //NOPMD

    /**
     * Plan ref that is generated for plans that are generated in
     * multiple steps.
     */
    private String savedPlanRef; //NOPMD

    /**
     * Sets the connection details directly.
     * @param path driver library path to set
     * @param clazz driver class name to set
     * @param url driver connection url to set
     * @param userID driver connection user ID to set
     * @param password driver connection password to set
     */
    public QueryPlanRunner(final String path,
            final String clazz, final String url, final String userID,
            final String password) {
        driverPath = path;
        driverClass = clazz;
        driverURL = url;
        driverUserID = userID;
        driverPassword = password;
        try {
            conn = getConnection(driverPath, driverClass, driverURL,
                    driverUserID, driverPassword);
            plan = PlanStatementFactory.getStatements(conn);
        } catch (Exception e) { //NOPMD NOCS
            //should have been caught elsewhere
        }
    }

    /**
     * Generates a query plan against the provided sql and stores that plan
     * in a new plan file specified by file path.
     * @param sql the query to plan
     * @param planFile the plan file to store the plan in
     * @return the file that contains the plan
     * @throws ClassNotFoundException if plan generator can not be created
     * @throws SQLException if error executing query plan
     * @throws IOException if plan generator can not be created
     * @throws InstantiationException if plan generator can not be created
     * @throws IllegalAccessException if plan generator can not be created
     */
    public final File generatePlan(final String sql, final File planFile)
            throws ClassNotFoundException, SQLException, InstantiationException,
            IllegalAccessException, IOException {
        final PrintStream stream = new PrintStream(planFile);
        final Iterator < String > planLines = generatePlan(sql);
        while (planLines.hasNext()) {
            stream.println(planLines.next());
        }
        stream.close();
        return planFile;
    }

    /**
     * Creates plan statement that may include parameters that need to
     * be filled before actually generating plan.
     * @param sql the sql to generate plan on
     * @return the prepared statement for generate plan step
     * @throws SQLException if error preparing query plan
     */
    public final PreparedStatement createPlanStatement(final String sql)
            throws SQLException {
        savedPlanRef = createPlan(sql);
        return conn.prepareStatement(plan.getDisplayStep(savedPlanRef, sql));
    }


    /**
     * Generates a query plan against the provided sql and stores that plan
     * in a new plan file specified by file path.
     * @param stmt the prepared statement that will get query plan
     * @return the file that contains the plan
     * @throws SQLException if error executing query plan
     */
    public final Iterator < String > generatePlan(final PreparedStatement stmt)
            throws SQLException {
        ResultSet results = null;
        try {
            results = stmt.executeQuery();
            final ArrayList < String > planLines = readPlan(stmt, results);
            results.close();
            stmt.close();
            removePlan(savedPlanRef);
            return planLines.iterator();
        } finally {
            if (results != null) {
                results.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * Generates a query plan against the provided sql and stores that plan
     * in a new plan file specified by file path.
     * @param sql the query to plan
     * @return the file that contains the plan
     * @throws SQLException if error executing query plan
     */
    public final Iterator < String > generatePlan(final String sql)
            throws SQLException {
        Statement statement = null;
        ResultSet results = null;
        try {
            final String planRef = createPlan(sql);
            statement = conn.createStatement();
            results = statement.executeQuery(plan.getDisplayStep(planRef, sql));
            final ArrayList < String > planLines = readPlan(statement, results);
            results.close();
            statement.close();
            removePlan(planRef);
            return planLines.iterator();
        } finally {
            if (results != null) {
                results.close();
            }
            if (statement != null) {
                statement.close();
            }
        }
    }

    /**
     * Creates the plan data (if necessary) for the query plan to be returned.
     * @param sql the query to plan
     * @return the reference string to plan
     * @throws SQLException if there is a problem creating plan
     */
    private String createPlan(final String sql) throws SQLException {
        Statement statement = null;
        final SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyyMMddHHmmssSSS", Locale.getDefault());
        final String planRef = dateFormat.format(new Date()) + "ref";
        try {
            statement = conn.createStatement();
            final String planSQL = plan.getGenerateStep(planRef, sql);
            if (planSQL.trim().length() > 0) { //NOPMD
                statement.execute(planSQL);
            }
        } finally {
            if (statement != null) {
                statement.close();
            }
        }
        return planRef;
    }

    /**
     * Reads the generated plan and returns as array list of strings.
     * @param statement the statement to get plan
     * @param resultsIn the results containing plan
     * @return the plan as an array list of strings
     * @throws SQLException if there is a problem reading plan
     */
    private ArrayList < String > readPlan(final Statement statement, //NOPMD
            final ResultSet resultsIn) throws SQLException {
        ResultSet results = resultsIn; //NOPMD
        final ArrayList < String > planLines =
            new ArrayList < String > (); //NOCS
        ResultSetMetaData rsMetaData = results.getMetaData();
        int numberColumns = rsMetaData.getColumnCount();
        int[] columnTypes = new int[numberColumns];
        StringBuffer line = new StringBuffer();
        for (int i = 0; i < numberColumns; i++) {
            if (i > 0) {
                line.append('|');
            }
            line.append(rsMetaData.getColumnName(i + 1));
            columnTypes[i] = rsMetaData.getColumnType(i + 1);
        }
        planLines.add(line.toString());
        while (results.next()) {
            planLines.add(readResult(results, numberColumns, columnTypes));
        }
        results.close();
        while (statement.getMoreResults()) {
            results = statement.getResultSet();
            rsMetaData = results.getMetaData();
            numberColumns = rsMetaData.getColumnCount();
            columnTypes = new int[numberColumns]; //NOPMD
            line = new StringBuffer(); //NOPMD
            for (int i = 0; i < numberColumns; i++) {
                if (i > 0) {
                    line.append('|');
                }
                line.append(rsMetaData.getColumnName(i + 1));
                columnTypes[i] = rsMetaData.getColumnType(i + 1);
            }
            while (results.next()) {
                planLines.add(readResult(results, numberColumns,
                        columnTypes));
            }
        }
        return planLines;
    }

    /**
     * Removes the created plan data (if necessary).
     * @param planRef the reference for plan
     * @throws SQLException if there is a problem creating plan
     */
    private void removePlan(final String planRef) throws SQLException {
        Statement statement = null;
        try {
            statement = conn.createStatement();
            final String planSQL = plan.getCleanupStep(planRef);
            if (planSQL.trim().length() > 0) { //NOPMD
                statement.execute(planSQL);
            }
            statement.close();
        } finally {
            if (statement != null) {
                statement.close();
            }
        }
    }

    /**
     * Returns the results record as a BAR separated string.
     * @param results the result set to read record from
     * @param numberColumns the number of columns in the record
     * @param columnTypes the types of columns in the record
     * @return the result record as string
     * @throws SQLException if there is a problem reading result set
     */
    private String readResult(final ResultSet results, final int numberColumns,
            final int[] columnTypes) throws SQLException {
        final StringBuffer line = new StringBuffer();
        for (int i = 0; i < numberColumns; i++) {
            if (i > 0) {
                line.append('|');
            }
            switch (columnTypes[i]) {
                case Types.INTEGER:
                    line.append(results.getInt(i + 1));
                    break;
                case Types.VARCHAR:
                    line.append(results.getString(i + 1));
                    break;
                default:
                    line.append(results.getObject(i + 1));
                    break;
            }
        }
        return line.toString();
    }

    /**
     * Returns whether the underlying database supports parameters in query
     * plan statements.
     * @return true if parameters are supported; false otherwise
     */
    public final boolean supportsParameters() {
        return plan.supportsParameters();
    }

    /**
     * Creates the connection that will be used to generate the query plan.
     * @param path path to jdbc driver library
     * @param driver jdbc driver class name
     * @param url url connection string
     * @param userID user id for connection
     * @param password password for connection
     * @return the connection to run the query plan
     * @throws ClassNotFoundException if can not find driver class
     * @throws SQLException if error performing connect
     * @throws InstantiationException if error creating driver
     * @throws IllegalAccessException if error creating driver
     * @throws MalformedURLException if error creating driver
     */
    public static final Connection getConnection(final String path,
            final String driver, final String url, final String userID,
            final String password) throws ClassNotFoundException, SQLException,
            InstantiationException, IllegalAccessException,
            MalformedURLException {
        final URL[] jarURL = {new File(path).toURL()};
        final ClassLoader classLoader = URLClassLoader.newInstance(
                jarURL);
        final Driver jdbcDriver = (Driver) classLoader.loadClass(driver).
                newInstance();
        final Properties connectionProps = new Properties();
        if (userID != null) {
            connectionProps.setProperty("user", userID);
        }
        if (password != null) {
            connectionProps.setProperty("password", password);
        }
        return jdbcDriver.connect(url, connectionProps);
    }
}
