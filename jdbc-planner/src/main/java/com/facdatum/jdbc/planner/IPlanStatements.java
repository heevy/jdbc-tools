/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.planner;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Interface that defines the methods that need to be implemented when
 * creating a new class that can generate the query plans used by the
 * RunQueryPlanAction. Instances of this class are created by the
 * PlanStatementFactory.
 */
public interface IPlanStatements {

    /**
     * Returns the SQL string that will run the generate query execution plan
     * step on the supported platform.
     * @param statementID the id of the planned statement
     * @param sql the sql statement to be planned
     * @return the execution plan query string
     */
    String getGenerateStep(String statementID, String sql);

    /**
     * Returns the SQL string that will display the results of a previously
     * generated execution plan.
     * @param statementID the id of the planned statement
     * @param sql the sql statement to be planned
     * @return the display plan query string
     */
    String getDisplayStep(String statementID, String sql);

    /**
     * Returns the SQL string that will remove the generated plans resources.
     * @param statementID the id of the planned statement
     * @return the delete plan query string
     */
    String getCleanupStep(String statementID);

    /**
     * Returns whether the driver supports parameters within SQL used in
     * query plans.
     * @return true if parameters are supported; false otherwise
     */
    boolean supportsParameters();

    /**
     * Sets the connection to be used when running the query plan steps.
     * @param conn the connection
     * @throws SQLException if there is a problem connecting
     * @throws IOException if there is a problem connecting
     */
    void setConnection(Connection conn) throws SQLException, IOException;
}
