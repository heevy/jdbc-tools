/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.planner;

import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.facdatum.jdbc.listener.JDBCCallListener;
import com.facdatum.jdbc.listener.JDBCListener;
import com.facdatum.jdbc.listener.ListenerCallEvent;
import com.facdatum.jdbc.listener.ListenerExceptionEvent;
import com.facdatum.jdbc.listener.ListenerReturnEvent;
import com.facdatum.jdbc.logger.JDBCLogger;

/**
 * Listens to the JDBC calls and identifies those that include a SQL query
 * as an argument. On the first run of each unique SQL statement a query
 * plan will be generated and placed in the log file.
 */
public class QueryPlanListener implements JDBCListener {

    /**
     * Holds reference to logger that has been configured.
     */
    private JDBCLogger logger; //NOPMD

    /**
     * Records whether listener has been configured correctly or not.
     */
    private boolean validConfig = false; //NOPMD

    /**
     * Holds the set of jdbc methods that prepare queries.
     */
    private final Set < String > prepareMethods = //NOPMD
        new HashSet < String > (); //NOCS

    /**
     * Holds the set of jdbc methods that set args to queries.
     */
    private final Set < String > setMethods = //NOPMD
        new HashSet < String > (); //NOCS

    /**
     * Holds the set of jdbc methods that execute queries.
     */
    private final Set < String > executeMethods = //NOPMD
        new HashSet < String > (); //NOCS

    /**
     * Contains set of sql statements that have had query plans run on them.
     */
    private final  Set < String > plannedSQL = //NOPMD
        new HashSet < String > (); //NOCS

    /**
     * Contains set of sql statements that have had query plans run on them.
     */
    private final  Map < PreparedStatement, SQLInfo > prepSQL = //NOPMD
        new HashMap < PreparedStatement, SQLInfo > (); //NOCS

    /**
     * Query plan runner used to generate plans.
     */
    private QueryPlanRunner planRunner; //NOPMD

    /**
     * Holds the SQL to be planned when the plan needs to be prepared
     * before execution.
     */
    private String savedSQL; //NOPMD

    /**
     * Holds the event used in acceptBegin method for later used in acceptEnd.
     */
    private ListenerCallEvent callEvent; //NOPMD

    /**
     * Output print stream for error messages.
     */
    private static PrintStream errLog = System.err;

    /**
     * Initialises the listener by configuring the set of JDBC calls with SQL
     * arguments and reading the connection properties for generating the
     * query plans.
     * @see com.facdatum.jdbc.listener.JDBCListener#init()
     */
    public final void init() {

        final JDBCListener[] listeners = JDBCCallListener.getListeners();
        for (int i = 0; i < listeners.length; i++) {
            if (listeners[i] instanceof JDBCLogger) {
                logger = (JDBCLogger) listeners[i];
            }
        }
        if (logger == null) {
            errLog.println("QueryPlanListener is dependent on JDBCLogger."
                    + " JDBCLogger is not a configured JDBCListener.");
        } else {
            prepareMethods.add("java.sql.Connection.prepareStatement");
            setMethods.add("java.sql.PreparedStatement.setArray");
            setMethods.add("java.sql.PreparedStatement.setAsciiStream");
            setMethods.add("java.sql.PreparedStatement.setBigDecimal");
            setMethods.add("java.sql.PreparedStatement.setBlob");
            setMethods.add("java.sql.PreparedStatement.setBoolean");
            setMethods.add("java.sql.PreparedStatement.setByte");
            setMethods.add("java.sql.PreparedStatement.setBytes");
            setMethods.add("java.sql.PreparedStatement.setCharacterStream");
            setMethods.add("java.sql.PreparedStatement.setClob");
            setMethods.add("java.sql.PreparedStatement.setDate");
            setMethods.add("java.sql.PreparedStatement.setDouble");
            setMethods.add("java.sql.PreparedStatement.setFloat");
            setMethods.add("java.sql.PreparedStatement.setInt");
            setMethods.add("java.sql.PreparedStatement.setLong");
            setMethods.add("java.sql.PreparedStatement.setNull");
            setMethods.add("java.sql.PreparedStatement.setObject");
            setMethods.add("java.sql.PreparedStatement.setRef");
            setMethods.add("java.sql.PreparedStatement.setShort");
            setMethods.add("java.sql.PreparedStatement.setString");
            setMethods.add("java.sql.PreparedStatement.setTime");
            setMethods.add("java.sql.PreparedStatement.setTimestamp");
            setMethods.add("java.sql.PreparedStatement.setUnicodeStream");
            setMethods.add("java.sql.PreparedStatement.setURL");
            executeMethods.add("java.sql.Statement.execute");
            executeMethods.add("java.sql.Statement.executeQuery");
            executeMethods.add("java.sql.Statement.executeUpdate");
            executeMethods.add("java.sql.PreparedStatement.execute");
            executeMethods.add("java.sql.PreparedStatement.executeQuery");
            executeMethods.add("java.sql.PreparedStatement.executeUpdate");

            JDBCPlannerProperties.init();
            writeProperties();

            Connection testConn = null; //NOPMD
            try {
                testConn = QueryPlanRunner.getConnection(
                        JDBCPlannerProperties.getDriverPath(),
                        JDBCPlannerProperties.getDriverClass(),
                        JDBCPlannerProperties.getDriverURL(),
                        JDBCPlannerProperties.getDriverUser(),
                        JDBCPlannerProperties.getDriverPassword());
                if (testConn != null) {
                    validConfig = true;
                    testConn.close();
                    planRunner = new QueryPlanRunner(
                            JDBCPlannerProperties.getDriverPath(),
                            JDBCPlannerProperties.getDriverClass(),
                            JDBCPlannerProperties.getDriverURL(),
                            JDBCPlannerProperties.getDriverUser(),
                            JDBCPlannerProperties.getDriverPassword());
                }
            } catch (Exception e) {
                logger.logError("Cannot instantiate runtime query plan JDBC"
                        + " connection. Check connection properties.", e);
                if (testConn != null) {
                    try {
                        testConn.close();
                    } catch (SQLException e2) { //NOPMD NOCS
                        //do nothing
                    }
                }
            }
        }
    }

    /**
     * Outputs the properties as debug messages.
     */
    private void writeProperties() {
        final StringBuffer buf = new StringBuffer(100);
        buf.append(JDBCPlannerProperties.class.getName());
        buf.append(" {");
        buf.append(JDBCPlannerProperties.PROP_PATH_KEY);
        buf.append('=');
        buf.append(JDBCPlannerProperties.getPropPath());
        buf.append(", ");
        buf.append(JDBCPlannerProperties.PROP_FILE_KEY);
        buf.append('=');
        buf.append(JDBCPlannerProperties.getPropFile());
        buf.append(", ");
        buf.append(JDBCPlannerProperties.DRIVER_PATH_KEY);
        buf.append('=');
        buf.append(JDBCPlannerProperties.getDriverPath());
        buf.append(", ");
        buf.append(JDBCPlannerProperties.DRIVER_CLASS_KEY);
        buf.append('=');
        buf.append(JDBCPlannerProperties.getDriverClass());
        buf.append(", ");
        buf.append(JDBCPlannerProperties.DRIVER_URL_KEY);
        buf.append('=');
        buf.append(JDBCPlannerProperties.getDriverURL());
        buf.append(", ");
        buf.append(JDBCPlannerProperties.DRIVER_USER_KEY);
        buf.append('=');
        buf.append(JDBCPlannerProperties.getDriverUser());
        buf.append(", ");
        buf.append(JDBCPlannerProperties.DRIVER_PWD_KEY);
        buf.append('=');
        buf.append(JDBCPlannerProperties.getDriverPassword());
        buf.append('}');
        logger.logDebug(buf.toString());
    }

    /**
     * If the call includes a SQL string as an argument then query plan
     * that SQL string and output to log.
     * @param event the call event information
     * @see com.facdatum.jdbc.listener.JDBCListener
     * #acceptBegin(com.facdatum.jdbc.listener.JDBCCallEvent)
     */
    public final void acceptBegin(final ListenerCallEvent event) {
        callEvent = event;
        if (validConfig
                && ((executeMethods.contains(event.getJDBCInterfaceMethod())
                        && event.getArgs().length > 0))
                        || prepareMethods.contains(
                                event.getJDBCInterfaceMethod())) {
            final Object[] args = event.getArgs();
            savedSQL = (String) args[0];
        } else if (validConfig
                && setMethods.contains(event.getJDBCInterfaceMethod())) {
            setSavedPlanArg(event);
        }
    }

    /**
     * Generates the plan for a call that can be generated without filling
     * in any query arguments.
     * @param event the call event information
     */
    private void generatePlanImmediate(final ListenerCallEvent event) {
        final Object[] args = event.getArgs();
        final String sql = (String) args[0];
        if (JDBCPlannerProperties.getRepeatGeneration()
                || !plannedSQL.contains(sql)) {
            try {
                final Iterator < String > planIter =
                        planRunner.generatePlan(sql);
                final StringBuffer planMsg = new StringBuffer(sql);
                while (planIter.hasNext()) {
                    planMsg.append('^');
                    planMsg.append(planIter.next());
                }
                logger.doLog(event.getTime(), 0,
                        event.getCallingMethod(),
                        MsgTypes.PLAN, planMsg.toString());
                plannedSQL.add(sql);
            } catch (Exception e) {
                logger.logError("Error generating runtime query plan.", e);
            }
        }
    }

    /**
     * Generates the plan statement for a call that requires arguments
     * to be filled in before generating the query plan.
     * @param event the call event information
     */
    private void createSavedPlan(final ListenerReturnEvent event) {
        if (JDBCPlannerProperties.getRepeatGeneration()
                || !plannedSQL.contains(savedSQL)) {
            try {
                plannedSQL.add(savedSQL);
                prepSQL.put((PreparedStatement) event.getWrappedResult(),
                        new SQLInfo(savedSQL));
            } catch (Exception e) {
                logger.logError("Error generating runtime query plan.",
                        e);
            }
        }
    }

    /**
     * Sets the arguments for a saved plan that needs its arguments to be
     * filled in before executing.
     * @param event the call event information
     */
    private void setSavedPlanArg(final ListenerCallEvent event) {
        if (prepSQL.containsKey((PreparedStatement)
                event.getObject())) {
            final Object[] args = event.getArgs();
            final SQLInfo sqlInfo = ((SQLInfo) prepSQL.get(
                    (PreparedStatement) event.getObject()));
            sqlInfo.setParam((Integer) args[0], args[1]);
        }
    }

    /**
     * Executes the saved plan once all its arguments have been filled in.
     * @param event the call event information
     */
    private void generatePlanSaved(final ListenerCallEvent event) {
        try {
            if (prepSQL.containsKey((PreparedStatement) event.getObject())) {
                final SQLInfo sqlInfo = (SQLInfo) prepSQL.get(
                        (PreparedStatement) event.getObject());
                final Iterator < String > planIter =
                    planRunner.generatePlan(sqlInfo.getParameterizedSQL());
                final StringBuffer planMsg = new StringBuffer(sqlInfo.getSQL());
                while (planIter.hasNext()) {
                    planMsg.append('^');
                    planMsg.append(planIter.next());
                }
                logger.doLog(event.getTime(), 0,
                        event.getCallingMethod(),
                        MsgTypes.PLAN, planMsg.toString());
                prepSQL.remove((PreparedStatement) event.getObject());
            }
        } catch (SQLException e) {
            logger.logError("Error generating runtime query plan.", e);
        }
    }

    /**
     * Nothing to do on exit when generating query plans.
     * @param event the return event information
     * @see com.facdatum.jdbc.listener.JDBCListener
     * #acceptEnd(com.facdatum.jdbc.listener.JDBCReturnEvent)
     */
    public final void acceptEnd(final ListenerReturnEvent event) {
        if (validConfig
                && executeMethods.contains(event.getJDBCInterfaceMethod())
                && callEvent.getArgs().length > 0) {
                generatePlanImmediate(callEvent);
        } else if (validConfig
                && executeMethods.contains(event.getJDBCInterfaceMethod())
                && callEvent.getArgs().length == 0) {
            generatePlanSaved(callEvent);
        } else if (validConfig
            && prepareMethods.contains(event.getJDBCInterfaceMethod())) {
            createSavedPlan(event);
        }
    }

    /**
     * Exceptions are not currently used by the query plan framework.
     * @param event the record of the JDBC exception
     */
    public void acceptException(final ListenerExceptionEvent event) {
        //do nothing
    }

    /**
     * Holder class for storing SQL and parameter information.
     */
    protected class SQLInfo {

        /**
         * The stored sql.
         */
        private String sql; //NOPMD

        /**
         * The positions of parameters in sql.
         */
        private Integer[] paramIdx; //NOPMD

        /**
         * The parameter values to be used in sql.
         */
        private Object [] params; //NOPMD

        /**
         * Creates SQLInfo for specified sql string.
         * @param sqlString the sql string
         */
        SQLInfo(final String sqlString) {
            this.sql = sqlString;
            this.paramIdx = findParams(sql);
            this.params = new Object[paramIdx.length];
        }

        /**
         * Returns the index positions in SQL string where parameter markers
         * appear.
         * @param sqlString the SQL string
         * @return the parameter positions
         */
        private Integer[] findParams(final String sqlString) {
            final ArrayList < Integer > paramLocations =
                new ArrayList < Integer > (); //NOCS
            final StringCharacterIterator chars =
                new StringCharacterIterator(sqlString);
            boolean inString = false;
            for (char c = chars.first(); c != CharacterIterator.DONE;
                    c = chars.next()) {
                if ((c == '?') && !inString) {
                    paramLocations.add(Integer.valueOf(chars.getIndex()));
                } else if (c == '\'' && !inString) {
                    inString = true;
                } else if (c == '\'' && inString) {
                    inString = false;
                }
            }
            return (Integer[]) paramLocations.toArray(new Integer[0]); //NOPMD
        }

        /**
         * Sets the parameter value at specified index.
         * @param index the parameter number
         * @param param the parameter
         */
        protected final void setParam(final Integer index, final Object param) {
            params[index - 1] = param;
        }

        /**
         * Checks if the SQL contains any parameters and if it does displays
         * a dialog to allow the user to enter parameter values.
         * @return the SQL with parameters filled in (or empty string if user
         * doesn't provide parameter values).
         */
        protected final String getParameterizedSQL()  {
            if (!planRunner.supportsParameters() && paramIdx.length > 0) {
                final StringBuffer filledSQL = new StringBuffer();
                filledSQL.append(sql.substring(0, paramIdx[0]));
                for (int i = 0; i < (paramIdx.length - 1); i++) {
                    filledSQL.append(formatParam(params[i]));
                    filledSQL.append(sql.substring(paramIdx[i] + 1,
                            paramIdx[i + 1]));
                }
                filledSQL.append(formatParam(params[paramIdx.length - 1]));
                filledSQL.append(sql.substring(
                        paramIdx[paramIdx.length - 1] + 1));
                return filledSQL.toString();
            } else {
                return sql;
            }
        }

        /**
         * Formats string and other parameters with quotes.
         * @param param the paramter object
         * @return the formatted string of parameter
         */
        private String formatParam(final Object param) {
            final StringBuffer paramString = new StringBuffer();
            if (!(param instanceof Number)) {
                paramString.append('\'');
            }
            paramString.append(param.toString());
            if (!(param instanceof Number)) {
                paramString.append('\'');
            }
            return paramString.toString();
        }

        /**
         * Returns the un-parameterized SQL string.
         * @return the raw SQL string
         */
        protected final String getSQL() {
            return sql;
        }
    }
}
