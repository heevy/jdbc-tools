/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.planner;

import java.io.File;
import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

import com.facdatum.jdbc.listener.JDBCListenerProperties;

/**
 * Provides access to all the properties that can be used to control the
 * JDBCPlanner. These can either be pass in as java properties at the
 * command line of loaded via the logging driver properties file
 * jdbc-planner.properties.
 */
public final class JDBCPlannerProperties {

    /**
     * The properties bundle that can be used to override the property file
     * name.
     */
    public static final String FILE_BUNDLE_REF =
            "com.facdatum.jdbc.config.JDBCPropertiesFileName";

    /**
     * Contains the key used to pick up the listener property directory path.
     */
    public static final String PROP_PATH_KEY = "jdbc.planner.prop.path";

    /**
     * Contains the configured listener property directory path.
     */
    private static String propPath;

    /**
     * Contains the key used to pick up the listener property file name.
     */
    public static final String PROP_FILE_KEY = "jdbc.planner.prop.filename";

    /**
     * Contains the default listener property file name.
     */
    public static final String PROP_FILE_DEF =
            "jdbc-planner";

    /**
     * Contains the configured listener property file name.
     */
    private static String propFile;

    /**
     * Contains the key used to pick up repeat generation flag.
     */
    public static final String REPEAT_KEY = "jdbc.planner.repeat";

    /**
     * Contains the default value for whether query plans should be
     * repeatedly generated.
     */
    public static final String REPEAT_DEF = "false";

    /**
     * Contains the configured listener property file name.
     */
    private static boolean repeatGen;

    /**
     * Contains the key used to pick up the path to jdbc driver library.
     */
    public static final String DRIVER_PATH_KEY = "driver.path";

    /**
     * Contains the configured jdbc driver library path.
     */
    private static String driverPath;

    /**
     * Contains the key used to pick up the path to jdbc driver classname.
     */
    public static final String DRIVER_CLASS_KEY = "driver.class";

    /**
     * Contains the configured jdbc driver classname.
     */
    private static String driverClass;

    /**
     * Contains the key used to pick up the path to jdbc driver connection url.
     */
    public static final String DRIVER_URL_KEY = "driver.url";

    /**
     * Contains the configured jdbc driver connection url.
     */
    private static String driverURL;

    /**
     * Contains the key used to pick up the path to jdbc driver user.
     */
    public static final String DRIVER_USER_KEY = "driver.user";

    /**
     * Contains the configured jdbc driver user.
     */
    private static String driverUser;

    /**
     * Contains the key used to pick up the path to jdbc driver password.
     */
    public static final String DRIVER_PWD_KEY = "driver.password";

    /**
     * Contains the configured jdbc driver password.
     */
    private static String driverPassword;

    static {
        JDBCPlannerProperties.init();
    }

    /**
     * Private default constructor to stops creation of instances.
     */
    private JDBCPlannerProperties() {
        super();
    }

    /**
     * Creates an instance of the logging properties by reading the
     * defined logging properties file.
     */
    public static void init() {
        propFile = System.getProperty(PROP_FILE_KEY,
                PROP_FILE_DEF);
        try {
            final Properties fileProperties = loadBundle(FILE_BUNDLE_REF);
            propFile = fileProperties.getProperty(
                    JDBCListenerProperties.SHARED_FILE_KEY, propFile);
        } catch (MissingResourceException e) { //NOPMD NOCS
            //use default
        }

        propPath = System.getProperty(PROP_PATH_KEY);
        Properties tempProperties = new Properties();
        try {
            if (propPath == null) {
                tempProperties = loadBundle(propFile);
            } else {
                tempProperties.load(new FileInputStream(new File(propPath,
                        propFile + ".properties")));
            }
        } catch (Exception e) { //NOPMD NOCS
            //defaults will be used
        }
        driverPath = tempProperties.getProperty(DRIVER_PATH_KEY);
        driverClass = tempProperties.getProperty(DRIVER_CLASS_KEY);
        driverURL = tempProperties.getProperty(DRIVER_URL_KEY);
        driverUser = tempProperties.getProperty(DRIVER_USER_KEY);
        driverPassword = tempProperties.getProperty(DRIVER_PWD_KEY);
        repeatGen = Boolean.parseBoolean(tempProperties.getProperty(REPEAT_KEY,
                REPEAT_DEF));

        driverPath = System.getProperty(DRIVER_PATH_KEY, driverPath);
        driverClass = System.getProperty(DRIVER_CLASS_KEY, driverClass);
        driverURL = System.getProperty(DRIVER_URL_KEY, driverURL);
        driverUser = System.getProperty(DRIVER_USER_KEY, driverUser);
        driverPassword = System.getProperty(DRIVER_PWD_KEY,
                driverPassword);
        repeatGen = Boolean.parseBoolean(System.getProperty(REPEAT_KEY,
                String.valueOf(repeatGen)));
    }

    /**
     * Loads properties from a ResourceBundle.
     * @param bundleName name of bundle to load
     * @return properties in bundle
     */
    protected static Properties loadBundle(final String bundleName) {
        final ResourceBundle bundle = ResourceBundle.getBundle(bundleName);
        final Properties props = new Properties();
        final Enumeration < String > keys = bundle.getKeys();
        while (keys.hasMoreElements()) {
            final String key = (String) keys.nextElement();
            props.put(key, bundle.getString(key));
        }
        return props;
    }

    /**
     * Clears all the system properties related to the JDBCPlanner to
     * their defaults.
     */
    public static void clearProperties() {
        System.clearProperty(PROP_PATH_KEY);
        System.clearProperty(PROP_FILE_KEY);
        System.clearProperty(DRIVER_PATH_KEY);
        System.clearProperty(DRIVER_CLASS_KEY);
        System.clearProperty(DRIVER_URL_KEY);
        System.clearProperty(DRIVER_USER_KEY);
        System.clearProperty(DRIVER_PWD_KEY);
        System.clearProperty(REPEAT_KEY);
    }

    /**
     * Gets the configured properties file path.
     * @return the properties file path
     */
    public static String getPropPath() {
        return propPath;
    }

    /**
     * Gets the configured properties file name.
     * @return the properties file name
     */
    public static String getPropFile() {
        return propFile;
    }

    /**
     * Gets the configured driver library path.
     * @return the driver library path
     */
    public static String getDriverPath() {
        return driverPath;
    }

    /**
     * Gets the configured driver classname.
     * @return the driver classname
     */
    public static String getDriverClass() {
        return driverClass;
    }

    /**
     * Gets the configured driver connection URL.
     * @return the driver URL
     */
    public static String getDriverURL() {
        return driverURL;
    }

    /**
     * Gets the configured driver connection user.
     * @return the driver user
     */
    public static String getDriverUser() {
        return driverUser;
    }

    /**
     * Gets the configured driver connection password.
     * @return the driver connection password
     */
    public static String getDriverPassword() {
        return driverPassword;
    }

    /**
     * Returns whether query plan should repeatedly generated or not.
     * @return true if query plans should be repeated; false otherwise
     */
    public static boolean getRepeatGeneration() { //NOPMD
        return repeatGen;
    }
}
