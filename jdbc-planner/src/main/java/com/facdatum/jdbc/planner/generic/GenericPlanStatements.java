/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.planner.generic;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.facdatum.jdbc.planner.IPlanStatements;

/**
 * This implementation of the IPlanStatments interface uses a properties file
 * to pick up the SQL statements to use to run the required steps to
 * generate a query plan. These property file is identified by examining the
 * type of the database that is in use and appending the database product name
 * to a properties file called GenericPlanStatements.
 */
public class GenericPlanStatements implements IPlanStatements {

    /**
     * The property key containing the generate step sql.
     */
    private static final String GENERATE_KEY = "generateSQL";

    /**
     * The property key containing the display step sql.
     */
    private static final String DISPLAY_KEY = "displaySQL";

    /**
     * The property key containing the cleanup step sel.
     */
    private static final String CLEANUP_KEY = "cleanupSQL";

    /**
     * The property key containing whether database supports parameters in
     * query plan SQL.
     */
    private static final String PARAMS_KEY = "supportsParameters";

    /**
     * Holds the configured generate step sql.
     */
    private String generateSQL; //NOPMD

    /**
     * Holds the configured display step sql.
     */
    private String displaySQL; //NOPMD

    /**
     * Holds the configured cleanup step sql.
     */
    private String cleanupSQL; //NOPMD

    /**
     * Holds the configure parameter support value.
     */
    private boolean supportsParams; //NOPMD

    /**
     * {@inheritDoc}
     * @see com.facdatum.jdbc.planner.IPlanStatements#setConnection(
     *      java.sql.Connection)
     */
    public final void setConnection(final Connection conn) throws SQLException,
            IOException {
        final String dbProduct = conn.getMetaData().getDatabaseProductName().
                toLowerCase();
        final String bundleName = GenericPlanStatements.class.
                getCanonicalName() + "_" + dbProduct;
        final ResourceBundle props = ResourceBundle.getBundle(bundleName);
        generateSQL = props.getString(GENERATE_KEY);
        displaySQL = props.getString(DISPLAY_KEY);
        cleanupSQL = props.getString(CLEANUP_KEY);
        supportsParams = Boolean.parseBoolean(props.getString(PARAMS_KEY));
    }

    /**
     * {@inheritDoc}
     * @see com.facdatum.jdbc.planner.IPlanStatements#getGenerateStep(
     *      java.lang.String, java.lang.String)
     */
    public final String getGenerateStep(final String statementID,
            final String sql) {
        return String.format(generateSQL, statementID, sql);
    }

    /**
     * {@inheritDoc}
     * @see com.facdatum.jdbc.planner.IPlanStatements#getDisplayStep(
     *      java.lang.String)
     */
    public final String getDisplayStep(final String statementID,
            final String sql) {
        return String.format(displaySQL, statementID, sql);
    }

    /**
     * {@inheritDoc}
     * @see com.facdatum.jdbc.planner.IPlanStatements#getCleanupStep(
     *      java.lang.String)
     */
    public final String getCleanupStep(final String statementID) {
        return String.format(cleanupSQL, statementID);
    }

    /**
     * {@inheritDoc}
     * @see com.facdatum.jdbc.planner.IPlanStatements
     *  #supportsParameters()
     */
    public final boolean supportsParameters() {
        return supportsParams;
    }
}
