/**
 * Copyright 2008 Facdatum Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facdatum.jdbc.planner;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Factory class that will read the PlanStatementFactory.properties file to
 * find the IPlanStatments class to use to run the generate query plan steps.
 */
public final class PlanStatementFactory {

    /**
     * Key in the properties file that holds the plan class.
     */
    public static final String PLAN_CLASS_KEY = "planClass";

    /**
     * Private constructor to stop instances of class being created.
     */
    private PlanStatementFactory() {
        //do nothing
    }

    /**
     * Returns the IPlanStatement class that has been configured in the
     * PlanStatementFactory.properties file.
     * @param conn the connection to use to create statements.
     * @return the configured class
     * @throws IOException if plan generator can not be created
     * @throws InstantiationException if plan generator can not be created
     * @throws IllegalAccessException if plan generator can not be created
     * @throws ClassNotFoundException if plan generator can not be created
     * @throws SQLException if problem creating connection
     */
    public static IPlanStatements getStatements(final Connection conn) throws
            IOException, InstantiationException, IllegalAccessException,
            ClassNotFoundException, SQLException {
        final ResourceBundle props = ResourceBundle.getBundle(
                PlanStatementFactory.class.getCanonicalName());
        final ClassLoader classLoader = Thread.currentThread().
                getContextClassLoader();
        final IPlanStatements plan = (IPlanStatements) classLoader.
                loadClass(props.getString(PLAN_CLASS_KEY)).newInstance();
        plan.setConnection(conn);
        return plan;
    }
}
